//
//  BannerSectionController.swift
//  ems-Manager
//
//  Created by mac on 2019/5/9.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import IGListKit

class BannerModels: ListDiffable {
    
    let pk: Int
    let name: String
    let handle: String
    
    init(pk: Int, name: String, handle: String) {
        self.pk = pk
        self.name = name
        self.handle = handle
    }

    func diffIdentifier() -> NSObjectProtocol {
        return self as! NSObjectProtocol

    }
    
    ///
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? BannerModels else { return false }
        return name == object.name && handle == object.handle
    }
}

class BannerSectionController: ListSectionController {
    private var bannerModels: BannerModels?

    override init() {
        super.init()
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let itemSize = floor(width)
        return CGSize(width: itemSize, height: 200)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell: UICollectionViewCell
        guard let bannerCell = collectionContext?.dequeueReusableCell(of: BannerCollectionViewCell.self,
                                                                      for: self,
                                                                      at: index) as? BannerCollectionViewCell else {
                                                                        fatalError()}
        
        cell = bannerCell
        
        return cell
        
    }
}
