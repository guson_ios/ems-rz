//
//  FunctionSectionController.swift
//  ems-Manager
//
//  Created by mac on 2019/5/9.
//  Copyright © 2019 mac. All rights reserved.
//
import IGListKit
import Foundation

final class GridItem: NSObject {
    
    let color: UIColor
    let itemCount: Int
    
    var items: [String] = []
    
    init(color: UIColor, itemCount: Int) {
        self.color = color
        self.itemCount = itemCount
        
        super.init()
        
        self.items = computeItems()
    }
    
    private func computeItems() -> [String] {
        return [Int](1...itemCount).map {
            String(describing: $0)
        }
    }
}

extension GridItem: ListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        print((object as! GridItem).itemCount as Any)
        
        return self === object ? true : self.isEqual(object)
    }
    
}

class FunctionSectionController: ListSectionController {
   
    var model: GridItem!
    
    
    override init() {
        super.init()
       
    }
    
    override func numberOfItems() -> Int {
        return 8
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let itemSize = floor(width / 4)
        return CGSize(width: itemSize, height: itemSize)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        
        let cell = collectionContext?.dequeueReusableCell(withNibName: FunctionCollectionViewCell.reuseIdentifier, bundle: nil, for: self, at: index)
            as! FunctionCollectionViewCell
        cell.valueLabel.text = index <= 3 ? "aefw":"ajfm;elkanafawefeawefawefawfwelkfna;lwefkn.a"
        cell.valueLabel.backgroundColor = self.model.color
        cell.configCell(index >= 3 ? cell.topContraint:cell.bottomContraint, model: FunctionViewModel(operateName: .scanOperate, imageName: "", norImageName: "", opModel: nil))
        return cell
        
    }
    
    override func didUpdate(to object: Any) {
        self.model = object as? GridItem
    }
}
