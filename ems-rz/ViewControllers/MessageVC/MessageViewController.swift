//
//  MessageViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/20.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class MessageViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var messageModels = [MessageModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "消息"
        self.tableView.register(MessageTableViewCell.nib, forCellReuseIdentifier: MessageTableViewCell.reuseIdentifier)
        self.tableView.tableViewRefreshClouse {[weak self] (pageNo, listCount) in
            if pageNo == 1 { self?.messageModels.removeAll()}

            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMsgList), parameter: nil, method: .get, success: { (result) -> (Void) in
                if let ms = [MessageModel].deserialize(from: result["data"]["records"].arrayObject) as? [MessageModel]{
                    self?.messageModels.append(contentsOf: ms)
                    self?.tableView.reloadData()
                    listCount(ms.count)
                }else{
                    listCount(0)
                }
            }) { (error) -> (Void) in
                listCount(0)

            }
        }
        
    }
}

extension MessageViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: MessageTableViewCell.reuseIdentifier, for: indexPath) as! MessageTableViewCell
        cell.confignCell(self.messageModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.messageModels[indexPath.row]
        if model.isRead == "0", let id = model.id {
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMsgSetRead + "/" + id), parameter: nil, method: .put, success: { (result) -> (Void) in
                model.isRead = "1"
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }) { (error) -> (Void) in
                
            }
        }
    }
}
