//
//  MessageTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/20.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var msgContentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.addShadowWithBezierPath()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confignCell(_ model: MessageModel?) {
        titleLabel.text = model?.title
        msgContentLabel.text = model?.msgContent
        timeLabel.text = String.timeFromFormatter("YYYY-MM-dd HH:mm:ss", timeString: (model?.createTime)!)
        redView.layer.cornerRadius = 3
        if model?.isRead == "0" {
            self.leftConstraint.constant = 12
            self.redView.isHidden = false
        }else{
            self.leftConstraint.constant = 2
            self.redView.isHidden = true
        }
    }
    
}
