//
//  MessageModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/2.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

class MessageModel: NSObject, HandyJSON {
    var toUser: String?
    var isRead: String?
    var id: String?
    var busMsgType: String?
    var readTime: String?
    var msgTypeName: String?
    var fromUser: String?
    var publishTime: String?
    var statusName: String?
    var busMsgTypeName: String?
    var createTime: String?
    var params: String?
    var title: String?
    var msgType: String?
    var msgContent: String?
    required override init() {}

}
