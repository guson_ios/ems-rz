//
//  LoginViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/23.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

extension DefaultsKeys {
    var userAccount: DefaultsKey<String?> { return .init("userAccount") }
    var password: DefaultsKey<String?> { return .init("password") }
}
struct ServerSelModel: SimplePickViewProtocol, HandyJSON {
    func pickViewModelToString() -> String? {
        return self.serverName
    }
    var serverName: String?
    var serverUrl: String?
}

class LoginViewController: BaseViewController {
    @IBOutlet weak var accountTextField: LeftImageDesignTxtxField!
    @IBOutlet weak var passWordTextField: LeftImageDesignTxtxField!
    @IBOutlet weak var serverSelLabel: UILabel!
    lazy var simplePickView: SimplePickView = {
        let pickView = SimplePickView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight))
        pickView.simplePickClosure = { [weak self] row , smodel in
            if let model = smodel as? ServerSelModel{
                if model.serverName == "自定义端" {
                    UIAlertController.alertShowTextField("自定义端", message: "格式如：218.56.157.76:9010", sureBtnTitle: "确定") { (text) in
                        self?.serverSelUrl(true, ServerSelModel(serverName: "自定义端" + (text ?? ""), serverUrl: "http://" + (text ?? "")))
                    }
                }else{
                    self?.serverSelUrl(true, ServerSelModel(serverName: model.serverName, serverUrl: model.serverUrl))
                }
            }
        }
        self.view.addSubview(pickView)
        return pickView
    }()
    
    func serverSelUrl(_ write: Bool, _ serverSelModel: ServerSelModel?) {
        let defServerModels = DefaultsKey<[String : Any]?>("serverSelModel", defaultValue: nil)
        if write {
            Defaults[key: defServerModels] = serverSelModel?.toJSON()
        }
        if Defaults[key: defServerModels] == nil {
            Defaults[key: defServerModels] = ServerSelModel(serverName: "测试环境外网", serverUrl: baseUrl.testOnLine.rawValue).toJSON()
            self.serverSelLabel.text = "应用服务器：" + "测试环境外网"
        }else{
            let serverSelModel = ServerSelModel.deserialize(from: Defaults[key: defServerModels])
            if let serverUrl =  serverSelModel?.serverUrl{
                afBaseUrl = serverUrl
                self.serverSelLabel.text = "应用服务器：" + (serverSelModel?.serverName ?? "")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Defaults.userAccount != nil {
            self.accountTextField.text = Defaults.userAccount
            self.passWordTextField.text = Defaults.password
        }
        self.serverSelUrl(false, nil)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func serverSelected(_ sender: Any) {
        let serverModels = [
            ServerSelModel(serverName: "测试环境外网", serverUrl: baseUrl.testOnLine.rawValue),
            ServerSelModel(serverName: "开发环境外网", serverUrl: baseUrl.s2.rawValue),
//            ServerSelModel(serverName: "自定义端", serverUrl: ""),
        ]
        self.simplePickView.pickerModels = serverModels
    }
    
    @IBAction func logoImageTap(_ sender: Any) {
    }
    
    @IBAction func loginBtnClick(_ sender: Any) {
        if let userAccount = self.accountTextField.text, let password = self.passWordTextField.text   {
            Defaults[\.userAccount] = userAccount
            Defaults[\.password] = password
//            let password256 = (Defaults.password! + passWordSha256).sha256()
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsLogin), parameter: ["userAccount": Defaults.userAccount!,"password": Defaults.password!], method: .post,headers: headersFunc(["User-Agent" : "ios"], contentType: .define), progressPlugin: self, success: { (result) -> (Void) in
                let model = LoginData.deserialize(from: result["data"].dictionaryObject)
                print(model?.token as Any)
                EmsGlobalObj.obj.loginData = model
                UIApplication.shared.keyWindow?.rootViewController = BaseTabbarViewController()
                UIApplication.shared.keyWindow?.makeKeyAndVisible()
                self.loginUserOrg(model)
                
            }) { (error) -> (Void) in
               
            }
        }
        
    }
    
    func loginUserOrg(_ model: LoginData?) {
        
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsloginUserOrg), parameter: nil, method: .get, success: { (result) -> (Void) in
            let model = UserInfoModel.deserialize(from: result["data"].dictionaryObject)
            EmsGlobalObj.obj.userInfo = model
        }) { (error) -> (Void) in
            
        }
    }
}

