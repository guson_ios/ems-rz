//
//  ScanViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/17.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import swiftScan

class ScanViewController: LBXScanViewController {
    
    /**
     @brief  闪关灯开启状态
     */
    var isOpenedFlash: Bool = false
    
    // MARK: - 底部几个功能：开启闪光灯、相册、我的二维码
    
    //底部显示的功能项
    var bottomItemsView: UIView?
    
    //相册
    var btnPhoto: UIButton = UIButton()
    
    //闪光灯
    var btnFlash: UIButton = UIButton()
    
    //我的二维码
    var btnMyQR: UIButton = UIButton()
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

        drawBottomItems()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "扫码"
        self.isOpenInterestRect = true
        self.scanStyle?.animationImage = UIImage(named: "qrcode_Scan_weixin_Line")
//        self.readyString = ""
        // Do any additional setup after loading the view.
    }
    
     func drawBottomItems() {
            if (bottomItemsView != nil) {

                return
            }

            let yMax = self.view.frame.maxY - self.view.frame.minY

            bottomItemsView = UIView(frame: CGRect(x: 0.0, y: yMax-100, width: self.view.frame.size.width, height: 100 ) )

        bottomItemsView!.backgroundColor = self.qRScanView?.backgroundColor

            self.view .addSubview(bottomItemsView!)

            let size = CGSize(width: 65, height: 87)

            self.btnFlash = UIButton()
            btnFlash.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            btnFlash.center = CGPoint(x: bottomItemsView!.frame.width/2, y: bottomItemsView!.frame.height/2)

            btnFlash.setImage(UIImage(named: "icon_dianjian"), for:UIControl.State.normal)
            btnFlash.addTarget(self, action: #selector(ScanViewController.openOrCloseFlash), for: UIControl.Event.touchUpInside)
            bottomItemsView?.addSubview(btnFlash)

            view.addSubview(bottomItemsView!)
        }
        
        //开关闪光灯
        @objc func openOrCloseFlash() {
            scanObj?.changeTorch()

            isOpenedFlash = !isOpenedFlash
            
            if isOpenedFlash
            {
                btnFlash.setImage(UIImage(named: "icon_dianjian"), for:UIControl.State.normal)
            }
            else
            {
                btnFlash.setImage(UIImage(named: "icon_dianjian"), for:UIControl.State.normal)

            }
        }

   
}


