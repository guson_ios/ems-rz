//
//  TreeViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/27.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import RATreeView
typealias TreeViewClosure<T> = (_ model: T) -> Void

class TreeViewController: BaseViewController {
    
    var basicDataRespVos: [BasicDataRespVos]? = []
    var treeViewClosure: TreeViewClosure<Any>?
    
    var treeViewexpandRowIndex: [Int]? = []

    lazy var treeView: RATreeView = {
        let treeView = RATreeView(frame: CGRect.zero, style: RATreeViewStyleGrouped)
        treeView.delegate = self
        treeView.dataSource = self
        return treeView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        var level = 4
        treeView.estimatedRowHeight = 50
        // Do any additional setup after loading the view.
        if #available(iOS 11.0, *) {
            self.treeView.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsBasicNameTree), parameter: ["basicName" : "设备类别"], method: .post, encoding: URLEncoding.httpBody,  progressPlugin: self, success: {[weak self] (result) -> (Void) in
            let model: BasicDataRespVos? = BasicDataRespVos.deserialize(from: result["data"].dictionaryObject)!
            var models = model?.basicDataRespVos?.first?.basicDataRespVos
            let bas = BasicDataRespVos()
            bas.basicName = "全部"
            bas.id = ""
            models?.insert(bas, at: 0)
            self?.basicDataRespVosSection( &models, section: &level)///只有三层
            self?.basicDataRespVos = models

//            self.searchModel(self.basicDataRespVos!)
            
            self?.treeView.reloadData()
        }) { (error) -> (Void) in
            
        }
        treeView.register(TreeViewTableViewCell.nib, forCellReuseIdentifier: TreeViewTableViewCell.reuseIdentifier)
        treeView.treeHeaderView = UIView(frame: CGRect.zero)
        view.addSubview(treeView)
        treeView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(view)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.treeView.reloadData()
    }
    
    func basicDataRespVosSection(_ vos: inout [BasicDataRespVos]?, section: inout Int) {
        
        if let baseVos = vos {
            
            for (_, vos) in baseVos.enumerated() {
                if let baseVos = vos.basicDataRespVos {
                    for (_, vos) in baseVos.enumerated() {

                        if let baseVos = vos.basicDataRespVos {
                            for (_, vos) in baseVos.enumerated() {
                                if let baseVos = vos.basicDataRespVos, baseVos.count > 0 {
                                    vos.basicDataRespVos = []
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    func searchModel(_ basicDataRespVos: [BasicDataRespVos], id: String) {
        let baseDatas = basicDataRespVos
        for (idx, model) in (baseDatas.enumerated()) {
            if model.id == id {
                self.treeViewexpandRowIndex?.append(idx)
                self.searchChildrenindex(baseDatas, datas: self.basicDataRespVos!)
//                var baseArr = self.basicDataRespVos
//                for (_ , m) in (self.treeViewexpandRowIndex?.reversed().enumerated())! {
//                    let item = baseArr![m]
//                    item.isOpen = true
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
//                        self.treeView.expandRow(forItem: item)
//                    }
//                    baseArr = item.basicDataRespVos
//                }
//                model.isOpen = false
//                self.treeView(self.treeView, didSelectRowForItem: model)
            }
            
            if model.basicDataRespVos?.count ?? 0 > 0 {
                self.searchModel(model.basicDataRespVos!, id: id)
            }
        }
    }
    
    func searchChildrenindex(_ childerns: [BasicDataRespVos], datas: [BasicDataRespVos]) -> Void {
        for (idx, model) in datas.enumerated() {
            if model.basicDataRespVos == childerns {
                self.treeViewexpandRowIndex?.append(idx)
                self.searchChildrenindex(datas, datas: self.basicDataRespVos!)
            }
            if model.basicDataRespVos?.count ?? 0 > 0 {
                self.searchChildrenindex(childerns, datas: model.basicDataRespVos!)
            }
        }
    }
    
}

extension TreeViewController: RATreeViewDelegate, RATreeViewDataSource{
    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if (item != nil) {
            let model = item as! BasicDataRespVos
            return model.basicDataRespVos?.count ?? 0
        }
        return self.basicDataRespVos?.count ?? 0
    }
    
    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {

        let model = item as! BasicDataRespVos
        let cell = treeView.dequeueReusableCell(withIdentifier: TreeViewTableViewCell.reuseIdentifier) as! TreeViewTableViewCell
        cell.confignTreeViewCell(model, level: treeView.levelForCell(forItem: item as Any), expanded: treeView.isCell(forItemExpanded: item as Any))
        
        cell.selBtn.rx.tap.bind { [weak self] in
            if let stself = self, (stself.treeViewClosure != nil) {
                stself.searchModel(stself.basicDataRespVos!, id: model.id!)
                if let selects = stself.treeViewexpandRowIndex?.reversed().enumerated()  {
                    var models: [BasicDataRespVos] = []
                    var changeModel = stself.basicDataRespVos
                    for (_, idx) in selects {
                        models.append(changeModel![idx])
                        changeModel = changeModel![idx].basicDataRespVos
                    }
                    stself.treeViewClosure!(models)
                    stself.navigationController?.popViewController(animated: true)
                }
            }
        }.disposed(by: cell.rx.reuseBag)
        return cell
    }
    
    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        if (item != nil) {
            let model = item as! BasicDataRespVos
            return model.basicDataRespVos?[index] as Any
        }
        return basicDataRespVos?[index] as Any
    }
    
    func treeView(_ treeView: RATreeView, didSelectRowForItem item: Any) {
        let model = item as! BasicDataRespVos
        model.isOpen = !(model.isOpen ?? false)
        treeView.reloadRows(forItems: [item], with: RATreeViewRowAnimationNone)
    }
    
    func treeView(_ treeView: RATreeView, heightForRowForItem item: Any) -> CGFloat {
        return 50
    }
}

