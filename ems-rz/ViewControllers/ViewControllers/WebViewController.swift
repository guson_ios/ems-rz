//
//  WebViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/4.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController {

    var webView : WKWebView = WKWebView()
    var requestUrl: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let urlTrans =  requestUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: urlTrans) {
                let urlRequest = URLRequest(url: url)
                webView.load(urlRequest)
            }
        }
        
        self.view.addSubview(webView)
        webView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view)
        }
    }
}
