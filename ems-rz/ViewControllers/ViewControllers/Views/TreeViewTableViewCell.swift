//
//  TreeViewTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/27.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class TreeViewTableViewCell: UITableViewCell {

    @IBOutlet weak var imageLeading: NSLayoutConstraint!
    @IBOutlet weak var openImageView: UIImageView!
    @IBOutlet weak var cententLabel: UILabel!
    @IBOutlet weak var selBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confignTreeViewCell(_ model: BasicDataRespVos, level: Int, expanded: Bool) -> Void {
        cententLabel.text = model.basicName
        openImageView.isHidden = model.basicDataRespVos?.count == 0 ? true : false
        imageLeading.constant = CGFloat((20 + level * 16) + (model.basicDataRespVos?.count == 0 ? -26 : 0))
        openImageView.image = UIImage(named: (model.isOpen! ? "icon_close" : "icon_open"))
    }

}
