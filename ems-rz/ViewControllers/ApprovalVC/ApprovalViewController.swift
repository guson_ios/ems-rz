//
//  ApprovalViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/21.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
import WebKit

class ApprovalViewController: BaseViewController {

    var webView : WKWebView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //        http://218.56.157.110:89/verifyLogin.do?loginid=lw&timestamp=1582277137362&loginTokenFromThird=37028162908d61f4111548a9d779a393d89a5cba&url=http://218.56.157.110:89/list.do?module=1&scope=9
       
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsGetFlowExaminedToken), parameter: nil, method: .get, success: { (result) -> (Void) in
            
        }) { (error) -> (Void) in
            
        }
        self.webViewLeftNavigationItem()
        
        
        let userAccount = EmsGlobalObj.obj.loginData?.userAccount ?? ""
        let currentTimeTamp = String.currentTimeTamp()
        let str = EMSSecrect + userAccount + currentTimeTamp
        print(str.sha1())
        let urlStr = "http://218.56.157.110:89/verifyLogin.do?loginid=\(userAccount)&timestamp=\(currentTimeTamp)&loginTokenFromThird=\(str.sha1())&url=http://218.56.157.110:89/list.do?module=1&scope=9"
        
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
//        request.timeoutInterval = 20.0f;
//        [request addValue:[NSString getCookieValue] forHTTPHeaderField:@"Cookie"];
        
        if let url = URL(string: urlStr) {
            if EmsGlobalObj.obj.userInfo?.useOrgId == "9000000000" {
                
            }

            var urlRequest = URLRequest(url: url)
            urlRequest.addValue("Authorization", forHTTPHeaderField: "Authorization")
            webView.load(urlRequest)
        }
        self.view.addSubview(webView)
        webView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view)
        }
    }
    
    func webViewLeftNavigationItem() {
        let leftItemControl = UIControl(frame: CGRect(x: 12, y: 0, width: 44, height: 44))
        leftItemControl.addTarget(self, action: #selector(clickLeftItemAction), for: .touchUpInside)
        
        let backImageView = UIImageView(frame: CGRect(x: 0, y: 10, width: 22, height: 22))
        backImageView.image = UIImage(named: "nav_icon_back")
        leftItemControl.addSubview(backImageView)
        let leftBarBtn = UIBarButtonItem(customView: leftItemControl)
        self.navigationItem.leftBarButtonItems = [leftBarBtn]
    }
    
    @objc func clickLeftItemAction() {
        if self.webView.canGoBack {
            self.webView.goBack()
        }
    }
}


//- (void)leftNavigationItem
//{
//    UIControl *leftItemControl = [[UIControl alloc] initWithFrame:CGRectMake(12, 0, 44, 44)];
//    [leftItemControl addTarget:self action:@selector(clickLeftItemAction:) forControlEvents:UIControlEventTouchUpInside];
//
//    UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 22, 22)];
//    back.image = [UIImage imageNamed:@"nav_icon_back"];
//    [leftItemControl addSubview:back];
//    UIBarButtonItem * leftBarBtn = [[UIBarButtonItem alloc] initWithCustomView:leftItemControl];
//
//    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//    [closeBtn setTitle:@"关闭   " forState:UIControlStateNormal];
//    closeBtn.frame = CGRectMake(0, 0, 44, 44);
//    [closeBtn setTitleColor:[UIColor labelTextColor] forState:UIControlStateNormal];
//    [closeBtn addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
//
//    UIBarButtonItem *closeBarBtn = [[UIBarButtonItem alloc]initWithCustomView:closeBtn];
//
//    self.navigationItem.leftBarButtonItems = @[leftBarBtn, closeBarBtn];
//}
//
//
//
//#pragma mark - Button Action
//
//- (void)clickLeftItemAction:(id)sender
//{
////    NSLog(@"======%@--%@===%@", [self.webView.backForwardList backList],self.webView.backForwardList.backItem.URL,self.webView.backForwardList.backItem.title);
//    if ([self.webView canGoBack]) {
//        [self.webView goBack];
//    }else {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}
//
//- (void)closeWebView{
//    [self.navigationController popViewControllerAnimated:YES];
//}
