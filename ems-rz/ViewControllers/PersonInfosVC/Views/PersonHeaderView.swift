//
//  PersonHeaderView.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/10.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class PersonHeaderView: UIView {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var orgLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadXibView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadXibView()
    }
}
