//
//  PersonSettingVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/14.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class PersonSettingVC: BaseViewController {

    @IBOutlet weak var bottomBtnView: BottomBtnView!
    @IBOutlet weak var tableView: UITableView!
    var tableDatas: [EquipmentInfoDetail] {
        return [
//            EquipmentInfoDetail(infoKey: "修改密码", infoValue: "", cellType: .leftImageHide),
                EquipmentInfoDetail(infoKey: "检查更新", infoValue: "", cellType: .leftImageHide)]
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "系统设置"
        self.bottomBtnView.btn.setTitle("退出登录", for: .normal)
        self.bottomBtnView.btn.rx.tap.subscribe { [weak self] _ in
            signOutViewController(self?.tabBarController)
        }.disposed(by: disposeBag)
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
    }

}
extension PersonSettingVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UniversalTableViewCell = tableView.dequeueReusableCell(withIdentifier: UniversalTableViewCell.reuseIdentifier, for: indexPath) as! UniversalTableViewCell
        cell.confignUniversalCell(self.tableDatas[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsAppsNewest), parameter: ["appType" : "2"], method: .get, success: { (result) -> (Void) in
            if let model = AppsNewestModel.deserialize(from: result["data"].dictionaryObject), let url = URL(string: model.packagePath ?? ""){
                UIAlertController.alertShow("提示", message: "未发现强制推送版本，可前往Safari下载页面查看", sureBtnTitle: "前往", cancelBtnTitle: "暂不", sureBtnClick: {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }, cancelBtnClick: nil)
            }
        }) { (error) -> (Void) in
            
        }
    }
}


func signOutViewController(_ tbvc: UITabBarController?) {
    tbvc?.removeFromParent()
    let loginVC = LoginViewController.loadViewControllewWithNib
    UIApplication.shared.keyWindow?.rootViewController = loginVC
    UIApplication.shared.keyWindow?.makeKeyAndVisible()
}

struct AppsNewestModel: HandyJSON {
    var modifiedTime: String?
    var createdUser: String?
    var status: String?
    var modifiedUser: String?
    var appVersion: String?
    var deleted: String?
    var packagePath: String?
    var updateComment: String?
    var createdTime: String?
    var id: String?
    var app: String?
}
