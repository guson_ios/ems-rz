//
//  PersonViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/17.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class PersonViewController: BaseViewController {

    @IBOutlet weak var mainTableView: UITableView!
    var tableDatas: [EquipmentInfoDetail] {
        return [EquipmentInfoDetail(infoKey: "我的资料", infoValue: "", cellType: .leftImageHide),
                EquipmentInfoDetail(infoKey: "系统设置", infoValue: "", cellType: .leftImageHide)]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainTableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        let headerView = PersonHeaderView(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 218))
        headerView.nameLabel.text = EmsGlobalObj.obj.loginData?.userName
        headerView.orgLabel.text = EmsGlobalObj.obj.userInfo?.useOrgName
        self.mainTableView.tableHeaderView = headerView
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsUserInfo), parameter: nil, method: .get, success: { (result) -> (Void) in
            
        }) { (error) -> (Void) in
            
        }
    }
    
}



extension PersonViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UniversalTableViewCell = tableView.dequeueReusableCell(withIdentifier: UniversalTableViewCell.reuseIdentifier, for: indexPath) as! UniversalTableViewCell
        cell.confignUniversalCell(self.tableDatas[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = PersonInfoVC.loadViewControllewWithNib
            self.show(vc, sender: nil)
        }
        if indexPath.row == 1 {
            let vc = PersonSettingVC.loadViewControllewWithNib
            self.show(vc, sender: nil)
        }
    }
}
