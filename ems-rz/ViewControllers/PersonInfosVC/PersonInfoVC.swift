//
//  PersonInfoVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/14.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class PersonInfoVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "我的资料"
        self.tableView.register(TextFieldTableViewCell.nib, forCellReuseIdentifier: TextFieldTableViewCell.reuseIdentifier)
        let ledgerModels = EquipmentInfoDetail.personInfoViewModels(nil).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: TextFieldTableViewCell.reuseIdentifier))
        }
        tableViewViewModel.append(ledgerModels)
    }
}

extension PersonInfoVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count 
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { (operate, cell) in
            
        })
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}

extension EquipmentInfoDetail{
    static func personInfoViewModels(_ model: EquipBaseInfoRespVo?) -> [EquipmentInfoDetail] {
        return [
            EquipmentInfoDetail(infoKey: "姓名", infoValue:  EmsGlobalObj.obj.loginData?.userName),
            EquipmentInfoDetail(infoKey: "性别", infoValue: "男"),
            EquipmentInfoDetail(infoKey: "手机号码*:", infoValue: "", cellStatus: true),
            EquipmentInfoDetail(infoKey: "电子邮箱*:", infoValue: "", cellStatus: true),
            EquipmentInfoDetail(infoKey: "所属单位", infoValue: EmsGlobalObj.obj
                .userInfo?.useCompanyName),
            EquipmentInfoDetail(infoKey: "所属部门:", infoValue: EmsGlobalObj.obj.userInfo?.useOrgName),
            EquipmentInfoDetail(infoKey: "所属角色:", infoValue: ""),
        ]
    }
}
