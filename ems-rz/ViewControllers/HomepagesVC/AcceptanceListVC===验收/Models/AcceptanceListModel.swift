//
//  AcceptanceListModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/9.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

struct AcceptanceStartModel: HandyJSON {
    var orderId: String?
    var orderCode: String?
    var planName: String?
    var equipName: String?
    var acceptanceUserName: String?
    var unionStatus: String?
}

struct AcceptanceListModel: HandyJSON, ViewModelOperate {
    
    var id: String?
    var orderId: String?
    var taskType: String?
    var equipName: String?
    var orderCode: String?
    var createdTime: String?
    var taskName: String?
    var planName: String?
}

struct MaintainAcceptModel: HandyJSON, ViewModelOperate {
    
    var orderId: String?
    var type: String?
    var acceptanceStatus: String? = "1"
    var acceptanceEvaluate: String?
    var acceptanceScore: String?
    var nextAcceptUserId: String?
    var acceptanceRoleType: String?
    var unionStatus: String?
    var fileReqVoList: [[String : String]]? = []
    var planRepairStandardInfoRespVoList: [RepairDetailsModel.PlanRepairStandardInfoRespVoModel]? = []
    ///提交时应删除字段
    var photos: [Any]? = []
    
    static func maintainAcceptanceModels(_ model: MaintainAcceptModel?, repairDetailModel: RepairDetailsModel?) -> [EquipmentInfoDetail]{
        var eqs = [
            EquipmentInfoDetail(infoKey: model?.unionStatus == "0" ? "技术员验收" : "联合验收", infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "验收人员", infoValue: EmsGlobalObj.obj.loginData?.userName),
            EquipmentInfoDetail(infoKey: "是否验收通过", infoValue: model?.acceptanceStatus, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "验收评价", infoValue: model?.acceptanceEvaluate, cellStatus: true, cellReuseIdentifier: NextGrowingCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "验收打分", infoValue: model?.acceptanceScore, cellReuseIdentifier: CosmosTableViewCell.reuseIdentifier),
        ]
        if model?.unionStatus == "0" {
            eqs.append(EquipmentInfoDetail(infoKey: "指派验收（班长）", infoValue: "", cellType: .leftImageHide))
        }
        return eqs
    }
}

struct MaintainAcceptViewModel {
    
    ///保养验收
    static func maintainAcceptViewModel(_ maintainAcceptModel: MaintainAcceptModel?, acceptanceListModel: AcceptanceListModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        if let model = acceptanceListModel  {
            let equSummary: [TableViewViewModel<ViewModelOperate>]  = [
                TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: EquSummaryCell.reuseIdentifier)),
                TableViewViewModel(baseViewModel: BaseViewModel(model: EquipmentInfoDetail(infoKey: "查看保养详情", infoValue: "", cellType: .leftImageHide, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier), cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
            ]
            tableViewViewModel.append(equSummary)
        }
        let equInfoModels = MaintainAcceptModel.maintainAcceptanceModels(maintainAcceptModel, repairDetailModel: nil).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
        }
        tableViewViewModel.append(equInfoModels)
        
        let photoShow: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: PhotoShowModel(), size: CGSize(width: KScreenWidth, height: 150), cellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier))]
        tableViewViewModel.append(photoShow)
        ///新增验收定额
        if let repairMoneyModel: [TableViewViewModel<ViewModelOperate>] = maintainAcceptMoneyViewModel(maintainAcceptModel) {
            tableViewViewModel.append(repairMoneyModel)
        }
        
        return tableViewViewModel
    }
}
