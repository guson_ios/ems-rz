//
//  AcceptanceListTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/9.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class AcceptanceListTableViewCell: UITableViewCell {
    @IBOutlet weak var label0: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.addShadowWithBezierPath()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confignAcceptanceListCell(_ model: AcceptanceListModel?) {
        label0.text = "工单号：" + (model?.orderCode ?? "")
        label1.text = model?.createdTime
        label2.text = model?.equipName
        label3.text = model?.taskName
    }
}
