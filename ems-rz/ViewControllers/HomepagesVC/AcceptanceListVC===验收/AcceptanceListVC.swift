//
//  AcceptanceListVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/9.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class AcceptanceListVC: BaseViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    lazy var acceptanceListModels: [AcceptanceListModel] = []
    var taskType = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.mj_header?.beginRefreshing()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "验收"
        self.tableView.register(AcceptanceListTableViewCell.nib, forCellReuseIdentifier: AcceptanceListTableViewCell.reuseIdentifier)
        self.menuView()
        self.netWorking()
        
    }
    func menuView() {
        let menuView = MenuView(frame: CGRect(x: (KScreenWidth - 200.0) / 2, y: 0, width: 200, height: 35), titleArray: ["维修验收", "润滑保养验收"])
        menuView.menuBtnClick = { [weak self] index in
            self?.taskType = index
            self?.tableView.mj_header?.beginRefreshing()
        }
        menuView.titleFont = UIFont.systemFont(ofSize: 16)
        topView.addSubview(menuView)
    }
    private func netWorking() {
        self.tableView.tableViewRefreshClouse { [weak self] (pageNo, listCount) in
            if pageNo == 1 { self?.acceptanceListModels.removeAll()}
            let taskType = self?.taskType == 0 ? "1" : "2"
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsTaskAcceptanceList), parameter: pageDict(current: pageNo).combinationDict(["taskType" : taskType]), method: .get, success: { (result) -> (Void) in
                if let models = [AcceptanceListModel].deserialize(from: result["data"]["records"].arrayObject) as? [AcceptanceListModel]{
                    self?.acceptanceListModels.append(contentsOf: models)
                    listCount(models.count)
                    self?.tableView.reloadData()
                }else{
                    listCount(0)
                }
            }) { (error) -> (Void) in
                listCount(0)
            }
        }
    }
}

extension AcceptanceListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.acceptanceListModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AcceptanceListTableViewCell = tableView.dequeueReusableCell(withIdentifier: AcceptanceListTableViewCell.reuseIdentifier, for: indexPath) as! AcceptanceListTableViewCell
        let acceptanceListModel = self.acceptanceListModels[indexPath.row]
        cell.confignAcceptanceListCell(acceptanceListModel)
        
        cell.rightBtn.rx.tap.subscribe {[weak self] _ in
            switch self?.taskType {
            case 0:
                if let vc = RepairStartVC.loadViewControllewWithNib as? RepairStartVC {
                    var repairListModel = RepairListModel()
                    repairListModel.id = self?.acceptanceListModels[indexPath.row].orderId
                    repairListModel.detailedStatus = "6"
                    vc.repairListModel = repairListModel
                    self?.show(vc, sender: nil)
                }
            case 1:
                
                if let vc = MaintainAcceptVC.loadViewControllewWithNib as? MaintainAcceptVC {
                    vc.acceptanceListModel = self?.acceptanceListModels[indexPath.row]
                    self?.show(vc, sender: nil)
                }
            default:
                break
            }
        }.disposed(by: cell.rx.reuseBag)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}
