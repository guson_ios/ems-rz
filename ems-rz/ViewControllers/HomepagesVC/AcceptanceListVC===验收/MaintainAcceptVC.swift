//
//  MaintainAcceptVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/13.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class MaintainAcceptVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bottomBtnView: BottomBtnView!
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    var acceptanceListModel: AcceptanceListModel?
    lazy var maintainAcceptModel = MaintainAcceptModel()
    
    lazy var repairMoneyModels = [[RepairMoneyChartViewModel]]()///定额维修项目ViewModel


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "验收详情"
        tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)
        tableView.register(SwitchTableViewCell.nib, forCellReuseIdentifier: SwitchTableViewCell.reuseIdentifier)
        tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        tableView.register(NextGrowingCell.nib, forCellReuseIdentifier: NextGrowingCell.reuseIdentifier)
        tableView.register(CosmosTableViewCell.nib, forCellReuseIdentifier: CosmosTableViewCell.reuseIdentifier)
        tableView.register(PhotoSelectShowCell.nib, forCellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier)
        tableView.register(RecordTableViewCell.nib, forCellReuseIdentifier: RecordTableViewCell.reuseIdentifier)
        tableView.register(RepairUnTableViewCell.nib, forCellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier)
        tableView.register(RepairMoneyChartCell.nib, forCellReuseIdentifier: RepairMoneyChartCell.reuseIdentifier)
        tableView.register(RepairMoneyAddTableViewCell.nib, forCellReuseIdentifier: RepairMoneyAddTableViewCell.reuseIdentifier)
        
        self.acceptStart()
        
        bottomBtnView.btn.setTitle("提交", for: .normal)
        bottomBtnView.btn.rx.tap.subscribe { [weak self] _ in
            
            if self?.maintainAcceptModel.unionStatus == "0", self?.maintainAcceptModel.nextAcceptUserId == nil{
                ZFToast.show(withMessage: "请先选择验收人员")
                return
            }
            
            
            let group = DispatchGroup.init()
            self?.uploadFile(group, files: self?.maintainAcceptModel.photos?.map({ (model) -> Data? in
                if let model = model as? ZLPhotoBrowerModel{
                    return model.imageData
                }
                return nil
            }) as? [Data], fileName: "file.png", mineType: "image/png", maintainDaily: "Photos")
            
            group.notify(queue: DispatchQueue.main) {
                self?.maintainAcceptModel.orderId = self?.acceptanceListModel?.orderId
                self?.maintainAcceptModel.type = "2"
                self?.maintainAcceptModel.photos = nil
                if self?.maintainAcceptModel.acceptanceEvaluate?.count ?? 0 <= 0{
                    self?.maintainAcceptModel.acceptanceEvaluate = " "
                }
                
                if let rmodels = self?.repairMoneyModels{
                    for (i, models) in rmodels.enumerated(){
                        var vModels = models
                        vModels.remove(at: 0)
                        for (j, dModels) in vModels.enumerated() {
                            if i == 1 {
                                self?.maintainAcceptModel.planRepairStandardInfoRespVoList?[j].repairProject = dModels.text
                            }
                            if i == 2 {
                                self?.maintainAcceptModel.planRepairStandardInfoRespVoList?[j].repairNumber = dModels.text
                            }
                            if i == 3 {
                                self?.maintainAcceptModel.planRepairStandardInfoRespVoList?[j].repairUnit = dModels.text
                            }
                            if i == 4 {
                                self?.maintainAcceptModel.planRepairStandardInfoRespVoList?[j].repairRemark = dModels.text
                            }
                            
                        }
                    }
                }
                
                print(self?.maintainAcceptModel.toJSONString())
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsAcceptanceEnd), parameter: nil, method: .post, encoding: self?.maintainAcceptModel.toJSONString() ?? "{}", headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
                    self?.popVc()
                }) { (error) -> (Void) in
                    
                }
            }
        }.disposed(by: disposeBag)
        
        if let vm = MaintainAcceptViewModel.maintainAcceptViewModel(maintainAcceptModel, acceptanceListModel: self.acceptanceListModel) {
            tableViewViewModel = vm
        }
    }
    // MARK: - 开始验收
    func acceptStart() {
        if let id = self.acceptanceListModel?.orderId{
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsAcceptanceStart), parameter: ["planMaintainOrderId" : id], method: .get, success: { (result) -> (Void) in
                self.maintainAcceptModel.unionStatus = result["data"]["unionStatus"].stringValue
                self.maintainAcceptModel.acceptanceRoleType = "3"
                if let vm = MaintainAcceptViewModel.maintainAcceptViewModel(self.maintainAcceptModel, acceptanceListModel: self.acceptanceListModel) {
                    self.tableViewViewModel = vm
                    self.tableView.reloadData()
                }
            }) { (error) -> (Void) in
                
            }
        }
    }
    private func popVc() {
        ZFToast.show(withMessage: "请求成功")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    // MARK: - 文件上传
    func uploadFile(_ group: DispatchGroup, files: [Data]?, fileName: String, mineType: String, maintainDaily: String) {
        //        print(fileName)
        if files?.count ?? 0 > 0 {
            for (_, file) in ((files?.enumerated())!) {
                if file.count > 0 {
                    group.enter()
                    AlamofireNetworking.sharedInstance.uploadFile({ (formData) in
                        if file.count > 0{
                            formData.append(file, withName: "file", fileName: fileName, mimeType: mineType)
                        }
                    }, urlString: afUrl(addUrl: emsFilesUpload), headers: headersFunc(contentType: .formData), progressPlugin: self, success: { (result) -> (Void) in
                        switch maintainDaily {
                        case "Photos":
                            self.maintainAcceptModel.fileReqVoList?.append(["name" : "maintainPhoto.png", "url" : result["data"].stringValue])
                            //                            case "Video":
                            //                                self.repaireModel.maintainDailyVideo?.append(["name" : "maintainVideo.mp4", "url" : result["data"].stringValue])
                            //                            case "Record":
                        //                                self.repaireModel.maintainDailyRecord?.append(["name" : "maintainRecord.mp3", "url" : result["data"].stringValue])
                        default:
                            break
                        }
                        group.leave()
                    }) { (error) -> (Void) in
                        group.leave()
                    }
                }
                
            }
        }
    }
}

extension MaintainAcceptVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            if indexPath.section != 0{
                switch (cell, model) {
                case (let cell as SwitchTableViewCell, _ as EquipmentInfoDetail):
                    
                    var mModel = self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? EquipmentInfoDetail
                    cell.rightSwitch.rx.isOn.bind { [weak self] (isOn) in
                        mModel?.infoValue = boolToStringValue(isOn)
                        self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = mModel!
                        self?.maintainAcceptModel.acceptanceStatus = boolToStringValue(isOn)

                    }.disposed(by: cell.rx.reuseBag)
  
                case (let cell as CosmosTableViewCell, _):
                    self?.maintainAcceptModel.acceptanceScore = String(Int(cell.cosmosView.rating))
                    cell.cosmosView.didFinishTouchingCosmos = { [weak self] cosmosValue in
                        print(cosmosValue)
                        self?.maintainAcceptModel.acceptanceScore = String(Int(cosmosValue))
                    }
                case (let cell as NextGrowingCell, _ as EquipmentInfoDetail):
                    cell.nextGrowingTextView.textView.text = self?.maintainAcceptModel.acceptanceEvaluate
                    cell.nextGrowingTextView.textView.rx.text.bind { [weak self] (text) -> Void in
                        self?.maintainAcceptModel.acceptanceEvaluate = text
                    }.disposed(by: cell.rx.reuseBag)
                    
                case (let cell as PhotoSelectShowCell,  _ as PhotoShowModel):
                    cell.photoSelectShowView.imageArrays = self?.maintainAcceptModel.photos
                    cell.photoSelectShowView.closure = {[weak self] imageArrays in
                        self?.maintainAcceptModel.photos = imageArrays
                        self?.tableView.reloadRows(at: [indexPath], with: .none)
                    }
                case (let cell as RepairMoneyChartCell,  _ as RepairMoneyShowModel ):
                    cell.chartCellView.chartClosure = {[weak self] (imageArrays, delIdx) in
                        
                        self?.maintainAcceptModel.planRepairStandardInfoRespVoList?.remove(at: delIdx!)
                        if let vm = maintainAcceptMoneyViewModel(self?.maintainAcceptModel){
                            self?.tableViewViewModel[3] = vm
                        }
                        self?.tableView.beginUpdates()
                        self?.tableView.reloadSections(IndexSet(integer: 3), with: .none)
                        self?.tableView.endUpdates()
                    }
                    cell.chartCellView.chartSubClosure = { [weak self] imageArrays in
                        if let im = imageArrays as? [[RepairMoneyChartViewModel]]{
                            self?.repairMoneyModels = im
                        }
                    }
                    let vm = RepairMoneyModel().repairMoneyChartModels(materials: self?.maintainAcceptModel.planRepairStandardInfoRespVoList)

                    cell.chartCellView.imageArrays = vm
                    if  vm.count > 0{
                        cell.conHeight.constant = CGFloat(vm[0].count * 36)
                    }
                break
                case (let cell as RepairMoneyAddTableViewCell, _):
                    cell.leftBtn.rx.tap.subscribe {[weak self] _ in
                        if let vc = repairMoneyListViewController.loadViewControllewWithNib as? repairMoneyListViewController {
                            vc.chartSelClosure = { [weak self] rModels in
                                
                                if  let pModels = rModels?.map({ (rModel) -> RepairDetailsModel.PlanRepairStandardInfoRespVoModel in
                                    var pModel = RepairDetailsModel.PlanRepairStandardInfoRespVoModel()
//                                    pModel.flowId = self?.repairDetailsModel?.maintainDailyRespVo?.flowId
                                    pModel.flowType = "2"
                                    pModel.standardFlag = "0"
                                    pModel.repairProject = rModel.repairProject
                                    pModel.repairNumber = "1"
                                    pModel.repairMoney = rModel.repairMoney
                                    pModel.standardRepairMoneyId = rModel.id
                                    pModel.repairUnit = rModel.unitName
                                    pModel.repairRemark = ""
                                    /* 如果是指派联合验收并且maintainType 为1时，必须指派设备队技术员
                                    
                                    
                                    unionStatus 为0时
                                    node 传递1
                                    unionStatus 为1时
                                    node 传递2*/
//                                    pModel.node = self?.repairDetailsModel?.maintainDailyRespVo?.accepStepStatus
                                    return pModel
                                }){

                                    self?.maintainAcceptModel.planRepairStandardInfoRespVoList?.append(contentsOf: pModels)
                                    if let vm = maintainAcceptMoneyViewModel(self?.maintainAcceptModel){
                                        self?.tableViewViewModel[3] = vm
                                    }
                                    self?.tableView.beginUpdates()
                                    self?.tableView.reloadSections(IndexSet(integer: 3), with: .none)
                                    self?.tableView.endUpdates()

                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                        self?.tableView.setContentOffset(CGPoint.zero, animated: true)
                                    }
                                }
                            }
                            self?.show(vc, sender: nil)
                        }
                    }.disposed(by: cell.rx.reuseBag)
                    cell.rightBtn.rx.tap.subscribe {[weak self] _ in

                        var pModel = RepairDetailsModel.PlanRepairStandardInfoRespVoModel()
//                        pModel.flowId = self?.maintainAcceptModel?.maintainDailyRespVo?.flowId
                        pModel.flowType = "2"
                        pModel.standardFlag = "1"
                        pModel.repairNumber = "1"
                        pModel.repairRemark = ""
                        pModel.standardRepairMoneyId = ""
                        pModel.repairUnit = "项"
                        pModel.repairMoney = ""
                        pModel.repairProject = ""
                        /* 如果是指派联合验收并且maintainType 为1时，必须指派设备队技术员
                         
                         
                         unionStatus 为0时
                         node 传递1
                         unionStatus 为1时
                         node 传递2*/
//                        pModel.node = self?.repairDetailsModel?.maintainDailyRespVo?.accepStepStatus
                        self?.maintainAcceptModel.planRepairStandardInfoRespVoList?.append(pModel)
                        if let vm = maintainAcceptMoneyViewModel(self?.maintainAcceptModel){
                            self?.tableViewViewModel[3] = vm
                        }
                        self?.tableView.beginUpdates()
                        self?.tableView.reloadSections(IndexSet(integer: 3), with: .none)
                        self?.tableView.endUpdates()
                        
                        //                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        //                            self?.tableView.setContentOffset(CGPoint.zero, animated: true)
                        //                        }
                    }.disposed(by: cell.rx.reuseBag)
                    break
                default:
                    break
                }
                
            }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
           
          let model = self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model
        switch (model, "6") {
        case (let model as EquipmentInfoDetail, "6"):
            if model.infoKey?.contains("指派验收") ?? false {
                if let vc = SelPersonViewController.loadViewControllewWithNib as? SelPersonViewController {
                    vc.resultClosure = { [weak self] userInfo in
                        var tranModel = model
                        tranModel.infoValue = userInfo?.userName
                        self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = tranModel
                        self?.maintainAcceptModel.nextAcceptUserId = userInfo?.id
                        self?.tableView.reloadRows(at: [indexPath], with: .none)
                    }
                    vc.roleType = .班组
                    vc.searchMaintainFlag = "0"
                    self.show(vc, sender: nil)
                }
            }
            if model.infoKey?.contains("查看保养详情") ?? false {
                if let vc = NewMaintainVC.loadViewControllewWithNib as? NewMaintainVC {
                    object_setClass(vc, NewMaintainComVC.self)
                    var newMModel = NewMaintainListModel()
                    newMModel.id = self.acceptanceListModel?.orderId
                    vc.newMaintainListModel = newMModel
                    self.show(vc, sender: nil)
                }
            }
        default:
            break
        }
        
       }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.001
    }
}
