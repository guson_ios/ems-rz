//
//  RepaireViewModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/23.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation
// MARK: - 报修界面model
struct EquipRepairModel {
    
    var equipSmallCategoryId: String?//小类ID
    
    
    var equipName: String?
    var equipCode: String?
    var equipSystemCode: String?
    
    var equipPartName: String?
    var equipPartsName: String?
    var equipId: String?
    var equipPartId: String?///部位
    var equipPartsId: String?///部件
    
    var faultCode: String?
    var faultPhenomenon: String?
    var faultMessage: String?
    
    var isEmergencyMaintain: String?
    var isStop: String?
    
    static func faultRepairViewModels(_ model: EquipRepairModel?) -> [EquipmentInfoDetail]{
           return [
               EquipmentInfoDetail(infoKey: "设备名称", infoValue: model?.equipName),
               EquipmentInfoDetail(infoKey: "设备编号", infoValue: model?.equipCode),
               EquipmentInfoDetail(infoKey: "设备系统编号", infoValue: model?.equipSystemCode),
               EquipmentInfoDetail(infoKey: "部位", infoValue: model?.equipPartName),
               EquipmentInfoDetail(infoKey: "部件", infoValue: model?.equipPartsName),
               EquipmentInfoDetail(infoKey: "故障发现时间", infoValue: String.currentTimeFormatter("YYYY-MM-dd HH:mm:ss")),
               EquipmentInfoDetail(infoKey: "故障代码", infoValue: model?.faultCode),
               EquipmentInfoDetail(infoKey: "故障现象", infoValue: model?.faultPhenomenon),
               EquipmentInfoDetail(infoKey: "故障描述", infoValue: model?.faultMessage, cellStatus: true, cellReuseIdentifier: NextGrowingCell.reuseIdentifier),
               EquipmentInfoDetail(infoKey: "应急维修", infoValue: model?.isEmergencyMaintain, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
               EquipmentInfoDetail(infoKey: "是否停机", infoValue: model?.isStop, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
           ]
       }
    
//    static func faultRepairSwitchViewModels(_ model: EquipRepairModel?) -> [EquipmentInfoDetail]{
//        return [
//
//        ]
//    }
}

// MARK: - 报修提交model
struct RepaireModel : HandyJSON{
    enum repairSourceEmun: String {
        case 日常维修 = "0"
        case 润滑保养 = "1"
        case 日检 = "2"
        case 巡检 = "3"
        case 点检 = "4"
    }
    
    var equipId: String?
    var equipPartId: String?
    var equipPartsId: String?
    var faultFoundTime: String?
    var faultId: String?
    var isEmergencyMaintain: String? = "0"//是否应急维修(0：否 1：是)
    var isStop: String? = "0"//是否停机 0 否 1 是    
    var remark: String?
    var repairSource: String? = repairSourceEmun.日常维修.rawValue //报修来源(0：日常维修 1：润滑保养 2：日检  3：巡检  4：点检  5：扫码报修)(默认为0-日常维修)
    var repairSourceId: String?//报修来源ID(润滑保养、日检 、巡检、点检)(日常维修为空)-----即报修与其他流程关联ID
    var repairStandardId: String?//报修标准id    
    
    var maintainDailyPhotos: [[String : String]]? = []
    var maintainDailyRecord: [[String : String]]? = []
    var maintainDailyVideo:  [[String : String]]? = []

    static func repaireViewModels(_ model: EquipRepairModel?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        let equInfoModels = EquipRepairModel.faultRepairViewModels(model).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
        }
     
        let photoShow: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: PhotoShowModel(), size: CGSize(width: KScreenWidth, height: 150), cellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier))]
        
         let record: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: PhotoShowModel(), cellReuseIdentifier: RecordTableViewCell.reuseIdentifier))]
        
        tableViewViewModel.append(equInfoModels)
        tableViewViewModel.append(photoShow)
        tableViewViewModel.append(record)
        return tableViewViewModel
    }
}

struct RepairMoneyShowModel: ViewModelOperate {
    
}

struct PhotoShowModel: ViewModelOperate {
    var imageArrays: [Any]?
}

struct RecordShowModel: ViewModelOperate {
    struct VideoModel {
        var url: String?
        var image: UIImage?
    }
    var imageArrays: [Any]?
    var recordData: Data?
}

struct RepaireFaultCodeModel: HandyJSON, SimplePickViewProtocol {
    func pickViewModelToString() -> String? {
        return faultCode
    }
    var faultCode: String?
    var faultPhenomenon: String?
    var id: String?
}


