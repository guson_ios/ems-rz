//
//  FaultRepairViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/21.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
class FaultRepairViewController: BaseViewController {
    typealias RepairResult = (_ rsult: Bool) -> Void

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bottomBtnView: BottomBtnView!
    //    equipId = "194234278935552"///故障原因
    //    equipSmallCategoryId = "100004001001000"///拿设备部位部件
    var equipRepairModel: EquipRepairModel? = EquipRepairModel()///显示页面
    
    var repairResult: RepairResult?//请求回调
    
    var imageArrays: [Any]? = []
    var audData: Data?
    var videoArrays: [Any]? = []

    var basicDataRespVos: [BasicDataRespVos]?///部位
    var equipPartsModel: [BasicDataRespVos]?///部件
    var faultCodesModel: [RepaireFaultCodeModel]?///故障原因
    var repaireModel = RepaireModel()///提交表达
    
    var isScanRepair = false ///是否扫码报修，如果是其他界面进入的，不需选择部位部件
    
    
    lazy var simplePickView: SimplePickView = {
        let pickView = SimplePickView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight))
        self.view.addSubview(pickView)
        return pickView
    }()
    
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        self.tableView.register(PhotoSelectShowCell.nib, forCellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier)
        self.tableView.register(RecordTableViewCell.nib, forCellReuseIdentifier: RecordTableViewCell.reuseIdentifier)
        self.tableView.register(SwitchTableViewCell.nib, forCellReuseIdentifier: SwitchTableViewCell.reuseIdentifier)
        self.tableView.register(NextGrowingCell.nib, forCellReuseIdentifier: NextGrowingCell.reuseIdentifier)
        
        self.title = "报修"
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        if let viewModels =  RepaireModel.repaireViewModels(equipRepairModel){
            self.tableViewViewModel = viewModels
        }
        self.repaireModel.faultFoundTime = String( Int64((Date().timeIntervalSince1970) * 1000))
        self.repaireModel.equipId = self.equipRepairModel?.equipId///
//        self.repaireModel.remark = "（日常维修、扫码报修默认为空）"
        if self.isScanRepair {//扫码的时候没部位...扫的就是部位
//            self.basicData()
        }else{
            self.faultManageList()
        }
        self.getEquipInfoBaseId()

        self.bottomBtnView.btn.setTitle("提交", for: .normal)
        bottomBtnView.btn.rx.tap.bind { [weak self] (_) -> Void in
            
            self?.bottomBtnView.btn.isEnabled = false
            let group = DispatchGroup.init()
            self?.uploadFile(group, files: self?.imageArrays?.map({ (model) -> Data? in
                if let model = model as? ZLPhotoBrowerModel{
                    return model.imageData
                }
                return nil
            }) as? [Data], fileName: "file.png", mineType: "image/png", maintainDaily: "Photos")
            self?.uploadFile(group, files: self?.videoArrays?.map({ (model) -> Data? in
                if let model = model as? ZLPhotoBrowerModel{
                    return model.imageData
                }
                return nil
            }) as? [Data], fileName: "file.mp4", mineType: "video/mp4", maintainDaily: "Video")
            
            
            self?.uploadFile(group, files: [self?.audData] as? [Data], fileName: "file.mp3", mineType: "video/mp3" ,maintainDaily: "Record")
            group.notify(queue: DispatchQueue.main) {
                print(self?.repaireModel.toJSONString() as Any)
                
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainDailyStart), parameter: nil, method: .post, encoding: self?.repaireModel.toJSONString() ?? "{}", headers: headersFunc(contentType: .json), progressPlugin: self, success: { [weak self] (result) -> (Void) in
                    if let stSelf = self{
                        ZFToast.show(withMessage: "提交成功")
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                            if ((stSelf.repairResult) != nil){
                               stSelf.repairResult?(true)
                            }
                            stSelf.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }) { (error) -> (Void) in
                    self?.bottomBtnView.btn.isEnabled = true

                }
            }
        }.disposed(by: disposeBag)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.reloadData()
    }
}

extension FaultRepairViewController{
    // MARK: - 获取设备信息
    func getEquipInfoBaseId() {
        if  self.repaireModel.equipId != nil  {
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipInfoBaseId + self.repaireModel.equipId!), parameter: nil, method: .get, encoding: URLEncoding.httpBody, success: { [weak self] (result) -> (Void) in
                let model = EquipInfoScanModel.deserialize(from: result["data"].dictionaryObject)
                self?.modelRefresh("设备编号", infoValue: model?.equipCode)
                self?.modelRefresh("设备名称", infoValue: model?.equipName)
                self?.modelRefresh("设备系统编号", infoValue: model?.equipSystemCode)
                self?.equipRepairModel?.equipSmallCategoryId = model?.equipSmallCategoryId
//                self.basicData()
                if self?.isScanRepair ?? false {//扫码的时候没部位...扫的就是部位
                    self?.basicData()
                }
            }) { (error) -> (Void) in
                ZFToast.show(withMessage: "获取设备信息失败")
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    // MARK: - 更新数据
    func modelRefresh(_ infoKey: String?, infoValue: String?) {
        self.tableViewViewModel =  self.tableViewViewModel.enumerated().map({ [weak self] (section, viewModels) -> [TableViewViewModel<ViewModelOperate>] in
            viewModels.enumerated().map { [weak self] (row, viewModelOperate) -> TableViewViewModel<ViewModelOperate>  in
                var modelOperate = viewModelOperate
                switch modelOperate.baseViewModel.model {
                case var model as EquipmentInfoDetail where model.infoKey == infoKey :
                    model.infoValue = infoValue
                    modelOperate.baseViewModel.model = model
                    if let stSelf = self {
                        DispatchQueue.main.async {///开了线程
                            stSelf.tableView.beginUpdates()
                            stSelf.tableView.reloadRows(at: [IndexPath(row: row, section: section)], with: .none)
                            stSelf.tableView.endUpdates()
                        }
                    }
                default:
                    break
                }
                return modelOperate
            }
        })
    }
    // MARK: - 设备部位部件
    func basicData() {
        
        if let equipSmallCategoryId = self.equipRepairModel?.equipSmallCategoryId{
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsBasicData), parameter: ["basicDataId" : equipSmallCategoryId], method: .get, headers: headersFunc(contentType: .define), progressPlugin: nil, success: { [weak self] (result) -> (Void) in
                self?.basicDataRespVos = [BasicDataRespVos].deserialize(from: result["data"].arrayObject) as? [BasicDataRespVos]
                if self?.basicDataRespVos?.count ?? 0 <= 0 {
                    ZFToast.show(withMessage: "没有部位")
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                        self?.navigationController?.popViewController(animated: true)
                    }
                    return
                }
                self?.modelRefresh("设备名称", infoValue: self?.basicDataRespVos?.first?.basicName)
                if let basicVos = self?.basicDataRespVos?.first?.basicDataRespVos{
                    for (i, vos) in basicVos.enumerated() {
                        if vos.id == self?.equipRepairModel?.equipPartId {
                            ///默认第一个，没有扫码的部位
                            if let tableView = self?.tableView {
                                self?.tableView(tableView, didSelectRowAt: IndexPath(row: 3, section: 0))
                            }
                            self?.simplePickView.isHidden = true
                            self?.simplePickView.simplePickClosure!(i, vos)
                        }
                    }
                }
            }) { (error) -> (Void) in
                if self.basicDataRespVos?.count ?? 0 <= 0 {
                    ZFToast.show(withMessage: "没有部位")
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }else{
            if self.basicDataRespVos?.count ?? 0 <= 0 {
                ZFToast.show(withMessage: "没有部位")
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    // MARK: - 故障原因
    func faultManageList() {
//        ["equipPartId": "206311524015104", "equipPartsId": "206311633071104", "equipId": "193538974698496"]

        print(["equipId" : self.repaireModel.equipId ?? "", "equipPartId" : self.repaireModel.equipPartId ?? "", "equipPartsId" : self.repaireModel.equipPartsId ?? ""])
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsFaultManageList), parameter: ["equipId" : self.repaireModel.equipId ?? "", "equipPartId" : self.repaireModel.equipPartId ?? "", "equipPartsId" : self.repaireModel.equipPartsId ?? ""], method: .get, headers: headersFunc(contentType: .json), progressPlugin: nil, success: {[weak self] (result) -> (Void) in
            self?.faultCodesModel = [RepaireFaultCodeModel].deserialize(from: result["data"].arrayObject) as? [RepaireFaultCodeModel]
            self?.faultCodesModel?.append(RepaireFaultCodeModel(faultCode: "新增故障代码", faultPhenomenon: "新增故障现象", id: nil))
            self?.modelRefresh("故障代码", infoValue: self?.faultCodesModel?.first?.faultCode)
            self?.modelRefresh("故障现象", infoValue: self?.faultCodesModel?.first?.faultPhenomenon)
            self?.repaireModel.faultId = self?.faultCodesModel?.first?.id
        }) { (error) -> (Void) in
            
        }
    }
    // MARK: - 文件上传
    func uploadFile(_ group: DispatchGroup, files: [Data]?, fileName: String, mineType: String, maintainDaily: String) {
//        print(fileName)
        if files?.count ?? 0 > 0 {
            for (i, file) in ((files?.enumerated())!) {
                if file.count > 0 {
                    print(file.count)
                    group.enter()
                    AlamofireNetworking.sharedInstance.uploadFile({ (formData) in
                        if file.count > 0{
                            formData.append(file, withName: "file", fileName: String(i) + fileName, mimeType: mineType)
                        }
                    }, urlString: afUrl(addUrl: emsFilesUpload), headers: headersFunc(contentType: .formData), progressPlugin: self, success: { (result) -> (Void) in
                        switch maintainDaily {
                        case "Photos":
                            self.repaireModel.maintainDailyPhotos?.append(["name" : "maintainPhoto.png", "url" : result["data"].stringValue])
                        case "Video":
                            self.repaireModel.maintainDailyVideo?.append(["name" : "maintainVideo.mp4", "url" : result["data"].stringValue])
                        case "Record":
                            self.repaireModel.maintainDailyRecord?.append(["name" : "maintainRecord.mp3", "url" : result["data"].stringValue])
                        default:
                            break
                        }
                        group.leave()
                    }) { (error) -> (Void) in
                        group.leave()
                    }
                }
                
            }
        }
    }
}

extension FaultRepairViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            switch cell {
            case let cell as SwitchTableViewCell:
                let infoKey = (model as? EquipmentInfoDetail)?.infoKey
                var mModel = self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? EquipmentInfoDetail
                cell.rightSwitch.rx.isOn.bind { [weak self] (value) -> Void in
                    if infoKey == "应急维修"{
                        self?.repaireModel.isEmergencyMaintain = value ? "1" : "0"
                    }else{
                        self?.repaireModel.isStop = value ? "1" : "0"
                    }
                    mModel?.infoValue = boolToStringValue(value)
                    self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = mModel!
                }.disposed(by: cell.rx.reuseBag)
            case let cell as NextGrowingCell:
                cell.nextGrowingTextView.textView.text = self?.repaireModel.remark
                cell.nextGrowingTextView.textView.rx.text.bind { [weak self] (text) -> Void in
                    self?.repaireModel.remark = text
                }.disposed(by: cell.rx.reuseBag)
                
            case let cell as PhotoSelectShowCell:
                cell.photoSelectShowView.imageArrays = self?.imageArrays
                cell.photoSelectShowView.closure = {[weak self] imageArrays in
                    self?.imageArrays = imageArrays
                    self?.tableView.reloadData()
                }
            case let cell as RecordTableViewCell:
                cell.photoShowView.selectPhoto = true
                cell.photoShowView.imageArrays = self?.videoArrays
                cell.photoShowView.selectPhoto = self?.videoArrays?.count ?? 0 > 0 ? false : true
                cell.photoShowView.closure = {[weak self] imageArrays in
                    self?.videoArrays = imageArrays
                }
                cell.recordView.recordData = self?.audData
                cell.recordView.delBtn.rx.tap.bind {[weak cell, weak self] _ in
                    cell?.recordView.recordData = nil
                    self?.audData = nil
                }.disposed(by: cell.rx.reuseBag)
                cell.recordView.recordDataclosure = { [weak self] recordData in
                    self?.audData = recordData
                }
            default:
                break
            }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if var model: EquipmentInfoDetail = self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? EquipmentInfoDetail {
            
            self.simplePickView.simplePickClosure = {[weak self] row , smodel in
                switch smodel {
                case let basicDataRespVos as BasicDataRespVos :
                    self?.modelRefresh(model.infoKey, infoValue: basicDataRespVos.basicName)
                    model.infoValue = basicDataRespVos.basicName
                    if model.infoKey == "部位" {
                        self?.equipPartsModel = basicDataRespVos.basicDataRespVos///部件
                        self?.modelRefresh("部件", infoValue: self?.equipPartsModel?.first?.basicName)
                        
                        self?.repaireModel.equipPartId = basicDataRespVos.id
                        self?.repaireModel.equipPartsId = self?.equipPartsModel?.first?.id
                    }else{
                        self?.repaireModel.equipPartsId = basicDataRespVos.id
                    }
                    self?.faultManageList()
                case let faultCodeModel as RepaireFaultCodeModel :
                    
                    self?.modelRefresh(model.infoKey, infoValue: faultCodeModel.faultCode)
                    self?.modelRefresh("故障现象", infoValue: faultCodeModel.faultPhenomenon)
                    self?.repaireModel.faultId = faultCodeModel.id
                default:
                    break
                }
            }
            switch (model.infoKey, self.isScanRepair) {
                
            case ("部位", true):
                self.simplePickView.pickerModels = self.basicDataRespVos?.first?.basicDataRespVos
            case ("部件", true):
                self.simplePickView.pickerModels = self.equipPartsModel
            case ("故障代码", _):
                if self.faultCodesModel == nil {
                    self.faultManageList()
                }else{
                    self.simplePickView.pickerModels = self.faultCodesModel
                }
            default:
                break
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}
