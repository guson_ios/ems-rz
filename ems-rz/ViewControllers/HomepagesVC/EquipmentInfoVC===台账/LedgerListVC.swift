//
//  LedgerViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/24.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit


class LedgerListVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var equipmentInfoModels: [EquipmentInfoDetail]?
    //    var equipInfoData: EquipInfoData?
    var searchModel: EquipBaseInfoSearch?
    var records: [EquipBaseInfoRespVo]? = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "台账查询列表"
        self.tableView.register(LedgerCell.nib, forCellReuseIdentifier: LedgerCell.reuseIdentifier)
      
        self.tableView.tableViewRefreshClouse {[weak self] (pageNo, listCount) in
            if pageNo == 1 { self?.records?.removeAll()}
//             + "?size=10&current=\(pageNo)"
            print(pageDict(current: pageNo))
//            print(self?.searchModel?.toJSON()?.dictToJsonString())
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipInfoList), parameter: pageDict(current: pageNo), method: .post, encoding: self?.searchModel?.toJSON()?.dictToJsonString() ?? "", headers: headersFunc(contentType: .json) , success: { (result) -> (Void) in
                let model = EquipInfoData.deserialize(from: result["data"].dictionaryObject)
                if let records = model?.records{
                    self?.records?.append(contentsOf: records)
                }
                self?.tableView.reloadData()
                listCount(model?.records?.count ?? 0)
            }) { (error) -> (Void) in
                listCount(0)
            }
        }
    }
}
extension LedgerListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.records?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LedgerCell = tableView.dequeueReusableCell(withIdentifier: LedgerCell.reuseIdentifier, for: indexPath) as! LedgerCell
        cell.confignLedgerCell(self.records?[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let infoId = self.records?[indexPath.row].id {
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipInfo), parameter: ["equipId":infoId], method: .get, success: { (result) -> (Void) in
                let model = EquipmentPanoramic.deserialize(from: result["data"].dictionaryObject)
                print(model?.equipBaseInfoRespVo?.equipState as Any)
                let vc = EquipmentInfoVC()
                vc.equipmentPanoramic = model
                self.show(vc, sender: nil)
            }) { (error) -> (Void) in
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}


