//
//  LedgeViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit


class LedgeViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnView: BottomBtnView!
    var searchModel = EquipBaseInfoSearch()
    
    var companyModel: CompanyModel? ///不需要选择公司了
    var basicDataRespVos: [BasicDataRespVos]? ///设备技术状况
    lazy var simplePickView: SimplePickView = {
        let pickView = SimplePickView.init(frame: CGRect(x: 0, y: -100, width: KScreenWidth, height: KScreenHeight))
        self.view.addSubview(pickView)
        return pickView
    }()
    
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.title = "台账"
        self.tableView.register(LedgerChartCell.nib, forCellReuseIdentifier: LedgerChartCell.reuseIdentifier)
        self.tableView.register(TextFieldTableViewCell.nib, forCellReuseIdentifier: TextFieldTableViewCell.reuseIdentifier)
        if let vm = LedgerViewModel.LedgerModels(nil, numModels: nil) {
            self.tableViewViewModel = vm
        }
        self.getEquipTotalNumber()
        self.btnView.btn.rx.tap.bind { [weak self] _ -> Void in
            
            let vc = UIStoryboard(name: LedgerStoryboard, bundle: nil).instantiateViewController(withIdentifier: LedgerListVC.vcIdentifier) as! LedgerListVC
            vc.searchModel = self?.searchModel
            self?.show(vc, sender: nil)
        }.disposed(by: disposeBag)
    }
}
extension LedgeViewController {
    func getEquipTotalNumber() {
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipTotalNumber), parameter: nil, method: .get, success: { result -> (Void) in
            let numModels = [
                EquOperatNumViewModel(operatText: "设备总数 ", numText: result["data"]["totalEquipNumber"].stringValue, imageStr: "ledger01"),
                EquOperatNumViewModel(operatText: "在用设备", numText: result["data"]["totalEquipUsingNumber"].stringValue, imageStr: "ledger02"),
                EquOperatNumViewModel(operatText: "在修设备 ", numText: result["data"]["totalEquipRepairNumber"].stringValue, imageStr: "ledger03"),
            ]
            if let vm = LedgerViewModel.LedgerModels(nil, numModels: numModels) {
                self.tableViewViewModel = vm
            }
            self.tableView.reloadData()
        }) { (error) -> (Void) in
            
        }
    }
    
    func getCompany() {
//        ["useCompanyName" : EmsGlobalObj.obj.userInfo?.useCompanyName ?? ""]
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsCommonCompany), parameter: nil, method: .get, headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
            var models = [CompanyModel].deserialize(from: result["data"].arrayObject)
            models?.insert(CompanyModel(id: nil, parentId: nil, orgCode: nil, orgName: "全部"), at: 0)
            self.simplePickView.pickerModels = models as? [SimplePickViewProtocol]
        }) { (error) -> (Void) in
            
        }
    }
    
//    func getCommonOrg(_ model: CompanyModel) {
    func getCommonOrg() {
        
        let model = EmsGlobalObj.obj.userInfo
        self.searchModel.companyId = model?.useCompanyId
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsCommonOrg), parameter: [ "useOrgName" : model?.useOrgName ?? "", "useCompanyId" : model?.useCompanyId ?? ""], method: .get, headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
            var models = [CompanyModel].deserialize(from: result["data"].arrayObject)
            models?.insert(CompanyModel(id: nil, parentId: nil, orgCode: nil, orgName: "全部"), at: 0)
            self.simplePickView.pickerModels = models as? [SimplePickViewProtocol]
        }) { (error) -> (Void) in
            
        }
    }
}

extension LedgeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { (operate, cell) in
            
            let info = operate as? EquipmentInfoDetail
            switch (info?.infoKey, cell) {
            case ("设备系统编号", let cell as TextFieldTableViewCell):
                cell.textField.rx.text.bind { [weak self] (text) -> Void in
                    if text?.count ?? 0 > 0{
                        self?.searchModel.equipSystemCode = text
                    }
                }.disposed(by: cell.rx.reuseBag)
            case ("关键字", let cell as TextFieldTableViewCell):
                cell.textField.rx.text.bind { [weak self] (text) -> Void in
                    if text?.count ?? 0 > 0{
                        self?.searchModel.keywords = text
                    }
                }.disposed(by: cell.rx.reuseBag)
            case (_, let cell as LedgerChartCell):
                cell.ledgerChartCollection.closure = { [weak self] _ in
                    if let vc = LedgeStatisticsVC.loadViewControllewWithNib as? LedgeStatisticsVC {
                        self?.show(vc, sender: nil)
                    }
                }
                break
            default:
                
                break
            }
            
        })
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if var model = self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? EquipmentInfoDetail {
            ///没什么不同
            simplePickView.simplePickClosure = { [weak self] row, pickModel in
                switch pickModel {
                case let pickModel as CompanyModel:
                    model.infoValue = pickModel.orgName
//                    if indexPath.row == 0 {
//                        self?.companyModel = pickModel
//                        self?.searchModel.companyId = pickModel.id
//                        self?.searchModel.useOrgId = ""
//                        var model1: EquipmentInfoDetail? = self?.tableViewViewModel[indexPath.section][indexPath.row + 1].baseViewModel.model as? EquipmentInfoDetail
//                        model1?.infoValue = "请选择 〉"
//                        self?.tableViewViewModel[indexPath.section][indexPath.row + 1].baseViewModel.model = model1!
//                        self?.tableView.reloadRows(at: [IndexPath(row: indexPath.row + 1, section: indexPath.section)], with: .none)
//                    }else{
                        self?.searchModel.useOrgId = pickModel.id
//                    }
                case let pickModel as BasicDataRespVos:
                    model.infoValue = pickModel.basicName
                    self?.searchModel.equipTechState = pickModel.id
                case let pickModel as EquStatus:
                    model.infoValue = pickModel.name
                    self?.searchModel.equipState = String(pickModel.start!)
                default:
                    
                    break
                }
                self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = model
                self?.tableView.reloadRows(at: [indexPath], with: .none)
            }
            switch model.infoKey {
            case "设备所属单位":
                self.getCompany()
            case "使用部门":
//                if self.companyModel != nil  && self.companyModel?.id != nil{
                    self.getCommonOrg()
//                }
            case "设备技术状况":
                if self.basicDataRespVos == nil {
                    AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsBasicNameTree), parameter: ["basicName" : "设备技术状况"], method: .post, encoding: URLEncoding.httpBody,  progressPlugin: self, success: { (result) -> (Void) in
                        let model: BasicDataRespVos? = BasicDataRespVos.deserialize(from: result["data"].dictionaryObject)!
                        self.basicDataRespVos = model?.basicDataRespVos?.first?.basicDataRespVos
                        let basicDataRespVoModel = BasicDataRespVos()
                        basicDataRespVoModel.basicName = "全部"
                        basicDataRespVoModel.id = ""
                        self.basicDataRespVos?.insert(basicDataRespVoModel, at: 0)
                        self.simplePickView.pickerModels = self.basicDataRespVos
                        self.simplePickView.isHidden = false
                    }) { (error) -> (Void) in
                        
                    }
                }else{
                    self.simplePickView.pickerModels = self.basicDataRespVos
                    self.simplePickView.isHidden = false
                }
                
            case "设备使用状态":
                self.simplePickView.pickerModels = EquStatus.equStatus()
                self.simplePickView.isHidden = false
            case "设备类别":
                let vc = TreeViewController.loadViewControllewWithNib as! TreeViewController
                vc.treeViewClosure = { [weak self] baseModels in
                    if let baseModels = baseModels as? [BasicDataRespVos] {
                        self?.searchModel.equipBigCategoryId = nil
                        self?.searchModel.equipMiddleCategoryId = nil
                        self?.searchModel.equipSmallCategoryId = nil
                        for (idx, baseModel) in baseModels.enumerated() {
                            switch idx {
                            case 0:
                                self?.searchModel.equipBigCategoryId = baseModel.id
                            case 1:
                                self?.searchModel.equipMiddleCategoryId = baseModel.id
                            case 2:
                                self?.searchModel.equipSmallCategoryId = baseModel.id
                            default:
                                break
                            }
                        }
                        model.infoValue = baseModels.last?.basicName
                        self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = model
                        self?.tableView.reloadRows(at: [indexPath], with: .none)
                    }
                }
                vc.title = "设备类别选择"
                self.show(vc, sender: nil)
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}

