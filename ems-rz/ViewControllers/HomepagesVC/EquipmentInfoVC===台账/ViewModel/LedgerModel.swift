//
//  LedgerModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/26.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation

struct EquStatus: SimplePickViewProtocol {
    func pickViewModelToString() -> String? {
        return self.name
    }
    var name: String?
    var start: Int?
    
     enum equStatusEnum: Int {
            case 全部 = 0
            case 在用 = 1
            case 在修 = 2
            case 租赁 = 3
            case 报废 = 4
            case 封存 = 5
        }
      static  func equStatus() -> [EquStatus] {
            return [
                EquStatus(name: "全部", start: 0),
                EquStatus(name: "在用", start: 1),
                EquStatus(name: "在修", start: 2),
                EquStatus(name: "租赁", start: 3),
                EquStatus(name: "报废", start: 4),
                EquStatus(name: "封存", start: 5),
            ]
        }
}

struct LedgerViewModel {
    
    static func LedgerModels(_ model: EquipBaseInfoRespVo?, numModels: [EquOperatNumViewModel]?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        let ledgerModels = EquipmentInfoDetail.equipmentInfoSearchViewModels(model).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: TextFieldTableViewCell.reuseIdentifier))
        }
      
        let photoShow: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: EquOperatNumShowModel(equOperatNumViewModel: numModels), size: CGSize(width: KScreenWidth, height: 150), cellReuseIdentifier: LedgerChartCell.reuseIdentifier))]
        
        tableViewViewModel.append(photoShow)
        tableViewViewModel.append(ledgerModels)
        return tableViewViewModel
    }
}

struct CertificateModel {
    static func certificateModels(_ model: EquipSpecialInfoOrCertificateRespVo?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        let certificateModels = EquipmentInfoDetail.equCertificateViewModels(model?.equipSpecialInfoRespVo).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
        }
        let photoShow: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: PhotoShowModel(), size: CGSize(width: KScreenWidth, height: 150), cellReuseIdentifier: PhotoShowTableViewCell.reuseIdentifier))]
        tableViewViewModel.append(certificateModels)
        tableViewViewModel.append(photoShow)
        return tableViewViewModel
    }
}

extension EquipmentInfoDetail{
    static func equipmentInfoSearchViewModels(_ model: EquipBaseInfoRespVo?) -> [EquipmentInfoDetail] {
        return [
//            EquipmentInfoDetail(infoKey: "设备所属单位", infoValue: "请选择 〉"),
            EquipmentInfoDetail(infoKey: "使用部门", infoValue: "请选择 〉"),
            EquipmentInfoDetail(infoKey: "设备类别", infoValue: "请选择 〉"),
            
            //            EquipmentInfoDetail(infoKey: "设备大类", infoValue: model?.equipBigCategoryId),
            //            EquipmentInfoDetail(infoKey: "设备中类", infoValue: model?.equipMiddleCategoryId),
            //            EquipmentInfoDetail(infoKey: "设备小类", infoValue: model?.equipSmallCategoryId),
            EquipmentInfoDetail(infoKey: "设备技术状况", infoValue: "请选择 〉"),
            EquipmentInfoDetail(infoKey: "设备使用状态", infoValue: "请选择 〉"),
            EquipmentInfoDetail(infoKey: "设备系统编号", infoValue: "输入设备系统编号", cellStatus: true),
            EquipmentInfoDetail(infoKey: "关键字", infoValue: "输入设备名称，型号", cellStatus: true),
        ]
    }
    
    static func equCertificateViewModels(_ model: EquipSpecialInfoOrCertificateRespVo.EquipSpecialInfoRespVo?) -> [EquipmentInfoDetail]{
        return [
            EquipmentInfoDetail(infoKey: "特种设备检查周期", infoValue: model?.specialDiscoverCycle),
            EquipmentInfoDetail(infoKey: "特种设备注册码", infoValue: model?.particularRegistrationCode),
            EquipmentInfoDetail(infoKey: "证书类别", infoValue: model?.certifiTypeName),
            EquipmentInfoDetail(infoKey: "证书编号", infoValue: model?.certifiCode),
            EquipmentInfoDetail(infoKey: "发布时间", infoValue: model?.releaseDate),
            EquipmentInfoDetail(infoKey: "到期时间", infoValue: model?.expireDate),
            EquipmentInfoDetail(infoKey: "有效期（月）", infoValue: model?.validDate),
            EquipmentInfoDetail(infoKey: "证书状态", infoValue: model?.certifiStateName),
        ]
    }
    
    static func equIndicatorViewModels(_ model: IndicatorDataModel?) -> [EquipmentInfoDetail] {
        return [
            EquipmentInfoDetail(infoKey: "完好率", infoValue: model?.intactRate),
            EquipmentInfoDetail(infoKey: "故障率", infoValue: model?.faultRate),
            EquipmentInfoDetail(infoKey: "利用率", infoValue: model?.utilizeRate),
            EquipmentInfoDetail(infoKey: "停机时长", infoValue: model?.shutdownHour),
            EquipmentInfoDetail(infoKey: "累计维修料耗金额", infoValue: model?.maintainAmount),
            EquipmentInfoDetail(infoKey: "累计保养料耗金额", infoValue: model?.maintenanceAmount),
            EquipmentInfoDetail(infoKey: "委外维修结算费用", infoValue: model?.maintainSettlementAmount),
            EquipmentInfoDetail(infoKey: "委外保养结算费用", infoValue: model?.maintenanceSettlementAmount),
        ]
    }
}


struct TechnologyModel: HandyJSON {
    var remark: String?
    var id: String?
    var specificId: String?
    var uploadUser: String?
    var deleted: String?
    var uploadDate: String?
    var dataName: String?
    var certifiPhotos: [CertifiPhotos]?
    var equipBigCategoryName: String?
    var specificName: String?
    var createdUser: String?
    var equipSmallCategoryName: String?
    var equipBigCategoryId: String?
    var modifiedTime: String?
    var equipMiddleCategoryName: String?
    var modifiedUser: String?
    var manufacturer: String?
    var dataFormat: String?
    var modelNumberName: String?
    var dataTypeName: String?
    var equipSmallCategoryId: String?
    var uploadUserName: String?
    var dataType: String?
    var modelNumberId: String?
    var createdTime: String?
    var equipMiddleCategoryId: String?
    
    struct CertifiPhotos: HandyJSON {
        var name: String?
        var typeName: String?
        var id: String?
        var url: String?
    }
}
