//
//  TreeModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/27.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation

class BasicDataRespVos    : NSObject, HandyJSON, SimplePickViewProtocol{
    func pickViewModelToString() -> String? {
        return self.basicName
    }
    
    var basicDataRespVos: [BasicDataRespVos]?
    var basicName: String?
    var id: String?
    var remark: String?
    var parentId: String?
    var staticFlag: String?
    var status: String?
    var isOpen: Bool? = false
    var level: Int? = 0
    
    required override init() {}
}


