//
//  ChartTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/21.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class ChartTableViewCell: UITableViewCell {
    @IBOutlet weak var chartCollectionView: ChartCollectionView!
    @IBOutlet weak var conHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

class ChartCollectionView: PhotoShowView {
    
    var chartClosure: ((_ imageArrays: [Any]?, _ delIdx: Int?) -> Void)?

    override func registerCollectionViewCell() {
        self.collectionView.register(ChartCollectionViewCell.nib, forCellWithReuseIdentifier: ChartCollectionViewCell.reuseIdentifier)
        let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 1
        flowLayout.minimumLineSpacing = 1
        flowLayout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.isScrollEnabled = true
        self.collectionView.bounces = false
        self.collectionView.backgroundColor = COL9FB9F9
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let sections = self.imageArrays as? [[String]] {
            return sections.count
        }
        return 10
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let sections = self.imageArrays as? [[String]] {
            return sections[section].count
        }
        return self.imageArrays?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ChartCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartCollectionViewCell.reuseIdentifier, for: indexPath) as! ChartCollectionViewCell
//        cell.contentView.backgroundColor = COL9FB9F9
        if let sections = self.imageArrays as? [[String]] {
            cell.label1.text = sections[indexPath.section][indexPath.row]
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: 70, height: 34)
        return size
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let array = self.imageArrays as? [[String]] {
            
            
            if array[indexPath.section][indexPath.row] == "删除" {
                for (i, arraySec) in array.enumerated() {
                    var arr = arraySec
                    arr.remove(at: indexPath.row)
                    self.imageArrays?[i] = arr
                }
                if (self.imageArrays?[indexPath.section] as? [String])?.count == 1 {
                    self.imageArrays = nil
                }
                self.chartClosure?(self.imageArrays, indexPath.row - 1)
            }
        }
    }
}

