//
//  EquOperatNumView.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/20.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

struct EquOperatNumShowModel: ViewModelOperate {
    var equOperatNumViewModel: [EquOperatNumViewModel]?
}
struct EquOperatNumViewModel {
    var operatText: String?
    var numText: String? = "0"
    var imageStr: String?
    var historyUrl: String?
    
    static func equOperatNumViewModels(_ model: EquipmentPanoramic?, monthWork: MonthWorkModel?) -> [EquOperatNumViewModel] {
        return [
            EquOperatNumViewModel(operatText: "点检", numText: monthWork?.pointCheckTimes, imageStr: "history5"),
            EquOperatNumViewModel(operatText: "日检", numText: monthWork?.checkDailyTimes, imageStr: "history1"),
            EquOperatNumViewModel(operatText: "巡检", numText: monthWork?.pollingTimes, imageStr: "history2"),
            EquOperatNumViewModel(operatText: "润滑保养", numText: monthWork?.maintainTimes, imageStr: "history3", historyUrl: emsGetMaintenanceHistory),
            EquOperatNumViewModel(operatText: "维修", numText: monthWork?.maintainDailyTimes, imageStr: "history7", historyUrl: emsGetMaintainHistory),
            EquOperatNumViewModel(operatText: "大修", numText: monthWork?.majorMaintainTimes, imageStr: "history6", historyUrl: emsGetPlanRepairOrderHistory),
            EquOperatNumViewModel(operatText: "保险维修", numText: monthWork?.maintainInsuranceTimes, imageStr: "history4", historyUrl: emsGetMaintainInsuranceHistory),
        ]
    }
    
    static func ledgerEquOperatNumViewModels(_ model: EquipmentPanoramic?) -> [EquOperatNumViewModel] {
           return [
               EquOperatNumViewModel(operatText: "设备总数 ", numText: "3000", imageStr: "ledger01"),
               EquOperatNumViewModel(operatText: "在用设备", numText: "300", imageStr: "ledger02"),
               EquOperatNumViewModel(operatText: "在修设备 ", numText: "106", imageStr: "ledger03"),
           ]
       }
    
    
}

class EquOperatNumView: UIView {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var operatLabel: UILabel!
    
    @IBOutlet weak var numLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadXibView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadXibView()
    }
    
    func confignEquOperatNumView(_ model: EquOperatNumViewModel?) {
        self.operatLabel.text = model?.operatText
        self.numLabel.text = model?.numText ?? "0"
        self.bgImageView.image = UIImage(named: model?.imageStr ?? "history1")
        self.bgImageView.isUserInteractionEnabled = true
    }
    
}
