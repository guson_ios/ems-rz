//
//  MaterialCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/20.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class MaterialCell: UITableViewCell {

    @IBOutlet weak var showBtn: UIButton!
    @IBOutlet weak var summary: EquSummaryView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configMaterialCell(_ model: TechnologyModel?) {
        summary.lable1.text = "资料名称：" + (model?.dataName ?? "")
        summary.label2.text = "上传人：" + (model?.uploadUserName ?? "")
        summary.label3.text = "上传时间：" + (model?.uploadDate ?? "")
        summary.label4.isHidden = true
    }
    
}
