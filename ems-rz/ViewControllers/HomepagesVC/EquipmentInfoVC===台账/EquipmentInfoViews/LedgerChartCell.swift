//
//  LedgerChartCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class LedgerChartCollection: PhotoShowView {
    
    
    override func registerCollectionViewCell() {
        self.collectionView.register(EquHistoryCell.nib, forCellWithReuseIdentifier: EquHistoryCell.reuseIdentifier)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: EquHistoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: EquHistoryCell.reuseIdentifier, for: indexPath) as! EquHistoryCell
        
        cell.equOperatNumView.confignEquOperatNumView(self.imageArrays?[indexPath.row] as? EquOperatNumViewModel)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: KScreenWidth / 3 - 10, height: 90)
        return size
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.closure?(nil)
    }
}
class LedgerChartCell: UITableViewCell {
    @IBOutlet weak var ledgerChartCollection: LedgerChartCollection!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.ledgerChartCollection.collectionView.backgroundColor = .white

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
