//
//  TextFieldTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/26.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {
    @IBOutlet weak var leftLabel: UILabel!
    
    @IBOutlet weak var textField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func confignTextFieldCell(_ model: EquipmentInfoDetail?) {
        textField.textColor = .lightGray
        leftLabel.text = model?.infoKey
        textField.placeholder = model?.infoValue
        textField.isUserInteractionEnabled = (model?.cellStatus)!
    }
    
}
