//
//  LedgerCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/24.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class LedgerCell: UITableViewCell {

    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confignLedgerCell(_ model: EquipBaseInfoRespVo?) {
        label1.text = model?.equipName
        label2.text = "设备编号：" + (model?.equipCode ?? "")
        label3.text = "设备所属单位：" + (model?.useCompanyName ?? "")
        label4.text = "使用部门：" + (model?.useOrgName ?? "")
        label5.text = "负责人：" + (model?.responsiCode ?? "")
        if let equipState = model?.equipState {
            btn1.setTitle(equipState, for: .normal)
        }
    }
}
