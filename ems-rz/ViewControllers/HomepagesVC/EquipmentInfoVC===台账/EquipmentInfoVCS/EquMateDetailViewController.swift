//
//  EquMateDetailViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/20.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class EquMateDetailViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var parameter: [String : String]?
    var historyUrl: String = ""
    lazy var historyDetailModels = [[String]]()///ViewModel
    var equipmentPanoramic: EquipmentPanoramic? 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(ChartTableViewCell.nib, forCellReuseIdentifier: ChartTableViewCell.reuseIdentifier)
        tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)

        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: historyUrl), parameter: parameter, method: .get, success: { (result) -> (Void) in
            if let models = [HistoryDetailModel].deserialize(from: result["data"].arrayObject) as? [HistoryDetailModel]{
                self.historyDetailModels = HistoryDetailModel.historyDetailModels(nil, materials: models)
                self.tableView.reloadData()
            }
        }) { (error) -> (Void) in
            
        }
    }
}

extension EquMateDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell: EquSummaryCell = tableView.dequeueReusableCell(withIdentifier: EquSummaryCell.reuseIdentifier, for: indexPath) as! EquSummaryCell
            cell.confignSummaryCell(self.equipmentPanoramic?.equipBaseInfoRespVo)
            return cell
        }
        let cell: ChartTableViewCell = tableView.dequeueReusableCell(withIdentifier: ChartTableViewCell.reuseIdentifier, for: indexPath) as! ChartTableViewCell
        cell.chartCollectionView.imageArrays = self.historyDetailModels
        if self.historyDetailModels.count > 0{
            cell.conHeight.constant = CGFloat(self.historyDetailModels[0].count * 36)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.001
    }
}

struct HistoryDetailModel: HandyJSON {
    var equipPartId: String?
    var pickingRemark: String?
    var faultCode: String?
    var dailyRecordCode: String?
    var equipPartName: String?
    var faultPhenomenon: String?
    var acceptanceStatus: String?
    var equipName: String?
    var equipId: String?
    var id: String?
    var maintainStartTime: String?
    var isPicking: String?
    var months: String?
    var createdTime: String?
    var useCompanyName: String?
    var useCompanyId: String?
    var useOrgName: String?
    var equipPartsId: String?
    var maintainCompleteTime: String?
    var faultFoundTime: String?
    var equipPartsName: String?
    var useOrgId: String?
    var years: String?
    static func historyDetailModels(_ model: [[String]]?, materials: [HistoryDetailModel]?) -> [[String]] {
        var results = [[String]]()
        
        let rowModels = ["时间", "工单号", "设备部位", "设备部件", "故障代码", "故障现象", "是否领用物资", "领用内容", "开工时间", "完工时间", "是否验收"]
        for (_ , rowModel) in rowModels.enumerated() {
            var models = [String]()
            models.append(rowModel)
            if let mater =  materials{
                for (_, materModel) in mater.enumerated() {
                    switch rowModel {
                    case "时间":
                        
                        models.append(String.timeFromFormatter("YYYY-MM-dd HH:mm", timeString: materModel.createdTime ?? ""))
                    case "工单号":
                        models.append(materModel.dailyRecordCode ?? "")
                    case "设备部位":
                        models.append(materModel.equipName ?? "")
                    case "设备部件":
                        models.append(materModel.equipPartsName ?? "")
                    case "故障代码":
                        models.append(materModel.faultCode ?? "")
                    case "故障现象":
                        models.append(materModel.faultPhenomenon ?? "")
                    case "是否领用物资":
                        models.append(materModel.isPicking ?? "" == "0" ? "否" : "是")
                    case "领用内容":
                        models.append(materModel.pickingRemark ?? "")
                    case "开工时间":
                        models.append(String.timeFromFormatter("YYYY-MM-dd HH:mm", timeString: materModel.maintainStartTime ?? ""))
                    case "完工时间":
                        models.append(String.timeFromFormatter("YYYY-MM-dd HH:mm", timeString: materModel.maintainCompleteTime ?? ""))
                    case "是否验收":
                        models.append(materModel.acceptanceStatus ?? "" == "0" ? "否" : "是")
                    default:
                        break
                    }
                }
            }
            results.append(models)
        }
        return results
    }
}
