//
//  EquipmntInfoVC.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/17.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

private let glt_iphoneX = (UIScreen.main.bounds.height >= 812.0)


class EquipmentPanoramicVC: BaseViewController, IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
         return itemInfo
     }
     
    var itemInfo = IndicatorInfo(title: "View123")
    
    var equipmentPanoramic: EquipmentPanoramic?
}

class EquipmentInfoVC: ButtonBarPagerTabStripViewController {
    
    var isReload = false
    var equipmentPanoramic: EquipmentPanoramic?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "设备履历"
        settings.style.buttonBarBackgroundColor = COLF4F4F4
        settings.style.buttonBarItemBackgroundColor = COLF4F4F4
        settings.style.buttonBarItemTitleColor = COL206EDD
        buttonBarView.backgroundColor = COLF4F4F4
        buttonBarView.selectedBarHeight = 1
        buttonBarView.selectedBar.backgroundColor = COL206EDD

    }
    
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let childViewControllers = [EquipmentInfoDetailVC.loadViewControllewWithNib,
                                    EquipmentHistoryVC.loadViewControllewWithNib,
                                    IndicatorViewController.loadViewControllewWithNib,
                                    EquCertificateVC.loadViewControllewWithNib,
                                    MaterialViewController.loadViewControllewWithNib
        ]
        let vcs = childViewControllers.map { (vc) -> EquipmentPanoramicVC in
            let vc = vc as! EquipmentPanoramicVC
            vc.equipmentPanoramic = self.equipmentPanoramic
            return vc
        }
        let itemInfos = [IndicatorInfo(title: "详细信息"), IndicatorInfo(title: "检修历史"), IndicatorInfo(title: "指标数据"), IndicatorInfo(title: "证书"), IndicatorInfo(title: "技术资料"), ]
        for (i, vc) in vcs.enumerated() {
            vc.itemInfo = itemInfos[i]
        }
        return vcs
    }
    
    override func reloadPagerTabStripView() {
        isReload = true
        if arc4random() % 2 == 0 {
            pagerBehaviour = .progressive(skipIntermediateViewControllers: arc4random() % 2 == 0, elasticIndicatorLimit: arc4random() % 2 == 0 )
        } else {
            pagerBehaviour = .common(skipIntermediateViewControllers: arc4random() % 2 == 0)
        }
        super.reloadPagerTabStripView()
    }
}
