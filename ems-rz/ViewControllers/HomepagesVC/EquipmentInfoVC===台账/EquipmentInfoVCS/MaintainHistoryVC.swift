//
//  MaintainHistoryVC.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/20.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class MaintainHistoryVC: EquipmentPanoramicVC, UICollectionViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var equSummaryView: EquSummaryView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var equSummaryViewHeight: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        self.equSummaryViewHeight = self.equSummaryView.frame.size.height + 15
        scrollView.frame = CGRect(x: 0, y: equSummaryViewHeight, width: KScreenWidth, height: KScreenHeight - equSummaryViewHeight)
        self.scrollView.contentSize = CGSize(width: KScreenWidth, height: KScreenHeight - equSummaryViewHeight)
        self.tableView.register(ChartTableViewCell.nib, forCellReuseIdentifier: ChartTableViewCell.reuseIdentifier)
    }
}

extension MaintainHistoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: ChartTableViewCell.reuseIdentifier, for: indexPath) as! ChartTableViewCell
        cell.chartCollectionView.imageArrays = ["sfae","asfe","ddk","sfae","asfe","ddk","sfae","asfe","ddk"]
        let collectViewWidth = cell.chartCollectionView.collectionView.collectionViewLayout.collectionViewContentSize.width
        if self.scrollView.contentSize.width != collectViewWidth {
            self.scrollView.contentSize = CGSize(width: collectViewWidth, height: KScreenHeight - self.equSummaryViewHeight)
            tableView.frame = CGRect(x: 0, y: 0, width: collectViewWidth, height: KScreenHeight - self.equSummaryViewHeight)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
}

