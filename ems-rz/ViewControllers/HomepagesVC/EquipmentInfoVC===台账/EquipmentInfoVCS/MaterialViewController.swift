//
//  MaterialViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/20.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class MaterialViewController: EquipmentPanoramicVC {

    @IBOutlet weak var tableView: UITableView!
    var technologyModels: [TechnologyModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.register(MaterialCell.nib, forCellReuseIdentifier: MaterialCell.reuseIdentifier)
        
        let model = self.equipmentPanoramic?.equipBaseInfoRespVo
        let parameter: [String : Any] = ["equipBigCategoryId" : model?.equipBigCategoryId ?? "", "equipMiddleCategoryId" : model?.equipMiddleCategoryId ?? "", "equipSmallCategoryId" : model?.equipSmallCategoryId ?? ""]
//        , "modelNumberId" : model?.modelNumber ?? ""
//        , "specificId" : model?.specificCode ?? ""
//        let parameter: [String : Any] = ["equipBigCategoryId" : model?.equipBigCategoryId ?? "", "equipMiddleCategoryId" : model?.equipMiddleCategoryId ?? "", "equipSmallCategoryId" : model?.equipSmallCategoryId ?? ""]

        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsTechnologyList), parameter: parameter , method: .get, headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
            self.technologyModels = [TechnologyModel].deserialize(from: result["data"]["records"].arrayObject) as? [TechnologyModel]
            self.tableView.reloadData()
        }) { (error) -> (Void) in
            
        }
    }
}

extension MaterialViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.technologyModels?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MaterialCell.reuseIdentifier, for: indexPath) as! MaterialCell
        cell.configMaterialCell(self.technologyModels?[indexPath.row])
        cell.showBtn.rx.tap.bind { [weak self] _ in
            if let vc = WebViewController.loadViewControllewWithNib as? WebViewController{
                if let url = self?.technologyModels?[indexPath.row].certifiPhotos?.first?.url {
                    vc.requestUrl = afBaseUrl + url
                    self?.show(vc, sender: nil)
                }
                
            }
        }.disposed(by: cell.rx.reuseBag)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
}
