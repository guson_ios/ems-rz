//
//  EquCertificateVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/10.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class EquCertificateVC: EquipmentPanoramicVC {

    @IBOutlet weak var tableView: UITableView!
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]? = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        self.tableView.register(PhotoShowTableViewCell.nib, forCellReuseIdentifier: PhotoShowTableViewCell.reuseIdentifier)
        tableViewViewModel = CertificateModel.certificateModels(self.equipmentPanoramic?.equipSpecialInfoOrCertificateRespVo)
       
        

    }
}


extension EquCertificateVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel?[section].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return (self.tableViewViewModel?[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            switch cell {
            case let cell as PhotoShowTableViewCell:
                
                cell.collectBgView.imageArrays = self?.equipmentPanoramic?.equipSpecialInfoOrCertificateRespVo?.equipSpecialInfoRespVo?.certifiPhotos?.map({ (certifiPhoto) -> UIImage? in
                    return UIImage.loadRemoteImage(afBaseUrl + (certifiPhoto.url ?? ""))
                })
                break
            default:
                break
            }
        }))!
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    
    
}
