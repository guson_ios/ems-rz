//
//  LedgeStatisticsVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/4/7.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
import Charts

extension BarChartView {

    private class BarChartFormatter: NSObject, IAxisValueFormatter {

        var labels: [String] = []

        func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            return labels[Int(value)]
        }

        init(labels: [String]) {
            super.init()
            self.labels = labels
        }
    }

    func setBarChartData(xValues: [String], yValues: [Double], label: String) {

        var dataEntries: [BarChartDataEntry] = []

        for i in 0..<yValues.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: yValues[i])
            dataEntries.append(dataEntry)
        }

        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "")

        let chartData = BarChartData(dataSet: chartDataSet)
        if yValues.count < 10 {
            chartData.barWidth = (1 + Double(yValues.count) * 0.1) / 16
        }
//        if (yValues.count > 1) {
//            let groupSpace = 0.48
//            let barSpace = 0.01
//            chartData.groupBars(fromX: 0.5, groupSpace: groupSpace, barSpace: barSpace)
//        }
        
        let chartFormatter = BarChartFormatter(labels: xValues)
        let xAxis = XAxis()
        xAxis.valueFormatter = chartFormatter
        self.xAxis.valueFormatter = xAxis.valueFormatter
        self.xAxis.labelCount = xValues.count

        self.data = chartData
    }
}

class LedgeStatisticsVC: BaseViewController {

    var columnChartModel = ColumnChartModel()
    let chartView = HorizontalBarChartView()
    var columnChartModelDicts = [String : [EquipColumnChartModel?]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "台账统计"
        
        chartView.chartDescription?.enabled = false
        chartView.scaleYEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.dragEnabled = false
        chartView.pinchZoomEnabled = false
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = true
        
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        xAxis.granularity = 1.0
        xAxis.spaceMin = 0.5
        xAxis.spaceMax = 0.5
        xAxis.granularityEnabled = true
        xAxis.granularity = 1
        
        
        chartView.rightAxis.enabled = true
        chartView.rightAxis.axisMinimum = 0
        chartView.rightAxis.granularity = 1.0
        
        chartView.leftAxis.axisMinimum = 0
        chartView.leftAxis.enabled = false
        self.view.addSubview(chartView)
        chartView.snp.makeConstraints { (maker) in
            maker.left.right.bottom.equalTo(self.view)
            maker.top.equalTo(self.view).offset(100)
        }
        
        self.getCommonOrg()
    }
}

extension LedgeStatisticsVC{
    
    func columnChartNetWorking() {
        
        let columnkey = (self.columnChartModel.useCompanyId ?? "") + (self.columnChartModel.useOrgId ?? "") + (self.columnChartModel.equipState ?? "")
        if self.columnChartModelDicts[columnkey] == nil {
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipColumnChart), parameter: self.columnChartModel.toJSON(), method: .get, success: { (result) -> (Void) in
                if let models = [EquipColumnChartModel].deserialize(from: result["data"].arrayObject){
                    self.chartView.setBarChartData(xValues: models.map({ (model) -> String in
                        model?.EQUIPSMALLCATEGORY ?? " "
                    }), yValues: models.map({ (model) -> Double in
                        model?.TOTAL ?? 0
                    }), label: "")
                    self.columnChartModelDicts[columnkey] = models
                }
            }) { (error) -> (Void) in
                
            }
        }else{
            if let models = self.columnChartModelDicts[columnkey]{
                self.chartView.setBarChartData(xValues: models.map({ (model) -> String in
                    model?.EQUIPSMALLCATEGORY ?? " "
                }), yValues: models.map({ (model) -> Double in
                    model?.TOTAL ?? 0
                }), label: "")
            }
        }
        
    }
    
    func getCommonOrg() {
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsCommonOrg), parameter: [ "useOrgName" : (EmsGlobalObj.obj.userInfo?.useOrgId)!, "useCompanyId" : (EmsGlobalObj.obj.userInfo?.useCompanyId)!], method: .get, headers: headersFunc(contentType: .json), success: { [weak self] (result) -> (Void) in
             var models = [CompanyModel].deserialize(from: result["data"].arrayObject)
            models?.insert(CompanyModel(id: nil, parentId: EmsGlobalObj.obj.userInfo?.useCompanyId, orgCode: nil, orgName: EmsGlobalObj.obj.userInfo?.useCompanyName), at: 0)
            self?.addMenuScrView(models: models)
           
        }) { (error) -> (Void) in
            
        }
    }
    
    func addMenuScrView(models: [CompanyModel?]?) {
        let menuScrView: MenuScrollerViewDef =  MenuScrollerViewDef(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 50), withTitleItems: models?.map({ (cModel) -> String in
            cModel?.orgName ?? " "
        }))!
        
        menuScrView.setMenuViewDidSelectedAtIndex { [weak self] (idx) in
            self?.columnChartModel.useCompanyId = models?[idx]?.parentId
            self?.columnChartModel.useOrgId = models?[idx]?.id
            self?.columnChartNetWorking()
        }
        menuScrView.selectedIndex = 0
        self.view.addSubview(menuScrView)

        let menuScrView1: MenuScrollerViewDef1 =  MenuScrollerViewDef1(frame: CGRect(x: 0, y: 50, width: KScreenWidth, height: 30), withTitleItems: EquStatus.equStatus().map({ (eModel) -> String in
            eModel.name ?? " "
        }))!
        menuScrView1.selectedIndex = 0
        menuScrView1.setMenuViewDidSelectedAtIndex { [weak self] (idx) in
            self?.columnChartModel.equipState = String(EquStatus.equStatus()[idx].start ?? 0)
            self?.columnChartNetWorking()
        }
        self.view.addSubview(menuScrView1)
    }
}




struct ColumnChartModel: HandyJSON {
    var useCompanyId: String?
    var useOrgId: String?
    var equipState: String? = "0"
}

struct EquipColumnChartModel: HandyJSON {
    var TOTAL: Double? = 0
    var EQUIPSMALLCATEGORY: String? = ""
}
