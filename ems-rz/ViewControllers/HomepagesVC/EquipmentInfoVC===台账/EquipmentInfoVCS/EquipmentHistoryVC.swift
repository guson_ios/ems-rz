//
//  EquipmentHistoryVC.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/20.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class EquipmentHistoryVC: EquipmentPanoramicVC {
    @IBOutlet weak var dateBtn: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var parameter: [String : String]?

    lazy var equOperatNumModels = EquOperatNumViewModel.equOperatNumViewModels(nil, monthWork: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(EquHistoryCell.nib, forCellWithReuseIdentifier: EquHistoryCell.reuseIdentifier)
        let layout = UICollectionViewFlowLayout()
        let width = (KScreenWidth - 36) / 3
        layout.itemSize = CGSize(width: width, height: width * 88 / 111)
        layout.minimumInteritemSpacing = 8;
        layout.minimumLineSpacing = 8;
        layout.sectionInset = UIEdgeInsets(top: 9, left: 8, bottom: 9, right: 8);
        layout.scrollDirection = .vertical
        self.collectionView.collectionViewLayout = layout
        let btnTitle = String.currentTimeFormatter("YYYY") + "年" + String.currentTimeFormatter("MM") + "月"
        self.dateBtn.setTitle(btnTitle, for: .normal)
        self.monthWorkData(String.currentTimeFormatter("YYYY"), month: String.currentTimeFormatter("MM"))
    }
    func monthWorkData(_ year: String, month: String) {
        let model = self.equipmentPanoramic?.equipBaseInfoRespVo
        self.parameter = ["equipId" : (model?.id)!, "years" : year, "months" : month]
        
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsGetOverhaulHistory), parameter: parameter, method: .get, success: { (result) -> (Void) in
            let monthWorkmodel = MonthWorkModel.deserialize(from: result["data"].dictionaryObject)
            self.equOperatNumModels = EquOperatNumViewModel.equOperatNumViewModels(nil, monthWork: monthWorkmodel)
            self.collectionView.reloadData()
        }) { (error) -> (Void) in
            
        }
    }
    
    @IBAction func timeSelectBtnClick(_ sender: Any) {
        self.confignDatePicker()
    }
}
extension EquipmentHistoryVC: PGDatePickerDelegate{
    func datePicker(_ datePicker: PGDatePicker!, didSelectDate dateComponents: DateComponents!) {
        print("dateComponents = ", dateComponents ?? "")
        if let year = dateComponents.year, let month = dateComponents.month {
            self.monthWorkData(String(year), month: String(month))
            let btnTitle = String(year) + "年" + String(month) + "月"
            self.dateBtn.setTitle(btnTitle, for: .normal)
        }
    }
}

extension EquipmentHistoryVC: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return equOperatNumModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EquHistoryCell.reuseIdentifier, for: indexPath) as! EquHistoryCell
        cell.equOperatNumView.confignEquOperatNumView(equOperatNumModels[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let historyUrl = self.equOperatNumModels[indexPath.row].historyUrl,  let vc = EquMateDetailViewController.loadViewControllewWithNib as? EquMateDetailViewController {
            vc.title = (self.equOperatNumModels[indexPath.row].operatText ?? "") + "记录"
            vc.historyUrl = historyUrl
            vc.parameter = self.parameter
            vc.equipmentPanoramic = self.equipmentPanoramic
            self.show(vc, sender: nil)
        }
    }
}

struct MonthWorkModel: HandyJSON {
    var maintainDailyTimes: String?
    var months: String?
    var maintainTimes: String?
    var useCompanyId: String?
    var useOrgId: String?
    var equipId: String?
    var majorMaintainTimes: String?
    var pointCheckTimes: String?
    var checkDailyTimes: String?
    var maintainInsuranceTimes: String?
    var useCompanyName: String?
    var pollingTimes: String?
    var useOrgName: String?
    var years: String?
}
