//
//  IndicatorViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/17.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class IndicatorViewController: EquipmentPanoramicVC {

    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var equipmentInfoModels: [EquipmentInfoDetail] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        let btnTitle = String.currentTimeFormatter("YYYY") + "年" + String.currentTimeFormatter("MM") + "月"
        self.dateBtn.setTitle(btnTitle, for: .normal)
        self.monthWorkData(String.currentTimeFormatter("YYYY"), month: String.currentTimeFormatter("MM"))
        
        self.equipmentInfoModels = EquipmentInfoDetail.equIndicatorViewModels(nil)
    }
    
    func monthWorkData(_ year: String, month: String) {
        let model = self.equipmentPanoramic?.equipBaseInfoRespVo
        let parameter = ["equipId" : (model?.id)!, "years" : year, "months" : month]
        
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipInfoIndicatorData), parameter: parameter, method: .get, success: { (result) -> (Void) in
            let model = IndicatorDataModel.deserialize(from: result["data"].dictionary)
            self.equipmentInfoModels = EquipmentInfoDetail.equIndicatorViewModels(model)
            self.tableView.reloadData()
        }) { (error) -> (Void) in
            
        }
    }
    
    @IBAction func timeSelectBtnClick(_ sender: Any) {
        self.confignDatePicker()
    }
}

extension IndicatorViewController: PGDatePickerDelegate{
    func datePicker(_ datePicker: PGDatePicker!, didSelectDate dateComponents: DateComponents!) {
        print("dateComponents = ", dateComponents ?? "")
        if let year = dateComponents.year, let month = dateComponents.month {
            self.monthWorkData(String(year), month: String(month))
            let btnTitle = String(year) + "年" + String(month) + "月"
            self.dateBtn.setTitle(btnTitle, for: .normal)
        }
    }
}
extension IndicatorViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.equipmentInfoModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UniversalTableViewCell = tableView.dequeueReusableCell(withIdentifier: UniversalTableViewCell.reuseIdentifier, for: indexPath) as! UniversalTableViewCell
        cell.confignUniversalCell(self.equipmentInfoModels[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}

struct IndicatorDataModel: HandyJSON {
    var useCompanyName: String?
    var faultRate: String?
    var years: String?
    var workHour: String?
    var maintainSettlementAmount: String?
    var faultHour: String?
    var noIntactHour: String?
    var useOrgName: String?
    var faultNumber: String?
    var shutdownHour: String?
    var utilizeRate: String?
    var maintenanceAmount: String?
    var calendarHour: String?
    var maintainAmount: String?
    var maintenanceSettlementAmount: String?
    var intactRate: String?
    var planMaintainHour: String?
    var months: String?
    var intactHour: String?
}
