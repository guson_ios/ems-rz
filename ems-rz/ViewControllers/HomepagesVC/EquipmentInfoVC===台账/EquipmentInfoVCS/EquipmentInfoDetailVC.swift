//
//  EquipmentInfoDetailVC.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/20.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import XLPagerTabStrip
class EquipmentInfoDetailVC: EquipmentPanoramicVC{
    
    @IBOutlet weak var mainTableView: UITableView!
    
    override var equipmentPanoramic: EquipmentPanoramic?  {
        
        didSet{
            self.equipmentInfoModels = EquipmentInfoDetail.equipmentInfoDetailViewModels(self.equipmentPanoramic?.equipBaseInfoRespVo, panoramic: self.equipmentPanoramic)
        }
    }
    
    var equipmentInfoModels: [EquipmentInfoDetail]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainTableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
    }
}


extension EquipmentInfoDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.equipmentInfoModels?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UniversalTableViewCell = tableView.dequeueReusableCell(withIdentifier: UniversalTableViewCell.reuseIdentifier, for: indexPath) as! UniversalTableViewCell
        cell.confignUniversalCell(self.equipmentInfoModels?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}
