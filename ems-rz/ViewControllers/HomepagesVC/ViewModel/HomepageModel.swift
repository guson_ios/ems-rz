//
//  HomepageModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/16.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation

struct CompanyModel: HandyJSON, SimplePickViewProtocol {
    func pickViewModelToString() -> String? {
        return self.orgName
    }
    
    var id: String?
    var parentId: String?
    var orgCode: String?
    var orgName: String?
}



class OperateItemModel    : NSObject, HandyJSON {
    
    class ChildrenItem    :NSObject, HandyJSON {
        var parentId: String?
        var createdUser: String?
        var funcType: String?
        var parentIds: String?
        var modifiedTime: String?
        var status: String?
        var funcCode: String?
        var id: String?
        var orderNo: String?
        var createdTime: String?
        var icon: String?
        var comment: String?
        var funcName: String?
        var modifiedUser: String?
        var children: [ChildrenItem]?
        required override init() {}
    }
    
    var id: String?
    var title: String?
    var modifiedUser: String?
    var funcName: String?
    var funcType: String?
    var createdUser: String?
    var parentId: String?
    var parentIds: String?
    var comment: String?
    var modifiedTime: String?
    var createdTime: String?
    var funcCode: String?
    var children: [ChildrenItem]?
    var status: String?
    var orderNo: String?
    required override init() {}

}

class LoginData    : NSObject, HandyJSON {
    var userAccount: String?
    var userName: String?
    var token: String?
    var menus: [OperateItemModel]?
    var permissions: String?
    var userOrgIds: String?
    required override init() {}
}

struct UserInfoModel    : HandyJSON {
    var useCompanyId: String?
    var useOrgName: String?
    var useOrgId: String?
    var useCompanyName: String?
}

open class EmsGlobalObj {
    
    var loginData: LoginData?
    var userInfo: UserInfoModel?
    public static var obj: EmsGlobalObj {
        struct Static {
            //Singleton instance. Initializing keyboard manger.
            static let obj = EmsGlobalObj()
        }
        /** @return Returns the default singleton instance. */
        return Static.obj
    }
}
