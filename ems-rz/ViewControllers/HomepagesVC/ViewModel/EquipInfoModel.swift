//
//  EquipBaseInfoRespVo.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/19.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
//MARK: - 搜索字段
class EquipBaseInfoSearch: NSObject, HandyJSON {
    var equipBigCategoryId: String?
    var equipSmallCategoryId: String?
    var equipMiddleCategoryId: String?
    var equipName: String?
    var modelNumber: String?
    var equipSystemCode: String?
    var equipCode: String?
    var specificCode: String?
    var equipTechState: String?
    var useOrgId: String?
    var companyId: String?
    var equipState: String?
    var keywords: String?
    required override init() {}
}

class EquipBaseInfoRespVo    : NSObject, HandyJSON {
    var equipWorkTypeName: String?
    var equipSmallCategoryId: String?
    var isParticular: String?
    var equipBigCategoryId: String?
    var equipTechStateName: String?
    var equipTechState: String?
    var insuranceDate: String?
    var equipStateName: String?
    var equipBigCategoryName: String?
    var equipSystemCode: String?
    var isParticularName: String?
    var equipMiddleCategoryName: String?
    var responsiCode: String?
    var specificCode: String?
    var companyId: String?
    var unitName: String?
    var id: String?///设备ID
    var equipName: String?
    var equipSmallCategoryName: String?
    var equipNameId: String?
    var equipState: String?
    var unit: String?
    var equipMiddleCategoryId: String?
    var useOrgId: String?
    var useOrgName: String?
    var modelNumber: String?
    var equipCode: String?
    var companyName: String?
    var equipWorkType: String?
    var principal: String?
    var useCompanyName: String?
    var productionCode: String?
    var manufacturer: String?
    var enginePower: String?
    var equipUseDate: String?
    var equipWeight: String?
    required override init() {}
}

struct EquipInfoData: HandyJSON {
    var records: [EquipBaseInfoRespVo]?
    var total: CGFloat = 0.0
    var size: CGFloat = 0.0
    var current: CGFloat = 0.0
    var searchCount: Bool?
    var pages: CGFloat = 0.0
}
//MARK: - EquipmentInfoDetail
struct EquipmentInfoDetail: ViewModelOperate {
    
    var infoKey: String?
    var infoValue: String?
    var cellStatus: Bool? = false///一个Bool状态
    var cellType: UniversalCellType? = .imageHide
    var cellReuseIdentifier: String = UniversalTableViewCell.reuseIdentifier///默认cell

    static func equipmentInfoDetailViewModels(_ model: EquipBaseInfoRespVo?, panoramic: EquipmentPanoramic?) -> [EquipmentInfoDetail]{
        return [
            EquipmentInfoDetail(infoKey: "设备大类", infoValue: model?.equipBigCategoryName),
            EquipmentInfoDetail(infoKey: "设备中类", infoValue: model?.equipMiddleCategoryName),
            EquipmentInfoDetail(infoKey: "设备小类", infoValue: model?.equipSmallCategoryName),
            EquipmentInfoDetail(infoKey: "设备名称", infoValue: model?.equipName),
            EquipmentInfoDetail(infoKey: "设备编号", infoValue: model?.equipCode),
            EquipmentInfoDetail(infoKey: "设备系统编号", infoValue: model?.equipSystemCode),
            EquipmentInfoDetail(infoKey: "设备型号", infoValue: model?.modelNumber),
            EquipmentInfoDetail(infoKey: "设备规格", infoValue: model?.specificCode),
            EquipmentInfoDetail(infoKey: "设备技术状况", infoValue: model?.equipTechStateName),
            EquipmentInfoDetail(infoKey: "设备使用状态", infoValue: model?.equipStateName),
            EquipmentInfoDetail(infoKey: "计量单位", infoValue: model?.unitName),
            EquipmentInfoDetail(infoKey: "保险期限", infoValue: model?.insuranceDate),
            EquipmentInfoDetail(infoKey: "设备所属单位", infoValue: model?.companyName),
            
            EquipmentInfoDetail(infoKey: "使用部门", infoValue: model?.useOrgName),
            EquipmentInfoDetail(infoKey: "负责人", infoValue: model?.principal),
            
            EquipmentInfoDetail(infoKey: "设备原值", infoValue: panoramic?.equipFinanceInfoOrSupplyInfoRespVo?.equipFinanceInfoRespVo?.price),
        ]
    }
    
    static func maintainDetailViewModels(_ model: EquipBaseInfoRespVo?) -> [EquipmentInfoDetail]{
        return [
            EquipmentInfoDetail(infoKey: "保养内容", infoValue: model?.equipName),
            EquipmentInfoDetail(infoKey: "技术要求 ", infoValue: model?.equipCode),
            EquipmentInfoDetail(infoKey: "设备系统编号", infoValue: model?.equipSystemCode),
            EquipmentInfoDetail(infoKey: "维修项目 ", infoValue: model?.unit),
            EquipmentInfoDetail(infoKey: "故障代码", infoValue: model?.companyName),
            EquipmentInfoDetail(infoKey: "故障现象", infoValue: model?.equipBigCategoryName),
        ]
    }
}


// MARK: - 设备台账
class EquipmentPanoramic: NSObject, HandyJSON {
    var equipBaseInfoRespVo: EquipBaseInfoRespVo?
    var equipFinanceInfoOrSupplyInfoRespVo: EquipFinanceInfoOrSupplyInfoRespVo?
    var equipSpecialInfoOrCertificateRespVo: EquipSpecialInfoOrCertificateRespVo?
    var equipFileRespVo: EquipFileRespVo?
    
    required override init() {}
}

class EquipFinanceInfoOrSupplyInfoRespVo    : NSObject, HandyJSON{
    class EquipFinanceInfoRespVo    : NSObject, HandyJSON {
        var netValue: String?
        var id: String?
        var alreadyLimit: String?
        var depreciationLimit: String?
        var price: String?
        var financeDebtCase: String?
        var equipAssetsCode: String?
        var equipId: String?
        
        required override init() {}
    }
    
    class EquipSupplyInfoRespVo    : NSObject, HandyJSON {
        var enginePower: String?
        var emissionStandard: String?
        var equipBuyDate: String?
        var equipUseDate: String?
        var manufacturer: String?
        var equipId: String?
        var equipWeight: String?
        var id: String?
        var supplyCompany: String?
        var productionCode: String?
        required override init() {}
    }

    var equipFinanceInfoRespVo: EquipFinanceInfoRespVo?
    var equipSupplyInfoRespVo: EquipSupplyInfoRespVo?
    required override init() {}
}



class EquipInsuranceCertificateRespVo    : NSObject, HandyJSON {
    required override init() {}
}

class EquipSpecialInfoOrCertificateRespVo    : NSObject, HandyJSON {
    
    class CertifiPhoto    : NSObject, HandyJSON {
        var typeName: String?
        var id: Int = 0
        var url: String?
        var name: String?
        required override init() {}
    }
    
    class EquipSpecialInfoRespVo    : NSObject, HandyJSON {
        var certifiTypeName: String?
        var certifiCode: String?
        var certifiPhotos: [CertifiPhoto]?
        var certifiUser: String?
        var certifiStateName: String?
        var companyName: String?
        var certifiState: String?
        var equipCode: String?
        var particularRegistrationCode: String?
        var certifiType: String?
        var equipSmallCategoryName: String?
        var validDate: String?
        var useOrgName: String?
        var equipId: String?
        var expireDate: String?
        var remark: String?
        var specialDiscoverCycle: String?
        var equipName: String?
        var equipSystemCode: String?
        var releaseDate: String?
        required override init() {}
    }
    var equipSpecialInfoRespVo: EquipSpecialInfoRespVo?
    var equipInsuranceCertificateRespVo: EquipInsuranceCertificateRespVo?
    required override init() {}

}


class EquipFileRespVo    : NSObject, HandyJSON {
    class PanoramicPhotos    : NSObject, HandyJSON {
        var id: String?
        var typeName: String?
        var url: String?
        var name: String?
        required override init() {}
    }
    var positionPhotos: PanoramicPhotos?
    var panoramicPhotos: PanoramicPhotos?
    var adjunctPhotos: PanoramicPhotos?
    required override init() {}
}

