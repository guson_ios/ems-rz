//
//  ScanRepairViewModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/14.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation
import UIKit
typealias CollectionViewViewModelClosure<T> = (_ model: T?, _ cell: UICollectionViewCell) -> Void
typealias TableViewViewModelClosure<T> = (_ model: T?, _ cell: UITableViewCell) -> Void

protocol TableViewCellViewModel {
    associatedtype T
    func collectionViewViewModel(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, cellClosure: @escaping CollectionViewViewModelClosure<T>) -> UICollectionViewCell
    
    func tableViewViewModel(_ tableView: UITableView, cellForItemAt indexPath: IndexPath, cellClosure: @escaping TableViewViewModelClosure<T>) -> UITableViewCell
}

///cell种类较多时候比较合适-------哪种模式该写的还是不会少
struct TableViewViewModel<T>: TableViewCellViewModel {
  
    var baseViewModel: BaseViewModel<T>
    ///
    func tableViewViewModel(_ tableView: UITableView, cellForItemAt indexPath: IndexPath, cellClosure: @escaping (T?, UITableViewCell) -> Void) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.baseViewModel.cellReuseIdentifier, for: indexPath)
        switch (baseViewModel.cellReuseIdentifier, baseViewModel.model, cell) {
        case  (LedgerChartCell.reuseIdentifier, let model as EquOperatNumShowModel, let cell as LedgerChartCell):
            cell.ledgerChartCollection.imageArrays = model.equOperatNumViewModel
            break
        // MARK: - RecordTableViewCell
        case  (RecordTableViewCell.reuseIdentifier, let model as RecordShowModel, let cell as RecordTableViewCell):
            cell.photoShowView.imageArrays = model.imageArrays
            cell.photoShowView.isHidden = model.imageArrays?.count ?? 0 > 0 ? false : true
            cell.recordLeading.constant = (model.recordData?.count ?? 0 <= 0) ? -82 : 2
            cell.recordView.recordData = model.recordData
            cell.recordView.delBtn.isHidden = true
            break
            
            
       // MARK: - PhotoSelectShowCell
        case  (PhotoSelectShowCell.reuseIdentifier, _ as Any, _ as PhotoSelectShowCell):
//                    cell.photoSelectShowView.imageArrays = ["fse", "afe", "fawef", "fse", ]
            break
        case  (PhotoShowTableViewCell.reuseIdentifier, let model as PhotoShowModel, let cell as PhotoShowTableViewCell):
            cell.collectBgView.imageArrays = model.imageArrays
            break
            
        // MARK: - RepairUnTableViewCell
        case  (RepairUnTableViewCell.reuseIdentifier, let model as  EquipmentInfoDetail, let cell as RepairUnTableViewCell):
            cell.confignRepairUNCell(model)
            
        // MARK: - UniversalTableViewCell
        case  (UniversalTableViewCell.reuseIdentifier, let model as  EquipmentInfoDetail, let cell as UniversalTableViewCell):
            cell.confignUniversalCell(model)
        case  (UniversalTableViewCell.reuseIdentifier, let model as  PartInfoModel, let cell as UniversalTableViewCell):
            cell.confignUniversalCell(model)
        case  (UniversalTableViewCell.reuseIdentifier, let model as  SpectionPartModel, let cell as UniversalTableViewCell):
            cell.confignUniversalCell(model)
        case  (UniversalTableViewCell.reuseIdentifier, let model as  NewMaintainPartModel, let cell as UniversalTableViewCell):
            cell.confignUniversalCell(model)
        case  (UniversalTableViewCell.reuseIdentifier, let model as CheckAppAlreadyDetail, let cell as UniversalTableViewCell):///已检
            cell.confignUniversalCell(model)
            
            
            
        case  (TextFieldTableViewCell.reuseIdentifier, let model as EquipmentInfoDetail, let cell as TextFieldTableViewCell):
            cell.confignTextFieldCell(model)
        case  (SwitchTableViewCell.reuseIdentifier, let model as EquipmentInfoDetail, let cell as SwitchTableViewCell):
            cell.confignWsitchCell(model)
       // MARK: - EquSummaryCell
        case  (EquSummaryCell.reuseIdentifier, let model as MaintainRecordModel, let cell as EquSummaryCell):///点检
            cell.confignSummaryCell(model)
         case  (EquSummaryCell.reuseIdentifier, let model as EquipInfoScanModel, let cell as EquSummaryCell):///基础数据请求
            cell.confignSummaryCell(model)
        case (EquSummaryCell.reuseIdentifier, let model as AcceptanceListModel, let cell as EquSummaryCell):///保养验收
            cell.confignSummaryCell(model)
            
        // MARK: - MaintainTableViewCell
        case  (MaintainTableViewCell.reuseIdentifier, let model as PositionModel, let cell as MaintainTableViewCell):///点检
            cell.confignMaintainCell(model)
        case  (MaintainTableViewCell.reuseIdentifier, let model as SpectionRecordModel, let cell as MaintainTableViewCell):///巡检
            cell.confignSpectionCell(model)
        case  (MaintainTableViewCell.reuseIdentifier, let model as DailyInspectionModel, let cell as MaintainTableViewCell):///日检
            cell.confignDailyCell(model)
         case  (MaintainTableViewCell.reuseIdentifier, let model as CheckAppAlreadyDetail, let cell as MaintainTableViewCell):///已检
            cell.confignDailyCell(model)
            
        // MARK: - NextGrowingCell
        case  (NextGrowingCell.reuseIdentifier, let model as EquipmentInfoDetail, let cell as NextGrowingCell):///已检
            cell.confingNextGrowingCell(model)
        default:
            break
        }

        cellClosure(self.baseViewModel.model, cell)
        return cell

    }
    
    func collectionViewViewModel(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, cellClosure: @escaping (T?, UICollectionViewCell) -> Void) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.baseViewModel.cellReuseIdentifier, for: indexPath)
        switch (baseViewModel.cellReuseIdentifier, baseViewModel.model, cell) {
        case (FunctionCollectionViewCell.reuseIdentifier, let model as FunctionViewModel, let cell as FunctionCollectionViewCell):
            if #available(iOS 12.0, *) {
                cell.configCell(indexPath.row < 4 ? cell.topContraint : cell.bottomContraint, model: model)
            }else{
                cell.configCell(cell.topContraint, model: model)
            }
            
        case (RepairCollectionViewCell.reuseIdentifier, let model as RepairListModel, let cell as RepairCollectionViewCell):
            cell.confingRepairCell(model)
        default:
            break
        }
        cellClosure(self.baseViewModel.model, cell)
        return cell
    }
}

struct BaseViewModel<T>{
    var model: T
    var size: CGSize = CGSize(width: 100, height: 100)
    let cellReuseIdentifier: String
}
