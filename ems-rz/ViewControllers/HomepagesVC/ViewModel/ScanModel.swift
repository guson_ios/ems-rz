//
//  ScanModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/13.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

struct ScanModel: HandyJSON {
    var equipId: String?
    var equipBigCategoryId: String?
    var equipMiddleCategoryId: String?
    var equipPositionId: String?
    var equipSmallCategoryId: String?
}
