//
//  DatePickerViewModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/17.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation
import UIKit
import PGDatePicker

extension UIViewController: PGPickerViewDelegate{
    func confignDatePicker(){
        let datePickerManager = PGDatePickManager()
        datePickerManager.isShadeBackground = true
        datePickerManager.style = .alertBottomButton
        let datePicker = datePickerManager.datePicker!
        datePicker.delegate = self as? PGDatePickerDelegate
        datePicker.isHiddenMiddleText = false
        datePicker.showUnit = .none
        datePicker.datePickerMode = .yearAndMonth
        self.present(datePickerManager, animated: false, completion: nil)
    }
}
