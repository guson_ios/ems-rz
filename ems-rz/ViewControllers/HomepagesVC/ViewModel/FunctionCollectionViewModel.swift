//
//  FunctionCollectionViewModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/14.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation
import UIKit

typealias ViewModelOperateClosure<T> = (_ result: T?) -> Void
///may be no use
protocol ViewModelOperate {
    func functionOperate<T>(_ operate :  @escaping ViewModelOperateClosure<T>) -> Void
}
extension ViewModelOperate{
    func functionOperate<T>(_ operate : @escaping ViewModelOperateClosure<T>) -> Void{
    
    }
}

struct HomepageModel {
    
    static func homepageViewModels(_ repairModels: [RepairListModel]?, _ repairNorModels: [RepairListModel]?, _ bModel: BacklogListModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        let models = FunctionViewModel.functionViewModels(bModel).map { (model) -> TableViewViewModel<ViewModelOperate> in
            
            return TableViewViewModel(baseViewModel: BaseViewModel(model: model, size: CGSize(width: KScreenWidth / 4.0, height: 160 / 2.0), cellReuseIdentifier: FunctionCollectionViewCell.reuseIdentifier))
        }
        let banners: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: BannerViewModel(), size: CGSize(width: KScreenWidth, height: 190 * KScaleW), cellReuseIdentifier: BannerCollectionViewCell.reuseIdentifier))]
        tableViewViewModel.append(banners)
        tableViewViewModel.append(models)
        
        
        var repairViewModels: [TableViewViewModel<ViewModelOperate>] = []
        if repairModels != nil {
            for (_, repairModel) in (repairModels?.enumerated())! {
                repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: repairModel, size: CGSize(width: (KScreenWidth / 2.0 - 10), height: 97), cellReuseIdentifier: RepairCollectionViewCell.reuseIdentifier)))
            }
        }
        let repairModelsCount = repairModels?.count ?? 0
        if repairModelsCount % 2 != 0 || repairModelsCount == 0 {
            let width = repairModelsCount == 0 ? (KScreenWidth - 10) : (KScreenWidth / 2.0 - 10)
            repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: RepairNormalViewModel(), size: CGSize(width: width, height: 97), cellReuseIdentifier: RepairNormalCollectionViewCell.reuseIdentifier)))
        }
        tableViewViewModel.append(repairViewModels)
        
        
        var repairNorViewModels: [TableViewViewModel<ViewModelOperate>] = []
        if repairNorModels != nil {
            for (_, repairModel) in (repairNorModels?.enumerated())! {
                repairNorViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: repairModel, size: CGSize(width: (KScreenWidth / 2.0 - 10), height: 97), cellReuseIdentifier: RepairCollectionViewCell.reuseIdentifier)))
            }
        }
        let repairNorModelsCount = repairNorModels?.count ?? 0
        if repairNorModelsCount % 2 != 0 || repairNorModelsCount == 0 {
            let width = repairNorModelsCount == 0 ? (KScreenWidth - 10) : (KScreenWidth / 2.0 - 10)
            repairNorViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: RepairNormalViewModel(), size: CGSize(width: width, height: 97), cellReuseIdentifier: RepairNormalCollectionViewCell.reuseIdentifier)))
        }
        tableViewViewModel.append(repairNorViewModels)
        return tableViewViewModel
    }
}
struct RepairViewModel: ViewModelOperate {
    
}

struct RepairNormalViewModel: ViewModelOperate {
    
}

struct BannerViewModel: ViewModelOperate {
}

struct SectionBadgeModel {
    var title: String?
    var num = 0
}
struct BacklogListModel: HandyJSON {
    var planMaintainNum: Int = 0
    var acceptanceNum: Int = 0
    var planCheckNum: Int = 0
    var repairNum: Int = 0
    var repairNum1: Int = 0
}
struct FunctionViewModel: ViewModelOperate {

    enum name: String {
        case scanOperate = "扫码作业"
        case spotCheck = "点检"
        case tourOperate = "巡检"
        case maintain = "润滑保养"
        case repair = "报修"
        case acceptance = "验收"
        case scanInquire = "扫码查询"
        case tool = "工属具"
    }
    let operateName: name
    let imageName: String
    let norImageName: String
    let opModel: OperateItemModel.ChildrenItem?
    var badgeNum: Int? = 0
    
    
    static func functionViewModels(_ bModel: BacklogListModel?) -> [FunctionViewModel] {
        let model = EmsGlobalObj.obj.loginData?.menus?.filter({$0.funcCode == "appMgmt"}).first?.children?.filter({$0.funcCode == "appHome"}).first?.children
        return [
            FunctionViewModel(operateName: .scanOperate, imageName: "icon_saoma_nor", norImageName: "icon_saoma_pre", opModel: model?.filter({$0.funcCode == "scanWork"}).first),
            FunctionViewModel(operateName: .spotCheck, imageName: "icon_dianjian_nor", norImageName: "icon_dianjian_pre", opModel: model?.filter({$0.funcCode == "pointCheck"}).first, badgeNum: bModel?.planCheckNum),
            FunctionViewModel(operateName: .tourOperate, imageName: "icon_xunjian_nor", norImageName: "icon_xunjian_pre", opModel: model?.filter({$0.funcCode == "patrolCheck"}).first),
            FunctionViewModel(operateName: .maintain, imageName: "icon_baoyang_nor", norImageName: "icon_baoyang_pre", opModel: model?.filter({$0.funcCode == "maintainLubrication"}).first, badgeNum: bModel?.planMaintainNum),
            FunctionViewModel(operateName: .repair, imageName: "icon_baoxiu_nor", norImageName: "icon_baoxiu_pre", opModel: model?.filter({$0.funcCode == "faultRepair"}).first),
            FunctionViewModel(operateName: .acceptance, imageName: "icon_yanshou_nor", norImageName: "icon_yanshou_pre", opModel: model?.filter({$0.funcCode == "acceptance"}).first, badgeNum: bModel?.acceptanceNum),
            FunctionViewModel(operateName: .scanInquire, imageName: "icon_saomachaxun_nor", norImageName: "icon_saomachaxun_pre", opModel: model?.filter({$0.funcCode == "scanCheck"}).first),
            FunctionViewModel(operateName: .tool, imageName: "icon_gongshuju_nor", norImageName: "icon_gongshuju_pre", opModel: model?.filter({$0.funcCode == "toolManage"}).first),
        ]
    }
    ///鸡肋
    func functionOperate<T>(_ operate: @escaping (T?) -> Void) {
        switch self.operateName {
        case .scanOperate:
            operate(self as? T)
        case .acceptance:
            operate(self as? T)
        default:
            operate(self as? T)
            break
        }
        
    }
}
