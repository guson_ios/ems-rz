//
//  UniversalCellViewModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/4.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation
//MARK: - EquipmentInfoDetail
struct UniversalCellViewModel: ViewModelOperate {
    
    var infoKey: String?
    var infoValue: String?
    var cellReuseIdentifier: String = UniversalTableViewCell.reuseIdentifier
    
    var cellStatus: Bool? = false///一个Bool状态
    var cellType: UniversalCellType? = .imageHide

}
