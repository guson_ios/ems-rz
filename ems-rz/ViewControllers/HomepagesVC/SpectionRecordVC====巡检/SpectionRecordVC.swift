//
//  SpectionRecordVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/15.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class SpectionRecordVC: BaseViewController {

    @IBOutlet weak var bottomTwoView: BottomTwoBtnView!
    @IBOutlet weak var tableView: UITableView!
    var scanModel: ScanModel?
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    var spectionRecordPlan: String?
    var spectionRecordModels: [SpectionRecordModel]?
    var equipInfoScanModel: EquipInfoScanModel?

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "巡检"
        self.tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)
        self.tableView.register(MaintainTableViewCell.nib, forCellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        self.spectionRecordPlanSave()
        self.getEquipInfoBaseId()
        
        self.bottomTwoView.leftBtn.rx.tap.bind { [weak self] _  in
            let vc = ScanViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.scanResultDelegate = self
            self?.show(vc, sender: nil)
            vc.hidesBottomBarWhenPushed = true
        }.disposed(by: disposeBag)
        
        self.bottomTwoView.rightBtn.rx.tap.bind { [weak self] _ -> Void in
           
            if let positionModels = self?.spectionRecordModels{
                var num = 0
                var faultNum = 0
                var standardCheckIds: [String] = []//检查标准数组
                for (_, positionModel) in positionModels.enumerated() {//部位
                    if let planId = self?.spectionRecordPlan, let positionId = positionModel.equipPositionId{
                        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                        if Defaults[key: defPositionModels] != nil {
                            num += 1
                            let pm = [SpectionPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [SpectionPartModel]
                            if let checkParts = pm {//本地保存报修标准 部位
                                for (_, checkPart) in checkParts.enumerated() {///部件
                                    if let standardChecks = checkPart.standardCheckRespVoList {
                                        for (_, standardCheck) in standardChecks.enumerated() {///报修标准
                                            if standardCheck.isCheck!{
                                                faultNum += 1
                                            }
                                            if let standardCheckID = standardCheck.id {
                                                standardCheckIds.append(standardCheckID)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                let parameter = ["id" : (self?.spectionRecordPlan)!, "faultNumber" : String(faultNum), "isRepair" : faultNum == 0 ? "0" : "1", "standardCheckIds" : standardCheckIds] as [String : Any]
                print(parameter.dictToJsonString())
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanInspectionRecordUpdate), parameter: nil, method: .post, encoding: parameter.dictToJsonString(), headers: headersFunc(contentType: .json), progressPlugin: self, success: { (result) -> (Void) in
                    for (_, positionModel) in positionModels.enumerated() {//部位
                        if let planId = self?.spectionRecordPlan, let positionId = positionModel.equipPositionId{
                            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                            Defaults.remove(defPositionModels)///提交后移除
                        }
                    }
                    ZFToast.show(withMessage: "提交成功")
                    self?.navigationController?.popViewController(animated: true)
                }) { (error) -> (Void) in

                }
            }
        }.disposed(by: disposeBag)
    }
}

extension SpectionRecordVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return  self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (operate, cell) in
            switch (operate, cell) {
            case (let model as SpectionRecordModel, let cell as MaintainTableViewCell):
                cell.rightBtn.isHidden = true
                if let planId = self?.spectionRecordPlan, let positionId = model.equipPositionId{
                    let isHidden = !(self?.searchDefData(planId, positionId: positionId) ?? false)
                    cell.rightBtn.isHidden = isHidden
                    cell.leftBtn.isHidden = isHidden
                    cell.rightBtn.rx.tap.bind { [weak self] _  in
                        self?.pushMaintainDetailVC(planId, equipPositionId: positionId)
                    }.disposed(by: cell.rx.reuseBag)
                    cell.leftBtn.rx.tap.bind { [weak self] _ in
                       
                        if model.spectionParts == nil{
                            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                            if
                                let positionModel =  [SpectionPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [SpectionPartModel]{
                                model.spectionParts = positionModel
                                self?.spectionRecordModels?[indexPath.section - 1] = model
                                if let vModel = SpectionRecordViewModel.spectionRecordModels(self?.spectionRecordModels, equipInfoScanModel: self?.equipInfoScanModel){
                                    self?.tableViewViewModel = vModel
                                    self?.tableView.reloadData()
                                }
                            }
                        }else{
                            model.spectionParts = nil
                            self?.spectionRecordModels?[indexPath.section - 1] = model
                            if let vModel = SpectionRecordViewModel.spectionRecordModels(self?.spectionRecordModels, equipInfoScanModel: self?.equipInfoScanModel){
                                self?.tableViewViewModel = vModel
                                self?.tableView.reloadData()
                            }
                        }
                    }.disposed(by: cell.rx.reuseBag)
                }
                
            default:
                break
            }
            
        })
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 20 : 0.001
    }
}

extension SpectionRecordVC: LBXScanViewControllerDelegate{
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        print(scanResult.strScanned as Any)
        if let scanStr = scanResult.strScanned {
            
            let dict = String.stringToDict(scanStr)
            let model = ScanModel.deserialize(from: dict)
            if let positionModels = self.spectionRecordModels {
                for (_, positionModel) in positionModels.enumerated() {
                    if model?.equipPositionId == positionModel.equipPositionId {
                        if let planId = self.spectionRecordPlan{
                            self.pushMaintainDetailVC(planId, equipPositionId: positionModel.equipPositionId!)
                        }else{
                            self.spectionRecordPlanSave()
                            ZFToast.show(withMessage: "生成计划失败")
                        }
                        return
                    }
                }
            }
            ZFToast.show(withMessage: "二维码不匹配")
        }
    }
}
extension SpectionRecordVC{
    // MARK: - 获取部位
    func planInspectionByType() {
        var dict = scanModel?.toJSON()
        dict?.removeValue(forKey: "equipId")
        dict?.removeValue(forKey: "equipPositionId")
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanInspectionByType), parameter: dict, method: .get, success: { (result) -> (Void) in
            let models = [SpectionRecordModel].deserialize(from: result["data"].arrayObject) as? [SpectionRecordModel]
            self.spectionRecordModels = models
            if let vModel = SpectionRecordViewModel.spectionRecordModels(models, equipInfoScanModel: self.equipInfoScanModel){
                self.tableViewViewModel = vModel
                self.tableView.reloadData()
            }
        }) { (error) -> (Void) in
            
        }
    }
    // MARK: - 设备信息
    func getEquipInfoBaseId() {
           if let equipId = self.scanModel?.equipId  {
               AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipInfoBaseId + equipId), parameter: nil, method: .get, encoding: URLEncoding.httpBody, success: { (result) -> (Void) in
                self.equipInfoScanModel = EquipInfoScanModel.deserialize(from: result["data"].dictionaryObject)
                if self.scanModel?.equipSmallCategoryId == nil{
                    self.scanModel?.equipSmallCategoryId = self.equipInfoScanModel?.equipSmallCategoryId
                    self.scanModel?.equipMiddleCategoryId = self.equipInfoScanModel?.equipMiddleCategoryId
                    self.scanModel?.equipSmallCategoryId = self.equipInfoScanModel?.equipSmallCategoryId
                }
                self.planInspectionByType()
                if let vModel = SpectionRecordViewModel.spectionRecordModels(self.spectionRecordModels, equipInfoScanModel: self.equipInfoScanModel){
                    self.tableViewViewModel = vModel
                    self.tableView.reloadData()
                }
               }) { (error) -> (Void) in

               }
           }
    }
    // MARK: - 生成巡检记录
    func spectionRecordPlanSave(){
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanInspectionRecordSave), parameter: ["equipInfoId" : scanModel?.equipId ?? ""], method: .post, encoding: URLEncoding.httpBody,  success: { (result) -> (Void) in
            self.spectionRecordPlan =  result["data"].stringValue
        }) { (error) -> (Void) in
            
        }
    }
    // MARK: - 本地
    func searchDefData(_ planId: String ,  positionId: String) -> Bool {
        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
        if Defaults[key: defPositionModels] != nil {
            return true
        }
        return false
    }
    // MARK: - 详情
    func pushMaintainDetailVC(_ planId: String, equipPositionId: String) {
        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + equipPositionId, defaultValue: nil)
        if  Defaults[key: defPositionModels] != nil {
            if let vc = SpectionRecordDetailVC.loadViewControllewWithNib as? SpectionRecordDetailVC {
                vc.spectionPartModel = [SpectionPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [SpectionPartModel]
                vc.scanModel = self.scanModel
                vc.repairSourceEmun = .巡检
                vc.spectionRecordPlan = self.spectionRecordPlan
                self.show(vc, sender: nil)
            }
        }else{
            var dict = scanModel?.toJSON()
            dict?.removeValue(forKey: "equipId")
            dict?.updateValue(equipPositionId, forKey: "equipPositionId")
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanInspectionByTypeAndPos), parameter: dict, method: .get, success: { (result) -> (Void) in
                let models = [SpectionPartModel].deserialize(from: result["data"].arrayObject) as? [SpectionPartModel]
                if let vc = SpectionRecordDetailVC.loadViewControllewWithNib as? SpectionRecordDetailVC{
                    vc.spectionPartModel = models
                    vc.scanModel = self.scanModel
                    vc.repairSourceEmun = .巡检
                    vc.spectionRecordPlan = self.spectionRecordPlan
                    self.show(vc, sender: nil)
                }
            }) { (error) -> (Void) in
                
            }
        }
    }
}
