//
//  SpectionRecordDetailVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/15.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class SpectionRecordDetailVC: BaseViewController {
    @IBOutlet weak var bottomBtnView: BottomBtnView!
    
    @IBOutlet weak var tableView: UITableView!
    var spectionPartModel: [SpectionPartModel]?
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]? = []
    var spectionRecordPlan: String?
    var scanModel: ScanModel?
    
    var equipPositionName: String?
    var repairSourceEmun: RepaireModel.repairSourceEmun?
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        self.tableView.register(SwitchTableViewCell.nib, forCellReuseIdentifier: SwitchTableViewCell.reuseIdentifier)
        self.tableViewViewModel = SpectionRecordViewModel.spectionDetailModels(spectionPartModel)
        self.bottomBtnView.btn.setTitle("保存", for: .normal)
        self.bottomBtnView.btn.rx.tap.bind { [weak self] _  in
            self?.defSave()
            self?.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
    }
    func defSave() {
        if let planId = self.spectionRecordPlan, let positionId = self.spectionPartModel?.first?.standardCheckRespVoList?.first?.equipPositionId{
            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
            Defaults[key: defPositionModels] = self.spectionPartModel?.toJSON() as? [[String : Any]]
        }
    }
}

extension SpectionRecordDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel?[section].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return (self.tableViewViewModel?[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            switch cell {
                
            case let cell as SwitchTableViewCell:
                var standardModel = self?.spectionPartModel?[indexPath.section].standardCheckRespVoList?[(indexPath.row / 5) - 1]
                let spectionPartModel = self?.spectionPartModel?[indexPath.section]
                if standardModel?.isCheck ?? false {
                    cell.rightSwitch.isUserInteractionEnabled = false
                    cell.rightSwitch.isOn = true
                    spectionPartModel?.intact = false
                }else{
                    cell.rightSwitch.rx.isOn.bind { [weak self, weak cell] (isOn) -> Void in
                        if isOn{
                            cell?.rightSwitch.isOn = false

                            let vc = FaultRepairViewController.loadViewControllewWithNib as! FaultRepairViewController
                            var equipRepairModel = EquipRepairModel()
                            
                            //                            let spectionPartModel = self?.spectionPartModel?[indexPath.section]
                            
                            
                            equipRepairModel.equipPartName = standardModel?.equipPositionName ?? self?.equipPositionName
                            equipRepairModel.equipPartId = standardModel?.equipPositionId
                            equipRepairModel.equipPartsName = standardModel?.equipPartName
                            equipRepairModel.equipPartsId = standardModel?.equipPartId
                            equipRepairModel.equipId = self?.scanModel?.equipId
                            
                            
                            vc.equipRepairModel = equipRepairModel
                            vc.repaireModel.equipPartId = standardModel?.equipPositionId
                            vc.repaireModel.equipPartsId = standardModel?.equipPartId
                            vc.repaireModel.repairSource = self?.repairSourceEmun?.rawValue
                            
                            vc.repaireModel.repairSourceId = self?.spectionRecordPlan
                            vc.repaireModel.repairStandardId = standardModel?.id
                            vc.repairResult = { [weak self] result in
                                standardModel?.isCheck = result
                                self?.spectionPartModel?[indexPath.section].standardCheckRespVoList?[(indexPath.row / 5) - 1] = standardModel!
                                self?.tableView.reloadRows(at: [indexPath], with: .none)
                                self?.defSave()
                                
                            }
                            self?.show(vc, sender: nil)
                        }
                    }.disposed(by: cell.rx.reuseBag)
                }
            default:
                break
            }
            
        }))!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.001
    }
}
