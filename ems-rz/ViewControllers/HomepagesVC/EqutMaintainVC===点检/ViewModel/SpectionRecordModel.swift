//
//  SpectionRecordModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/15.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation
// MARK: - 巡检标准
struct SpectionPartStandardModel: HandyJSON {
    
    var isCheck: Bool? = false
    
    var equipSmallCategoryId: String?
    var modifiedUser: String?
    var equipMiddleCategoryId: String?
    var checkMethod: String?
    var id: String?///检查标准ID
    var deleted: String?
    var equipPartId: String?
    var checkMeasure: String?
    var equipBigCategoryId: String?
    var equipPartName: String?
    var equipSmallCategoryName: String?
    var equipPositionId: String?
    var checkStandard: String?
    var modifiedTime: String?
    var checkContent: String?
    var createdTime: String?
    var equipBigCategoryName: String?
    var createdUser: String?
    var equipPositionName: String?
    var equipMiddleCategoryName: String?
    
    func spectionPartStandardViewModels() -> [EquipmentInfoDetail] {
        return [
            EquipmentInfoDetail(infoKey: "检查内容", infoValue: checkMethod),
            EquipmentInfoDetail(infoKey: "评判标准", infoValue: checkStandard),
            EquipmentInfoDetail(infoKey: "处理措施", infoValue: checkMeasure),
            EquipmentInfoDetail(infoKey: "检查方法", infoValue: checkMethod),
           
        ]
    }
}
// MARK: - 巡检部件
class SpectionPartModel: NSObject, HandyJSON, ViewModelOperate {
    var equipPart: String?///部件
    var standardCheckRespVoList: [SpectionPartStandardModel]?
    var intact: Bool? = true///部件是否完好
    required override init() {}
}
// MARK: - 巡检部位
class SpectionRecordModel: NSObject, HandyJSON, ViewModelOperate {
    var equipPositionName: String?
    var checkContent: String?
    var checkMethod: String?
    var equipBigCategoryId: String?
    var equipSmallCategoryId: String?
    var checkMeasure: String?
    var checkStandard: String?
    var createdUser: String?
    var modifiedTime: String?
    var id: String?
    var createdTime: String?
    var equipMiddleCategoryId: String?
    var deleted: String?
    var equipMiddleCategoryName: String?
    var equipPartId: String?
    var equipSmallCategoryName: String?
    var equipPositionId: String?
    var equipBigCategoryName: String?
    var modifiedUser: String?
    var spectionParts: [SpectionPartModel]?
    required override init() {}
}
struct SpectionRecordViewModel {
    ///巡检
    static func spectionRecordModels(_ spectionRecords: [SpectionRecordModel]?, equipInfoScanModel: EquipInfoScanModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []

        if let model = equipInfoScanModel  {
            let equSummary: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: EquSummaryCell.reuseIdentifier))]
            tableViewViewModel.append(equSummary)
        }
        
        if let models = spectionRecords {
            for (_, model) in models.enumerated() {
                var spectionViewModels: [TableViewViewModel<ViewModelOperate>] = []

                spectionViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)))
                if let partModels = model.spectionParts {
                    for (_, partModel) in partModels.enumerated() {
                        spectionViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: partModel, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)))
                    }
                }
                tableViewViewModel.append(spectionViewModels)

            }
        }
        return tableViewViewModel
    }
    
    ///巡检详情
    static func spectionDetailModels(_ models: [SpectionPartModel]?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let partModels = models {
            for (_, partModel) in partModels.enumerated() {
                var vm: [TableViewViewModel<ViewModelOperate>] = []
                let baseViewModel = EquipmentInfoDetail(infoKey: partModel.equipPart, infoValue: "")
                let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                vm.append(partViewModel)
                if let spectionPartStandardModels = partModel.standardCheckRespVoList {
                    for (_, spectionPartStandardModel) in spectionPartStandardModels.enumerated() {

                        vm += spectionPartStandardModel.spectionPartStandardViewModels().map { (model) -> TableViewViewModel<ViewModelOperate> in
                            return TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                        }
                        
                        
                        let baseViewModel = EquipmentInfoDetail(infoKey: "是否报修", infoValue: "", cellStatus: spectionPartStandardModel.isCheck)
                        let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier))
                        vm.append(partViewModel)
                    }
                }
                tableViewViewModel.append(vm)
            }
        }
        return tableViewViewModel
    }
}
