//
//  MaintainListModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/9.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation
// MARK: - 部件检查标准信息
class StandardModel: NSObject, HandyJSON, ViewModelOperate {
    var checkContent: String?
    var checkMeasure: String?
    var checkMethod: String?
    var checkStandard: String?
    var id: String?

    var isCheck: Bool? = false///是否报修
    required override init() {}

}
// MARK: - 部件
class PartInfoModel: NSObject, HandyJSON, ViewModelOperate {
   
    var partName: String?
    var standardCheckAppRespVoList: [StandardModel]?
    var partId: String?
    var intact: Bool? = true///部件是否完好
    
    required override init() {}
}

// MARK: - 部位
class PositionModel: NSObject, HandyJSON, ViewModelOperate {
   var positionId: String?
   var planCheckAppPartInfoRespVoList: [PartInfoModel]?
   var positionName: String?
   required override init() {}
}

class CheckAppAlreadyDetail: NSObject, HandyJSON, ViewModelOperate{
    var equipPositionName: String?
    var isRepair: String?
    var flowId: String?
    var equipPartName: String?
    var isOpen: Bool = false
    var checks: [CheckAppAlreadyDetail]? = []
    
    
    required override init() {}
}
class MaintainRecordModel: NSObject, HandyJSON, ViewModelOperate {
    
    var useCompanyName: String?
    var startTimeDisplay: String?
    var status: String?//* 计划状态      * 是否可以点检的按钮的颜色，      * 1表示正常，绿色      * 2表示延迟，红色      * 3表示不可点检，灰色
    var planId: String?
    var useOrgName: String?
    var nextTime: String?
    var equipName: String?
    var equipId: String?
    var initialTime: String?
    var checkUserName: String?
    var endTimeDisplay: String?
    var planCheckAppPositionInfoRespVoList: [PositionModel]?
    var planCheckAppAlreadyDetailsRespVoList: [CheckAppAlreadyDetail]?
    var planName: String?
    var actualTime: String?
    required override init() {}
    func statusColor() -> UIColor {
        switch status {
        case "1":
            return  UIColor(hexString: "#3A88FC")
        case "2":
            return  UIColor(hexString: "#F97890")
        case "3", "4":
            return  UIColor(hexString: "#B7B8BA")
        default:
            return  UIColor(hexString: "#B7B8BA")
        }
    }
    
    func statusEnable(_ checkStatus: Int) -> Bool {
        if checkStatus == 1 {
            return true
        }
        switch status {
        case "1", "2":
            return true
        case "3", "4":
            return false
        default:
            return false
        }
    }
    
}

