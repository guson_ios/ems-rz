//
//  MaintainModel.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class MaintainModel: NSObject, HandyJSON, ViewModelOperate {
//    static func maintainDetailViewModels(_ model: EquipBaseInfoRespVo?) -> [EquipmentInfoDetail]{
//           return [
//               EquipmentInfoDetail(infoKey: "保养内容", infoValue: model?.equipName),
//               EquipmentInfoDetail(infoKey: "技术要求 ", infoValue: model?.equipCode),
//               EquipmentInfoDetail(infoKey: "设备系统编号", infoValue: model?.equipSystemCode),
//               EquipmentInfoDetail(infoKey: "维修项目 ", infoValue: model?.unit),
//               EquipmentInfoDetail(infoKey: "故障代码", infoValue: model?.companyName),
//               EquipmentInfoDetail(infoKey: "故障现象", infoValue: model?.equipBigCategoryName),
//           ]
//       }
//
    static func checkDetailViewModels(_ model: StandardModel?) -> [EquipmentInfoDetail]{
        return [
            EquipmentInfoDetail(infoKey: "检查内容", infoValue: model?.checkContent),
            EquipmentInfoDetail(infoKey: "评判标准", infoValue: model?.checkStandard),
            EquipmentInfoDetail(infoKey: "处理措施", infoValue: model?.checkMeasure),
            EquipmentInfoDetail(infoKey: "检查方法", infoValue: model?.checkMethod),
           
        ]
    }
    
    required override init() {}
}

struct MaintainViewModel {
    
    ///保养
    static func maintainModels(_ maintainRecordModel: MaintainRecordModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let model = maintainRecordModel  {
            let equSummary: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: EquSummaryCell.reuseIdentifier))]
            tableViewViewModel.append(equSummary)
        }
        if let positionModels = maintainRecordModel?.planCheckAppPositionInfoRespVoList?.enumerated()  {
            for (_ , positionModel) in positionModels {//部位
                
                var repairViewModels: [TableViewViewModel<ViewModelOperate>] = []

                repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: positionModel, size: CGSize(width: (KScreenWidth / 2.0 - 10), height: 84 * KScaleW), cellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)))
                if let partModels = positionModel.planCheckAppPartInfoRespVoList {
                    for (_, partModel) in partModels.enumerated() {
                        repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: partModel, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)))
                    }
                }
                
                tableViewViewModel.append(repairViewModels)
            }
        }
        return tableViewViewModel
    }
    ///已保养
    static func maintaindModels(_ maintainRecordModel: MaintainRecordModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let model = maintainRecordModel  {
            let equSummary: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: EquSummaryCell.reuseIdentifier))]
            tableViewViewModel.append(equSummary)
        }
        
        if let positionModels = maintainRecordModel?.planCheckAppAlreadyDetailsRespVoList  {
            var tmpPositions = [CheckAppAlreadyDetail]()
                        
            for (_, model) in positionModels.enumerated() {
                if tmpPositions.contains(where: { (item) -> Bool in
                    if model.equipPartName == item.equipPartName, model.equipPositionName == item.equipPositionName{
                        if model.isRepair == "报修" {
                            item.isRepair = model.isRepair
                        }
                        return true
                    }
                    return false
                }) {
                    
                }else{
                    tmpPositions.append(model)
                }
                    
            }
//            print(tmpPositions)
            var tmp1Positions = [CheckAppAlreadyDetail]()

            for (_, model) in tmpPositions.enumerated() {
                if tmp1Positions.contains(where: { (item) -> Bool in
                    return model.equipPositionName == item.equipPositionName
                }) {
                    
                }else{
                    tmp1Positions.append(model)
                }
            }
//            var tmp2Positions = [[CheckAppAlreadyDetail]]()
            for (_, model) in tmp1Positions.enumerated() {
//                var tmp0Positions = [CheckAppAlreadyDetail]()
                model.checks = []
                for (_, part) in tmpPositions.enumerated() {
                    if model.equipPositionName == part.equipPositionName  {
//                        tmp0Positions.append(part)
                        model.checks?.append(part)
                    }
                }
//                tmp2Positions.append(tmp0Positions)
            }
            for (_, positionModels) in tmp1Positions.enumerated() {
                var repairViewModels: [TableViewViewModel<ViewModelOperate>] = []
                repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: positionModels, size: CGSize(width: (KScreenWidth / 2.0 - 10), height: 84 * KScaleW), cellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)))
                if positionModels.isOpen , let ps = positionModels.checks {
                    for (_, positionModel) in ps.enumerated() {
                        repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: positionModel, size: CGSize(width: (KScreenWidth / 2.0 - 10), height: 84 * KScaleW), cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)))
                    }
                }
                
                tableViewViewModel.append(repairViewModels)
            }

//            for (_ , positionModels) in tmp2Positions.enumerated() {
//                var repairViewModels: [TableViewViewModel<ViewModelOperate>] = []
//                for (_, positionModel) in positionModels.enumerated() {
//                    repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: positionModel, size: CGSize(width: (KScreenWidth / 2.0 - 10), height: 84 * KScaleW), cellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)))
//                }
//                tableViewViewModel.append(repairViewModels)
//
//            }
        }
        return tableViewViewModel
    }
    
    
    ///保养详情
    static func maintainDetailModels(_ models: [PositionModel]?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let partInfoModels = models?.first?.planCheckAppPartInfoRespVoList {
            for (_ , partInfoModel) in partInfoModels.enumerated() {
                var vm: [TableViewViewModel<ViewModelOperate>] = []
                let baseViewModel = EquipmentInfoDetail(infoKey: "部件名称", infoValue: partInfoModel.partName)
                let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                vm.append(partViewModel)
                if let standardModels = partInfoModel.standardCheckAppRespVoList{
                    for (_ , standardModel) in standardModels.enumerated() {
                        vm += MaintainModel.checkDetailViewModels(standardModel).map { (model) -> TableViewViewModel<ViewModelOperate> in
                            return TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                        }
                        let baseViewModel = EquipmentInfoDetail(infoKey: "是否报修", infoValue: "", cellStatus: standardModel.isCheck)
                        let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier))
                        vm.append(partViewModel)
                    }
                }
                tableViewViewModel.append(vm)
            }
        }
        return tableViewViewModel
    }
}


