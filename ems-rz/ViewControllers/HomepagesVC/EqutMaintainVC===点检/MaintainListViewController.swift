//
//  MaintainListViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/9.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
///部分原因点检写到了保养页面
class MaintainListViewController: BaseViewController {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var maintainRecordModels: [MaintainRecordModel]? = []
    var checkStatus = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.mj_header?.beginRefreshing()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "点检任务列表"
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.tableView.register(MaintainListCell.nib, forCellReuseIdentifier: MaintainListCell.reuseIdentifier)
        self.menuView()
        self.netWorking()
        self.addTicketStatueView()
    }
    private func netWorking() {
        self.tableView.tableViewRefreshClouse {[weak self] (pageNo, listCount) in
            if pageNo == 1 { self?.maintainRecordModels?.removeAll()}
            let url = self?.checkStatus == 0 ? emsPlanCheckAppBacklogList : emsPlanCheckAppAlreadyList
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: url), parameter: pageDict(current: pageNo), method: .get, success: { (result) -> (Void) in
                let models = [MaintainRecordModel].deserialize(from: result["data"]["records"].arrayObject) as? [MaintainRecordModel]
                self?.maintainRecordModels?.append(contentsOf: models ?? [])
                self?.tableView.reloadData()
                listCount(models?.count ?? 0)
            }) { (error) -> (Void) in
                listCount(0)
            }
        }
        
    }
    
    func addTicketStatueView() {
        let ticketStatueView = TicketStatueView(frame: CGRect(x: 0, y: 40, width: KScreenWidth, height: 50))
        ticketStatueView.confignTicketStstueView(WorkOrderTypeModel.ticketStatueModel(WorkOrderTypeModel.TicketType.SpotCheckListType))
        self.view.addSubview(ticketStatueView)
    }
    
    func menuView() {
        let menuView = MenuView(frame: CGRect(x: (KScreenWidth - 100.0) / 2, y: 0, width: 120, height: 35), titleArray: ["待检", "已检"])
        menuView.menuBtnClick = { [weak self] index in
            self?.checkStatus = index
            self?.tableView.mj_header?.beginRefreshing()
        }
        menuView.titleFont = UIFont.systemFont(ofSize: 16)
        topView.addSubview(menuView)
    }
    
}

extension MaintainListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return maintainRecordModels?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MaintainListCell.reuseIdentifier, for: indexPath) as! MaintainListCell
        cell.configMaintainListCell(maintainRecordModels?[indexPath.row], checkStatus: self.checkStatus)
        cell.rightBtn.rx.tap.bind { [weak self] _ in
            self?.rightBtnTap(indexPath)
        }.disposed(by: cell.rx.reuseBag)
        cell.rightBtn.isHidden = false
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func rightBtnTap(_ indexPath: IndexPath) {
        let model = maintainRecordModels?[indexPath.row]
        if self.checkStatus == 0{
            let vc = MaintainViewController.loadViewControllewWithNib as! MaintainViewController
            vc.maintainRecordModel = model
            self.show(vc, sender: nil)
        }
        if self.checkStatus == 1 {
            if let vc = MaintainCheckedVC.loadViewControllewWithNib as? MaintainCheckedVC {
                vc.maintainRecordModel =  model
                self.show(vc, sender: nil)
            }
        }
    }
}
