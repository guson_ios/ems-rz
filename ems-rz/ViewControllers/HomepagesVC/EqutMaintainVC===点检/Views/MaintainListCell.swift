//
//  MaintainListCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/9.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit



class MaintainListCell: UITableViewCell {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    
    @IBOutlet weak var label5: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configMaintainListCell(_ model: MaintainRecordModel?, checkStatus: Int) {
        topView.backgroundColor = model?.statusColor()
        let btnTitle = checkStatus == 0 ? "开始点检" : "查看"
        rightBtn.setTitle(btnTitle, for: .normal)
        rightBtn.isUserInteractionEnabled = model?.statusEnable(checkStatus) ?? false
        if !(model?.statusEnable(checkStatus) ?? false) {
            rightBtn.layer.borderColor = model?.statusColor().cgColor
            rightBtn.setTitleColor(model?.statusColor(), for: .normal)
        }else{
            rightBtn.layer.borderColor = COL3787FF.cgColor
            rightBtn.setTitleColor(COL3787FF, for: .normal)
        }
        
        label1.text = "工单号：" + (model?.planId ?? "")
        if checkStatus == 0 {
            label2.text = (model?.startTimeDisplay ?? "") + "至" + (model?.endTimeDisplay ?? "")
        }else{
            label2.text = "执行时间：" + (model?.actualTime ?? "")
        }
        
        label3.text = "设备名称：" + (model?.equipName ?? "")
        label4.text = "计划标题：" + (model?.planName ?? "")
    }
    
    func configMaintainListCell(_ model: NewMaintainListModel?, checkStatus: Int) {
        
        
        topView.backgroundColor = model?.statusColor()
        let btnTitle = checkStatus == 0 ? "开始保养" : "查看"
        rightBtn.isHidden = false
        rightBtn.setTitle(btnTitle, for: .normal)
//        rightBtn.isUserInteractionEnabled = model?.statusEnable(checkStatus) ?? false
//        if !(model?.statusEnable(checkStatus) ?? false) {
            rightBtn.layer.borderColor = model?.statusColor().cgColor
            rightBtn.setTitleColor(model?.statusColor(), for: .normal)
//        }else{
//            rightBtn.layer.borderColor = COL3787FF.cgColor
//            rightBtn.setTitleColor(COL3787FF, for: .normal)
//        }
        
        topView.backgroundColor = model?.statusColor()
        label1.text = "工单号：" + (model?.planRecordCode ?? "")
        label2.text = "工单号：" + (model?.planRecordCode ?? "")
        label5.text = (model?.createdTime ?? "")
        label5.isHidden = false
        label3.text = "设备名称：" + (model?.equipName ?? "")
        label4.text = "计划标题：" + (model?.planName ?? "")
    }
    
}
