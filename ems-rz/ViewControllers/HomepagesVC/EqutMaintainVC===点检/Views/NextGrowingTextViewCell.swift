//
//  NextGrowingTextViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import NextGrowingTextView

class NextGrowingTextViewCell: UITableViewCell {
    
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextGrowingTextView: NextGrowingTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        nextGrowingTextView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        nextGrowingTextView.textView.textAlignment = .left
        nextGrowingTextView.layer.borderWidth = 1
        nextGrowingTextView.layer.borderColor = COLF4F4F4.cgColor
        nextGrowingTextView.placeholderAttributedText = NSAttributedString(
            string: "输入...",
            attributes: [
                .font: nextGrowingTextView.textView.font!,
                .foregroundColor: UIColor.gray
            ]
        )
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

//extension NextGrowingTextViewCell: UITextViewDelegate{
//    func textViewDidEndEditing(_ textView: UITextView) {
//        print(self.nextGrowingTextView.contentSize.height)
//        if self.textViewHeightConstraint.constant != self.nextGrowingTextView.contentSize.height {
//            if  let tableView = self.superView(of: UITableView.self), let indexPath = tableView.indexPath(for: self){
//                self.textViewHeightConstraint.constant = self.nextGrowingTextView.contentSize.height
//                tableView.reloadRows(at: [indexPath], with: .none)
//            }
//        }
//
//    }
//}

