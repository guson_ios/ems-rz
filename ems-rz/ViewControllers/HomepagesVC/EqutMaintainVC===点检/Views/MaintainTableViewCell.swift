//
//  MaintainTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class MaintainTableViewCell: UITableViewCell {

    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func confignMaintainCell(_ model: PositionModel) {
        leftLabel.text = model.positionName
    }
    
    func confignSpectionCell(_ model: SpectionRecordModel)  {
        leftLabel.text = model.equipPositionName
    }
    
    func confignDailyCell(_ model: DailyInspectionModel) {
        leftLabel.text = model.equipPositionName
    }
    func confignNewMaintainCell(_ model: NewMaintainPositionModel.NewPositionModel) {
        leftLabel.text = model.positionName
    }
    func confignDailyCell(_ model: CheckAppAlreadyDetail) {
        leftLabel.text = model.equipPositionName
    }
}
