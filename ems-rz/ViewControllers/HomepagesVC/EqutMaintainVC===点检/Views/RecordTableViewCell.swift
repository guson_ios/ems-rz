//
//  RecordTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/2.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class VideoShowView: PhotoShowView {
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: 80, height: 80)
        return size
    }
}

class RecordTableViewCell: UITableViewCell {
    @IBOutlet weak var photoShowView: VideoShowView!
    @IBOutlet weak var recordView: RecordView!
    @IBOutlet weak var recordLeading: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.photoShowView.lineCount = 1
        self.photoShowView.maxSelectCount = 1
        self.photoShowView.imageOrVideo = true
        self.photoShowView.defimage = "bg_video"
        self.recordView.recordDataLength()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

class RecordView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var delBtn: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    
    var recordDataclosure: ((_ data: Data?) -> Void)?

    var recordData: Data? {
        didSet{
            self.recordDataLength()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadXibView()
        self.recordViewAction()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadXibView()
        self.recordViewAction()
    }
    func recordViewAction() {
        
    }
    
    
    @IBAction func imageTap(_ sender: Any) {
        
        if self.recordData?.count ?? 0 > 0{
            LVRecordTool.shared()?.playRecordingFile()
        }else{
            
            if let vc = UIApplication.shared.keyWindow?.rootViewController {
                let soundRecordView = SoundRecordStartView(frame: vc.view.frame)
                soundRecordView.recordDataBlock = { [weak self] recordData in
                    if let strongSelf = self {
                        strongSelf.recordData = recordData
                        strongSelf.recordDataclosure!(recordData)
                    }
                }
                vc.view.addSubview(soundRecordView)
            }
        }
    }
    func recordDataLength() {
        if self.recordData?.count ?? 0 > 0{
            imageView.image = UIImage(named: "icon_tape")
            delBtn.isHidden = false
            let player = try? AVAudioPlayer(data: recordData!)
            timeLabel.text = String(lround(Double(player?.duration ?? 0))) + "s"
            timeLabel.isHidden = false
        }else{
            delBtn.isHidden = true
            timeLabel.isHidden = true
            imageView.image = UIImage(named: "buwei_icon_tape")
        }
    }
}
