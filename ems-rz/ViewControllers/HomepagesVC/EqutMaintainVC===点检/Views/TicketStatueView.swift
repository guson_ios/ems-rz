//
//  TicketStatueView.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/3.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit


struct WorkOrderTypeModel {
    
    var btntitle = ""
    var title = ""
    var styles: [StyleModel] = []
    var spacing: CGFloat = 0
//    var ticketType = TicketType.SpotCheckListType
    
    enum TicketType {
        case SpotCheckListType
        case CaseTestListType
        case MaintainListType
    }
    
    struct StyleModel {
        var styleTitle = ""
        var styleColor = ""
    }
    
   static func ticketStatueModel(_ ticketTye: TicketType) -> WorkOrderTypeModel {
        var model = WorkOrderTypeModel()
        switch ticketTye {
        case .SpotCheckListType:
            model.title = "点检"
            model.btntitle = "开始点检"
            model.spacing = 70 * KScaleW
            model.styles = [StyleModel(styleTitle: "正常执行", styleColor: "#3A88FC"), StyleModel(styleTitle: "延迟", styleColor: "#F97890"), StyleModel(styleTitle: "不可执行", styleColor: "#B7B8BA")]
        case .MaintainListType:
            model.title = "保养"
            model.btntitle = "开始保养"
            model.spacing = 101 * KScaleW
            model.styles = [StyleModel(styleTitle: "正常执行", styleColor: "#3A88FC"), StyleModel(styleTitle: "验收不通过", styleColor: "#F97890")]

        default:
            break
        }
        return model
    }
}
class TicketStatueView: UIView {

    func confignTicketStstueView(_ orderModel: WorkOrderTypeModel) {
        
        var strWidth: CGFloat = 0
        for (_, model) in orderModel.styles.enumerated() {
            let width = NSString.boundingALLRect(withSize: model.styleTitle, font: UIFont.systemFont(ofSize: 10), size: CGSize(width: 0, height: CGFloat.greatestFiniteMagnitude)).width
            strWidth += width
        }
        let stylesCount = orderModel.styles.count
        let spacing0: CGFloat = KScreenWidth - strWidth - orderModel.spacing * 2
        
        let spacing: CGFloat = (spacing0 - CGFloat(stylesCount * 10)) / CGFloat(stylesCount - 1)
        var titleWidth: CGFloat = 0
        for (i, model) in orderModel.styles.enumerated() {
            let width = NSString.boundingALLRect(withSize: model.styleTitle, font: UIFont.systemFont(ofSize: 10), size: CGSize(width: 0, height: CGFloat.greatestFiniteMagnitude)).width
            let view = UIView(frame: CGRect(x: orderModel.spacing + titleWidth + CGFloat(i) * spacing, y: 0, width: 7, height: 7))
            view.backgroundColor = UIColor(hexString: model.styleColor)
            let label = UILabel(frame: CGRect(x: orderModel.spacing + titleWidth + 10 + CGFloat(i) * spacing, y: 0, width: width, height: 10))
            label.font = UIFont.systemFont(ofSize: 10)
            label.text = model.styleTitle
            label.textColor = COLA1A0A0
            self.addSubview(view)
            self.addSubview(label)
            titleWidth += (width + 10)
        }    
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
