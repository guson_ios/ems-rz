//
//  MaintainCheckedVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/4.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class MaintainCheckedVC: BaseViewController {

    var maintainRecordModel: MaintainRecordModel?
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "点检详情"
        self.tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)
        self.tableView.register(MaintainTableViewCell.nib, forCellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)

        if let vm =  MaintainViewModel.maintaindModels(maintainRecordModel){
            self.tableViewViewModel = vm
        }
    }
}
extension MaintainCheckedVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return  self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (operate, cell) in
            switch (operate, cell) {
            case (let model as CheckAppAlreadyDetail, let cell as MaintainTableViewCell):
                cell.rightBtn.setTitle("明细", for: .normal)
                cell.rightBtn.rx.tap.bind { [weak self] _ in
                    model.isOpen = !model.isOpen
                    if let vm =  MaintainViewModel.maintaindModels(self?.maintainRecordModel){
                        self?.tableViewViewModel = vm
                    }
                    self?.tableView.reloadData()
                }.disposed(by: cell.rx.reuseBag)
            default:
                break
            }
        })
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 20 : 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let model: CheckAppAlreadyDetail? = self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? CheckAppAlreadyDetail
            switch model?.isRepair {
            case "报修":
                if let vc = RepairStartVC.loadViewControllewWithNib as? RepairStartVC {
                    var repairListModel = RepairListModel()
                    repairListModel.id = model?.flowId
                    vc.repairListModel = repairListModel
                    vc.checkStatus = 1
                    self.show(vc, sender: nil)
                }
            default:
                break
            }
        }
        
    }
}
