//
//  MaintainViewController.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
extension DefaultsKeys {
    var checkPlantId: DefaultsKey<String?> { return .init("checkPlantId") }
}
class MaintainViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomTwoView: BottomTwoBtnView!
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    var models: [Any]? = []
    
    var maintainRecordModel: MaintainRecordModel?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "点检"
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        // Do any additional setup after loading the view.
        self.tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)
        self.tableView.register(MaintainTableViewCell.nib, forCellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        if let vm =  MaintainViewModel.maintainModels(maintainRecordModel){
            self.tableViewViewModel = vm
        }
        let parameter = ["planId" : self.maintainRecordModel?.planId]
        self.bottomTwoView.leftBtn.rx.tap.bind { [weak self] _  in
            if let planId = self?.maintainRecordModel?.planId {
                let planId = DefaultsKey<String>(planId, defaultValue: "")
                if Defaults[key: planId].count <= 0{///获取关联报修ID
                    AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanCheckAppUpAdd), parameter: nil, method: .post, encoding: parameter.dictToJsonString(), headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
                        Defaults[key: planId] = result["data"].stringValue
                        
                    }) { (error) -> (Void) in
                        
                    }
                }
            }
            let vc = ScanViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.scanResultDelegate = self
            self?.show(vc, sender: nil)
            vc.hidesBottomBarWhenPushed = true
        }.disposed(by: disposeBag)
       
        self.bottomTwoView.rightBtn.rx.tap.bind { [weak self] _ -> Void in
            if let positionModels = self?.maintainRecordModel?.planCheckAppPositionInfoRespVoList{
                var num = 0
                var faultNum = 0
                for (_, positionModel) in positionModels.enumerated() {//部位
                    if let planId = self?.maintainRecordModel?.planId, let positionId = positionModel.positionId{
                        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                        if Defaults[key: defPositionModels] != nil {
                            num += 1
                            let pm = [PositionModel].deserialize(from: Defaults[key: defPositionModels]) as? [PositionModel]
                            if let checkParts = pm?.first?.planCheckAppPartInfoRespVoList?.enumerated() {//本地保存报修标准
                                for (_, partInfoModel) in checkParts {
                                    if let standardModels = partInfoModel.standardCheckAppRespVoList?.enumerated()  {
                                        for (_, standardModel) in standardModels {
                                            if standardModel.isCheck! {
                                                faultNum += 1
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                }
                
                if num == positionModels.count {//全部扫码
                    let planId = DefaultsKey<String>((self?.maintainRecordModel?.planId!)!, defaultValue: "")
                    print(Defaults[key: planId])
                    let parameter = ["planId":self?.maintainRecordModel?.planId, "faultNum" : String(faultNum), "endTimeDisplay" : self?.maintainRecordModel?.endTimeDisplay, "id" : Defaults[key: planId]]
                    AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanCheckAppUpdate), parameter: nil, method: .post, encoding: parameter.dictToJsonString(), headers: headersFunc(contentType: .json), progressPlugin: self, success: { (result) -> (Void) in
                        Defaults.remove(planId)///提交后移除===点检才有的。。。
                        for (_, positionModel) in positionModels.enumerated() {//部位移除
                            if let planId = self?.maintainRecordModel?.planId, let positionId = positionModel.positionId{
                                let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                                Defaults.remove(defPositionModels)///提交后移除
                            }
                        }
                        self?.navigationController?.popViewController(animated: true)
                    }) { (error) -> (Void) in
                        
                    }
                }else{
                    ZFToast.show(withMessage: "需所有部位检查完才可提交")
                }
            }
        }.disposed(by: disposeBag)      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.reloadData()
    }

}
extension MaintainViewController: LBXScanViewControllerDelegate{
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        print(scanResult.strScanned as Any)
        if let scanStr = scanResult.strScanned {
            
            let dict = String.stringToDict(scanStr)
            let model = ScanModel.deserialize(from: dict)
            if let positionModels = self.maintainRecordModel?.planCheckAppPositionInfoRespVoList {
                for (_, positionModel) in positionModels.enumerated() {
                    if model?.equipPositionId == positionModel.positionId , model?.equipId == self.maintainRecordModel?.equipId{
                        if let planId = self.maintainRecordModel?.planId{
                            self.pushMaintainDetailVC(planId, positionId: positionModel.positionId!)
                        }
                        return
                    }
                }
            }
            ZFToast.show(withMessage: "二维码不匹配")
        }
    }
}

extension MaintainViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        return  self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (operate, cell) in
            switch (operate, cell) {
            case (let model as PositionModel, let cell as MaintainTableViewCell):
                cell.rightBtn.isHidden = true
                if let planId = self?.maintainRecordModel?.planId, let positionId = model.positionId{
                    let isHidden = !(self?.searchDefData(planId, positionId: positionId) ?? false)
                    cell.rightBtn.isHidden = isHidden
                    cell.leftBtn.isHidden = isHidden
                    cell.rightBtn.rx.tap.bind { [weak self] _  in
                        self?.pushMaintainDetailVC(planId, positionId: positionId)
                    }.disposed(by: cell.rx.reuseBag)
                    cell.leftBtn.rx.tap.bind { [weak self] _ in
                        if model.planCheckAppPartInfoRespVoList == nil{
                            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                            if
                                let positionModel =  [PositionModel].deserialize(from: Defaults[key: defPositionModels]) as? [PositionModel]{
                                for (_ , pModel) in positionModel.enumerated(){
                                    if  pModel.positionId == model.positionId{
                                        model.planCheckAppPartInfoRespVoList = pModel.planCheckAppPartInfoRespVoList
                                        if let vm =  MaintainViewModel.maintainModels(self?.maintainRecordModel){
                                            self?.tableViewViewModel = vm
                                            self?.tableView.reloadData()
                                        }
                                    }
                                }
                            }
                        }else{
                            model.planCheckAppPartInfoRespVoList = nil
                            if let vm =  MaintainViewModel.maintainModels(self?.maintainRecordModel){
                                self?.tableViewViewModel = vm
                                self?.tableView.reloadData()
                            }
                        }
                    }.disposed(by: cell.rx.reuseBag)
                }
                
            default:
                break
            }
            
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let planId = self.maintainRecordModel?.planId, let positionId = self.maintainRecordModel?.planCheckAppPositionInfoRespVoList?[indexPath.row].positionId {
//            self.pushMaintainDetailVC(planId, positionId: positionId)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 20 : 0.001
    }
}
// MARK: -
extension MaintainViewController{
    func pushMaintainDetailVC(_ planId: String, positionId: String) {
        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
        if  Defaults[key: defPositionModels] != nil {
            let vc = MaintainDetailVC.loadViewControllewWithNib as! MaintainDetailVC
            vc.positionModels = [PositionModel].deserialize(from: Defaults[key: defPositionModels]) as? [PositionModel]
            vc.maintainRecordModel = self.maintainRecordModel
            self.show(vc, sender: nil)
        }else{
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanCheckAppStandardList), parameter: ["planId" : planId, "equipPositionId" : positionId] , method: .get, progressPlugin: self, success: { (result) -> (Void) in
                let models = [PositionModel].deserialize(from: result["data"].arrayObject) as? [PositionModel]
                //                    Defaults[key: defPositionModels] = models?.toJSON() as? [[String : Any]]///本地存储
                let vc = MaintainDetailVC.loadViewControllewWithNib as! MaintainDetailVC
                vc.positionModels = models
                vc.maintainRecordModel = self.maintainRecordModel
                self.show(vc, sender: nil)
            }) { (error) -> (Void) in
                
            }
        }
    }
    
    func searchDefData(_ planId: String ,  positionId: String) -> Bool {
        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
//        Defaults.remove(defPositionModels)
        if Defaults[key: defPositionModels] != nil {
            return true
        }
        return false
    }
}
