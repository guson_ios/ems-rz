//
//  MaintainDetailVC.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class MaintainDetailVC: BaseViewController {
    
    @IBOutlet weak var bottomBtnView: BottomBtnView!
    @IBOutlet weak var tableView: UITableView!
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    var positionModels: [PositionModel]?
    var maintainRecordModel: MaintainRecordModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "点检详情"
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        self.tableView.register(PhotoSelectShowCell.nib, forCellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier)
        self.tableView.register(NextGrowingTextViewCell.nib, forCellReuseIdentifier: NextGrowingTextViewCell.reuseIdentifier)
        self.tableView.register(SwitchTableViewCell.nib, forCellReuseIdentifier: SwitchTableViewCell.reuseIdentifier)
        if let viewModels =  MaintainViewModel.maintainDetailModels(positionModels){
            self.tableViewViewModel = viewModels
        }
        self.bottomBtnView.btn.setTitle("保存", for: .normal)
        self.bottomBtnView.btn.rx.tap.bind { [weak self] _  in
            self?.defSave()
            self?.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
    }
    
    func defSave() {
        if let planId = self.maintainRecordModel?.planId, let positionId = self.positionModels?.first?.positionId{
            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
            Defaults[key: defPositionModels] = self.positionModels?.toJSON() as? [[String : Any]]
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.reloadData()
    }
}

extension MaintainDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            switch cell {
            case let _ as UniversalTableViewCell:
            
                break
//            case let cell as PhotoSelectShowCell:
//                cell.photoSelectShowView.closure = {[weak self] row in
//                    self?.tableView.reloadData()
//                }
            case let cell as SwitchTableViewCell:
                var partModel = self?.positionModels?.first?.planCheckAppPartInfoRespVoList?[indexPath.section]
                var standardModel = self?.positionModels?.first?.planCheckAppPartInfoRespVoList?[indexPath.section].standardCheckAppRespVoList?[(indexPath.row / 5) - 1]
                
                if standardModel?.isCheck ?? false {
                    cell.rightSwitch.isUserInteractionEnabled = false
                    cell.rightSwitch.isOn = true
                    let part1Model = partModel
                    part1Model?.intact = false
                    partModel = part1Model
                    
                }else{
                    cell.rightSwitch.rx.isOn.bind { [weak self, weak cell] (isOn) -> Void in
                        if isOn{
                            cell?.rightSwitch.isOn = false
                            let vc = FaultRepairViewController.loadViewControllewWithNib as! FaultRepairViewController
                            var equipRepairModel = EquipRepairModel()
                            let positionModel = self?.positionModels?.first
                            equipRepairModel.equipPartName = positionModel?.positionName
                            equipRepairModel.equipPartId = positionModel?.positionId
                            equipRepairModel.equipPartsName = positionModel?.planCheckAppPartInfoRespVoList?[indexPath.section].partName
                            equipRepairModel.equipPartsId = positionModel?.planCheckAppPartInfoRespVoList?[indexPath.section].partId
                            equipRepairModel.equipId = self?.maintainRecordModel?.equipId
                            vc.equipRepairModel = equipRepairModel
                            vc.repaireModel.equipPartId = positionModel?.positionId
                            vc.repaireModel.equipPartsId = positionModel?.planCheckAppPartInfoRespVoList?[indexPath.section].partId
                            vc.repaireModel.repairSource = RepaireModel.repairSourceEmun.点检.rawValue
                            vc.repaireModel.repairStandardId = standardModel?.id
                            
                            let planId = DefaultsKey<String>((self?.maintainRecordModel!.planId)!, defaultValue: "")
                            vc.repaireModel.repairSourceId = Defaults[key: planId]///保存本地
                            vc.repairResult = { [weak self] result in
                                let sm = standardModel
                                sm?.isCheck = result
                                standardModel = sm
                                self?.tableView.reloadRows(at: [indexPath], with: .none)
                                self?.defSave()
                            }
                            self?.show(vc, sender: nil)
                        }
                    }.disposed(by: cell.rx.reuseBag)
                }
            default:
                print("")
            }
            
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.001
    }
}
