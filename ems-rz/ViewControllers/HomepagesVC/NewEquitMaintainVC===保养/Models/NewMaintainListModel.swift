//
//  NewMaintainListModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/18.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

struct NewMaintainListModel: HandyJSON {
    var planRecordCode: String?
    var equipSystemCode: String?
    var modelNumber: String?
    var maintainEndTime: String?
    var useContent: String?
    var equipBigCategoryId: String?
    var equipName: String?
    var maintainUserName: String?
    var planName: String?
    var flowId: String?
    var equipCode: String?
    var acceptanceStatus: String?
    var useCompanyId: String?
    var planMaintainId: String?
    var maintainType: String?
    var equipId: String?
    var modifiedTime: String?
    var useOrgId: String?
    var maintainCompanyId: String?
    var modifiedUser: String?
    var equipSmallCategoryId: String?
    var equipMiddleCategoryId: String?
    var useOrgName: String?
    var createdTime: String?
    var maintainCompanyName: String?
    var status: String?
    var dispatchUserId: String?
    var useCompanyName: String?
    var createdUserName: String?
    var id: String?///记录ID
    var createdUser: String?
    var maintainUserId: String?
    var dispatchUserName: String?
    var maintainStartTime: String?
    var deleted: String?
    var useFlag: String?
    var differTime: String?
    
    func statusColor() -> UIColor {
        switch status {
        case "1", "0", "2":
            return  COL7AAAF4
//        case "2":
//            return  COL3A88FC
        case "3", "4":
            return  COLF97890
        default:
            return  COL7AAAF4
        }
    }
}

class NewMaintainPositionModel: NSObject, HandyJSON ,ViewModelOperate{
    class NewPositionModel: NSObject, HandyJSON ,ViewModelOperate{
        var positionName: String?
        var planMaintainPartRespVoList: [NewMaintainPartModel]?
        var positionId: String?
        var isShow = true
        
        required override init() {}
    }
    var positionRespVoList: [NewPositionModel]?
    var planRecordCode: String?
    var equipMiddleCategoryId: String?
    var equipSmallCategoryId: String?
    var planName: String?
    var equipBigCategoryId: String?
    var equipName: String?
    var orderId: String?
    var equipId: String?
    var overdueOperationTime: String?
    var planMaintainId: String?
    required override init() {}
}

class NewMaintainPartModel: NSObject, HandyJSON, ViewModelOperate {
    class NewMaintainPatrStandardModel: NSObject, HandyJSON {
        var isCheck: Bool? = false
        var createdTime: String?
        var equipPositionId: String?
        var createdUser: String?
        var maintainContent: String?
        var deleted: String?
        var id: String?
        var equipMiddleCategoryName: String?
        var equipMiddleCategoryId: String?
        var equipPartId: String?
        var equipBigCategoryName: String?
        var modifiedTime: String?
        var equipBigCategoryId: String?
        var equipPartName: String?
        var equipPositionName: String?
        var modifiedUser: String?
        var equipSmallCategoryName: String?
        var technicalRequire: String?
        var remark: String?
        var equipSmallCategoryId: String?
        var faultStatus: String?
        
        
        func maintainPatrDetailViewModels() -> [EquipmentInfoDetail]{
            return [
                EquipmentInfoDetail(infoKey: "保养内容", infoValue: maintainContent),
                EquipmentInfoDetail(infoKey: "技术标准", infoValue: technicalRequire),
            ]
        }
        required override init() {}
    }
    var partName: String?
    var standardMaintainRespVoList: [NewMaintainPatrStandardModel]?
    var partId: String?
    var intact: Bool? = true///部件是否完好
    required override init() {}
}

struct NewMaintainViewModel {
    ///润滑保养
    static func maintainModels(_ maintainRecordModel: NewMaintainPositionModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let model = maintainRecordModel  {
            let equSummary: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: EquSummaryCell.reuseIdentifier))]
            tableViewViewModel.append(equSummary)
        }
        if let positionModels = maintainRecordModel?.positionRespVoList?.enumerated()  {
            for (_ , positionModel) in positionModels {
                var repairViewModels: [TableViewViewModel<ViewModelOperate>] = []
                repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: positionModel, size: CGSize(width: (KScreenWidth / 2.0 - 10), height: 84 * KScaleW), cellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)))
                if positionModel.isShow, let partModels = positionModel.planMaintainPartRespVoList {
                    for (_, partModel) in partModels.enumerated() {
                        repairViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: partModel, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)))
                    }
                }
                tableViewViewModel.append(repairViewModels)
            }
        }
        return tableViewViewModel
    }
    ///润滑保养详情
    static func maintainPartDetailModels(_ models: [NewMaintainPartModel]?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let partInfoModels = models {
            for (_ , partInfoModel) in partInfoModels.enumerated() {
                var vm: [TableViewViewModel<ViewModelOperate>] = []
                let baseViewModel = EquipmentInfoDetail(infoKey: "部件名称", infoValue: partInfoModel.partName)
                let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                vm.append(partViewModel)
                if let standardModels = partInfoModel.standardMaintainRespVoList{
                    for (_ , standardModel) in standardModels.enumerated() {
                        
                        vm += standardModel.maintainPatrDetailViewModels().map { (model) -> TableViewViewModel<ViewModelOperate> in
                            return TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                        }
                        let baseViewModel = EquipmentInfoDetail(infoKey: "是否报修", infoValue: "", cellStatus: standardModel.isCheck)
                        let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier))
                        vm.append(partViewModel)
                    }
                }
                tableViewViewModel.append(vm)
            }
        }
        return tableViewViewModel
    }
}

// 安全公告的数量模型
struct MaintenAgreeModel:HandyJSON
{
    var parentId: String?
    var basicName: String?
    var id: String?
    var sort: String?
    var remark: String?
    var basicDataRespVos: String?
    var staticFlag: String?
    var status: String?
}
