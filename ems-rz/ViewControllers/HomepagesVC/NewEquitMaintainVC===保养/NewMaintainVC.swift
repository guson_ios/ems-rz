//
//  NewMaintainVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/18.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class NewMaintainVC: BaseViewController {
    
    @IBOutlet weak var bottomTwoViewCon: NSLayoutConstraint!
    @IBOutlet weak var bottomTwoView: BottomTwoBtnView!
    @IBOutlet weak var tableView: UITableView!
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    
    var newMaintainListModel: NewMaintainListModel?
    var newMaintainPositionModel: NewMaintainPositionModel?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)
        self.tableView.register(MaintainTableViewCell.nib, forCellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)

        self.planMaintainOrderPosition()
        self.title = "保养"
        self.bottomTwoView.leftBtn.rx.tap.bind { [weak self] _  in
            let vc = ScanViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.scanResultDelegate = self
            self?.show(vc, sender: nil)
            vc.hidesBottomBarWhenPushed = true
        }.disposed(by: disposeBag)
        
        self.bottomTwoView.rightBtn.rx.tap.bind { [weak self] _  in
            
            if let positionModels = self?.newMaintainPositionModel?.positionRespVoList{
                var num = 0
                for (_, positionModel) in positionModels.enumerated() {
                    if let planId = self?.newMaintainListModel?.id, let positionId = positionModel.positionId{
                        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                        if Defaults[key: defPositionModels] != nil {
                            num += 1
                        }
                    }
                }
                
                if num == positionModels.count {
                    if let planMaintainOrderId = self?.newMaintainListModel?.id{
                        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanMaintainOrderEnd), parameter: ["planMaintainOrderId" : planMaintainOrderId], method: .get, success: { (result) -> (Void) in
                            
                            for (_, positionModel) in positionModels.enumerated() {//部位移除
                                if let planId = self?.newMaintainListModel?.id, let positionId = positionModel.positionId{
                                    let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                                    Defaults.remove(defPositionModels)///提交后移除
                                }
                            }
                            self?.navigationController?.popViewController(animated: true)
                        }) { (error) -> (Void) in
                            
                        }
                    }
                }else{
                    ZFToast.show(withMessage: "需所有部位检查完才可提交")
                }
                
            }
        }.disposed(by: disposeBag)
        
    }
}

extension NewMaintainVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        return  self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (operate, cell) in
            switch (operate, cell) {
            case (let model as NewMaintainPositionModel, let cell as EquSummaryCell):
                cell.confignSummaryCell(model)
            case (let model as NewMaintainPositionModel.NewPositionModel, let cell as MaintainTableViewCell):
                cell.confignNewMaintainCell(model)
                cell.rightBtn.isHidden = true
                if let planId = self?.newMaintainListModel?.id, let positionId = model.positionId{
                    let isHidden = !(self?.searchDefData(planId, positionId: positionId) ?? false)
                    cell.rightBtn.isHidden = isHidden
                    cell.leftBtn.isHidden = isHidden
                    cell.rightBtn.rx.tap.bind { [weak self] _  in
                        self?.pushNewMaintainDetailVC(planId, equipPositionId: positionId)
                    }.disposed(by: cell.rx.reuseBag)
                    cell.leftBtn.rx.tap.bind { [weak self] _ in
                        if model.planMaintainPartRespVoList == nil{
                            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                            if
                                let positionModel =  [NewMaintainPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [NewMaintainPartModel]{
                                model.planMaintainPartRespVoList = positionModel
                                if let viewModes = NewMaintainViewModel.maintainModels(self?.newMaintainPositionModel){
                                    self?.tableViewViewModel = viewModes
                                    self?.tableView.reloadData()
                                }
                            }
                        }else{
                            model.planMaintainPartRespVoList = nil
                            if let viewModes = NewMaintainViewModel.maintainModels(self?.newMaintainPositionModel){
                                self?.tableViewViewModel = viewModes
                                self?.tableView.reloadData()
                            }
                        }
                    }.disposed(by: cell.rx.reuseBag)
                    
                }
                
            default:
                break
            }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let planId = self.newMaintainListModel?.id, let positionId = self.newMaintainPositionModel?.positionRespVoList?[indexPath.section - 1].positionId{
//            self.pushNewMaintainDetailVC(planId, equipPositionId: positionId)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 20 : 0.001
    }
}

extension NewMaintainVC: LBXScanViewControllerDelegate{
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        print(scanResult.strScanned as Any)
        if let scanStr = scanResult.strScanned {
            
            let dict = String.stringToDict(scanStr)
            let model = ScanModel.deserialize(from: dict)
            if let positionModels = self.newMaintainPositionModel?.positionRespVoList {
                for (_, positionModel) in positionModels.enumerated() {
                    if model?.equipPositionId == positionModel.positionId {
                        if let planId = self.newMaintainListModel?.id{
                            self.pushNewMaintainDetailVC(planId, equipPositionId: positionModel.positionId!)
                        }
                        return
                    }
                }
            }
            ZFToast.show(withMessage: "二维码不匹配")
        }
    }
    
    // MARK: - 获取部位
    func planMaintainOrderPosition() {
        if let planMaintainOrderId = self.newMaintainListModel?.id {
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanMaintainOrderPosition), parameter: ["planMaintainOrderId" : planMaintainOrderId], method: .get, success: { (result) -> (Void) in
                self.newMaintainPositionModel = NewMaintainPositionModel.deserialize(from: result["data"].dictionaryObject)
                if let viewModes = NewMaintainViewModel.maintainModels(self.newMaintainPositionModel){
                    self.tableViewViewModel = viewModes
                    self.tableView.reloadData()
                }
                self.getMaintainOrderPart()
            }) { (error) -> (Void) in
                
            }
        }
        
    }
    @objc public func getMaintainOrderPart() {
        
    }
    // MARK: - 本地
    @objc  func searchDefData(_ planId: String ,  positionId: String) -> Bool {
        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
        if Defaults[key: defPositionModels] != nil {
            return true
        }
        return false
    }
    // MARK: - 详情
    func pushNewMaintainDetailVC(_ planId: String, equipPositionId: String) {
        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + equipPositionId, defaultValue: nil)
        if  Defaults[key: defPositionModels] != nil {
            if let vc = NewMaintainDetailVC.loadViewControllewWithNib as? NewMaintainDetailVC {
                vc.newMaintainPartModels = [NewMaintainPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [NewMaintainPartModel]
                vc.newMaintainListModel = self.newMaintainListModel
                self.show(vc, sender: nil)
            }
        }else{
            let mainModel = MaintainOrderPart(planMaintainOrderId: self.newMaintainListModel?.id, equipBigCategoryId: self.newMaintainPositionModel?.equipBigCategoryId, equipMiddleCategoryId: self.newMaintainPositionModel?.equipMiddleCategoryId, equipPositionId: equipPositionId, equipSmallCategoryId: self.newMaintainPositionModel?.equipSmallCategoryId)
//            print(mainModel.toJSONString())
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanMaintainOrderPart), parameter: nil , method: .post, encoding: mainModel.toJSONString() ?? "{}", headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
                let models = [NewMaintainPartModel].deserialize(from: result["data"]["planMaintainPartRespVoList"].arrayObject) as? [NewMaintainPartModel]
                if let vc = NewMaintainDetailVC.loadViewControllewWithNib as? NewMaintainDetailVC{
                    vc.newMaintainPartModels = models
                    vc.newMaintainListModel = self.newMaintainListModel
                    self.show(vc, sender: nil)
                }
            }) { (error) -> (Void) in
                
            }
        }
    }
}


struct MaintainOrderPart: HandyJSON {
    var planMaintainOrderId: String?
    var equipBigCategoryId: String?
    var equipMiddleCategoryId: String?
    var equipPositionId: String?
    var equipSmallCategoryId: String?
    var equipPartId: String?
    var standardId: String?
    
}
