//
//  MaintainAgreeMentVC.swift
//  ems-rz
//
//  Created by mac on 2020/4/15.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MaintainAgreeMentVC: BaseViewController
{
    var tableView:UITableView!
    
    var maintenId:String!
    
    //初始化一个可变空数组
    var maintenAgreeModels: [MaintenAgreeModel] = []
    
    // 同意与不同意的点击按钮事件，同意则继续保养流程，不同意则返回到前一个页面
    @objc func onRadioButtonValueChanged(button:RadioButton )
    {
        if button.isSelected
        {
            // 点已同意和不同意都是返回前一个页面，点同意出发block执行开始保养计划
            if button.tag==101
            {
                self.planMaintionStart(self.maintenId)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
        
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "维修作业安全告知"
        
        //创建表格视图
        self.tableView = UITableView()
        self.tableView.backgroundColor = UIColor.init(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 30;
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none

        
        //创建一个重用的单元格
        self.tableView!.register(MainTainAgreeMentCell.self, forCellReuseIdentifier: "MainTainAgreeMentCell")
        self.view.addSubview(self.tableView!)
        
        self.tableView.snp.makeConstraints { (make) in
            
            make.top.equalTo(0)
            make.leading.equalTo(0)
            if #available(iOS 11.0, *)
            {
                make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-40)
            }
            else
            {
                make.bottom.equalTo(-40)
            }
            make.trailing.equalTo(0)
        }
        
        let btnBgView = UIView()
        self.view.addSubview(btnBgView)
        btnBgView.backgroundColor = self.tableView.backgroundColor
        btnBgView.snp.makeConstraints { (make) in
            
            make.top.equalTo(self.tableView.snp.bottom)
            make.leading.equalTo(0)
            make.height.equalTo(40)
            make.trailing.equalTo(0)
        }
        
        
        let agreebtn = RadioButton(frame: CGRect(x: (KScreenWidth/2.0-135)/2.0, y: 0, width: 135, height: 35))
        btnBgView.addSubview(agreebtn)
        agreebtn.tag = 101
        agreebtn.addTarget(self, action: #selector(onRadioButtonValueChanged(button:)), for: .touchUpInside)
        agreebtn.setTitle("已阅读并同意", for: .normal)
        agreebtn.setTitleColor(UIColor.blue, for: .normal)
        agreebtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        agreebtn.setImage(UIImage(named:"unchecked.png"), for: .normal)
        agreebtn.setImage(UIImage(named:"checked.png"), for: .selected)
        agreebtn.contentHorizontalAlignment = .left
        agreebtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)
 
        
        let notAgreebtn = RadioButton(frame: CGRect(x:KScreenWidth/2.0+(KScreenWidth/2.0-86)/2.0, y: 0, width: 86, height: 35))
        btnBgView.addSubview(notAgreebtn)
        notAgreebtn.tag = 102
        notAgreebtn.addTarget(self, action: #selector(onRadioButtonValueChanged(button:)), for: .touchUpInside)
        notAgreebtn.setTitle("不同意", for: .normal)
        notAgreebtn.setTitleColor(UIColor.red, for: .normal)
        notAgreebtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        notAgreebtn.setImage(UIImage(named:"unchecked.png"), for: .normal)
        notAgreebtn.setImage(UIImage(named:"checked.png"), for: .selected)
        notAgreebtn.contentHorizontalAlignment = .left
        notAgreebtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)
        
        var buttons: [RadioButton] = []
        buttons.append(agreebtn)
        buttons.append(notAgreebtn)
        agreebtn.groupButtons = buttons

        
        //请求安全公告内容
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintenanceAgreement), parameter: ["basicName" : "维修作业安全告知"], method: .post, success: { (result) -> (Void) in
            
            if let models = [MaintenAgreeModel].deserialize(from: result["data"].arrayObject) as? [MaintenAgreeModel]
            {
                self.maintenAgreeModels.append(contentsOf: models)
                self.tableView.reloadData()
            }
            
        }) { (error) -> (Void) in
            
        }
    }
}

// 请求完成以后回到
extension MaintainAgreeMentVC
{
    func planMaintionStart(_ planMaintainOrderId: String)
    {
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanMaintainOrderStart), parameter: ["planMaintainOrderId" : planMaintainOrderId], method: .get, success: { (result) -> (Void) in
            
            self.navigationController?.popViewController(animated: true)

        }) { (error) -> (Void) in

        }
    }
}

extension MaintainAgreeMentVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.maintenAgreeModels.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MainTainAgreeMentCell = tableView.dequeueReusableCell(withIdentifier: "MainTainAgreeMentCell", for: indexPath) as! MainTainAgreeMentCell
        let model = self.maintenAgreeModels[indexPath.row]
        cell.agreeMent?.text = model.basicName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
