//
//  MainTainAgreeMentCell.swift
//  ems-rz
//
//  Created by mac on 2020/4/15.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
import SnapKit

class MainTainAgreeMentCell: UITableViewCell
{
    var agreeMent     : UILabel?
    var baView        : UIView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCell.SelectionStyle.none
         
        self.createCellUI()
    }
     
    func createCellUI()
    {
        self.contentView.backgroundColor = UIColor.init(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
        
        let bgview = UIView()
        bgview.backgroundColor = UIColor.white
        self.contentView.addSubview(bgview)
        
        self.agreeMent = UILabel()
        self.agreeMent?.textColor = UIColor.darkGray
        self.agreeMent?.font = UIFont.systemFont(ofSize: 15)
        self.agreeMent?.numberOfLines = 0;
        bgview.addSubview(agreeMent!)
        
        bgview.snp.makeConstraints { (make) in
            
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(0)
            make.trailing.equalToSuperview().offset(-10)
        }
        
        self.agreeMent?.snp.makeConstraints { (make) in
            
            make.top.equalToSuperview().offset(0)
            make.left.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(0)
            make.trailing.equalToSuperview().offset(0)
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
