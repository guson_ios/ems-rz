//
//  NewMaintainDetailVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/19.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class NewMaintainDetailVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomBtnView: BottomBtnView!
    
    @IBOutlet weak var bottomBtnViewCon: NSLayoutConstraint!
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    var newMaintainPartModels: [NewMaintainPartModel]?
    var newMaintainListModel: NewMaintainListModel?///只是获取记录ID

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "保养详情"
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
               self.tableView.register(PhotoSelectShowCell.nib, forCellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier)
               self.tableView.register(NextGrowingTextViewCell.nib, forCellReuseIdentifier: NextGrowingTextViewCell.reuseIdentifier)
               self.tableView.register(SwitchTableViewCell.nib, forCellReuseIdentifier: SwitchTableViewCell.reuseIdentifier)
        
        if let viewModels = NewMaintainViewModel.maintainPartDetailModels(newMaintainPartModels) {
            self.tableViewViewModel = viewModels
        }
        self.bottomBtnView.btn.setTitle("保存", for: .normal)
        self.bottomBtnView.btn.rx.tap.bind { [weak self] _  in
            self?.defSave()
            self?.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
    }
    func defSave() {
        if let planId = self.newMaintainListModel?.id, let positionId = self.newMaintainPartModels?.first?.standardMaintainRespVoList?.first?.equipPositionId{
            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
            Defaults[key: defPositionModels] = self.newMaintainPartModels?.toJSON() as? [[String : Any]]
        }
    }
}


extension NewMaintainDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            switch cell {
            case let cell as UniversalTableViewCell:
                cell.leftLabel.textColor = indexPath.row == 0 ? COL3787FF : COL666666
                cell.rightLabel.textColor = indexPath.row == 0 ? COL3787FF : COL666666
            case let cell as SwitchTableViewCell:
                var standardModel = self?.newMaintainPartModels?[indexPath.section].standardMaintainRespVoList?[(indexPath.row / 3) - 1]
                var partModel = self?.newMaintainPartModels?[indexPath.section]
                if standardModel?.isCheck ?? false {
                    cell.rightSwitch.isUserInteractionEnabled = false
                    cell.rightSwitch.isOn = true
                    let partModel1 = partModel
                    partModel1?.intact = false
                    partModel = partModel1
                }else{
                    cell.rightSwitch.rx.isOn.bind { [weak self] (isOn) -> Void in
                        if isOn{
                            let vc = FaultRepairViewController.loadViewControllewWithNib as! FaultRepairViewController
                            var equipRepairModel = EquipRepairModel()
                            equipRepairModel.equipPartName = standardModel?.equipPositionName
                            equipRepairModel.equipPartId = standardModel?.equipPositionId
                            equipRepairModel.equipPartsName = standardModel?.equipPartName
                            equipRepairModel.equipPartsId = standardModel?.equipPartId
                            equipRepairModel.equipId = self?.newMaintainListModel?.equipId
                            vc.equipRepairModel = equipRepairModel
                            vc.repaireModel.equipPartId = standardModel?.equipPositionId
                            vc.repaireModel.equipPartsId = standardModel?.equipPartId
                            vc.repaireModel.repairSource = RepaireModel.repairSourceEmun.润滑保养.rawValue
                            vc.repaireModel.repairStandardId = standardModel?.id
                            vc.repairResult = { [weak self] result in
                                let sm = standardModel
                                sm?.isCheck = result
                                standardModel = sm
                                self?.tableView.reloadRows(at: [indexPath], with: .none)
                                self?.defSave()
                            }
                            self?.show(vc, sender: nil)
                        }
                    }.disposed(by: cell.rx.reuseBag)
                }
            default:
                print("")
            }
            
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.001
    }
}

class NewMaintainDetailComVC: NewMaintainDetailVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bottomBtnViewCon.constant = 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            switch cell {
            case let cell as UniversalTableViewCell:
                cell.leftLabel.textColor = indexPath.row == 0 ? COL3787FF : COL666666
                cell.rightLabel.textColor = indexPath.row == 0 ? COL3787FF : COL666666
            case let cell as SwitchTableViewCell:
                let standardModel = self?.newMaintainPartModels?[indexPath.section].standardMaintainRespVoList?[(indexPath.row / 3) - 1]
                cell.rightSwitch.isEnabled = false
                cell.rightSwitch.isOn = standardModel?.faultStatus?.stringBoolValue() ?? false
            default:
                break
            }
        })
    }
}

