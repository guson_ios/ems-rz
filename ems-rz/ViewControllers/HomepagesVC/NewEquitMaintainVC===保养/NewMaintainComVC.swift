//
//  NewMaintainComVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/18.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class NewMaintainComVC: NewMaintainVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bottomTwoViewCon.constant = 0
    }
    
    override func getMaintainOrderPart() {
        if let models = self.newMaintainPositionModel?.positionRespVoList?.enumerated() {
            for (i , model) in models {
                let mainModel = MaintainOrderPart(planMaintainOrderId: self.newMaintainListModel?.id, equipBigCategoryId: self.newMaintainPositionModel?.equipBigCategoryId, equipMiddleCategoryId: self.newMaintainPositionModel?.equipMiddleCategoryId, equipPositionId: model.positionId, equipSmallCategoryId: self.newMaintainPositionModel?.equipSmallCategoryId)
                //            print(mainModel.toJSONString())
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanMaintainOrderPart), parameter: nil , method: .post, encoding: mainModel.toJSONString() ?? "{}", headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
                    let models = [NewMaintainPartModel].deserialize(from: result["data"]["planMaintainPartRespVoList"].arrayObject) as? [NewMaintainPartModel]
                    self.newMaintainPositionModel?.positionRespVoList?[i].planMaintainPartRespVoList = models
                    if let viewModes = NewMaintainViewModel.maintainModels(self.newMaintainPositionModel){
                        self.tableViewViewModel = viewModes
                        self.tableView.reloadData()
                    }
                    //                        if let vc = NewMaintainDetailVC.loadViewControllewWithNib as? NewMaintainDetailVC{
                    //                            vc.newMaintainPartModels = models
                    //                            vc.newMaintainListModel = self.newMaintainListModel
                    //                            self.show(vc, sender: nil)
                    //                        }
                }) { (error) -> (Void) in
                    
                }
            }
        }
    }
    override func searchDefData(_ planId: String, positionId: String) -> Bool {
        return true
    }
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        return  self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (operate, cell) in
            switch (operate, cell) {
            case (let model as NewMaintainPositionModel, let cell as EquSummaryCell):
                cell.confignSummaryCell(model)
            case (let model as NewMaintainPositionModel.NewPositionModel, let cell as MaintainTableViewCell):
                cell.confignNewMaintainCell(model)
                cell.rightBtn.isHidden = true
                if let planId = self?.newMaintainListModel?.id, let positionId = model.positionId{
                    let isHidden = !(self?.searchDefData(planId, positionId: positionId) ?? false)
                    cell.rightBtn.isHidden = isHidden
                    cell.leftBtn.isHidden = isHidden
                    cell.rightBtn.setTitle("查看", for: .normal)
                    cell.rightBtn.rx.tap.bind { [weak self] _  in
                        if let vc = NewMaintainDetailVC.loadViewControllewWithNib as? NewMaintainDetailVC{
                            object_setClass(vc, NewMaintainDetailComVC.self)
                            vc.newMaintainPartModels = model.planMaintainPartRespVoList
                            vc.newMaintainListModel = self?.newMaintainListModel
                            self?.show(vc, sender: nil)
                        }
                    }.disposed(by: cell.rx.reuseBag)
                    cell.leftBtn.rx.tap.bind { [weak self] _ in
                        model.isShow = !model.isShow
                        if let viewModes = NewMaintainViewModel.maintainModels(self?.newMaintainPositionModel){
                            self?.tableViewViewModel = viewModes
                            self?.tableView.reloadData()
                        }
                    }.disposed(by: cell.rx.reuseBag)
                    
                }
                
            default:
                break
            }
        })
    }
}
