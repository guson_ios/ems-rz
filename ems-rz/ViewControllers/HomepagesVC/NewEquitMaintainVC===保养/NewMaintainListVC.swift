//
//  NewMaintainListVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/18.
//  Copyright © 2020 xyj. All rights reserved.
//  没有半点复用的欲望

import UIKit

class NewMaintainListVC: BaseViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var newMaintainListModel: [NewMaintainListModel]? = []
    var checkStatus = 0

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.mj_header?.beginRefreshing()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "润滑保养列表"
        // Do any additional setup after loading the view.
        self.tableView.register(MaintainListCell.nib, forCellReuseIdentifier: MaintainListCell.reuseIdentifier)
        self.menuView()
        self.netWorking()
        self.addTicketStatueView()
    }
    private func netWorking() {
        self.tableView.tableViewRefreshClouse {[weak self] (pageNo, listCount) in
            if pageNo == 1 { self?.newMaintainListModel?.removeAll()}
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanMaintainOrderList), parameter: pageDict(current: pageNo).combinationDict(["status" : String(self?.checkStatus ?? 0)]), method: .get, headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
                let models = [NewMaintainListModel].deserialize(from: result["data"]["records"].arrayObject) as? [NewMaintainListModel]
                self?.newMaintainListModel?.append(contentsOf: models ?? [])
                self?.tableView.reloadData()
                listCount(models?.count ?? 0)
            }) { (error) -> (Void) in
                listCount(0)
            }
        }
    }
    func addTicketStatueView() {
        let ticketStatueView = TicketStatueView(frame: CGRect(x: 0, y: 40, width: KScreenWidth, height: 50))
        ticketStatueView.confignTicketStstueView(WorkOrderTypeModel.ticketStatueModel(WorkOrderTypeModel.TicketType.MaintainListType))
        self.view.addSubview(ticketStatueView)
    }
    func menuView() {
        let menuView = MenuView(frame: CGRect(x: (KScreenWidth - 100.0) / 2, y: 0, width: 120, height: 35), titleArray: ["待办", "已办"])
        menuView.menuBtnClick = { [weak self] index in
            self?.checkStatus = index
            self?.tableView.mj_header?.beginRefreshing()
        }
        menuView.titleFont = UIFont.systemFont(ofSize: 16)
        topView.addSubview(menuView)
    }

}

extension NewMaintainListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newMaintainListModel?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MaintainListCell.reuseIdentifier, for: indexPath) as! MaintainListCell
        cell.configMaintainListCell(newMaintainListModel?[indexPath.row], checkStatus: self.checkStatus)
        cell.rightBtn.rx.tap.bind { [weak self] _  in
            
            let model = self?.newMaintainListModel?[indexPath.row]
            
            //4月15号之前的待办流程增加安全协议
//            if self?.checkStatus == 0, let id = model?.id {
//                self?.planMaintionStart(id)
//            }
            
            //timi 4月15号增加维修的安全通知协议
            if self?.checkStatus == 0
            {
                if let vc = MaintainAgreeMentVC.loadViewControllewWithNib as? MaintainAgreeMentVC
                {
                    object_setClass(vc, MaintainAgreeMentVC.self)
                    vc.maintenId = model?.id
                    self?.show(vc, sender: nil)
                }
            }
            
            //点击已办的查看按钮执行的操作
            if self?.checkStatus == 1
            {
                if let vc = NewMaintainVC.loadViewControllewWithNib as? NewMaintainVC
                {
                    object_setClass(vc, NewMaintainComVC.self)
                    vc.newMaintainListModel = model
                    self?.show(vc, sender: nil)
                }
            }
            
        }.disposed(by: cell.rx.reuseBag)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}

//4月15号之前的待办点击按钮的事件移到了安全协议的页面
//extension NewMaintainListVC{
//    func planMaintionStart(_ planMaintainOrderId: String) {
//        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanMaintainOrderStart), parameter: ["planMaintainOrderId" : planMaintainOrderId], method: .get, success: { (result) -> (Void) in
//
//        }) { (error) -> (Void) in
//
//        }
//    }
//}
