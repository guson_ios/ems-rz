//
//  PickingViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/5.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class PickingViewController: BaseViewController {

    @IBOutlet weak var bottomBtnView: BottomBtnView!
    @IBOutlet weak var tableView: UITableView!
    lazy var materialSystemModel: [MaterialSystemModel] = []
    lazy var materialSystemModelSel: [MaterialSystemModel] = []

    lazy var materialSystemModelView: [MaterialSystemModel] = []

    var resultClosure = {(_ user: [MaterialSystemModel]?) in
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "物资申领"
        self.tableView.register(PickingTableViewCell.nib, forCellReuseIdentifier: PickingTableViewCell.reuseIdentifier)
//        self.segmentedControl()
        self.netWorking("")
        self.bottomBtnView.btn.setTitle("确认", for: .normal)
        self.bottomBtnView.btn.rx.tap.subscribe { [weak self] _ in
            self?.resultClosure(self?.materialSystemModelSel)
            self?.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
    }
    
    private func netWorking(_ materNam: String) {
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
        self.tableView.tableViewRefreshClouse { [weak self] (pageNo, listCount) in
            if pageNo == 1 { self?.materialSystemModel.removeAll()}
            AlamofireNetworking.sharedInstance.requestData(afBaseUrl + emsMaterialSystem, parameter: pageDict(current: pageNo).combinationDict(["materNam" : materNam]), method: .get, success: { (result) -> (Void) in
                if let models =  [MaterialSystemModel].deserialize(from: result["data"]["records"].arrayObject) as? [MaterialSystemModel]{
                    self?.materialSystemModel.append(contentsOf: models)
                    listCount(models.count)
                }else{
                    listCount(0)
                }
            }) { (error) -> (Void) in
                listCount(0)
            }
        }
    }
    
//    func segmentedControl() {
//        let segmented = UISegmentedControl(items: ["选择", "已选"])
//
//        //        UISegmentedControl(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
//        segmented.frame = CGRect(x: 0, y: 0, width: 150, height: 30)
//        segmented.layer.borderWidth = 1
//        segmented.layer.borderColor = UIColor.white.cgColor
//        segmented.setTitleTextAttributes([
//            .foregroundColor: UIColor.white], for: .normal)
//        segmented.setTitleTextAttributes([
//            .foregroundColor: COL3787FF], for: .selected)
//        segmented.addTarget(self, action: #selector(segmentedSel(_:)), for: .valueChanged)
//        segmented.selectedSegmentIndex = 0
//        self.navigationItem.titleView = segmented
//    }
//
//    @objc func segmentedSel(_ segmented: UISegmentedControl)  {
//        print(segmented.selectedSegmentIndex)
//        switch segmented.selectedSegmentIndex {
//        case 0:
//            materialSystemModelView = materialSystemModel
//        case 1:
//            materialSystemModelView = materialSystemModelSel
//        default:
//            break
//        }
//
//    }
}

extension PickingViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.searchText = textField.text ?? ""
//        let models = self.userModels
//        self.userModelsView = models.filter({($0.userName?.contains(textField.text ?? "") ?? false)})
        self.netWorking(textField.text ?? "")
//        self.tableView.reloadData()
    }
}

extension PickingViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.materialSystemModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PickingTableViewCell = tableView.dequeueReusableCell(withIdentifier: PickingTableViewCell.reuseIdentifier, for: indexPath) as! PickingTableViewCell
        
        cell.confignPickingCell(self.materialSystemModel[indexPath.row], numResult: { [weak self] num in
            print(num)
            self?.materialSystemModel[indexPath.row].applyNum = String(num)
            if self?.materialSystemModelSel.contains(where: { (modelSel) -> Bool in
                modelSel.matCod == self?.materialSystemModel[indexPath.row].matCod
            }) ?? false{
               self?.materialSystemModelSel = self?.materialSystemModelSel.map({ (modelSel) -> MaterialSystemModel in
                    if modelSel.matCod == self?.materialSystemModel[indexPath.row].matCod{
                        var model = modelSel
                        model.applyNum = String(num)
                        return model
                    }
                    return modelSel
                    
               }) ?? []
            }else{
                if let modelSel = self?.materialSystemModel[indexPath.row] {
                    self?.materialSystemModelSel.append(modelSel)
                }
            }
            
        })
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(self.materialSystemModelSel)
    }
}
