//
//  DesignBtnView.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/11.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation


class DesignBtnView: UIButton {
    
    var titleText: String?
    
    
    override func draw(_ rect: CGRect) {
        self.backgroundColor = .yellow
//        (self.titleText as NSString?)?.draw(at: self.center, withAttributes: [
//            .foregroundColor: UIColor.red])
        (self.titleText as NSString?)?.draw(in: self.frame, withAttributes: [
        .foregroundColor: UIColor.red,
        .font: UIFont.systemFont(ofSize: 16)])
    }
}
