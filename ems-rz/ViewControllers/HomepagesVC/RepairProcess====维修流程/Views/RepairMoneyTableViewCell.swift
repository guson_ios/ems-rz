//
//  RepairMoneyTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/4/14.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class RepairMoneyTableViewCell: UITableViewCell {
    @IBOutlet weak var repairMoneyCollView: RepairMoneyCollectionView!
    
    @IBOutlet weak var conHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


class RepairMoneyCollectionView: PhotoShowView {
    
    var chartClosure: ((_ selIndex: [Int]?) -> Void)?
    lazy var selIndexs = [Int]()
    
    
    override func registerCollectionViewCell() {
        self.collectionView.register(ChartCollectionViewCell.nib, forCellWithReuseIdentifier: ChartCollectionViewCell.reuseIdentifier)
        self.collectionView.register(RepairMoneySelCell.nib, forCellWithReuseIdentifier: RepairMoneySelCell.reuseIdentifier)
        let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 1
        flowLayout.minimumLineSpacing = 1
        flowLayout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.isScrollEnabled = true
        self.collectionView.bounces = false
        self.collectionView.backgroundColor = COL9FB9F9
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let sections = self.imageArrays as? [[String]] {
            return sections.count
        }
        return 10
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let sections = self.imageArrays as? [[String]] {
            return sections[section].count
        }
        return self.imageArrays?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ChartCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartCollectionViewCell.reuseIdentifier, for: indexPath) as! ChartCollectionViewCell
//        cell.contentView.backgroundColor = COL9FB9F9
        if let sections = self.imageArrays as? [[String]] {
            let text = sections[indexPath.section][indexPath.row]
            cell.label1.text = text
            
            if text.contains("选择Cell") {
                let selCell: RepairMoneySelCell = collectionView.dequeueReusableCell(withReuseIdentifier: RepairMoneySelCell.reuseIdentifier, for: indexPath) as! RepairMoneySelCell
                selCell.selBtn.rx.tap.subscribe { [weak selCell, weak self] _ in
                    selCell?.selBtn.isSelected = !(selCell?.selBtn.isSelected ?? false)
                    if self?.selIndexs.contains(indexPath.row) ?? false {
                        self?.selIndexs.removeAll(where: {$0 == indexPath.row})
                    }else{
                        self?.selIndexs.append(indexPath.row)
                    }
                    self?.chartClosure?(self?.selIndexs)
                }.disposed(by: selCell.rx.reuseBag)
                return selCell
            }
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: 70, height: 34)
        return size
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    }
}
