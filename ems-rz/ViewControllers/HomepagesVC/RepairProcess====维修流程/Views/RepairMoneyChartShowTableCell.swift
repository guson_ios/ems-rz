//
//  RepairMoneyChartShowTableCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/4/16.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class RepairMoneyChartShowTableCell: UITableViewCell {
    @IBOutlet weak var chartCellView: RepairMoneyChartCollectionView!
    
    @IBOutlet weak var conHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
