//
//  RepairUnTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/16.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class RepairUnTableViewCell: UITableViewCell {
    @IBOutlet weak var leftLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confignRepairUNCell(_ equModel: EquipmentInfoDetail) {
        leftLabel.text = equModel.infoKey
    }
}
