//
//  CosmosTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/4.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
import Cosmos

class CosmosTableViewCell: UITableViewCell {
    @IBOutlet weak var cosmosView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
