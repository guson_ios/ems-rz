//
//  PickingTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/10.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class PickingTableViewCell: UITableViewCell {
    @IBOutlet weak var numberCalculateView: NumberCalculate!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func confignPickingCell(_ model: MaterialSystemModel?, numResult: @escaping ((Int) -> Void)) {
        label1.text = model?.materNam
        label3.text = model?.materKindNam
        label4.text = model?.matCod
        label2.text = "库存" + (model?.stockNum ?? "") + (model?.cunitNam ?? "")
        numberCalculateView.showNum = Int(model?.applyNum ?? "0") ?? 0
        self.numberCalculateView.resultNumber = { (num: Int) in
            numResult(num)
        }
    }
    
}
