//
//  RepairMoneyChartCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/4/14.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit




class RepairMoneyChartCell: UITableViewCell {
    @IBOutlet weak var conHeight: NSLayoutConstraint!
    
    @IBOutlet weak var chartCellView: RepairMoneyChartCollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

struct RepairMoneyChartViewModel {
    var text: String?
    var textFieldEnable: Bool? = false
}

class RepairMoneyChartCollectionView: PhotoShowView {
    
//    var chartClosure: ((_ selIndex: [Int]?) -> Void)?
    var chartClosure: ((_ imageArrays: [Any]?, _ delIdx: Int?) -> Void)?
    var chartSubClosure: ((_ imageArrays: [Any]?) -> Void)?
    lazy var selIndexs = [Int]()
    
    
    override func registerCollectionViewCell() {
        self.collectionView.register(RepairMoneyChartCollectionViewCell.nib, forCellWithReuseIdentifier: RepairMoneyChartCollectionViewCell.reuseIdentifier)
        self.collectionView.register(ChartCollectionViewCell.nib, forCellWithReuseIdentifier: ChartCollectionViewCell.reuseIdentifier)

        let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 1
        flowLayout.minimumLineSpacing = 1
        flowLayout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.isScrollEnabled = true
        self.collectionView.bounces = false
        self.collectionView.backgroundColor = COL9FB9F9
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let sections = self.imageArrays as? [[RepairMoneyChartViewModel]] {
            return sections.count
        }
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let sections = self.imageArrays as? [[RepairMoneyChartViewModel]] {
            return sections[section].count
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RepairMoneyChartCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: RepairMoneyChartCollectionViewCell.reuseIdentifier, for: indexPath) as! RepairMoneyChartCollectionViewCell
        if let sections = self.imageArrays as? [[RepairMoneyChartViewModel]] {
            let rModel = sections[indexPath.section][indexPath.row]
            
            if rModel.textFieldEnable ?? false {
                cell.textField.text = rModel.text
                cell.textField.rx.controlEvent([.editingDidEnd]) //状态可以组合
                    .asObservable()
                    .subscribe(onNext: { [weak cell, weak self]  _ in
                        var ima = sections[indexPath.section]
                        ima[indexPath.row].text = cell?.textField.text
                        if cell?.textField.text != rModel.text{
                            self?.imageArrays?[indexPath.section] = ima
                        }
                        self?.chartSubClosure?(self?.imageArrays)
                    }).disposed(by: cell.rx.reuseBag)
            }else{
                let chartCell: ChartCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartCollectionViewCell.reuseIdentifier, for: indexPath) as! ChartCollectionViewCell
                chartCell.label1.text = rModel.text
                return chartCell
            }
            
            
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if (self.imageArrays as? [[RepairMoneyChartViewModel]]) != nil {
//            let rModel = sections[indexPath.section][indexPath.row]
//            if rModel.textFieldEnable ?? false {
//                return CGSize(width: 150, height: 34)
//            }
//            return CGSize(width:70, height: 34)
//        }
        let size = CGSize(width: 70, height: 34)
        return size

    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let array = self.imageArrays as? [[RepairMoneyChartViewModel]] {
            if array[indexPath.section][indexPath.row].text == "删除" {
                for (i, arraySec) in array.enumerated() {
                    var arr = arraySec
                    arr.remove(at: indexPath.row)
                    self.imageArrays?[i] = arr
                }
                if (self.imageArrays?[indexPath.section] as? [RepairMoneyChartViewModel])?.count == 1 {
                    self.imageArrays = nil
                }
                self.chartClosure?(self.imageArrays, indexPath.row - 1)
            }
        }
    }
}

