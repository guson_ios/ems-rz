//
//  RepairListTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/21.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class RepairListTableViewCell: UITableViewCell {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label6: UILabel!
    @IBOutlet weak var label7: UILabel!
    @IBOutlet weak var label8: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.addShadowWithBezierPath()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confignRepairListCell(_ model: RepairListModel) {
        self.label1.text = "工单号：" + (model.dailyRecordCode ?? "")
        self.label2.text = model.detailedStatusStr()
        self.label3.text = "设备名称：" + (model.equipName ?? "")
        self.label4.text = "部位：" + (model.equipPartName ?? "")
        self.label5.text = "报修人：" + (model.faultRepairUser ?? "")
        self.label6.text = "报修时间：" + (model.faultRepairTime ?? "")
        self.label7.text = "设备所属单位：" + (model.useCompanyName ?? "")
        self.label8.text = "使用部门：" + (model.useOrgName ?? "")
    }
    
}
