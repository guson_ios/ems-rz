//
//  SelPersonViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/4.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit


enum RoleModelENUM: String {
    case 技术员 = "1"
    case 班组 = "2"///gaofei
    case 司机 = "3"///dinglifeng
    case 维修队技术员 = "4" ///qinjian
}

class SelPersonViewController: BaseViewController {
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var roleType: RoleModelENUM?
    
    var searchMaintainFlag: String = "0"
    var searchText = ""
    lazy var userModels = [SystemLoginUserInfoList]()
    
    var userModelsView = [SystemLoginUserInfoList]()

//    var clouse =  ((_ user: UserInfoDetailModel) -> Void)?
    var resultClosure = {(_ user: SystemLoginUserInfoList?) in
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "人员选择"
        tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        let par = ["useOrgId" : EmsGlobalObj.obj.userInfo?.useOrgId ?? "", "searchMaintainFlag" : self.searchMaintainFlag]
        print(par)
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsUsersRoleUseList), parameter: par, method: .get, success: { (result) -> (Void) in
            
            if let models = [RoleUseList].deserialize(from: result["data"].arrayObject) as? [RoleUseList]{
                let roleModels =  models.filter { (roleModel) -> Bool in
                    roleModel.roleType == self.roleType?.rawValue
                }
                if roleModels.count > 0 {
                    if let sysModels =  roleModels.first?.systemLoginUserInfoList {
                        self.userModels.append(contentsOf: sysModels)
                        self.userModelsView.append(contentsOf: sysModels)
                    }
                }

            }
            self.tableView.reloadData()
        }) { (error) -> (Void) in

        }
    }
}

extension SelPersonViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.searchText = textField.text ?? ""
        let models = self.userModels
        self.userModelsView = models.filter({($0.userName?.contains(textField.text ?? "") ?? false)})
        
        self.tableView.reloadData()
    }
}
extension SelPersonViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userModelsView.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UniversalTableViewCell = tableView.dequeueReusableCell(withIdentifier: UniversalTableViewCell.reuseIdentifier, for: indexPath) as! UniversalTableViewCell
        cell.confignUniversalCell(.imageHide)
        cell.leftLabel.text = self.userModelsView[indexPath.row].userName
        cell.rightLabel.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.resultClosure(self.userModelsView[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
}
