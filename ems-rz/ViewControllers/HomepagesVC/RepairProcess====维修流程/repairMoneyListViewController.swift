//
//  repairMoneyListViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/4/14.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class repairMoneyListViewController: BaseViewController {

    @IBOutlet weak var bottomTwoBtnView: BottomTwoBtnView!
    @IBOutlet weak var tableView: UITableView!
    lazy var repairMoneyModels = [[String]]()
    lazy var repairModels = [RepairMoneyModel]()
    var selIndexs: [Int]? = [Int]()
    var chartSelClosure: ((_ selModels: [RepairMoneyModel]?) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(RepairMoneyTableViewCell.nib, forCellReuseIdentifier: RepairMoneyTableViewCell.reuseIdentifier)
        tableView.register(RepairMoneyChartCell.nib, forCellReuseIdentifier: RepairMoneyChartCell.reuseIdentifier)

        bottomTwoBtnView.leftBtn.setTitle("提交", for: .normal)
        bottomTwoBtnView.rightBtn.setTitle("取消", for: .normal)
        bottomTwoBtnView.leftBtn.rx.tap.subscribe { [weak self] _ in
            if let selIndex = self?.selIndexs{
                var models = [RepairMoneyModel]()
                for (_ , index) in selIndex.enumerated(){
                    if let rModel = self?.repairModels[index - 1] {
                        models.append(rModel)
                    }
                }
                self?.chartSelClosure?(models)
                self?.navigationController?.popViewController(animated: true)
            }
        }.disposed(by: disposeBag)
        bottomTwoBtnView.rightBtn.rx.tap.subscribe { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
        
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsStandardRepairMoney), parameter: pageDict("10000", current: 1), method: .post,encoding: "{}", headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
            if let models = [RepairMoneyModel].deserialize(from: result["data"]["records"].arrayObject) as? [RepairMoneyModel]{
                self.repairModels = models
                self.repairMoneyModels = RepairMoneyModel().repairMoneyModels(materials: models)
                self.tableView.reloadData()
            }
        }) { (error) -> (Void) in
            
        }
        

    }
}

extension repairMoneyListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepairMoneyTableViewCell.reuseIdentifier, for: indexPath) as! RepairMoneyTableViewCell
        cell.repairMoneyCollView.chartClosure = {[weak self] selIndexs in
            self?.selIndexs = selIndexs
        }
        cell.repairMoneyCollView.imageArrays = self.repairMoneyModels
        if self.repairMoneyModels.count > 0{
            cell.conHeight.constant = CGFloat(self.repairMoneyModels[0].count * 36)
        }
        return cell
    }
}
