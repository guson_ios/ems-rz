//
//  ChairCollTestViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/5.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class ChairCollTestViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.register(ChartTableViewCell.nib, forCellReuseIdentifier: ChartTableViewCell.reuseIdentifier)

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChairCollTestViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: ChartTableViewCell.reuseIdentifier, for: indexPath) as! ChartTableViewCell
        cell.chartCollectionView.imageArrays = ["sfae","asfe","ddk","sfae","asfe"]
//        let collectViewWidth = cell.chartCollectionView.collectionView.collectionViewLayout.collectionViewContentSize.width
//        if cell.chartCollectionView.collectionView.contentSize.width != collectViewWidth  {
//            cell.chartCollectionView.collectionView.contentSize = CGSize(width: collectViewWidth - 50, height: cell.chartCollectionView.collectionView.contentSize.height)
//        }
//        if self.scrollView.contentSize.width != collectViewWidth {
//            self.scrollView.contentSize = CGSize(width: collectViewWidth, height: KScreenHeight - self.equSummaryViewHeight)
//            tableView.frame = CGRect(x: 0, y: 0, width: collectViewWidth, height: KScreenHeight - self.equSummaryViewHeight)
//        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 216
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
}
