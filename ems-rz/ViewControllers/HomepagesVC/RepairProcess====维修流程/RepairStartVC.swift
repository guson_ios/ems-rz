//
//  RepairStartVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/3.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class RepairStartVC: BaseViewController {

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBtnView: BottomBtnView!
    @IBOutlet weak var tableView: UITableView!
    var repairListModel: RepairListModel?///之前页面传过来的
    var checkStatus = 0

    // MARK: - 步骤1、4
    lazy var repairStartModel = RepairStartModel(isPicking: "0", isStop: "0")///开始维修
    lazy var materialModels = [[String]]()///物资ViewModel
    lazy var repairMoneyModels = [[RepairMoneyChartViewModel]]()///定额维修项目ViewModel
    
    // MARK: - 步骤2
    lazy var repairConfirmNewFault = RepairConfirmNewFault()///步骤2是否发现新故障
    var faultCodesModel: [RepaireFaultCodeModel]?///故障原因
    lazy var simplePickView: SimplePickView = {
        let pickView = SimplePickView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight))
        self.view.addSubview(pickView)
        pickView.simplePickClosure = {[weak self] row , smodel in
            switch smodel {
            
            case let faultCodeModel as RepaireFaultCodeModel :
                self?.repairConfirmNewFault.faultId = faultCodeModel.id
                self?.repairConfirmNewFault.faultCode = faultCodeModel.faultCode
                self?.repairConfirmNewFault.faultPhenomenon = faultCodeModel.faultPhenomenon
                self?.confignViewModel()
                
            default:
                break
            }
        }
        return pickView
    }()
    
    lazy var menuScrView: MenuScrollerView? = {
        
        
        
        if let titles = self.repairDetailsModel?.processResultRespVoList?.map({ (rModel) -> String in
            return rModel.nodeName ?? "nodeName"
        }), titles.count > 0{
            let menuScrView: MenuScrollerView =  MenuScrollerView(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 50), withTitleItems: titles)!
            menuScrView.backgroundColor = COLF4F4F4
            menuScrView.setMenuViewDidSelectedAtIndex {[weak self] (index) in
                self?.repairDetailsModel?.processResultStepName = titles[index]
                if let vm = repairProcessMaintainViewModel(self?.repairDetailsModel){
                    self?.tableViewViewModel[1] = vm
                }
//                self?.tableView.reloadData()
                self?.tableView.beginUpdates()
                self?.tableView.reloadSections(IndexSet(integer: 1), with: .none)
                self?.tableView.endUpdates()
            }
            menuScrView.selectedIndex = titles.count - 1
            self.tableView.reloadData()
            return menuScrView
        }
        return nil
    }()

    // MARK: - 步骤6
    lazy var repaireAcceptanceModel = RepaireAcceptanceModel()///技术员/班长/司机验收
    var repairDetailsModel: RepairDetailsModel?///详情
    
    
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(SwitchTableViewCell.nib, forCellReuseIdentifier: SwitchTableViewCell.reuseIdentifier)
        tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        tableView.register(NextGrowingCell.nib, forCellReuseIdentifier: NextGrowingCell.reuseIdentifier)
        tableView.register(CosmosTableViewCell.nib, forCellReuseIdentifier: CosmosTableViewCell.reuseIdentifier)
        tableView.register(ChartTableViewCell.nib, forCellReuseIdentifier: ChartTableViewCell.reuseIdentifier)
        tableView.register(PhotoSelectShowCell.nib, forCellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier)
        tableView.register(PhotoShowTableViewCell.nib, forCellReuseIdentifier: PhotoShowTableViewCell.reuseIdentifier)
        tableView.register(RecordTableViewCell.nib, forCellReuseIdentifier: RecordTableViewCell.reuseIdentifier)
        tableView.register(RepairUnTableViewCell.nib, forCellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier)
        tableView.register(RepairMoneyChartCell.nib, forCellReuseIdentifier: RepairMoneyChartCell.reuseIdentifier)
        tableView.register(RepairMoneyChartShowTableCell.nib, forCellReuseIdentifier: RepairMoneyChartShowTableCell.reuseIdentifier)

        tableView.register(RepairMoneyAddTableViewCell.nib, forCellReuseIdentifier: RepairMoneyAddTableViewCell.reuseIdentifier)

        
        

        self.confignViewModel()
        self.requestRepairDetailData()
        self.bottomConstraint.constant = self.checkStatus == 0 ? 50 : 0
        self.bottomBtnView.btn.rx.tap.subscribe { [weak self] _ in
            
            switch self?.repairListModel?.detailedStatus {
            case "1":
                self?.repairStartModel.id = self?.repairListModel?.id

                self?.repairStartModel.applyRequestDetail =   self?.repairStartModel.materialSystemModels?.map({ (model) -> MaterialSystemRequestModel in
                    var model1 = MaterialSystemRequestModel()
                    model1.materName = model.materNam
                    model1.materCode = model.matCod
                    model1.price = model.priceVal
                    model1.materKindName = model.materKindNam
                    model1.standModelTxt = model.standModelTxt
                    model1.unitName = model.cunitNam
                    return model1
                })
                self?.repairStartModel.materialSystemModels = nil
                print(self?.repairStartModel.toJSONString() as Any)
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainStart), parameter: nil, method: .post, encoding: self?.repairStartModel.toJSONString() ?? "{}", headers: headersFunc(contentType: .json),  success: { (result) -> (Void) in
                    self?.popVc()
                }) { (error) -> (Void) in
                    
                }
            case "2":
                self?.repairConfirmNewFault.id = self?.repairListModel?.id
                self?.repairConfirmNewFault.faultFoundTime = String( Int64((Date().timeIntervalSince1970) * 1000))
                self?.repairConfirmNewFault.faultPhenomenon = nil
                self?.repairConfirmNewFault.faultCode = nil
                print(self?.repairConfirmNewFault.toJSONString() as Any)
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainConfirmNewFault), parameter: nil, method: .post, encoding: self?.repairConfirmNewFault.toJSONString() ?? "{}", headers:  headersFunc(contentType: .json), progressPlugin: self,  success: { (result) -> (Void) in
                    self?.popVc()
                }) { (error) -> (Void) in
                    
                }
            case "3", "4":
                self?.repairStartModel.id = self?.repairListModel?.id
                self?.repairStartModel.applyRequestDetailList =   self?.repairStartModel.materialSystemModels?.map({ (model) -> MaterialSystemRequestModel in
                    var model1 = MaterialSystemRequestModel()
                    model1.materName = model.materNam
                    model1.materCode = model.matCod
                    model1.price = model.priceVal
                    model1.materKindName = model.materKindNam
                    model1.standModelTxt = model.standModelTxt
                    model1.unitName = model.cunitNam
                    return model1
                })
                self?.repairStartModel.materialSystemModels = nil
                self?.repairStartModel.isStop = nil
                
                print(self?.repairStartModel.toJSONString() as Any)

                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainIsPickingConfirm), parameter: nil, method: .post, encoding: self?.repairStartModel.toJSONString() ?? "{}", headers: headersFunc(contentType: .json), progressPlugin: self, success: { (result) -> (Void) in
                    self?.popVc()
                }) { (error) -> (Void) in
                    
                }
                break
            case "5":
                let par = ["id" : self?.repairListModel?.id ?? ""]
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainComplete), parameter: nil, method: .post, encoding: par.dictToJsonString(), headers: headersFunc(contentType: .json), progressPlugin: self,  success: { (result) -> (Void) in
                    self?.popVc()
                }) { (error) -> (Void) in
                    
                }
            case "6":
                if self?.repairDetailsModel?.maintainDailyRespVo?.accepStepStatus == "2" {
                    if self?.repairDetailsModel?.maintainDailyRespVo?.maintainBranch == "1" {
                        if self?.repaireAcceptanceModel.driverId == nil || self?.repaireAcceptanceModel.squadLeaderId == nil {
                            ZFToast.show(withMessage: "请先选择验收人员")
                            return
                        }
                    }else{
                        if self?.repaireAcceptanceModel.driverId == nil || self?.repaireAcceptanceModel.squadLeaderId == nil, self?.repaireAcceptanceModel.technologyId == nil {
                            ZFToast.show(withMessage: "请先选择验收人员")
                            return
                        }
                    }
                }
                let group = DispatchGroup.init()
                self?.uploadFile(group, files: self?.repaireAcceptanceModel.photos?.map({ (model) -> Data? in
                    if let model = model as? ZLPhotoBrowerModel{
                        return model.imageData
                    }
                    return nil
                }) as? [Data], fileName: "file.png", mineType: "image/png", maintainDaily: "Photos")
                
                group.notify(queue: DispatchQueue.main) {
                    self?.repaireAcceptanceModel.photos = nil
                    self?.repaireAcceptanceModel.id = self?.repairListModel?.id
                    if let rmodels = self?.repairMoneyModels{
                        for (i, models) in rmodels.enumerated(){
                            var vModels = models
                            vModels.remove(at: 0)
                            for (j, dModels) in vModels.enumerated() {
                                if i == 1 {
                                    self?.repairDetailsModel?.planRepairStandardInfoRespVoList?[j].repairProject = dModels.text
                                }
                                if i == 2 {
                                    self?.repairDetailsModel?.planRepairStandardInfoRespVoList?[j].repairNumber = dModels.text
                                }
                                if i == 3 {
                                    self?.repairDetailsModel?.planRepairStandardInfoRespVoList?[j].repairUnit = dModels.text
                                }
                                if i == 4 {
                                    self?.repairDetailsModel?.planRepairStandardInfoRespVoList?[j].repairRemark = dModels.text
                                }
                                
                            }
                        }
                    }
                    
//                    print(self?.repairDetailsModel?.planRepairStandardInfoRespVoList)
                    self?.repaireAcceptanceModel.planRepairStandardInfoRespVoList = self?.repairDetailsModel?.planRepairStandardInfoRespVoList
                    
                    self?.repaireAcceptanceModel.acceptanceEvaluate = self?.repaireAcceptanceModel.acceptanceEvaluate ?? " "
                    print(self?.repaireAcceptanceModel.toJSONString() as Any)
                    
                    AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainAcceptanceUser), parameter: nil, method: .post, encoding: self?.repaireAcceptanceModel.toJSONString() ?? "", headers: headersFunc(contentType: .json), progressPlugin: self, success: { (result) -> (Void) in
                        self?.popVc()
                    }) { (error) -> (Void) in

                    }
                }
                
                
            default:
                break
            }
            
        }.disposed(by: disposeBag)
        
    }
    
    // MARK: - 详情
    func requestRepairDetailData() {
        if let id =  self.repairListModel?.id{
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainDailyDetails), parameter: ["dailyId" : id], method: .get, success: { (result) -> (Void) in
                self.repairDetailsModel = RepairDetailsModel.deserialize(from: result["data"].dictionaryObject)
                let ps = self.repairDetailsModel?.planRepairStandardInfoRespVoList
                self.repairDetailsModel?.planRepairStandardInfoRespVoPostList = ps///用来显示
                // 1：维修队技术员验收 2：设备队技术员验收 3：联合验收
                //1的时候acceptanceUserList里的planRepairStandardInfoRespVoList 可问后台，替换掉外层的就可以
//                self.repairDetailsModel?.planRepairStandardInfoRespVoList = self.repairDetailsModel?.acceptanceUserList?.first.planRepairStandardInfoRespVoList
                if var lastModel = self.repairDetailsModel?.processResultRespVoList?.last{
                    lastModel.nodeName = lastModel.nextNodeName
                    self.repairDetailsModel?.processResultRespVoList?.append(lastModel)
                }
                self.confignViewModel()
            }) { (error) -> (Void) in
                
            }
        }
    }
    
    func confignViewModel() {
        switch self.repairListModel?.detailedStatus {
        case "1":
            self.bottomBtnView.btn.setTitle("开始维修", for: .normal)
            if  let vm =  RepaireStartViewModel.repaireStartViewModels(self.repairStartModel, repairDetailModel: self.repairDetailsModel){
                tableViewViewModel = vm
            }
        case "2":
            self.bottomBtnView.btn.setTitle("确认", for: .normal)
            if  let vm =  RepairConfirmNewFaultViewModel.repaireConfirmNewFaultViewModels(self.repairConfirmNewFault, repairDetailModel: self.repairDetailsModel){
                tableViewViewModel = vm
            }
        case "3", "4":
            self.bottomBtnView.btn.setTitle("提交", for: .normal)
            if  let vm =  RepaireStartViewModel.maintainIsPickingConfirm(self.repairStartModel, repairDetailModel: self.repairDetailsModel){
                tableViewViewModel = vm
            }
            break
        case "5":
            self.bottomBtnView.btn.setTitle("提交验收", for: .normal)
            if  let vm =  RepaireAcceptanceSubmitViewModel.repaireAcceptanceSubmitViewModels(self.repairStartModel, repairDetailModel: self.repairDetailsModel){
                tableViewViewModel = vm
            }
        case "6":
            self.bottomBtnView.btn.setTitle("维修验收", for: .normal)
            if  let vm =  RepaireStartViewModel.repaireAcceptanceViewModels(self.repaireAcceptanceModel, repairDetailModel: self.repairDetailsModel){
                tableViewViewModel = vm
            }
        default:
            ///其他状态
            if  let vm =  RepaireAcceptanceSubmitViewModel.repaireAcceptanceSubmitViewModels(self.repairStartModel, repairDetailModel: self.repairDetailsModel){
                tableViewViewModel = vm
            }
        }
        if self.checkStatus != 0, tableViewViewModel.count > 2 {
            tableViewViewModel.removeSubrange(1..<(tableViewViewModel.count - 1))
        }
        self.tableView.reloadData()
    }
    
    
    
    private func popVc() {
        ZFToast.show(withMessage: "请求成功")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension RepairStartVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            if indexPath.section != 0{
                switch (cell, model) {
                case (let cell as SwitchTableViewCell, let eModel as EquipmentInfoDetail):
                    
                    var mModel = self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? EquipmentInfoDetail
                    cell.rightSwitch.rx.isOn.bind { [weak self] (isOn) -> Void in
                        mModel?.infoValue = boolToStringValue(isOn)
                        self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = mModel!
                        
                        switch (eModel.infoKey, self?.repairListModel?.detailedStatus) {
                        case ("是否停机", "1"):
                            self?.repairStartModel.isStop = boolToStringValue(isOn)
                        case ("是否领料", "1"), ("是否领料", "4"), ("是否领料", "3"):
                            self?.repairStartModel.isPicking = boolToStringValue(isOn)
                            if isOn, isOn != eModel.infoValue?.stringBoolValue() {
                                if let vc = PickingViewController.loadViewControllewWithNib as? PickingViewController {
                                    vc.resultClosure = { [weak self] models in
                                        self?.repairStartModel.isPicking = models?.count ?? 0 <= 0 ? "0" : "1"
                                        self?.repairStartModel.materialSystemModels = models
                                        self?.materialModels = MaterialSystemModel().materialModels(nil, materials: models)
                                        self?.confignViewModel()
                                    }
                                    self?.show(vc, sender: nil)
                                }
                            }else if !isOn, isOn != eModel.infoValue?.stringBoolValue(){
                                self?.repairStartModel.materialSystemModels = nil
                                self?.materialModels = []
                                self?.confignViewModel()
                            }
                        case ("是否验收通过", "6"):
                            self?.repaireAcceptanceModel.acceptanceStatus = boolToStringValue(isOn)
                            
                        case ("是否发现新故障", "2"):
                            self?.repairConfirmNewFault.isNewFault = boolToStringValue(isOn)
                        case ("是否更正原有故障信息", "2"):
                            if self?.repairConfirmNewFault.isUpdateFault != boolToStringValue(isOn) {
                                if !isOn {
                                    self?.repairConfirmNewFault = RepairConfirmNewFault()
                                }
                                self?.repairConfirmNewFault.isUpdateFault = boolToStringValue(isOn)
                                self?.confignViewModel()
                            }
                            self?.repairConfirmNewFault.isUpdateFault = boolToStringValue(isOn)
                        case ("是否停机", "2"):
                            self?.repairConfirmNewFault.isStop = boolToStringValue(isOn)
                        case ("应急维修", "2"):
                            self?.repairConfirmNewFault.isEmergencyMaintain = boolToStringValue(isOn)
                        default:
                            break
                        }
                    }.disposed(by: cell.rx.reuseBag)
                    break
                case (let cell as ChartTableViewCell, _):
                    cell.chartCollectionView.imageArrays = self?.materialModels
                    cell.chartCollectionView.chartClosure = {[weak self] (imageArrays, delIdx) in
                        if let ims = imageArrays as? [[String]] {
                            self?.materialModels = ims
                            self?.repairStartModel.materialSystemModels?.remove(at: delIdx!)
                            self?.tableView.reloadRows(at: [indexPath], with: .none)
                        }else{
                            self?.repairStartModel.isPicking = "0"
                            self?.repairStartModel.materialSystemModels = nil
                            self?.materialModels = []
                            self?.confignViewModel()
                        }
                    }
                    if let im = self?.materialModels, im.count > 0{
                        cell.conHeight.constant = CGFloat(im[0].count * 36)
                    }
                case (let cell as CosmosTableViewCell, _):
                    self?.repaireAcceptanceModel.acceptanceScore = String(Int(cell.cosmosView.rating))
                    cell.cosmosView.didFinishTouchingCosmos = { [weak self] cosmosValue in
                        print(cosmosValue)
                        self?.repaireAcceptanceModel.acceptanceScore = String(Int(cosmosValue))
                    }
                case (let cell as NextGrowingCell, let eModel as EquipmentInfoDetail):
                    cell.nextGrowingTextView.textView.text = self?.repaireAcceptanceModel.acceptanceEvaluate
                    cell.nextGrowingTextView.textView.rx.text.bind { [weak self] (text) -> Void in
                        switch (eModel.infoKey, self?.repairListModel?.detailedStatus) {
                        case ("验收评价", "6"):
                            self?.repaireAcceptanceModel.acceptanceEvaluate = text
                        default:
                            break
                        }
                    }.disposed(by: cell.rx.reuseBag)
                    
                case (let cell as PhotoSelectShowCell,  _ as PhotoShowModel):
                cell.photoSelectShowView.imageArrays = self?.repaireAcceptanceModel.photos
                cell.photoSelectShowView.closure = {[weak self] imageArrays in
                    self?.repaireAcceptanceModel.photos = imageArrays
                    self?.tableView.reloadRows(at: [indexPath], with: .none)
                }
                case (let cell as RepairMoneyChartCell,  _ as RepairMoneyShowModel ):
                    cell.chartCellView.chartClosure = {[weak self] (imageArrays, delIdx) in
                        self?.repairDetailsModel?.planRepairStandardInfoRespVoList?.remove(at: delIdx!)
                        if let vm = repairMoneyViewModel(self?.repairDetailsModel){
                            self?.tableViewViewModel[4] = vm
                        }
                        self?.tableView.beginUpdates()
                        self?.tableView.reloadSections(IndexSet(integer: 4), with: .none)
                        self?.tableView.endUpdates()
                    }
                    cell.chartCellView.chartSubClosure = { [weak self] imageArrays in
                       
                        if let im = imageArrays as? [[RepairMoneyChartViewModel]]{
                            self?.repairMoneyModels = im
                        }
                    }
                    let vm = RepairMoneyModel().repairMoneyChartModels(materials: self?.repairDetailsModel?.planRepairStandardInfoRespVoList)
                    
                    cell.chartCellView.imageArrays = vm
                    if  vm.count > 0{
                        cell.conHeight.constant = CGFloat(vm[0].count * 36)
                    }
                case (let cell as RepairMoneyChartShowTableCell,  _ as EquipmentInfoDetail ):
                    let vm = RepairMoneyModel().repairMoneyChartModelsShow(materials: self?.repairDetailsModel?.planRepairStandardInfoRespVoPostList)
                    cell.chartCellView.imageArrays = vm
                    if  vm.count > 0{
                        cell.conHeight.constant = CGFloat(vm[0].count * 36)
                    }
                case (let cell as RepairMoneyAddTableViewCell, _):
                    cell.leftBtn.rx.tap.subscribe {[weak self] _ in
                        if let vc = repairMoneyListViewController.loadViewControllewWithNib as? repairMoneyListViewController {
                            vc.chartSelClosure = { [weak self] rModels in

                                if  let pModels = rModels?.map({ (rModel) -> RepairDetailsModel.PlanRepairStandardInfoRespVoModel in
                                    var pModel = RepairDetailsModel.PlanRepairStandardInfoRespVoModel()
                                    pModel.flowId = self?.repairDetailsModel?.maintainDailyRespVo?.flowId
                                    pModel.flowType = "1"
                                    pModel.standardFlag = "0"
                                    pModel.repairProject = rModel.repairProject
                                    pModel.repairNumber = "1"
                                    pModel.repairMoney = rModel.repairMoney
                                    pModel.standardRepairMoneyId = rModel.id
                                    pModel.repairUnit = rModel.unitName
                                    pModel.repairRemark = ""
                                    // 1：维修队技术员验收 2：设备队技术员验收 3：联合验收
                                    pModel.node = self?.repairDetailsModel?.maintainDailyRespVo?.accepStepStatus
                                    return pModel
                                }){

                                    self?.repairDetailsModel?.planRepairStandardInfoRespVoList?.append(contentsOf: pModels)
                                    if let vm = repairMoneyViewModel(self?.repairDetailsModel){
                                        self?.tableViewViewModel[4] = vm
                                    }
                                    self?.tableView.beginUpdates()
                                    self?.tableView.reloadSections(IndexSet(integer: 4), with: .none)
                                    self?.tableView.endUpdates()
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                        self?.tableView.setContentOffset(CGPoint.zero, animated: true)
                                    }
                                }
                            }
                            self?.show(vc, sender: nil)
                        }
                    }.disposed(by: cell.rx.reuseBag)
                    cell.rightBtn.rx.tap.subscribe {[weak self] _ in
                        
                        var pModel = RepairDetailsModel.PlanRepairStandardInfoRespVoModel()
                        pModel.flowId = self?.repairDetailsModel?.maintainDailyRespVo?.flowId
                        pModel.flowType = "1"
                        pModel.standardFlag = "1"
                        pModel.repairNumber = "1"
                        pModel.repairRemark = ""
                        pModel.standardRepairMoneyId = ""
                        pModel.repairUnit = "项"
                        pModel.repairMoney = ""
                        pModel.repairProject = ""
                        // 1：维修队技术员验收 2：设备队技术员验收 3：联合验收
                        pModel.node = self?.repairDetailsModel?.maintainDailyRespVo?.accepStepStatus
                        self?.repairDetailsModel?.planRepairStandardInfoRespVoList?.append(pModel)
                        if let vm = repairMoneyViewModel(self?.repairDetailsModel){
                            self?.tableViewViewModel[4] = vm
                        }
                        self?.tableView.beginUpdates()
                        self?.tableView.reloadSections(IndexSet(integer: 4), with: .none)
                        self?.tableView.endUpdates()
                        
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                            self?.tableView.setContentOffset(CGPoint.zero, animated: true)
//                        }
                    }.disposed(by: cell.rx.reuseBag)
                    break
                default:
                    break
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let model = self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model
        switch (model, self.repairListModel?.detailedStatus) {
        case (let model as EquipmentInfoDetail, "6"):
            if model.infoKey?.contains("指派验收") ?? false {
                if let vc = SelPersonViewController.loadViewControllewWithNib as? SelPersonViewController {
                    vc.resultClosure = { [weak self] userInfo in///keyiyouhua
                        var tranModel = model
                        tranModel.infoValue = userInfo?.userName
                        self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = tranModel
                        if model.infoKey?.contains("班长") ?? false {
                            self?.repaireAcceptanceModel.squadLeaderId = userInfo?.id
                        }
                        if model.infoKey?.contains("司机") ?? false {
                            self?.repaireAcceptanceModel.driverId = userInfo?.id
                        }
                        if model.infoKey?.contains("维修队") ?? false {
                            self?.repaireAcceptanceModel.technologyId = userInfo?.id
                        }
                        self?.tableView.reloadRows(at: [indexPath], with: .none)
                    }
                    if model.infoKey?.contains("班长") ?? false {
                        vc.roleType = .班组
                    }
                    if model.infoKey?.contains("司机") ?? false {
                        vc.roleType = .司机
                    }
                    if model.infoKey?.contains("维修队") ?? false {
                        vc.roleType = .维修队技术员
                    }
                    vc.searchMaintainFlag = (model.infoKey?.contains("维修队"))! ? "1" : "0"
                    self.show(vc, sender: nil)
                }
            }
        case (let model as EquipmentInfoDetail, "2"):
            if model.infoKey == "故障代码" {
                if self.faultCodesModel == nil {
                    self.faultManageList()
                }else{
                    self.simplePickView.pickerModels = self.faultCodesModel
                }
            }
            
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        if section == 0, self.repairDetailsModel != nil {
            return self.menuScrView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 50.001 : 20.001
    }
}


extension RepairStartVC{
    // MARK: - 故障原因
    func faultManageList() {
        
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsFaultManageList), parameter: ["equipId" : self.repairDetailsModel?.maintainDailyRespVo?.equipId ?? "", "equipPartId" : self.repairDetailsModel?.maintainDailyRespVo?.equipPartId ?? "", "equipPartsId" : self.repairDetailsModel?.maintainDailyRespVo?.equipPartsId ?? ""], method: .get, headers: headersFunc(contentType: .json), progressPlugin: nil, success: { (result) -> (Void) in
            self.faultCodesModel = [RepaireFaultCodeModel].deserialize(from: result["data"].arrayObject) as? [RepaireFaultCodeModel]
            self.simplePickView.pickerModels = self.faultCodesModel
        }) { (error) -> (Void) in
            
        }
    }
    
     // MARK: - 文件上传
        func uploadFile(_ group: DispatchGroup, files: [Data]?, fileName: String, mineType: String, maintainDaily: String) {
    //        print(fileName)
            if files?.count ?? 0 > 0 {
                for (_, file) in ((files?.enumerated())!) {
                    if file.count > 0 {
                        group.enter()
                        AlamofireNetworking.sharedInstance.uploadFile({ (formData) in
                            if file.count > 0{
                                formData.append(file, withName: "file", fileName: fileName, mimeType: mineType)
                            }
                        }, urlString: afUrl(addUrl: emsFilesUpload), headers: headersFunc(contentType: .formData), progressPlugin: self, success: { (result) -> (Void) in
                            switch maintainDaily {
                            case "Photos":
                                self.repaireAcceptanceModel.fileReqVoList?.append(["name" : "maintainPhoto.png", "url" : result["data"].stringValue])
//                            case "Video":
//                                self.repaireModel.maintainDailyVideo?.append(["name" : "maintainVideo.mp4", "url" : result["data"].stringValue])
//                            case "Record":
//                                self.repaireModel.maintainDailyRecord?.append(["name" : "maintainRecord.mp3", "url" : result["data"].stringValue])
                            default:
                                break
                            }
                            group.leave()
                        }) { (error) -> (Void) in
                            group.leave()
                        }
                    }
                    
                }
            }
        }
}
