//
//  RepairMoneyModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/4/14.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

struct RepairMoneyModel: HandyJSON {
    var repairCode: String?
    var modifiedTime: String?
    var deleted: String?
    var createCompanyName: String?
    var modifiedUser: String?
    var id: String?
    var createdUser: String?
    var repairProject: String?
    var createOrgId: String?
    var unitId: String?
    var createCompanyId: String?
    var repairMoney: String?
    var unitName: String?
    var repairType: String?
    var createdTime: String?
    var createOrgName: String?
    
    func repairMoneyModels (materials: [RepairMoneyModel]?) -> [[String]] {
        var results = [[String]]()
        
        let rowModels = ["  ", "维修定额编号", "维修大类", "维修项目", "计量单位", "定额（元）"]
        for (_ , rowModel) in rowModels.enumerated() {
            var models = [String]()
            models.append(rowModel)
            if let mater =  materials{
                for (_, materModel) in mater.enumerated() {
                    switch rowModel {
                    case "维修定额编号":
                        models.append(materModel.repairCode ?? "")
                    case "维修大类":
                        models.append(materModel.repairType ?? "")
                    case "维修项目":
                        models.append(materModel.repairProject ?? "")
                    case "计量单位":
                        models.append(materModel.unitName ?? "")
                    case "定额（元）":
                        models.append(materModel.repairMoney ?? "")
                    case "  ":
                        models.append("选择Cell")
                    default:
                        break
                    }
                }
            }
            results.append(models)
        }
        return results
    }
    
    func repairMoneyChartModels (materials: [RepairDetailsModel.PlanRepairStandardInfoRespVoModel]?) -> [[RepairMoneyChartViewModel]] {
        var results = [[RepairMoneyChartViewModel]]()
        if materials?.count ?? 0 <= 0 {
            return results
        }
        let rowModels = [RepairMoneyChartViewModel(text: "是否定额"),
                         RepairMoneyChartViewModel(text: "维修项目"),
                         RepairMoneyChartViewModel(text: "数量"),
                         RepairMoneyChartViewModel(text: "单位"),
                         RepairMoneyChartViewModel(text: "备注"),
                         RepairMoneyChartViewModel(text: "操作"),
        ]
        for (_ , rowModel) in rowModels.enumerated() {
            var models = [RepairMoneyChartViewModel]()
            models.append(rowModel)
            if let mater =  materials{
                for (_, materModel) in mater.enumerated() {
                    if materModel.standardFlag == "0" {
                        switch rowModel.text {
                        case "是否定额":
                            models.append(RepairMoneyChartViewModel(text: (materModel.standardFlag == "0") ? "是" : "否"))
                        case "维修项目":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairProject))
                        case "数量":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairNumber, textFieldEnable: true))
                        case "单位":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairUnit))
                        case "备注":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairRemark, textFieldEnable: true))
                        case "操作":
                            models.append(RepairMoneyChartViewModel(text: "删除"))
                        default:
                            break
                        }
                    }else{
                        switch rowModel.text {
                        case "是否定额":
                            models.append(RepairMoneyChartViewModel(text: (materModel.standardFlag == "0") ? "是" : "否"))
                        case "维修项目":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairProject, textFieldEnable: true))
                        case "数量":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairNumber, textFieldEnable: true))
                        case "单位":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairUnit, textFieldEnable: true))
                        case "备注":
                            models.append(RepairMoneyChartViewModel(text: materModel.repairRemark, textFieldEnable: true))
                        case "操作":
                            models.append(RepairMoneyChartViewModel(text: "删除"))
                        default:
                            break
                        }
                    }
                    
                }
            }
            results.append(models)
        }
        return results
    }
    
    
    func repairMoneyChartModelsShow (materials: [RepairDetailsModel.PlanRepairStandardInfoRespVoModel]?) -> [[RepairMoneyChartViewModel]] {
        var results = [[RepairMoneyChartViewModel]]()
        if materials?.count ?? 0 <= 0 {
            return results
        }
        let rowModels = [RepairMoneyChartViewModel(text: "是否定额"),
                         RepairMoneyChartViewModel(text: "维修项目"),
                         RepairMoneyChartViewModel(text: "数量"),
                         RepairMoneyChartViewModel(text: "单位"),
                         RepairMoneyChartViewModel(text: "备注"),
//                         RepairMoneyChartViewModel(text: "操作"),
        ]
        for (_ , rowModel) in rowModels.enumerated() {
            var models = [RepairMoneyChartViewModel]()
            models.append(rowModel)
            if let mater =  materials{
                for (_, materModel) in mater.enumerated() {
                    switch rowModel.text {
                    case "是否定额":
                        models.append(RepairMoneyChartViewModel(text: (materModel.standardFlag == "0") ? "是" : "否"))
                    case "维修项目":
                        models.append(RepairMoneyChartViewModel(text: materModel.repairProject))
                    case "数量":
                        models.append(RepairMoneyChartViewModel(text: materModel.repairNumber))
                    case "单位":
                        models.append(RepairMoneyChartViewModel(text: materModel.repairUnit))
                    case "备注":
                        models.append(RepairMoneyChartViewModel(text: materModel.repairRemark))
//                    case "操作":
//                        models.append(RepairMoneyChartViewModel(text: "删除"))
                    default:
                        break
                    }
                }
            }
            results.append(models)
        }
        return results
    }
}

struct RepairMoneyModelViewModel: HandyJSON {
    
}
