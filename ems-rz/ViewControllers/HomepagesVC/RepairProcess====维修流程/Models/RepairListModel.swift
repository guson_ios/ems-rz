//
//  RepairListModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/21.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

//1、维修队/外协单位——开始维修      *     2、维修队/外协单位——维修工确认是否有新发现故障      *     3、维修队/外协单位——技术员故障确认      *     4、维修队/外协单位——班组人员确认是否领用物资      *     5、维修队/外协单位——维修完成      *     6、维修队/外协单位——维修验收      *     7、维修队/外协单位——录入维修内容      */


struct RepairListModel: HandyJSON, ViewModelOperate {
    var maintainBranch: String? ///维修分支(1：设备队 2：维修队 3：外协单位 )
    var detailedStatus: String?/**      * 详细步骤状态      *     0、流程启动      *     //================设备队================      *     1、设备队——开始维修      *     5、设备队——维修完成      *     6、设备队——维修验收      *     //================维修队/外协单位================      *     1、维修队/外协单位——开始维修      *     2、维修队/外协单位——维修工确认是否有新发现故障      *     3、维修队/外协单位——技术员故障确认      *     4、维修队/外协单位——班组人员确认是否领用物资      *     5、维修队/外协单位——维修完成      *     6、维修队/外协单位——维修验收      *     7、维修队/外协单位——录入维修内容      */
    var equipCode: String?
    var equipName: String?
    var dailyRecordCode: String?
    var equipSystemCode: String?
    var useOrgName: String?
    var nowStepType: String? ///当前步骤类型 1:开始操作 2:验收操作 3:录入操作 4:组织招标谈判 5:维修完成 6：确认故障 7：：确认领用物资
    var faultRepairTime: String?
    var useCompanyName: String?
    var id: String?
    var equipPartName: String?
    var faultRepairUser: String?
    
    func detailedStatusStr() -> String {
        switch self.detailedStatus {
        case "1":
            return "开始维修"
        case "2":
            return "维修工确认是否有新发现故障"
        case "3":
            return "技术员故障确认"
        case "4":
            return "班组人员确认是否领用物资"
        case "5":
            return "维修完成"
        case "6":
            return "维修验收"
        case "7":
            return "录入维修内容"
        case "8":
        return "维修结束"
        default:
            return "任务"
        }
    }
}

