//
//  MaterialSystemModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/5.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

struct MaterialSystemModel: HandyJSON {
    var stockNum: String?
    var materKindNam: String?
    var billTim: String?
    var standModelTxt: String?
    var cunitNam: String?
    var matCod: String?
    var materNam: String?
    var taxPrice: String?
    var priceVal: String?
    var applyNum: String?

    
//    var applyNum: String? = "1"
//    var suppliesPlanId: String?
//    var price: String?
    
    func materialModels(_ model: [[String]]?, materials: [MaterialSystemModel]?) -> [[String]] {
        var results = [[String]]()
        
        let rowModels = ["物资编码", "物资类别", "物资名称", "物资型号", "申请数量", "单位", "操作"]
        for (_ , rowModel) in rowModels.enumerated() {
            var models = [String]()
            models.append(rowModel)
            if let mater =  materials{
                for (_, materModel) in mater.enumerated() {
                    switch rowModel {
                    case "物资编码":
                        models.append(materModel.matCod ?? "")
                    case "物资类别":
                        models.append(materModel.materKindNam ?? "")
                    case "物资名称":
                        models.append(materModel.materNam ?? "")
                    case "物资型号":
                        models.append(materModel.standModelTxt ?? "")
                    case "申请数量":
                        models.append(materModel.applyNum ?? "")
                    case "单位":
                        models.append(materModel.cunitNam ?? "")
                    case "操作":
                        models.append("删除")
                    default:
                        break
                    }
                }
            }
            results.append(models)
        }
        return results
    }
}
