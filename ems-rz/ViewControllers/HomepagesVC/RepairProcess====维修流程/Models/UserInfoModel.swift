//
//  UserInfoModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/4.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation


struct SystemLoginUserInfoList: HandyJSON {
    
    var userName: String?
    var createdTime: String?
    var mobile: String?
    var tel: String?
    var sex: String?
    var id: String?
    var modifiedTime: String?
    var modifiedUser: String?
    var deleted: String?
    var orgId: String?
    var address: String?
    var email: String?
    var status: String?
    var userAccount: String?
    var passwd: String?
    var createdUser: String?
}

struct RoleUseList: HandyJSON {
    var roleType: String?
    var roleName: String?
    var systemLoginUserInfoList: [SystemLoginUserInfoList]?
}
