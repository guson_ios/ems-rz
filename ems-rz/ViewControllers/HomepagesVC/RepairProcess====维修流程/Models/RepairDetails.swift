//
//  RepairDetails.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/5.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation


struct RepairDetailsModel: HandyJSON {
    
    
    var processResultStepName: String?///维修流程步骤
    
    var maintainDailyRespVo: MaintainDailyRespVoModel?
    var standardMoneyRespVoList: [Any]?
    var acceptanceUserList: [AcceptanceUserList]? = []
    var processResultRespVoList: [ProcessResultRespVoModel]?
    var applyRequestDetailList: [Any]?
    var planRepairStandardInfoRespVoList: [PlanRepairStandardInfoRespVoModel]? = []
    var planRepairStandardInfoRespVoPostList: [PlanRepairStandardInfoRespVoModel]? = []///维修完成提交list
    
    
    struct PlanRepairStandardInfoRespVoModel: HandyJSON {
        var repairNumber: String?
        var repairUnit: String?
        var flowType: String?
        var id: String?
        var repairMoney: String?
        var flowId: String?
        var standardFlag: String?
        var standardRepairMoneyId: String?
        var repairProjectName: String?
        var repairRemark: String?
        var node: String?
        var repairProject: String?
        
    }
    
    struct AcceptanceUserList {
        var acceptanceEvaluate: String?
        var createdUser: String?
        var modifiedTime: String?
        var orderId: String?
        var id: String?
        var type: String?
        var deleted: String?
        var acceptanceUserName: String?
        var acceptanceScore: String?
        var acceptanceTime: String?
        var acceptanceStatus: String?
        var createdTime: String?
        var acceptanceStatusName: String?
        var acceptanceRoleType: String?
        var acceptanceUserId: String?
        var modifiedUser: String?
        var planRepairStandardInfoRespVoList: [PlanRepairStandardInfoRespVoModel]? = []

    }
    
    struct ProcessResultRespVoModel: HandyJSON {
        var status: String?
        var loginId: String?
        var operatorName: String?
        var nextNodeId: String?
        var requestName: String?
        var nextOperator: String?
        var nodeName: String?
        var maintainType: String?
        var operateTime: String?
        var remark: String?
        var nextNodeName: String?
        var requestId: String?
        var operateDate: String?
        var useFlag: String?
        var operatorId: String?
        var nodeId: String?
        var repairStandardInfoStatus: String?
        
        
        static func maintainSwitchViewModels(_ model: ProcessResultRespVoModel?, _ repairDetailModel: RepairDetailsModel?) -> [EquipmentInfoDetail]{
//            let equs = [
//                EquipmentInfoDetail(infoKey: model?.nodeName, infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier),
//                EquipmentInfoDetail(infoKey: "确认人", infoValue: model?.operatorName),
//                EquipmentInfoDetail(infoKey: "审批意见", infoValue: model?.remark),
//            ]
            if repairDetailModel?.planRepairStandardInfoRespVoPostList?.count ?? 0 > 0 && model?.repairStandardInfoStatus == "1" {
                   return [
                    EquipmentInfoDetail(infoKey: model?.nodeName, infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier),
                    EquipmentInfoDetail(infoKey: "确认人", infoValue: model?.operatorName),
                    EquipmentInfoDetail(infoKey: "审批意见", infoValue: model?.remark),
                    EquipmentInfoDetail(infoKey: "", infoValue: nil, cellReuseIdentifier: RepairMoneyChartShowTableCell.reuseIdentifier),
                ]
            }
            return [
                EquipmentInfoDetail(infoKey: model?.nodeName, infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "确认人", infoValue: model?.operatorName),
                EquipmentInfoDetail(infoKey: "审批意见", infoValue: model?.remark),
            ]
            
//            if repairDetailModel?.planRepairStandardInfoRespVoList?.count ?? 0 <= 0  {
//                return [
//                    EquipmentInfoDetail(infoKey: model?.nodeName, infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier),
//                    EquipmentInfoDetail(infoKey: "确认人", infoValue: model?.operatorName),
//                    EquipmentInfoDetail(infoKey: "审批意见", infoValue: model?.remark),
//                    EquipmentInfoDetail(infoKey: nil, infoValue: nil, cellReuseIdentifier: RepairMoneyAddTableViewCell.reuseIdentifier)
//                ]
//            }
//
//            let equs = [
//                EquipmentInfoDetail(infoKey: model?.nodeName, infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier),
//                EquipmentInfoDetail(infoKey: "确认人", infoValue: model?.operatorName),
//                EquipmentInfoDetail(infoKey: "审批意见", infoValue: model?.remark),
//                EquipmentInfoDetail(infoKey: "", infoValue: nil, cellReuseIdentifier: RepairMoneyChartCell.reuseIdentifier),
//                EquipmentInfoDetail(infoKey: nil, infoValue: nil, cellReuseIdentifier: RepairMoneyAddTableViewCell.reuseIdentifier)
//            ]
//            return equs
        }
        
        mutating func mapping(mapper: HelpingMapper) {
            mapper.specify(property: &operatorName, name: "operator")
        }
    }
    
    struct MaintainDailyRespVoModel: HandyJSON {
        var id: String?
        var repairSourceId: String?
        var externalUnitName: String?
        var equipId: String?
        var useCompanyId: String?
        var equipSmallCategoryName: String?
        var externalUnit: String?
        var useOrgId: String?
        var maintainUnit: String?
        var equipBigCategoryName: String?
        var dailyRecordCode: String?
        var useOrgName: String?
        var isPicking: String?
        var isPickingName: String?
//        var unionStatus: String?
        var repairStandardId: String?
        var faultRepairUser: String?
        var isMisinformationName: String?
        var isUpdateFaultName: String?
        var isNewFault: String?
        var isUpdateFault: String?
        var faultConfirmUserId: String?
        var faultId: String?
        var faultPhenomenon: String?
        var totalNonApprovedCost: String?
        var faultRepairTime: String?
        var detailedStatus: String?
        var isNewFaultName: String?
        var faultFoundTime: String?
        var maintainStartTime: String?
        var equipPartName: String?
        var flowId: String?
        var faultCode: String?
        var isStopName: String?
        var statusName: String?
        var remark: String?
        var maintainCompleteTime: String?
        var isOneseifRepair: String?
        var status: String?
        var isPermissions: String?
        var useCompanyName: String?
        var equipSmallCategoryId: String?
        var equipMiddleCategoryName: String?
        var executorUserId: String?
        var equipSystemCode: String?
        var repairSource: String?
        var acceptanceStatusName: String?
        var accepStepStatus: String?

        var totalReceiveSuppliesCost: String?
        var maintainUserName: String?
        var isStop: String?
        var isEmergencyMaintainName: String?
        var equipBigCategoryId: String?
        var isEmergencyMaintain: String?
        var equipName: String?
        var faultConfirmUserName: String?
        var equipCode: String?
        var isOneseifRepairName: String?
        var equipPartId: String?
        var totalNonBudgetExpense: String?
        var downtime: String?
        var maintainUser: String?
        var equipPartsId: String?
        var equipPartsName: String?
        var failureTime: String?
        var maintainUserId: String?
        var solution: String?
        var maintainBranch: String? ///维修分支(1：设备队 2：维修队 3：外协单位 )    
        var isMisinformation: String?
        var nonQuotaMaintain: String?
        var equipMiddleCategoryId: String?
        
        var maintainDailyVideo: [MaintainDailyDataModel]?
        var maintainDailyPhotos: [MaintainDailyDataModel]?
        var maintainDailyAccessory: [MaintainDailyDataModel]?
        var maintainDailyRecord: [MaintainDailyDataModel]?

        struct MaintainDailyDataModel: HandyJSON {
            var name: String?
            var typeName: String?
            var id: String?
            var url: String?
        }
        
//        func repairUnCell() -> String {
//
//            switch self.detailedStatus {
//            case "1":
//                switch self.maintainBranch {
//                case "1":
//                    return "设备队-班组人员指令确认"
//                case "2":
//                    return "维修队-维修工指令确认"
//                case "3":
//                    return "维修队-维修工指令确认"
//                default:
//                    break
//                }
//            case "2", "3", "4":
//                return "开始维修"
//
//            default:
//                return ""
//                break
//            }
//            return ""
//        }
    }
    
    static func repairDetailViewModels(_ model: MaintainDailyRespVoModel?) -> [EquipmentInfoDetail]{
        return [
            EquipmentInfoDetail(infoKey: "工单号：" + (model?.dailyRecordCode ?? ""), infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "设备名称", infoValue: model?.equipName),
            EquipmentInfoDetail(infoKey: "设备编号", infoValue: model?.equipCode),
            EquipmentInfoDetail(infoKey: "设备系统编号", infoValue: model?.equipSystemCode),
            EquipmentInfoDetail(infoKey: "部位", infoValue: model?.equipPartName),
            EquipmentInfoDetail(infoKey: "部件", infoValue: model?.equipPartsName),
            EquipmentInfoDetail(infoKey: "故障发现时间", infoValue: model?.faultFoundTime),
            EquipmentInfoDetail(infoKey: "故障代码", infoValue: model?.faultCode),
            EquipmentInfoDetail(infoKey: "故障现象", infoValue: model?.faultPhenomenon),
            EquipmentInfoDetail(infoKey: "故障描述", infoValue: model?.remark, cellStatus: false, cellReuseIdentifier: NextGrowingCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "应急维修", infoValue: model?.isEmergencyMaintain, cellStatus: true, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "是否停机", infoValue: model?.isStop, cellStatus: true, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
        ]
    }
}
