//
//  RepairConfirmNewFault.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/6.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation


struct RepairConfirmNewFault: HandyJSON {
    
    var id: String?
    var faultFoundTime: String?
    var faultId: String?
    var isEmergencyMaintain: String?
    var isStop: String?
    var isNewFault: String? = "0"
    var isUpdateFault: String? = "0"
    var remark: String?
    var maintainDailyPhotos: [[String : String]]?
    var maintainDailyRecord: [[String : String]]?
    var maintainDailyVideo: [[String : String]]?
    
    ///故障代码，故障现象
    var faultCode: String?
    var faultPhenomenon: String?
    
    static func faultRepairViewModels(_ model: RepairConfirmNewFault?) -> [EquipmentInfoDetail]{
        
        if model?.isUpdateFault?.stringBoolValue() ?? false {
            
            return [
                EquipmentInfoDetail(infoKey: "是否发现新故障", infoValue: model?.isNewFault, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "是否更正原有故障信息", infoValue: model?.isUpdateFault, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "故障发现时间", infoValue: String.currentTimeFormatter("YYYY-MM-dd HH:mm:ss")),
                EquipmentInfoDetail(infoKey: "故障代码", infoValue: model?.faultCode),
                EquipmentInfoDetail(infoKey: "故障现象", infoValue: model?.faultPhenomenon),
                EquipmentInfoDetail(infoKey: "故障描述", infoValue: model?.remark, cellStatus: true, cellReuseIdentifier: NextGrowingCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "应急维修", infoValue: model?.isEmergencyMaintain, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "是否停机", infoValue: model?.isStop, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
            ]
            
        }
        return[
            EquipmentInfoDetail(infoKey: "是否发现新故障", infoValue: model?.isNewFault, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "是否更正原有故障信息", infoValue: model?.isUpdateFault, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
        ]
    }
}

struct RepairConfirmNewFaultViewModel {
    
    // MARK: - 是否发现新故障
    static func repaireConfirmNewFaultViewModels(_ model: RepairConfirmNewFault?, repairDetailModel: RepairDetailsModel?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        if let mainequInfoModels = repairMaintainViewModel(repairDetailModel){
//            tableViewViewModel.append(mainequInfoModels)
            tableViewViewModel.append(contentsOf: mainequInfoModels)
        }
        
        var equInfoModels = RepairConfirmNewFault.faultRepairViewModels(model).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
        }
        equInfoModels.insert(repairNodeName(repairDetailModel), at: 0)
        tableViewViewModel.append(equInfoModels)
        return tableViewViewModel
    }
        
}
