//
//  RepairStartViewModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/3.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation


struct MaterialSystemRequestModel: HandyJSON {
    var suppliesPlanId: String? = ""
    var materName: String?
    var materCode: String?
    var applyNum: String? = "1"
    var price: String?
    var materKindName: String?
    var standModelTxt: String?
    var unitName: String?

}

// MARK: - 开始维修
struct RepairStartModel: HandyJSON {
    
    var id: String? = nil
    var isPicking: String? = "0" //是否领料
    var isStop: String? = "0"
    var materialSystemModels: [MaterialSystemModel]?///用于构造View
    var applyRequestDetail: [MaterialSystemRequestModel]?///用于提交
    var applyRequestDetailList: [MaterialSystemRequestModel]?///用于提交步骤4

    static func repairSwitchViewModels(_ model: RepairStartModel?, repairDetailModel: RepairDetailsModel?) -> [EquipmentInfoDetail]{
        
        if repairDetailModel?.maintainDailyRespVo?.maintainBranch == "1" {
            return [
                EquipmentInfoDetail(infoKey: "维修人员", infoValue: EmsGlobalObj.obj.loginData?.userName),
                EquipmentInfoDetail(infoKey: "是否领料", infoValue: model?.isPicking, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
            ]
        }
        return [
            EquipmentInfoDetail(infoKey: "维修人员", infoValue: EmsGlobalObj.obj.loginData?.userName),
            EquipmentInfoDetail(infoKey: "是否停机", infoValue: model?.isStop, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "是否领料", infoValue: model?.isPicking, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
        ]
    }
    
    static func maintainSwitchViewModels(_ model: RepairStartModel?) -> [EquipmentInfoDetail]{
        return [
            EquipmentInfoDetail(infoKey: "是否领料", infoValue: model?.isPicking),
        ]
    }
    
}

// MARK: - 报修提交model
struct RepaireStartViewModel : HandyJSON{
    // MARK: - 开始维修
    static func repaireStartViewModels(_ model: RepairStartModel?, repairDetailModel: RepairDetailsModel?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        if let mainequInfoModels = repairMaintainViewModel(repairDetailModel){
            tableViewViewModel.append(contentsOf: mainequInfoModels)
        }
        
       
        var equInfoModels = RepairStartModel.repairSwitchViewModels(model, repairDetailModel: repairDetailModel).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
        }
        if model?.materialSystemModels?.count ?? 0 > 0 {
           let vm = TableViewViewModel(baseViewModel: BaseViewModel(model: EquipmentInfoDetail() as ViewModelOperate, cellReuseIdentifier: ChartTableViewCell.reuseIdentifier))
            equInfoModels.append(vm)
        }
        equInfoModels.insert(repairNodeName(repairDetailModel), at: 0)
        tableViewViewModel.append(equInfoModels)
        
        
        return tableViewViewModel
    }
    
    // MARK: - 维修验收
    static func repaireAcceptanceViewModels(_ model: RepaireAcceptanceModel?, repairDetailModel: RepairDetailsModel?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        if let mainequInfoModels = repairMaintainViewModel(repairDetailModel){
            tableViewViewModel.append(contentsOf: mainequInfoModels)
        }
        var equInfoModels = RepaireAcceptanceModel.repaireAcceptanceModels(model, repairDetailModel: repairDetailModel).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
        }
        let photoShow: [TableViewViewModel<ViewModelOperate>] = [TableViewViewModel(baseViewModel: BaseViewModel(model: PhotoShowModel(), size: CGSize(width: KScreenWidth, height: 150), cellReuseIdentifier: PhotoSelectShowCell.reuseIdentifier))]
        
        
        
//         let record: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: PhotoShowModel(), cellReuseIdentifier: RecordTableViewCell.reuseIdentifier))]
        //        tableViewViewModel.append(record)

        equInfoModels.insert(repairNodeName(repairDetailModel), at: 0)
        tableViewViewModel.append(equInfoModels)
        tableViewViewModel.append(photoShow)
        if let repairMoneyModel: [TableViewViewModel<ViewModelOperate>] = repairMoneyViewModel(repairDetailModel) {
            tableViewViewModel.append(repairMoneyModel)
        }
        return tableViewViewModel
    }
    
    ///维修队-班组人员/外修队长-确认是否领用物资
    static  func maintainIsPickingConfirm(_ model: RepairStartModel?, repairDetailModel: RepairDetailsModel?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        if let mainequInfoModels = repairMaintainViewModel(repairDetailModel){
            tableViewViewModel.append(contentsOf: mainequInfoModels)
        }
        
        
        var equInfoModels = RepairStartModel.maintainSwitchViewModels(model).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier))
        }
        equInfoModels.insert(repairNodeName(repairDetailModel), at: 0)
        tableViewViewModel.append(equInfoModels)

        
        if model?.materialSystemModels?.count ?? 0 > 0 {
           let vm = TableViewViewModel(baseViewModel: BaseViewModel(model: EquipmentInfoDetail() as ViewModelOperate, cellReuseIdentifier: ChartTableViewCell.reuseIdentifier))
            tableViewViewModel.append([vm])
        }
  
        return tableViewViewModel
    }
}


// MARK: - 维修验收Model
struct RepaireAcceptanceModel: HandyJSON {
    
    var id: String?
    var squadLeaderId: String?
    var driverId: String?
    var technologyId: String?
    var acceptanceRoleType: String?
    var acceptanceStatus: String? =  "1"
    var acceptanceEvaluate: String? = " "
    var acceptanceScore: String?
    var fileReqVoList: [[String : String]]? = []
    var planRepairStandardInfoRespVoList: [RepairDetailsModel.PlanRepairStandardInfoRespVoModel]? = []
    
    
    ///提交时应删除字段
    var photos: [Any]? = []
    
    
    
    static func repaireAcceptanceModels(_ model: RepaireAcceptanceModel?, repairDetailModel: RepairDetailsModel?) -> [EquipmentInfoDetail]{
        if repairDetailModel?.maintainDailyRespVo?.accepStepStatus == "2"  {
            if repairDetailModel?.maintainDailyRespVo?.maintainBranch == "1" {
                return [
                    EquipmentInfoDetail(infoKey: "验收人员", infoValue: EmsGlobalObj.obj.loginData?.userName),
                    EquipmentInfoDetail(infoKey: "是否验收通过", infoValue: model?.acceptanceStatus, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
                    EquipmentInfoDetail(infoKey: "验收评价", infoValue: model?.acceptanceEvaluate, cellStatus: true, cellReuseIdentifier: NextGrowingCell.reuseIdentifier),
                    EquipmentInfoDetail(infoKey: "验收打分", infoValue: model?.acceptanceScore, cellReuseIdentifier: CosmosTableViewCell.reuseIdentifier),
//                    EquipmentInfoDetail(infoKey: "指派验收（维修队）", infoValue: "", cellType: .leftImageHide),
                    EquipmentInfoDetail(infoKey: "指派验收（班长）", infoValue: "", cellType: .leftImageHide),
                    EquipmentInfoDetail(infoKey: "指派验收（司机）", infoValue: "", cellType: .leftImageHide),
                ]
            }
            return [
                EquipmentInfoDetail(infoKey: "验收人员", infoValue: EmsGlobalObj.obj.loginData?.userName),
                EquipmentInfoDetail(infoKey: "是否验收通过", infoValue: model?.acceptanceStatus, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "验收评价", infoValue: model?.acceptanceEvaluate, cellStatus: true, cellReuseIdentifier: NextGrowingCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "验收打分", infoValue: model?.acceptanceScore, cellReuseIdentifier: CosmosTableViewCell.reuseIdentifier),
                EquipmentInfoDetail(infoKey: "指派验收（维修队）", infoValue: "", cellType: .leftImageHide),
                EquipmentInfoDetail(infoKey: "指派验收（班长）", infoValue: "", cellType: .leftImageHide),
                EquipmentInfoDetail(infoKey: "指派验收（司机）", infoValue: "", cellType: .leftImageHide),
            ]
        }
        
        return [
            EquipmentInfoDetail(infoKey: "验收人员", infoValue: EmsGlobalObj.obj.loginData?.userName),
            EquipmentInfoDetail(infoKey: "是否验收通过", infoValue: model?.acceptanceStatus, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "验收评价", infoValue: model?.acceptanceEvaluate, cellStatus: true, cellReuseIdentifier: NextGrowingCell.reuseIdentifier),
            EquipmentInfoDetail(infoKey: "验收打分", infoValue: model?.acceptanceScore, cellReuseIdentifier: CosmosTableViewCell.reuseIdentifier),
        ]
    }
}


// MARK: - 维修完成、提交验收
struct RepaireAcceptanceSubmit {
    
    static func repaireAcceptanceSubmitViewModels(_ model: RepairStartModel?) -> [EquipmentInfoDetail]{
        return [
            EquipmentInfoDetail(infoKey: "确认人", infoValue: EmsGlobalObj.obj.loginData?.userName, cellType: .leftImageHide),
        ]
    }
}

struct RepaireAcceptanceSubmitViewModel {
    static func repaireAcceptanceSubmitViewModels(_ model: RepairStartModel?, repairDetailModel: RepairDetailsModel?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let mainequInfoModels = repairMaintainViewModel(repairDetailModel){
            tableViewViewModel.append(contentsOf: mainequInfoModels)
        }
        var equInfoModels = RepaireAcceptanceSubmit.repaireAcceptanceSubmitViewModels(model).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
            return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
        }
        equInfoModels.insert(repairNodeName(repairDetailModel), at: 0)
        tableViewViewModel.append(equInfoModels)
        return tableViewViewModel
    }
}
// MARK: - 流程平台步骤
func repairProcessMaintainViewModel(_ repairDetailModel: RepairDetailsModel?) -> [TableViewViewModel<ViewModelOperate>]? {
    
    let processResultRespVoModel =  repairDetailModel?.processResultRespVoList?.filter({$0.nodeName == repairDetailModel?.processResultStepName})
    let mainequInfoModels = RepairDetailsModel.ProcessResultRespVoModel.maintainSwitchViewModels(processResultRespVoModel?.first, repairDetailModel).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
        return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
    }
    return mainequInfoModels
}
// MARK: - 维修验收增加定额
func repairMoneyViewModel(_ repairDetailModel: RepairDetailsModel?) -> [TableViewViewModel<ViewModelOperate>]? {
    
    var repairMoneyModel = [TableViewViewModel<ViewModelOperate>]()
    if  (repairDetailModel?.maintainDailyRespVo?.accepStepStatus == "1" || repairDetailModel?.maintainDailyRespVo?.accepStepStatus == "2") {
        if repairDetailModel?.planRepairStandardInfoRespVoList?.count ?? 0 > 0 {
            repairMoneyModel = [
                TableViewViewModel(baseViewModel: BaseViewModel(model: RepairMoneyShowModel(), cellReuseIdentifier:RepairMoneyChartCell.reuseIdentifier)),
                TableViewViewModel(baseViewModel: BaseViewModel(model: RepairMoneyShowModel(), cellReuseIdentifier:RepairMoneyAddTableViewCell.reuseIdentifier)),
            ]
        }else{
            repairMoneyModel = [
                TableViewViewModel(baseViewModel: BaseViewModel(model: RepairMoneyShowModel(), cellReuseIdentifier:RepairMoneyAddTableViewCell.reuseIdentifier)),
            ]
        }
    }
    return repairMoneyModel
}

// MARK: - 保养验收增加定额
func maintainAcceptMoneyViewModel(_ repairDetailModel: MaintainAcceptModel?) -> [TableViewViewModel<ViewModelOperate>]? {
    var repairMoneyModel = [TableViewViewModel<ViewModelOperate>]()
//    if repairDetailModel?.planRepairStandardInfoRespVoList?.count ?? 0 > 0 {
        repairMoneyModel = [
            TableViewViewModel(baseViewModel: BaseViewModel(model: RepairMoneyShowModel(), cellReuseIdentifier:RepairMoneyChartCell.reuseIdentifier)),
            TableViewViewModel(baseViewModel: BaseViewModel(model: RepairMoneyShowModel(), cellReuseIdentifier:RepairMoneyAddTableViewCell.reuseIdentifier)),
        ]
//    }
    return repairMoneyModel
}

// MARK: - 维修节点名称
func repairNodeName(_ repairDetailModel: RepairDetailsModel?) -> TableViewViewModel<ViewModelOperate> {
    let nextNodeName = repairDetailModel?.processResultRespVoList?.last?.nextNodeName
    let equInfoModel = EquipmentInfoDetail(infoKey: nextNodeName, infoValue: "", cellReuseIdentifier: RepairUnTableViewCell.reuseIdentifier)
    
    return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
}

// MARK: - 报修详情
func repairMaintainViewModel(_ repairDetailModel: RepairDetailsModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
    var mainequInfoModels = RepairDetailsModel.repairDetailViewModels(repairDetailModel?.maintainDailyRespVo).map { (equInfoModel) -> TableViewViewModel<ViewModelOperate> in
        return TableViewViewModel(baseViewModel: BaseViewModel(model: equInfoModel, cellReuseIdentifier: equInfoModel.cellReuseIdentifier))
    }
    if let photosModel = repairDetailModel?.maintainDailyRespVo?.maintainDailyPhotos {
        let photos = photosModel.map { (mModel) -> UIImage? in
            
            return UIImage.loadRemoteImage(afBaseUrl + (mModel.url ?? ""))
        }
        
        let photoShow: TableViewViewModel<ViewModelOperate>  = TableViewViewModel(baseViewModel: BaseViewModel(model: PhotoShowModel(imageArrays: photos as [Any]), size: CGSize(width: KScreenWidth, height: 100), cellReuseIdentifier: PhotoShowTableViewCell.reuseIdentifier))
        mainequInfoModels.append(photoShow)
        
    }
    
    if let videoModel = repairDetailModel?.maintainDailyRespVo?.maintainDailyVideo , let rectModel = repairDetailModel?.maintainDailyRespVo?.maintainDailyRecord, (videoModel.count > 0 || rectModel.count > 0) {
        
        var recordShowModel = RecordShowModel()
        let videos = videoModel.map { (mModel) -> RecordShowModel.VideoModel? in
            if let path = (afBaseUrl + (mModel.url ?? "")).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let image: UIImage = UIImage.getVideoPreViewImage(path) {
                var viModel = RecordShowModel.VideoModel()
                viModel.url = path
                viModel.image = image
                return viModel
            }
            return nil
        }.filter({$0 != nil})
        
        recordShowModel.imageArrays = videos as [Any]
        if rectModel.count > 0 {
            let path = afBaseUrl + (rectModel.first?.url ?? "")
            if let url = URL(string: path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                recordShowModel.recordData = try? Data(contentsOf: url)
            }
        }
        let record: TableViewViewModel<ViewModelOperate>  = TableViewViewModel(baseViewModel: BaseViewModel(model: recordShowModel, cellReuseIdentifier: RecordTableViewCell.reuseIdentifier))
        mainequInfoModels.append(record)
    }
    var result = [mainequInfoModels]
    if  let mainePmodel = repairProcessMaintainViewModel(repairDetailModel){
        result.append(mainePmodel)
    }
    return result

}
