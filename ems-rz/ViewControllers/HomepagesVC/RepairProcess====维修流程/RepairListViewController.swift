//
//  RepairListViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/21.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class RepairListViewController: BaseViewController {
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    var repairListModels: [RepairListModel] = []
    var checkStatus = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.mj_header?.beginRefreshing()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.title = "维修待办列表"
        self.menuView()
        self.tableView.register(RepairListTableViewCell.nib, forCellReuseIdentifier: RepairListTableViewCell.reuseIdentifier)
        self.tableView.tableViewRefreshClouse {[weak self] (pageNo, listCount) in
            if pageNo == 1 { self?.repairListModels.removeAll()}
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainDailyAppList), parameter: pageDict(current: pageNo).combinationDict(["type" : String(self?.checkStatus ?? 0)]), method: .get, headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
                
                if let models = [RepairListModel].deserialize(from: result["data"]["records"].arrayObject) as? [RepairListModel]{
                    self?.repairListModels.append(contentsOf: models)
                    listCount(models.count)
                    self?.tableView.reloadData()
                }else{
                    listCount(0)
                }
            }) { (error) -> (Void) in
                listCount(0)
            }
        }
    }
    
    func menuView() {
        let menuView = MenuView(frame: CGRect(x: (KScreenWidth - 100.0) / 2, y: 0, width: 120, height: 35), titleArray: ["待办", "已办"])
        menuView.menuBtnClick = { [weak self] index in
            self?.checkStatus = index
            self?.tableView.mj_header?.beginRefreshing()
        }
        menuView.titleFont = UIFont.systemFont(ofSize: 16)
        menuView.selectedIndex = self.checkStatus
        topView.addSubview(menuView)
    }
}

extension RepairListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repairListModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RepairListTableViewCell = tableView.dequeueReusableCell(withIdentifier: RepairListTableViewCell.reuseIdentifier, for: indexPath) as! RepairListTableViewCell
        cell.confignRepairListCell(self.repairListModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = RepairStartVC.loadViewControllewWithNib as? RepairStartVC {
            vc.repairListModel = self.repairListModels[indexPath.row]
            vc.checkStatus = self.checkStatus
            self.show(vc, sender: nil)
        }
        
    }
}
