//
//  DailyInspectionVC.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/16.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class DailyInspectionVC: BaseViewController {

    @IBOutlet weak var bottomTwoBtnView: BottomTwoBtnView!
    @IBOutlet weak var tableView: UITableView!
    var equipInfoScanModel: EquipInfoScanModel?
    var defModel: ScanWorkDefModel? = ScanWorkDefModel()

    var dailyModel: DailyModel?
    
    var scanModel: ScanModel?
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    var spectionRecordPlan: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "日检"
        self.tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)
        self.tableView.register(MaintainTableViewCell.nib, forCellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)

        self.dailySpectionRecordPlanSave()
        self.planDailyInspectionByType()
        
        self.bottomTwoBtnView.leftBtn.rx.tap.bind { [weak self] _  in
            let vc = ScanViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.scanResultDelegate = self
            self?.show(vc, sender: nil)
            vc.hidesBottomBarWhenPushed = true
        }.disposed(by: disposeBag)
        
        self.bottomTwoBtnView.rightBtn.rx.tap.bind { [weak self] _ -> Void in
            
            if let positionModels = self?.dailyModel?.standardCheckRespVoList{
                var num = 0
                var faultNum = 0
                var standardCheckIds: [String] = []//检查标准数组
                for (_, positionModel) in positionModels.enumerated() {//部位
                    if let planId = self?.spectionRecordPlan, let positionId = positionModel.equipPositionId{
                        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                        if Defaults[key: defPositionModels] != nil {
                            num += 1
                            let pm = [SpectionPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [SpectionPartModel]
                            if let checkParts = pm {//本地保存报修标准 部位
                                for (_, checkPart) in checkParts.enumerated() {///部件
                                    if let standardChecks = checkPart.standardCheckRespVoList {
                                        for (_, standardCheck) in standardChecks.enumerated() {///报修标准
                                            if standardCheck.isCheck!, let standardCheckID = standardCheck.id {
                                                faultNum += 1
                                                standardCheckIds.append(standardCheckID)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if num == positionModels.count {//全部扫码
                    let parameter = ["id" : (self?.spectionRecordPlan)!, "faultNumber" : String(faultNum), "isRepair" : faultNum == 0 ? "0" : "1", "dailyInspectionId" : self?.dailyModel?.id ?? "0"]
                    print(parameter.dictToJsonString())
                    AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanDailyInspectionRecordUpdate), parameter: nil, method: .post, encoding: parameter.dictToJsonString(), headers: headersFunc(contentType: .json), progressPlugin: self, success: { (result) -> (Void) in
                        for (_, positionModel) in positionModels.enumerated() {//部位
                            if let planId = self?.spectionRecordPlan, let positionId = positionModel.equipPositionId{
                                let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                                Defaults.remove(defPositionModels)///提交后移除
                            }
                        }
                        ///更新作业计划
                        self?.defModel?.spectionRecordIsFinish = true
                        self?.saveDefModel()
                        ZFToast.show(withMessage: "提交成功")
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                            self?.navigationController?.popViewController(animated: true)
                        }
                    }) { (error) -> (Void) in
                        
                    }
                }else{
                    ZFToast.show(withMessage: "需所有部位检查完才可提交")
                }
            }
        }.disposed(by: disposeBag)
        
    }
}


extension DailyInspectionVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return  self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (operate, cell) in
            switch (operate, cell) {
            case (let model as DailyInspectionModel, let cell as MaintainTableViewCell):
                cell.rightBtn.isHidden = true
                if let planId = self?.spectionRecordPlan, let positionId = model.equipPositionId{
                    let isHidden = !(self?.searchDefData(planId, positionId: positionId) ?? false)
                    cell.rightBtn.isHidden = isHidden
                    cell.leftBtn.isHidden = isHidden
                    cell.rightBtn.rx.tap.bind { [weak self] _  in
                        self?.pushMaintainDetailVC(planId, equipPositionId: positionId, equipPositionName: model.equipPositionName)
                    }.disposed(by: cell.rx.reuseBag)
                    
                    cell.leftBtn.rx.tap.bind { [weak self] _ in
                        if model.spectionParts == nil{
                            let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
                            if
                                let positionModel =  [SpectionPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [SpectionPartModel]{
                                model.spectionParts = positionModel
                                
                                self?.dailyModel?.standardCheckRespVoList?[indexPath.section - 1] = model
                                if let viewModels = DailySpectionViewModel.dailySpectionViewModel(self?.dailyModel?.standardCheckRespVoList, equipInfoScanModel: self?.equipInfoScanModel){
                                    self?.tableViewViewModel = viewModels
                                    self?.tableView.reloadData()
                                }
                            }
                        }else{
                            model.spectionParts = nil
                            self?.dailyModel?.standardCheckRespVoList?[indexPath.section - 1] = model
                            if let viewModels = DailySpectionViewModel.dailySpectionViewModel(self?.dailyModel?.standardCheckRespVoList, equipInfoScanModel: self?.equipInfoScanModel){
                                self?.tableViewViewModel = viewModels
                                self?.tableView.reloadData()
                            }
                        }
                    }.disposed(by: cell.rx.reuseBag)
                }
                
            default:
                break
            }
            
        })
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 20 : 0.001
    }
}

extension DailyInspectionVC: LBXScanViewControllerDelegate{
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        print(scanResult.strScanned as Any)
        if let scanStr = scanResult.strScanned {
            
            let dict = String.stringToDict(scanStr)
            let model = ScanModel.deserialize(from: dict)
            if let positionModels = self.dailyModel?.standardCheckRespVoList {
                for (_, positionModel) in positionModels.enumerated() {
                    if model?.equipPositionId == positionModel.equipPositionId {
                        if let planId = self.spectionRecordPlan{
                            self.pushMaintainDetailVC(planId, equipPositionId: positionModel.equipPositionId!, equipPositionName: positionModel.equipPositionName)
                        }else{
                            self.dailySpectionRecordPlanSave()
                            ZFToast.show(withMessage: "生成计划失败")
                        }
                        return
                    }
                }
            }
            ZFToast.show(withMessage: "二维码不匹配")
        }
    }
}
extension DailyInspectionVC{
    
    func saveDefModel() {
        var def: DefaultsKey<[String : Any]?>
        if let userAccount = EmsGlobalObj.obj.loginData?.userAccount , let equipId = self.scanModel?.equipId  {
            def = DefaultsKey<[String : Any]?>(userAccount + equipId, defaultValue: nil)
            Defaults[key: def] = defModel?.toJSON()
        }
    }
    // MARK: - 生成日检记录
    func dailySpectionRecordPlanSave(){
        
        if self.defModel?.spectionRecordPlan == nil {
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanDailyInspectionRecordSave), parameter: ["equipInfoId" : scanModel?.equipId ?? ""], method: .post, encoding: URLEncoding.httpBody,  success: { (result) -> (Void) in
                self.spectionRecordPlan =  result["data"].stringValue
                self.defModel?.spectionRecordPlan = self.spectionRecordPlan
                self.saveDefModel()
                
            }) { (error) -> (Void) in
                
            }
        }else{
            self.spectionRecordPlan = self.defModel?.spectionRecordPlan
        }
    }
    // MARK: - 获取部位
    func planDailyInspectionByType() {
        var dict = scanModel?.toJSON()
        dict?.removeValue(forKey: "equipId")
        dict?.removeValue(forKey: "equipPositionId")
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanDailyInspectionByType), parameter: dict, method: .get, success: { (result) -> (Void) in
            let model = DailyModel.deserialize(from: result["data"].dictionaryObject)
            self.dailyModel = model
            if let viewModels = DailySpectionViewModel.dailySpectionViewModel(model?.standardCheckRespVoList, equipInfoScanModel: self.equipInfoScanModel){
                self.tableViewViewModel = viewModels
                self.tableView.reloadData()
            }
        }) { (error) -> (Void) in
            
        }
    }
    // MARK: - 本地
       func searchDefData(_ planId: String ,  positionId: String) -> Bool {
           let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + positionId, defaultValue: nil)
           if Defaults[key: defPositionModels] != nil {
               return true
           }
           return false
       }
    // MARK: - 详情
    func pushMaintainDetailVC(_ planId: String, equipPositionId: String, equipPositionName: String?) {
        let defPositionModels = DefaultsKey<[[String : Any]]?>(planId + equipPositionId, defaultValue: nil)
        if  Defaults[key: defPositionModels] != nil {
            if let vc = SpectionRecordDetailVC.loadViewControllewWithNib as? SpectionRecordDetailVC {
                vc.spectionPartModel = [SpectionPartModel].deserialize(from: Defaults[key: defPositionModels]) as? [SpectionPartModel]
                vc.scanModel = self.scanModel
                vc.repairSourceEmun = .日检
                vc.spectionRecordPlan = self.spectionRecordPlan
                vc.equipPositionName = equipPositionName
                self.show(vc, sender: nil)
            }
        }else{
            var dict = scanModel?.toJSON()
            dict?.removeValue(forKey: "equipId")
            dict?.updateValue(equipPositionId, forKey: "equipPositionId")
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanDailyInspectionByTypeAndPos), parameter: dict, method: .get, success: { (result) -> (Void) in
                let models = [SpectionPartModel].deserialize(from: result["data"].arrayObject) as? [SpectionPartModel]
                if let vc = SpectionRecordDetailVC.loadViewControllewWithNib as? SpectionRecordDetailVC{
                    vc.spectionRecordPlan = self.spectionRecordPlan
                    vc.spectionPartModel = models
                    vc.scanModel = self.scanModel
                    vc.repairSourceEmun = .日检
                    vc.equipPositionName = equipPositionName
                    self.show(vc, sender: nil)
                }
            }) { (error) -> (Void) in
                
            }
        }
    }
}



