//
//  ScanWorkViewController.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/15.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
///什么鬼流程
struct ScanWorkDefModel: HandyJSON {
    var workTime: String?
    var workTimeTamp: String?///时间戳

    var workTimeLong: String?///时间差

    var cargoId: String?
    var cargoName: String?
    var isWorkStart: Bool? = false///是否开始作业
    
    var spectionRecordPlan: String?///日检计划ID
    var spectionRecordIsFinish: Bool = false///日检是否完成
}

class ScanWorkViewController: BaseViewController {
    
    @IBOutlet weak var bottomBtnView: BottomBtnView!
    @IBOutlet weak var tableView: UITableView!

    lazy var simplePickView: SimplePickView = {
           let pickView = SimplePickView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight))
           self.view.addSubview(pickView)
           return pickView
       }()
    
    var scanModel: ScanModel?
    var equipInfoScanModel: EquipInfoScanModel?
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    var basicDataRespVos: [BasicDataRespVos]?
    var cargoId: String = ""
    var scanWorkDefModel = ScanWorkDefModel()
    var dailyModel: DailyModel?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.workStart()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "扫码作业"
        self.tableView.register(EquSummaryCell.nib, forCellReuseIdentifier: EquSummaryCell.reuseIdentifier)
        self.tableView.register(UniversalTableViewCell.nib, forCellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)
        
        if self.scanModel?.equipSmallCategoryId != nil {
            self.planDailyInspectionByType()
        }
        self.getEquipInfoBaseId()
        self.getEquipWorkCargoList()
        bottomBtnView.btn.rx.tap.bind { [weak self] _ in
            if self?.scanWorkDefModel.isWorkStart ?? false{
                if self?.scanWorkDefModel.spectionRecordIsFinish ?? false || self?.dailyModel?.standardCheckRespVoList?.count ?? 0 <= 0  {
                    self?.equipWorkFinish()
                }else{
                    self?.showDailyInspectionVC()
                }
            }else{
                if self?.scanWorkDefModel.cargoId != nil {
                    self?.equipWorkStart()
                }else{
                    ZFToast.show(withMessage: "请先选择作业货种")
                }
            }
        }.disposed(by: disposeBag)
    }
    
    // MARK: - 获取部位
    func planDailyInspectionByType() {
        var dict = self.scanModel?.toJSON()
        dict?.removeValue(forKey: "equipId")
        dict?.removeValue(forKey: "equipPositionId")
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsPlanDailyInspectionByType), parameter: dict, method: .get, success: { (result) -> (Void) in
            let model = DailyModel.deserialize(from: result["data"].dictionaryObject)
            self.dailyModel = model
            self.workStart()
            if model?.standardCheckRespVoList?.count ?? 0 <= 0{
                ZFToast.show(withMessage: "未制定日检计划")
            }
            
        }) { (error) -> (Void) in
            
        }
    }
    
    ///开始日检
    func workStart() {
        var def: DefaultsKey<[String : Any]?>
        if let userAccount = EmsGlobalObj.obj.loginData?.userAccount , let equipId = self.scanModel?.equipId  {
            def = DefaultsKey<[String : Any]?>(userAccount + equipId, defaultValue: nil)
//            Defaults.remove(def)///提交后移除
            if Defaults[key: def] != nil{
                let defDict:[String : Any]? =  Defaults[key: def]
                if let defModel = ScanWorkDefModel.deserialize(from: defDict) {
                    
                    let currentDay = String.currentTimeFormatter("YYYY-MM-dd").components(separatedBy: "-").last
                    let day =  defModel.workTime?.components(separatedBy: " ").first?.components(separatedBy: "-").last
                    
                    if currentDay == day {
                        self.scanWorkDefModel = defModel
                        self.cargoId = scanWorkDefModel.cargoId ?? ""
                        if let workTimeTamp =  self.scanWorkDefModel.workTimeTamp{
                            self.scanWorkDefModel.workTimeLong = String.currentTimeDifference(workTimeTamp, dateFormat: "HH:mm")
                        }
                        if let viewModels = EquipInfoScanViewModel.equipInfoScanViewModel(self.equipInfoScanModel, scanWorkDefModel: scanWorkDefModel) {
                            self.tableViewViewModel = viewModels
                            self.tableView.reloadData()
                        }
                        if scanWorkDefModel.spectionRecordIsFinish || self.dailyModel?.standardCheckRespVoList?.count ?? 0 <= 0 {
                            bottomBtnView.btn.setTitle("结束作业", for: .normal)
                        }else{
                            bottomBtnView.btn.setTitle("开始日检", for: .normal)
                        }
                    }else{
                        Defaults.remove(def)
                        self.workStart()
                    }
                }
                
            }else{
                bottomBtnView.btn.setTitle("开始作业", for: .normal)
            }
        }
    }
    
    func showDailyInspectionVC() {
        if let vc = DailyInspectionVC.loadViewControllewWithNib as? DailyInspectionVC{
            vc.scanModel = self.scanModel
            vc.equipInfoScanModel = self.equipInfoScanModel
            
            vc.defModel = scanWorkDefModel
            self.show(vc, sender: nil)
        }
    }
    ///开始作业
    func equipWorkStart() {
        let par = ["equipId" : self.scanModel?.equipId!, "cargoId" : self.cargoId, "isForceEnd" : "1"]
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipWorkStart), parameter: nil, method: .post, encoding: par.dictToJsonString(), headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
            if let userAccount = EmsGlobalObj.obj.loginData?.userAccount , let equipId = self.scanModel?.equipId  {
                let def = DefaultsKey<[String : Any]?>(userAccount + equipId, defaultValue: nil)
                self.scanWorkDefModel.workTime = String.currentTimeFormatter("YYYY-MM-dd HH:mm")
                self.scanWorkDefModel.workTimeTamp = String.currentTimeTamp()
                self.scanWorkDefModel.isWorkStart = true
                Defaults[key: def] = self.scanWorkDefModel.toJSON()
                
                ///有日检计划
                if self.dailyModel?.standardCheckRespVoList?.count ?? 0 > 0 {
                    self.showDailyInspectionVC()
                }else{
                    self.workStart()
                }
            }
        }) { (error) -> (Void) in
            
        }
    }
    func equipWorkFinish() {
        let par = ["equipId" : (self.scanModel?.equipId)!, "cargoId" : self.cargoId]
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipWorkFinish), parameter: nil, method: .post, encoding: par.dictToJsonString(), headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
            self.removeDef()
        }) { (error) -> (Void) in
            if let error = error as? JSON{
                if error["msg"].stringValue.contains("不存在正在工作的作业") {
                    self.removeDef()
                }
            }
        }
    }
    
    func removeDef() {
        if let userAccount = EmsGlobalObj.obj.loginData?.userAccount , let equipId = self.scanModel?.equipId  {
            let def = DefaultsKey<[String : Any]?>(userAccount + equipId, defaultValue: nil)
            Defaults.remove(def)///提交后移除
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
extension ScanWorkViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewViewModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        return self.tableViewViewModel[indexPath.section][indexPath.row].tableViewViewModel(tableView, cellForItemAt: indexPath, cellClosure: { [weak self] (model: ViewModelOperate? , cell) in
            
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 2 , !(self.scanWorkDefModel.isWorkStart ?? false) {
            self.simplePickView.simplePickClosure = {[weak self] row , smodel in
                if let model = smodel as? BasicDataRespVos, var cellModel = self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? EquipmentInfoDetail {
                    if let cargoId = model.id {
                        cellModel.infoValue = model.basicName
                        self?.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model = cellModel
                        self?.tableView.reloadRows(at: [indexPath], with: .none)
                        self?.cargoId = cargoId
                        self?.scanWorkDefModel.cargoId = cargoId
                        self?.scanWorkDefModel.cargoName = model.basicName
                    }
                }
            }
            self.simplePickView.isHidden = false
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.001
    }
}

extension ScanWorkViewController{
    func getEquipInfoBaseId() {
        if let equipId = self.scanModel?.equipId  {
            AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipInfoBaseId + equipId), parameter: nil, method: .get, encoding: URLEncoding.httpBody, success: { (result) -> (Void) in
                self.equipInfoScanModel = EquipInfoScanModel.deserialize(from: result["data"].dictionaryObject)
                if self.scanModel?.equipSmallCategoryId == nil{
                    self.scanModel?.equipSmallCategoryId = self.equipInfoScanModel?.equipSmallCategoryId
                    self.scanModel?.equipMiddleCategoryId = self.equipInfoScanModel?.equipMiddleCategoryId
                    self.scanModel?.equipSmallCategoryId = self.equipInfoScanModel?.equipSmallCategoryId
                }
                self.planDailyInspectionByType()
                if let viewModels = EquipInfoScanViewModel.equipInfoScanViewModel(self.equipInfoScanModel, scanWorkDefModel: self.scanWorkDefModel){
                    self.tableViewViewModel = viewModels
                    self.tableView.reloadData()
                }
            }) { (error) -> (Void) in

            }
        }
    }
    
    func getEquipWorkCargoList() {
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipWorkCargoList), parameter: nil, method: .get, success: { (result) -> (Void) in
            let basicDataRespVos = [BasicDataRespVos].deserialize(from: result["data"].arrayObject) as? [BasicDataRespVos]
            self.basicDataRespVos = basicDataRespVos
//            self.tableView(self.tableView, didSelectRowAt: IndexPath(row: 2, section: 1))
            self.simplePickView.pickerModels = self.basicDataRespVos?.first?.basicDataRespVos
            self.simplePickView.isHidden = true
            self.simplePickView.simplePickClosure?(2, self.basicDataRespVos?.first?.basicDataRespVos?.first)
        }) { (error) -> (Void) in
            
        }
    }
}

