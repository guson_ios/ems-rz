//
//  DailyInspectionModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/16.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

struct DailyModel: HandyJSON {
    var standardCheckRespVoList: [DailyInspectionModel]?
    var id: String?///日检计划id
    var equipMiddleCategoryId: String?
    var equipMiddleCategory: String?
    var equipBigCategory: String?
    var standardCheckIds: String?
    var equipBigCategoryId: String?
    var planTitle: String?
    var equipSmallCategory: String?
    var remark: String?
    var equipSmallCategoryId: String?
}

class DailyInspectionModel: NSObject, HandyJSON, ViewModelOperate {
    var equipPartId: String?
    var equipPositionId: String?
    var modifiedUser: String?
    var checkMethod: String?
    var modifiedTime: String?
    var deleted: String?
    var equipSmallCategoryName: String?
    var checkMeasure: String?
    var equipPartName: String?
    var equipBigCategoryName: String?
    var equipPositionName: String?
    var createdUser: String?
    var equipMiddleCategoryId: String?
    var checkStandard: String?
    var id: String?
    var equipSmallCategoryId: String?
    var checkContent: String?
    var createdTime: String?
    var equipMiddleCategoryName: String?
    var spectionParts: [SpectionPartModel]?
    
    required override init() {}

}

struct DailySpectionViewModel {
    ///日检
    static func dailySpectionViewModel(_ spectionRecords: [DailyInspectionModel]?, equipInfoScanModel: EquipInfoScanModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        
        if let model = equipInfoScanModel  {
            let equSummary: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: EquSummaryCell.reuseIdentifier))]
            tableViewViewModel.append(equSummary)
        }
        
        if let models = spectionRecords {
            for (_, model) in models.enumerated() {
                var spectionViewModels: [TableViewViewModel<ViewModelOperate>] = []
                spectionViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: MaintainTableViewCell.reuseIdentifier)))
                if let partModels = model.spectionParts {
                    for (_, partModel) in partModels.enumerated() {
                        spectionViewModels.append(TableViewViewModel(baseViewModel: BaseViewModel(model: partModel, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier)))
                    }
                }
                tableViewViewModel.append(spectionViewModels)
            }
        }
        return tableViewViewModel
    }
    
    ///巡检详情
    static func spectionDetailModels(_ models: [SpectionPartModel]?) -> [[TableViewViewModel<ViewModelOperate>]]?{
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let partModels = models {
            for (_, partModel) in partModels.enumerated() {
                var vm: [TableViewViewModel<ViewModelOperate>] = []
                let baseViewModel = EquipmentInfoDetail(infoKey: partModel.equipPart, infoValue: "")
                let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                vm.append(partViewModel)
                if let spectionPartStandardModels = partModel.standardCheckRespVoList {
                    for (_, spectionPartStandardModel) in spectionPartStandardModels.enumerated() {

                        vm += spectionPartStandardModel.spectionPartStandardViewModels().map { (model) -> TableViewViewModel<ViewModelOperate> in
                            return TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
                        }
                        
                        
                        let baseViewModel = EquipmentInfoDetail(infoKey: "是否报修", infoValue: "", cellStatus: spectionPartStandardModel.isCheck)
                        let partViewModel = TableViewViewModel(baseViewModel: BaseViewModel(model: baseViewModel as ViewModelOperate, cellReuseIdentifier: SwitchTableViewCell.reuseIdentifier))
                        vm.append(partViewModel)
                    }
                }
                tableViewViewModel.append(vm)
            }
        }
        return tableViewViewModel
    }
}
