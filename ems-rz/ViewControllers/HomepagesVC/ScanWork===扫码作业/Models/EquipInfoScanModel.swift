//
//  EquipInfoScanModel.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/15.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation

struct EquipInfoScanModel: HandyJSON, ViewModelOperate{
    var useOrgId: String?
    var equipSystemCode: String?
    var unitName: String?
    var equipTechStateName: String?
    var equipState: String?
    var equipMiddleCategoryName: String?
    var equipBigCategoryName: String?
    var useOrgName: String?
    var responsiCode: String?
    var equipBigCategoryId: String?
    var equipMiddleCategoryId: String?
    var companyName: String?
    var equipSmallCategoryName: String?
    var equipCode: String?
    var id: String?
    var companyId: String?
    var equipSmallCategoryId: String?
    var equipName: String?
    var insuranceDate: String?
    var equipStateName: String?
    var modelNumber: String?
    var equipTechState: String?
    var unit: String?
    var isParticularName: String?
    var specificCode: String?
    var principal: String?
    var isParticular: String?
    
    func scanWorkContent(_ model: ScanWorkDefModel?) -> [EquipmentInfoDetail] {
        return [
            EquipmentInfoDetail(infoKey: "开机小时表数", infoValue: model?.workTime),
            EquipmentInfoDetail(infoKey: "累计时间", infoValue: model?.workTimeLong),
            EquipmentInfoDetail(infoKey: "作业货种", infoValue: model?.cargoName, cellType: .leftImageHide),
        ]
    }
}

struct EquipInfoScanViewModel {
    ///巡检
    static func equipInfoScanViewModel(_ equipInfoScanModel: EquipInfoScanModel?, scanWorkDefModel: ScanWorkDefModel?) -> [[TableViewViewModel<ViewModelOperate>]]? {
        var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]]  = []
        if let model = equipInfoScanModel  {
            let equSummary: [TableViewViewModel<ViewModelOperate>]  = [TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: EquSummaryCell.reuseIdentifier))]
            tableViewViewModel.append(equSummary)
        }
        
        if let scanModels =  equipInfoScanModel?.scanWorkContent(scanWorkDefModel).map({ (model) -> TableViewViewModel<ViewModelOperate> in
            TableViewViewModel(baseViewModel: BaseViewModel(model: model, cellReuseIdentifier: UniversalTableViewCell.reuseIdentifier))
        }) {
            tableViewViewModel.append(scanModels)
        }
        return tableViewViewModel
    }
}

