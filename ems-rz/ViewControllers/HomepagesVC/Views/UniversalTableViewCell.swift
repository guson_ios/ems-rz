//
//  UniversalTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/17.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

enum UniversalCellType {
    case leftImageHide
    case rightImageHide
    case imageHide
    case defineCell
}
class UniversalTableViewCell: UITableViewCell {

    @IBOutlet weak var leftImageWidthCon: NSLayoutConstraint!
    @IBOutlet weak var leftLabelLeadingCon: NSLayoutConstraint!
    @IBOutlet weak var rightImageWidthCon: NSLayoutConstraint!
    @IBOutlet weak var rightImgLeadingCon: NSLayoutConstraint!
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var leftLabel: UILabel!
    
    @IBOutlet weak var rightLabel: UILabel!
    
    @IBOutlet weak var rightImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func confignUniversalCell(_ cellType: UniversalCellType = .defineCell){
        self.cellConstraint(cellType)
    }
    
    private func cellConstraint(_ cellType: UniversalCellType = .defineCell){
        switch cellType {
        case .leftImageHide:
            self.leftImageWidthCon.constant = 0
            self.leftLabelLeadingCon.constant = 0
        case .rightImageHide:
            self.rightImageWidthCon.constant = 0
            self.rightImgLeadingCon.constant = 0
        case .imageHide:
            self.leftImageWidthCon.constant = 0
            self.leftLabelLeadingCon.constant = 0
            self.rightImageWidthCon.constant = 0
            self.rightImgLeadingCon.constant = 0
            
        case .defineCell: break
                    
        }
    }
    
    /// EquipmentInfoDetail
    func confignUniversalCell(_ model: EquipmentInfoDetail?) {
        self.cellConstraint((model?.cellType)!)
        self.leftLabel.text     = model?.infoKey
        self.rightLabel.text    = model?.infoValue
    }
    func confignUniversalCell(_ model: PartInfoModel?) {
        self.cellConstraint(.imageHide)
        self.leftLabel.text = model?.partName
        self.rightLabel.text = (model?.intact)! ? "完好" : "不完好"
    }
    
    func confignUniversalCell(_ model: NewMaintainPartModel?) {
        self.cellConstraint(.imageHide)
        self.leftLabel.text = model?.partName
        self.rightLabel.text = (model?.intact)! ? "完好" : "不完好"
    }
    
    func confignUniversalCell(_ model: SpectionPartModel?) {
        self.cellConstraint(.imageHide)
        self.leftLabel.text = model?.equipPart
        self.rightLabel.text = (model?.intact)! ? "完好" : "不完好"
    }
    
    func confignUniversalCell(_ model: CheckAppAlreadyDetail?) {
        self.cellConstraint(.imageHide)
        self.leftLabel.text = model?.equipPartName
        self.rightLabel.text = model?.isRepair
    }
}

