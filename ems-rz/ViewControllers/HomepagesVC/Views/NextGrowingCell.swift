//
//  NextGrowingCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/26.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit
import NextGrowingTextView

class NextGrowingCell: UITableViewCell {
    @IBOutlet weak var conHeight: NSLayoutConstraint!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var nextGrowingTextView: NextGrowingTextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nextGrowingTextView.maxNumberOfLines = 8
        nextGrowingTextView.textView.font = UIFont.systemFont(ofSize: 16)
        nextGrowingTextView.textView.textColor = COL666666
        nextGrowingTextView.textView.showsVerticalScrollIndicator = false
        nextGrowingTextView.textView.textContainerInset = .init(top: 8, left: 0, bottom: 8, right: 6)
        var str = "请输入"
        let textWidth = nextGrowingTextView.textView.frame.size.width - 32
        var size = NSString.calculationTextNeedSize(withText: str, font: 16, width: textWidth)
        while size.width < textWidth{
            str = " " + str
            size = NSString.calculationTextNeedSize(withText: str, font: 16, width: textWidth)
        }

        nextGrowingTextView.placeholderAttributedText = NSAttributedString(
            string: str,
            attributes: [
                .font: nextGrowingTextView.textView.font!,
                .foregroundColor: COL999999
            ]
        )
        let cHeight = self.conHeight.constant
        nextGrowingTextView.delegates.willChangeHeight = { [weak self] height in
            if height != self?.conHeight.constant{
                let superTableView = self?.superView(of: UITableView.self)
                superTableView?.beginUpdates()
                self?.conHeight.constant = height > cHeight ? height : cHeight
                superTableView?.endUpdates()
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confingNextGrowingCell(_ model: EquipmentInfoDetail?) {
        leftLabel.textColor = COL666666
        leftLabel.text = model?.infoKey
        nextGrowingTextView.textView.textAlignment = .right
        nextGrowingTextView.textView.text = model?.infoValue
//        if let infoValue = model?.infoValue {
//            
//        }
        nextGrowingTextView.isUserInteractionEnabled = model?.cellStatus ?? true
    }
    
}

