//
//  EquCollectionReusableView.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/21.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class EquReusableView: UIView {
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    
    @IBAction func rightBtnClick(_ sender: Any) {
    
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadXibView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadXibView()
    }
}

class EquCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var equReView: EquReusableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
