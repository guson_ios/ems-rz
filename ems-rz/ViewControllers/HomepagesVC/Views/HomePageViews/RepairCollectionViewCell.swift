//
//  RepairCollectionViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/16.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class RepairCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.addShadowWithBezierPath()
    }
    
    func confingRepairCell(_ model: RepairListModel) {
        label1.text = "单号：" + (model.dailyRecordCode ?? "")
        label2.text = model.faultRepairTime
        label3.text = model.equipName
        label4.text = model.detailedStatusStr()
    }
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let size = self.contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var cellFrame = layoutAttributes.frame
        cellFrame.size.height = size.height
//        cellFrame.size.width = size.width
        layoutAttributes.frame = cellFrame
        return layoutAttributes;

    }
}
