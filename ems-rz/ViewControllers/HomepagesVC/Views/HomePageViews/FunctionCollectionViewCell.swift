//
//  FunctionCollectionViewCell.swift
//  ems-Manager
//
//  Created by mac on 2019/4/29.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

struct FunctionCellConstraint {
    var cellWidth: CGFloat?
    var imageToTopConstraint: CGFloat?
    var labelToImageConstraint: CGFloat?
    var labelToBottomConstraint: CGFloat?
}


 class FunctionCollectionViewCell: UICollectionViewCell {
    let topContraint = FunctionCellConstraint.init(cellWidth: KScreenWidth/4 - 0.1, imageToTopConstraint: 11, labelToImageConstraint: 7, labelToBottomConstraint: 7)
    let bottomContraint = FunctionCellConstraint.init(cellWidth: KScreenWidth/4 - 0.1,imageToTopConstraint: 10, labelToImageConstraint: 15, labelToBottomConstraint: 17)
    
    
    
    @IBOutlet weak var imageToTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelToImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelToBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellWidth: NSLayoutConstraint!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var valueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    func configCell<T>(_ constraint: FunctionCellConstraint, model: T){
        
        self.cellWidth.constant = constraint.cellWidth!
        self.imageToTopConstraint.constant = constraint.imageToTopConstraint!
        self.labelToImageConstraint.constant = constraint.labelToImageConstraint!
        self.labelToBottomConstraint.constant = constraint.labelToBottomConstraint!
        
        if let viewModel = model as? FunctionViewModel {
            self.valueLabel.text = viewModel.operateName.rawValue
            self.imageView.image = UIImage(named: (viewModel.opModel != nil) ? viewModel.imageName : viewModel.norImageName)
            self.isUserInteractionEnabled = (viewModel.opModel != nil) ? true : false
            self.imageView.showBadge(with: .number, value: viewModel.badgeNum ?? 0, animationType: .none)
        }
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let size = self.contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var cellFrame = layoutAttributes.frame
        cellFrame.size.height = size.height
        cellFrame.size.width = size.width
        layoutAttributes.frame = cellFrame
        return layoutAttributes;

    }
    
}

