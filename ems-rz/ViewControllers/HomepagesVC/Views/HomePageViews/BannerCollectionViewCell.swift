//
//  BannerCollectionViewCell.swift
//  ems-Manager
//
//  Created by mac on 2019/5/9.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
//import SDCycleScrollView
class BannerCollectionViewCell: UICollectionViewCell {
    
   
    override init(frame: CGRect) {
        super.init(frame: frame)
//        let cycleScrollView = SDCycleScrollView(frame: self.frame, delegate: self as? SDCycleScrollViewDelegate, placeholderImage: UIImage(named: "bd_banner"))
//        self.addSubview(cycleScrollView!)
        let imageView = UIImageView(frame: frame)
        imageView.image = UIImage(named: "homePage_banner")
        self.contentView.addSubview(imageView)
//        self.contentView.backgroundColor = .red
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
