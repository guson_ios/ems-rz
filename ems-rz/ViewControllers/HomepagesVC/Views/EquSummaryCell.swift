//
//  EquSummaryCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/25.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit

class EquSummaryCell: UITableViewCell {

    @IBOutlet weak var summaryView: EquSummaryView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confignSummaryCell(_ maintainRecordModel: MaintainRecordModel?) {
        let peoStr = "作业人员：" + (EmsGlobalObj.obj.loginData?.userName ?? "")
        let mString = NSMutableAttributedString(string: peoStr  + "\n" + "设备名称：" + (maintainRecordModel?.equipName ?? ""))
        summaryView.lable1.attributedText = NSString.getOrderDetailString(mString, lineSpace: 10)
//        summaryView.lable1.text = "设备名称：" + (maintainRecordModel?.equipName ?? "")
        summaryView.label2.text = "所属单位：" + (maintainRecordModel?.useCompanyName ?? "")
        summaryView.label3.text = "所属部门：" + (maintainRecordModel?.useOrgName ?? "")
        summaryView.label4.isHidden = true
    }
    
    func confignSummaryCell(_ equipInfoScanModel: EquipInfoScanModel?) {
        let peoStr = "作业人员：" + (EmsGlobalObj.obj.loginData?.userName ?? "")
        let mString = NSMutableAttributedString(string: peoStr  + "\n" + "设备名称：" + (equipInfoScanModel?.equipName ?? ""))
        summaryView.lable1.attributedText = NSString.getOrderDetailString(mString, lineSpace: 10)

//        summaryView.lable1.text = "设备名称：" + (equipInfoScanModel?.equipName ?? "")
        summaryView.label2.text = "所属单位：" + (equipInfoScanModel?.companyName ?? "")
        summaryView.label3.text = "所属部门：" + (equipInfoScanModel?.useOrgName ?? "")
        summaryView.label4.isHidden = true
    }
    
    func confignSummaryCell(_ newMaintainPositionModel: NewMaintainPositionModel?) {
        let peoStr = "作业人员：" + (EmsGlobalObj.obj.loginData?.userName ?? "")
        let mString = NSMutableAttributedString(string: peoStr  + "\n" + "设备名称：" + (newMaintainPositionModel?.equipName ?? ""))
        summaryView.lable1.attributedText = NSString.getOrderDetailString(mString, lineSpace: 10)

//        summaryView.lable1.text =  "设备名称：" + (newMaintainPositionModel?.equipName ?? "")
        summaryView.label2.text = "工单号：" + (newMaintainPositionModel?.planRecordCode ?? "")
        summaryView.label3.text = "计划名称：" + (newMaintainPositionModel?.planName ?? "")
        summaryView.label4.isHidden = true
    }
    
    
    func confignSummaryCell(_ model: AcceptanceListModel?) {
        summaryView.lable1.text = "工单号：" + (model?.orderCode ?? "")
        summaryView.label2.text = "计划名称：" + (model?.planName ?? "")
        summaryView.label3.text = "保养员：" + (EmsGlobalObj.obj.loginData?.userName ?? "")
        summaryView.label4.text = "设备名称：" + (model?.equipName ?? "")
    }
    
    func confignSummaryCell(_ model: EquipBaseInfoRespVo?) {
        summaryView.lable1.text = "设备名称：" + (model?.equipName ?? "")
        summaryView.label2.text = "设备系统编号：" + (model?.equipSystemCode ?? "")
        summaryView.label3.text = "所属单位：" + (model?.companyName ?? "")
        summaryView.label4.text = "使用部门：" + (model?.useOrgName ?? "")
    }
    
}
