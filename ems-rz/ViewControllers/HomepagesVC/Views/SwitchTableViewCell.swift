//
//  SwitchTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/6.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

struct SwitchTableViewModel {
    
    
}

let cellEnable = "switchCellEnable"

class SwitchTableViewCell: UITableViewCell {
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func switchSelected(_ sender: Any) {
    }
    
    func confignWsitchCell(_ model: EquipmentInfoDetail?) {
        leftLabel.text = model?.infoKey
        rightSwitch.isOn = model?.infoValue?.stringBoolValue() ?? false
        if model?.cellStatus ?? false{
            rightSwitch.isEnabled =  false
        }else{
            rightSwitch.isEnabled = true
        }
       
    }
}
