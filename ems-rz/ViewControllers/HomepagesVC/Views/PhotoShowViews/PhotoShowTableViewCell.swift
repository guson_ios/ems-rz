//
//  PhotoShowTableViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/18.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import ZLPhotoBrowser

typealias assetResult = (_ fileData: Data?, _ fileName: String?) -> Void
class PhotoShowView: UIView {
    
    var imageOrVideo: Bool = false///默认选择图片
    var maxSelectCount = 6
    var defimage = "bd_camera" ///默认选择图片背景
    
    
    var lineCount: Int = 4{
        willSet{
            
        }
        didSet{
            self.collectionViewRemakeConstraint()
        }
    }
    var closure: ((_ imageArrays: [Any]?) -> Void)?
    var selectPhoto: Bool = false{
        willSet{
            
        }
        didSet{
            self.collectionViewRemakeConstraint()
        }
    }
    
    public var imageArrays: [Any]?{
        willSet{
            
        }
        didSet{
            self.collectionViewRemakeConstraint()
        }
    }
    
    public  let collectionView : UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = COLF4F4F4
        collectionView.isScrollEnabled = false
        return collectionView
    }()
    
    override func draw(_ rect: CGRect) {
        self.collectionView.reloadData()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addCollectionView()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.addCollectionView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = self.bounds
    }
    
    func addCollectionView() {
        self.addSubview(self.collectionView)
        self.registerCollectionViewCell()
        collectionView.frame = self.bounds
        collectionView.delegate = self
        collectionView.dataSource = self
        let collectViewHeight = collectionView.collectionViewLayout.collectionViewContentSize.height
        let collectViewWidth = self.collectionView.collectionViewLayout.collectionViewContentSize.width
        self.collectionView.snp.makeConstraints({ (maker) in
            maker.edges.equalTo(self)
            maker.height.equalTo(collectViewHeight).priorityLow()
            maker.width.equalTo(collectViewWidth).priorityLow()
        })
    }
    
    func collectionViewRemakeConstraint() {
        self.collectionView.reloadData()
        let collectViewHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height
        let collectViewWidth = self.collectionView.collectionViewLayout.collectionViewContentSize.width
        self.collectionView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(self)
            maker.height.equalTo(collectViewHeight).priorityHigh()
            maker.width.equalTo(collectViewWidth).priorityHigh()
        }
    }
    
    func registerCollectionViewCell() {
        collectionView.register(ImageCollectionViewCell.nib, forCellWithReuseIdentifier: ImageCollectionViewCell.reuseIdentifier)
    }

}

extension PhotoShowView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.imageArrays?.count ?? 0) + (self.selectPhoto ? 1 : 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
        if indexPath.row >= self.imageArrays?.count ?? 0 {
            cell.deleteBtn.isHidden = true
            cell.imageView.image = UIImage(named: self.defimage)
        }else{
            cell.deleteBtn.isHidden = !self.selectPhoto
            if let image: UIImage = self.imageArrays?[indexPath.row] as? UIImage {
                cell.imageView.image = image
            }
            if let photoBrowerModel = self.imageArrays?[indexPath.row] as? ZLPhotoBrowerModel , let data = photoBrowerModel.showData{
                cell.imageView.image = UIImage(data: data)
            }
            if let viModel = self.imageArrays?[indexPath.row] as? RecordShowModel.VideoModel{
                cell.imageView.image = viModel.image
            }
        }
        cell.deleteBtn.rx.tap.bind { [weak self] _ -> Void  in
            if let strongSelf = self{
                strongSelf.imageArrays?.remove(at: indexPath.row)
                strongSelf.closure!(strongSelf.imageArrays)
            }
        }.disposed(by: cell.rx.reuseBag)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = (self.bounds.size.width - 10) / CGFloat(self.lineCount)
        let size = CGSize(width: itemWidth, height: itemWidth - CGFloat(self.lineCount * 2))
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photoActionSheet = ZLPhotoActionSheet()
        photoActionSheet.configuration.maxSelectCount = self.maxSelectCount
        photoActionSheet.configuration.maxPreviewCount = 5
        photoActionSheet.configuration.allowSelectVideo = self.imageOrVideo
        photoActionSheet.configuration.allowSelectImage = !self.imageOrVideo
        photoActionSheet.configuration.allowSelectOriginal = false
        photoActionSheet.configuration.allowEditImage = false
        photoActionSheet.configuration.allowRecordVideo = self.imageOrVideo
        photoActionSheet.configuration.exportVideoType = .mp4
        photoActionSheet.configuration.saveNewImageAfterEdit = false
        
        photoActionSheet.arrSelectedAssets = photoActionSheet.configuration.maxSelectCount > 1 ? NSMutableArray(array: self.getPhotoAssets()) : nil
        photoActionSheet.sender = UIApplication.getTopViewController()
        if indexPath.row >= self.imageArrays?.count ?? 0{
            photoActionSheet.showPhotoLibrary()

            photoActionSheet.selectImageBlock = { [weak self] (images: [UIImage]?,assets: [PHAsset], isOriginal: Bool)  in
                
                if self?.imageOrVideo ?? false {
                    self?.selectPhoto = true
                    for (i , asset) in assets.enumerated() {
                        if (asset.mediaType == PHAssetMediaType.video){
                            let phv = PHVideoRequestOptions()
                            phv.version = .current
                            phv.deliveryMode = .fastFormat///低等级
//                            phv.isNetworkAccessAllowed = true
                            PHImageManager.default().requestAVAsset(forVideo: asset, options: phv) { (avAssets, audioMix, dict) in
                                let avUrl = avAssets as? AVURLAsset
                                if let url = avUrl?.url, let resultData = try? Data(contentsOf: url){
                                    
                                    DispatchQueue.main.async {
                                        self?.imageArrays?.removeAll()
                                        let resource = PHAssetResource.assetResources(for: asset).first
                                        let photoBrowerModel = ZLPhotoBrowerModel()
                                        self?.getImageFromPHAsset(asset, result: { (data, fileName) in
                                            photoBrowerModel.showData = images?[i].compressImage()
                                        })
                                        photoBrowerModel.imageData = resultData
                                        photoBrowerModel.imageName = resource?.originalFilename
                                        photoBrowerModel.localIdentifier = asset.localIdentifier
                                        self?.imageArrays?.append(photoBrowerModel)
                                        self?.closure?(self?.imageArrays)
                                    }
                                }
                            }
                        }
                    }
                }else{
                    self?.imageArrays?.removeAll()
                    for (i, asset) in assets.enumerated() {
                        self?.imageArrays?.append(ZLPhotoBrowerModel())
                        self?.getImageFromPHAsset(asset, result: { (data, fileName) in
                            let photoBrowerModel = ZLPhotoBrowerModel()
                            photoBrowerModel.imageData = images?[i].compressImage()
                            photoBrowerModel.showData = images?[i].compressImage()
                            photoBrowerModel.imageName = fileName
                            photoBrowerModel.localIdentifier = asset.localIdentifier
                            self?.imageArrays?[i] = photoBrowerModel
                        })
                        
                    }
                    self?.closure?(self?.imageArrays)

                }
            }
            
        }else{
            
            if let viModels = self.imageArrays as? [RecordShowModel.VideoModel]{
                let previewPhotos = viModels.map { (viModel) -> [AnyHashable : Any] in
                    if let url = URL(string: viModel.url ?? ""){
                        return GetDictForPreviewPhoto(url, .urlVideo)
                    }
                    return [:]
                }
                photoActionSheet.previewPhotos(previewPhotos, index: indexPath.row, hideToolBar: true, complete: { _ in})
            }
            
            if let photosImage = self.imageArrays as? [UIImage] {
               let previewPhotos = photosImage.map { (image) -> [AnyHashable : Any] in
                    GetDictForPreviewPhoto(image, .uiImage)
                }
                photoActionSheet.previewPhotos(previewPhotos, index: indexPath.row, hideToolBar: true, complete: { _ in})
                
            }
            if (self.imageArrays as? [ZLPhotoBrowerModel]) != nil {
                photoActionSheet.previewSelectedPhotos(self.getPhotoImages(), assets: self.getPhotoAssets(), index: indexPath.row, isOriginal: false)
            }
            
        }
    }
    func getPhotoImages() -> [UIImage] {
        var images: [UIImage] = []
        if let imageArrays = self.imageArrays {
            for (_, zlModel) in imageArrays.enumerated() {
                if let model = zlModel as? ZLPhotoBrowerModel, let imageData = model.imageData {
                    if let image =  UIImage(data: imageData){
                        images.append(image)
                    }
                }
            }
        }
        return images
    }
    func getPhotoAssets() -> [PHAsset] {
        var assets: [PHAsset] = []
        if let imageArrays = self.imageArrays {
            for (_, zlModel) in imageArrays.enumerated() {
                if let model = zlModel as? ZLPhotoBrowerModel, let localIdentifier = model.localIdentifier{
                    let result = PHAsset.fetchAssets(withLocalIdentifiers: [localIdentifier], options: nil)
                    if result.count > 0 {
                        assets.append(result[0])
                    }
                }else{
                    assets.append(PHAsset())
                }
            }
        }
        return assets
    }

    func getImageFromPHAsset(_ asset: PHAsset,  result: @escaping assetResult) {
        var data = Data()
        let resource = PHAssetResource.assetResources(for: asset).first
        let mediaType = self.imageOrVideo ? PHAssetMediaType.video : PHAssetMediaType.image
        if asset.mediaType == mediaType {
            let options = PHImageRequestOptions()
            options.version = .current
            options.deliveryMode = .highQualityFormat
            options.isSynchronous = true
            PHImageManager.default().requestImageData(for: asset, options: options) { (resultData, dataUTI, orientation, info) in
                if let resultData = resultData{
                    data = resultData
                }
            }
        }
        if data.count > 0 {
            result(data, resource?.originalFilename)
        }else{
            result(nil, nil)
        }
    }
}





class PhotoShowTableViewCell: UITableViewCell {

    @IBOutlet weak var collectBgView: PhotoShowView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.collectBgView.selectPhoto = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
