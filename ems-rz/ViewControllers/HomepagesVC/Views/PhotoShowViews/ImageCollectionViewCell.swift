//
//  ImageCollectionViewCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/18.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit


class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
   
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let size = self.contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var cellFrame = layoutAttributes.frame
        cellFrame.size.height = size.height
        cellFrame.size.width = size.width
        layoutAttributes.frame = cellFrame
        return layoutAttributes;

    }
}
