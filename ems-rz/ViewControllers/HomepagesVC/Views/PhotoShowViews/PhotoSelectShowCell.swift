//
//  PhotoSelectShowCell.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/23.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import ZLPhotoBrowser



class PhotoSelectShowCell: UITableViewCell {

    @IBOutlet weak var photoSelectShowView: PhotoShowView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.photoSelectShowView.selectPhoto = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
