//
//  HomepageViewController.swift
//  ems-Manager
//
//  Created by mac on 2019/4/29.
//  Copyright © 2019 mac. All rights reserved.bnnb    sddsds
//

import UIKit

class HomepageViewController: BaseViewController {
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    var scanModel: FunctionViewModel?
    var tableViewViewModel: [[TableViewViewModel<ViewModelOperate>]] = []
    lazy var backlogModel = BacklogListModel()///
    var sectionBadge: [SectionBadgeModel]?///

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.maintainDailyAppList()

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.mainCollectionView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
            // Fallback on earlier versions
        }
        if let viewModel = HomepageModel.homepageViewModels(nil, nil, nil) {
             self.tableViewViewModel = viewModel
        }
        self.configCollectionLayout()
//        self.maintainDailyAppList()
    }
    func configCollectionLayout() {
        self.mainCollectionView.register(FunctionCollectionViewCell.nib, forCellWithReuseIdentifier: FunctionCollectionViewCell.reuseIdentifier)
        self.mainCollectionView.register(BannerCollectionViewCell.self, forCellWithReuseIdentifier: BannerCollectionViewCell.reuseIdentifier)
        self.mainCollectionView.register(RepairCollectionViewCell.nib, forCellWithReuseIdentifier: RepairCollectionViewCell.reuseIdentifier)
        self.mainCollectionView.register(RepairNormalCollectionViewCell.nib, forCellWithReuseIdentifier: RepairNormalCollectionViewCell.reuseIdentifier)
        self.mainCollectionView.register(EquCollectionReusableView.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: EquCollectionReusableView.reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        if #available(iOS 12.0, *) {
            layout.estimatedItemSize = CGSize(width: 100, height: 100)
        }
        self.mainCollectionView.collectionViewLayout = layout
    }
    // MARK: - 已办代办
    func maintainDailyAppList() {
        self.sectionBadge = [SectionBadgeModel(title: "维修待办", num: 0), SectionBadgeModel(title: "维修已办", num: 0)]
        var repairModels: [[RepairListModel]?] = [[], []]
        let semaphore = DispatchSemaphore(value: 1)
        let queue = dispatch_queue_global_t(label: "queue")

        for i in 0...1 {
            queue.async {
                _ = semaphore.wait(timeout: DispatchTime.distantFuture)
                AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsMaintainDailyAppList), parameter: pageDict("6", current: 1).combinationDict(["type" : String(i)]), method: .get, headers: headersFunc(contentType: .json), success: { (result) -> (Void) in
                    if let models = [RepairListModel].deserialize(from: result["data"]["records"].arrayObject) as? [RepairListModel]{
                        repairModels[i] = models
                        self.sectionBadge?[i].num = (i == 0) ? (Int(result["data"]["total"].stringValue) ?? 0) : 0
                    }
                    semaphore.signal()
                }) { (error) -> (Void) in
                    semaphore.signal()
                }
            }
        }
        queue.async {
            _ = semaphore.wait(timeout: DispatchTime.distantFuture)
            DispatchQueue.main.async {
                if let viewModel = HomepageModel.homepageViewModels(repairModels[0], repairModels[1], self.backlogModel) {
                    self.tableViewViewModel = viewModel
                    self.mainCollectionView.reloadData()
                    semaphore.signal()
                }
            }
        }
        AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsBacklogList), parameter: nil, method: .get, success: { (result) -> (Void) in
            if let model = BacklogListModel.deserialize(from: result["data"].dictionaryObject) {
                self.backlogModel = model
                if let viewModel = HomepageModel.homepageViewModels(repairModels[0], repairModels[1], self.backlogModel) {
                    self.tableViewViewModel = viewModel
                    self.mainCollectionView.reloadData()
                }
            }
        }) { (error) -> (Void) in
            
        }
    }
}

extension HomepageViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.tableViewViewModel.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tableViewViewModel[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.tableViewViewModel[indexPath.section][indexPath.row].collectionViewViewModel(collectionView, cellForItemAt: indexPath, cellClosure: { (model: ViewModelOperate?, cell) in
        })
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model.functionOperate({ [weak self] (model: FunctionViewModel?)  in
            switch model?.operateName {
            case .acceptance:
                if let vc = AcceptanceListVC.loadViewControllewWithNib as? AcceptanceListVC {
                    self?.show(vc, sender: nil)
                }
            case .maintain:
                let vc = NewMaintainListVC.loadViewControllewWithNib
                self?.show(vc, sender: nil)
            case .repair, .scanInquire, .tourOperate, .scanOperate:
                self?.scanModel = model
                let vc = ScanViewController()
                vc.hidesBottomBarWhenPushed = true
                vc.scanResultDelegate = self
                self?.show(vc, sender: nil)
                vc.hidesBottomBarWhenPushed = false
            case .spotCheck:
                let vc = MaintainListViewController.loadViewControllewWithNib
                self?.show(vc, sender: nil)
            default:
                break
            }
        })
        if indexPath.section > 1 {
            if let model = self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.model as? RepairListModel {
                if let vc = RepairStartVC.loadViewControllewWithNib as? RepairStartVC {
                    vc.repairListModel = model
                    vc.checkStatus = indexPath.section - 2
                    self.show(vc, sender: nil)
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: EquCollectionReusableView.reuseIdentifier, for: indexPath) as! EquCollectionReusableView
        if let sectionBadgeModel = self.sectionBadge{
            let sModel = sectionBadgeModel[indexPath.section - 2]
            reusableView.equReView.leftLabel.text = sModel.title
            reusableView.equReView.leftLabel.showBadge(with: .number, value: sModel.num, animationType: .none)
        }
        reusableView.equReView.rightBtn.rx.tap.subscribe { [weak self] _ in
            if let vc = RepairListViewController.loadViewControllewWithNib as? RepairListViewController {
                vc.checkStatus = indexPath.section - 2
                self?.show(vc, sender: nil)
            }
        }.disposed(by: reusableView.rx.reuseBag)
        return reusableView
    }
}

extension HomepageViewController: LBXScanViewControllerDelegate{
    
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        print(scanResult.strScanned as Any)
        if let scanStr = scanResult.strScanned {
            let model = ScanModel.deserialize(from: String.stringToDict(scanStr))
            switch self.scanModel?.operateName {
            case .repair:
                if let vc = FaultRepairViewController.loadViewControllewWithNib as? FaultRepairViewController {
                    vc.equipRepairModel?.equipSmallCategoryId = model?.equipSmallCategoryId
                    vc.equipRepairModel?.equipId = model?.equipId
                    vc.equipRepairModel?.equipPartId = model?.equipPositionId
                    vc.isScanRepair = true
                    self.show(vc, sender: nil)
                }
            case .tourOperate:
                if let vc = SpectionRecordVC.loadViewControllewWithNib as? SpectionRecordVC {
                    vc.scanModel = model
                    self.show(vc, sender: nil)
                }
            case .scanOperate:
                if let vc = ScanWorkViewController.loadViewControllewWithNib as? ScanWorkViewController {
                    vc.scanModel = model
                    self.show(vc, sender: nil)
                }
            case .scanInquire:
                if let equipId = model?.equipId{
                    AlamofireNetworking.sharedInstance.requestData(afUrl(addUrl: emsEquipInfo), parameter: ["equipId" : equipId], method: .get, success: { (result) -> (Void) in
                        let model = EquipmentPanoramic.deserialize(from: result["data"].dictionaryObject)
                        let vc = EquipmentInfoVC()
                        vc.equipmentPanoramic = model
                        self.show(vc, sender: nil)
                    }) { (error) -> (Void) in   }
                }
            default:
                break
            }
        }
    }
}

extension HomepageViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.tableViewViewModel[indexPath.section][indexPath.row].baseViewModel.size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 || section == 1 {
           return UIEdgeInsets.zero
        }
        return UIEdgeInsets(top: 5, left: 6, bottom: 5, right: 6)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return section == 1 ? 0 : 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return section == 1 ? 0 : 6
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
       return (section == 0 || section == 1) ? CGSize.zero : CGSize(width: KScreenWidth, height: 44)
    }
}


