//
//  IGHomePageViewController.swift
//  ems-Manager
//
//  Created by mac on 2019/5/9.
//  Copyright © 2019 mac. All rights reserved.
//
import IGListKit
import UIKit

class IGHomePageViewController: BaseViewController, ListAdapterDataSource {

    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 100, height: 40)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor(red: 0.831_372_549, green: 0.945_098_039, blue: 0.964_705_882, alpha: 1)
        return collectionView
    }()
    
    let data = [
//        BannerModels(pk: 1, name: "model", handle: "spe"),
//        GridItem(color: UIColor(red: 112 / 255.0, green: 192 / 255.0, blue: 80 / 255.0, alpha: 1), itemCount: 3),

        ] as [Any]
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    // MARK: ListAdapterDataSource
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return data as! [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        case is GridItem: return FunctionSectionController()
        default:          return BannerSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? { return UIView(frame: self.view.frame) }


}
