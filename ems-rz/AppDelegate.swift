//
//  AppDelegate.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/14.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

//@available(iOS 13.0, *)
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    // timi 2020-04月开始接手,timi是谁，timi是易团福
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 13.0, *){
            
        }else{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = LoginViewController.loadViewControllewWithNib
            self.window?.makeKeyAndVisible()
        }
        let keyboardManager = IQKeyboardManager.shared
        keyboardManager.enable = true
        keyboardManager.shouldResignOnTouchOutside = true
        keyboardManager.enableAutoToolbar = true
        keyboardManager.keyboardDistanceFromTextField = 20
        keyboardManager.layoutIfNeededOnUpdate = true
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

