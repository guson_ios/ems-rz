//
//  UIAlertController+Extension.swift
//  ems-rz
//
//  Created by Gusont on 2020/3/17.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation


extension UIAlertController{
    static func alertShow(_ title: String?, message: String?, sureBtnTitle: String?, cancelBtnTitle: String?, sureBtnClick: (() -> ())?, cancelBtnClick: (() -> ())?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if cancelBtnTitle != nil {
            alert.addAction(UIAlertAction(title: cancelBtnTitle, style: .destructive, handler: { (action) in
                cancelBtnClick?()
            }))
        }
        if sureBtnTitle != nil {
            alert.addAction(UIAlertAction(title: sureBtnTitle, style: .default, handler: { (action) in
                sureBtnClick?()
            }))
        }
        UIApplication.getTopViewController()?.present(alert, animated: true, completion: nil)
    }
    
   static func alertShowTextField(_ title: String?, message: String?, sureBtnTitle: String?, sureBtnClick: ((_ text: String?) -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.keyboardType = .URL
        }
        if sureBtnTitle != nil {
            alert.addAction(UIAlertAction(title: sureBtnTitle, style: .default, handler: { (action) in
                sureBtnClick?(alert.textFields?.last?.text)
            }))
        }
        UIApplication.getTopViewController()?.present(alert, animated: true, completion: nil)
        
    }
    
}

