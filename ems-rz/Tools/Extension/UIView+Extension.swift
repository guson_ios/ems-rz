//
//  UIView+Extension.swift
//  ems-Manager
//
//  Created by mac on 2019/5/6.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UITableViewCell,UICollectionViewCell reuseIdentifier nib


extension UIView{
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    func loadXibView(){
        if let bundles = Bundle.main.loadNibNamed(type(of: self).reuseIdentifier, owner: self, options: nil) {
            for (_, bundle) in bundles.enumerated() {
                if let view = bundle as? UIView,  view.isKind(of: UIView.self)  {
                    view.frame = self.bounds
                    self.addSubview(view)
                    return
                }
            }
        }
    }
}

extension UIView{
    func addShadowWithBezierPath() {
        self.layer.shadowColor = COLshadowColor.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 5.0
        self.layer.cornerRadius = 4
        self.layer.shadowOffset = CGSize.zero
    }
}


extension UIView {
    //返回该view所在的父view
    func superView<T: UIView>(of: T.Type) -> T? {
        for view in sequence(first: self.superview, next: { $0?.superview }) {
            if let father = view as? T {
                return father
            }
        }
        return nil
    }
}

