//
//  NSString+Category.h
//  SharedParking
//
//  Created by gui_huan on 2018/4/16.
//  Copyright © 2018年 gui_huan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Category)


+(CGSize) boundingALLRectWithSize:(NSString*) txt Font:(UIFont*) font Size:(CGSize) size;
/**
 字符串中某个字符的自定义颜色

 @param totalString 完整字符串
 @param totalColor 完整字符串颜色
 @param specialString 所需更改的字符
 @param SpecialColor 所需更改的字符颜色
 @param fontSize 所需更改的字符大小
 @return <#return value description#>
 */
+ (NSAttributedString *)getShowAttributedStringWithTotalString:(NSString *)totalString totalColor:(UIColor *)totalColor specialString:(NSString *)specialString specialColor:(UIColor *)SpecialColor specialfontOfSize:(CGFloat)fontSize;

///同下
+ (NSAttributedString *)getShowAttributedStringWithNormalString:(NSString *)normalString normalColor:(UIColor *)normalColor specialString:(NSString *)specialString specialColor:(UIColor *)SpecialColor specialfontOfSize:(CGFloat)fontSize;
/**
 ///两种不同形式的String组合
 
 @param normalString 普通状态String
 @param specialString 另一种显示
 @param color <#color description#>
 @param fontSize <#fontSize description#>
 @return <#return value description#>
 */
+ (NSAttributedString *)getShowAttributedStringWithNormalString:(NSString *)normalString specialString:(NSString *)specialString specialColor:(UIColor *)color specialfontOfSize:(CGFloat)fontSize;

/*!
 @method
 @describe 计算获取文本的大小
 */
+ (CGSize)calculationTextNeedSizeWithText:(NSString *)text
                                     font:(CGFloat)font
                                    width:(CGFloat)width;



/**
 文本间距

 @param str 文本
 @param space 间距
 @return <#return value description#>
 */
+ (NSMutableAttributedString *)getOrderDetailString:(NSAttributedString *)str lineSpace:(CGFloat)space;
/**
 <#Description#>

 @param size 依附视图的大小
 @param text 文本
 @param font 文本大小
 @return 文本居于视图中心时的起始位置
 */
+ (CGPoint)calculationPointWithSize:(CGSize)size
                                text:(NSString *)text
                                font:(CGFloat)font;

/**
 调用一个get方法，也不算添加属性，仅方便书写
 金钱单位转换
 */
@property (nonatomic, copy)NSString *moneyConversion;

//stringToDictionary
+ (NSDictionary *)stringToDictionary:(NSString *)str;

/**
 //把没有双引号和用了单引号的json字符串转化为标准格式字符串;


 @param json <#json description#>
 @return <#return value description#>
 */
+ (NSString *)changeJsonStringToTrueJsonString:(NSString *)json;



/**
 <#Description#>

 @param aString 获取汉字转成拼音字符串 通讯录模糊搜索 支持拼音检索 首字母 全拼 汉字 搜索
 @return <#return value description#>
 */
+ (NSString *)transformToPinyin:(NSString *)aString;


/**
 获取Cookie缓存

 @return <#return value description#>
 */
+ (NSMutableString*)getCookieValue;

@end
