//
//  NSString+Category.m
//  SharedParking
//
//  Created by gui_huan on 2018/4/16.
//  Copyright © 2018年 gui_huan. All rights reserved.
//

#import "NSString+Category.h"
#import <objc/runtime.h>
@implementation NSString (Category)
static char *MoneyConversion = "moneyConversion";

+(CGSize) boundingALLRectWithSize:(NSString*) txt Font:(UIFont*) font Size:(CGSize) size{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:txt];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:2.0f];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, [txt length])];
    
    CGSize realSize = CGSizeZero;
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_6_1
    CGRect textRect = [txt boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font,NSParagraphStyleAttributeName:style} context:nil];
    realSize = textRect.size;
#else
    realSize = [txt sizeWithFont:font constrainedToSize:size];
#endif
    
    realSize.width = ceilf(realSize.width);
    realSize.height = ceilf(realSize.height);
    return realSize;
}


+ (NSAttributedString *)getShowAttributedStringWithTotalString:(NSString *)totalString totalColor:(UIColor *)totalColor specialString:(NSString *)specialString specialColor:(UIColor *)SpecialColor specialfontOfSize:(CGFloat)fontSize{
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString: totalString];
    NSRange normalRange = [totalString rangeOfString:totalString];
    [attributeString addAttribute:NSForegroundColorAttributeName value:totalColor range:normalRange];
    
    NSRange range = [totalString rangeOfString:specialString];
    [attributeString addAttribute:NSForegroundColorAttributeName value:SpecialColor range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:range];
    return attributeString;
}

+ (NSAttributedString *)getShowAttributedStringWithNormalString:(NSString *)normalString normalColor:(UIColor *)normalColor specialString:(NSString *)specialString specialColor:(UIColor *)SpecialColor specialfontOfSize:(CGFloat)fontSize{
    NSString * totalString = [NSString stringWithFormat:@"%@%@", normalString, specialString];
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString: totalString];
    NSRange normalRange = [totalString rangeOfString:normalString];
    [attributeString addAttribute:NSForegroundColorAttributeName value:normalColor range:normalRange];
    
    NSRange range = [totalString rangeOfString:specialString];
    [attributeString addAttribute:NSForegroundColorAttributeName value:SpecialColor range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:range];
    return attributeString;
}


+ (NSAttributedString *)getShowAttributedStringWithNormalString:(NSString *)normalString specialString:(NSString *)specialString specialColor:(UIColor *)color specialfontOfSize:(CGFloat)fontSize{
    
    NSString * totalString = [NSString stringWithFormat:@"%@%@", normalString, specialString ?: @""];
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString: totalString];
    NSRange range = [totalString rangeOfString:specialString ?: @""];
    
    [attributeString addAttribute:NSForegroundColorAttributeName value:color range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:range];
    return attributeString;
}

#pragma mark - 根据宽度计算文本的高度
+ (CGSize)calculationTextNeedSizeWithText:(NSString *)text
                                     font:(CGFloat)font
                                    width:(CGFloat)width{
    if (text) {
        NSDictionary *fontDic = @{NSFontAttributeName :[UIFont systemFontOfSize:font],
                                  };
        
        CGRect fontRect =  [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:fontDic context:nil];
        return fontRect.size;
    }
    return CGSizeZero;
    
}
+ (CGPoint)calculationPointWithSize:(CGSize)size
                                text:(NSString *)text
                                font:(CGFloat)font{
    NSDictionary *fontDic = @{NSFontAttributeName :[UIFont systemFontOfSize:font],
                              };
    
    CGSize textSize = [text sizeWithAttributes:fontDic];
    CGFloat pointX = (size.width - textSize.width) /2.;
    CGFloat pointY = (size.height - textSize.height) / 2.;
    return CGPointMake(pointX, pointY);

}


+ (NSMutableAttributedString *)getOrderDetailString:(NSAttributedString *)str lineSpace:(CGFloat)space{
    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    // 行间距设置
    [paragraphStyle setLineSpacing:space];
    NSMutableAttributedString  *setString = [[NSMutableAttributedString alloc] initWithAttributedString:str];
    [setString  addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    return setString;
}

- (NSString *)moneyConversion{
    
    NSString *str = [NSString stringWithFormat:@"%.2f", ([self integerValue] /100.0)];
    return str;
}
- (void)setMoneyConversion:(NSString *)moneyConversion{
    ///以分为单位
    objc_setAssociatedObject(self, MoneyConversion, moneyConversion, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+(NSDictionary *)stringToDictionary:(NSString *)str{
    NSError *err;
    NSString *jsonStr = [NSString changeJsonStringToTrueJsonString:str];
    NSData *data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
    if (err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return resultDic;
}
//把没有双引号和用了单引号的json字符串转化为标准格式字符串;
+ (NSString *)changeJsonStringToTrueJsonString:(NSString *)json
{
    // 将没有双引号的替换成有双引号的
    NSString *validString = [json stringByReplacingOccurrencesOfString:@"(\\w+)\\s*:([^A-Za-z0-9_])"
                                                            withString:@"\"$1\":$2"
                                                               options:NSRegularExpressionSearch
                                                                 range:NSMakeRange(0, [json length])];
    
    
    //把'单引号改为双引号"
    validString = [validString stringByReplacingOccurrencesOfString:@"([:\\[,\\{])'"
                                                         withString:@"$1\""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    
    validString = [validString stringByReplacingOccurrencesOfString:@"'([:\\],\\}])"
                                                         withString:@"\"$1"
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    
    //再重复一次 将没有双引号的替换成有双引号的
    validString = [validString stringByReplacingOccurrencesOfString:@"([:\\[,\\{])(\\w+)\\s*:"
                                                         withString:@"$1\"$2\":"
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    return validString;
}

+ (NSString *)transformToPinyin:(NSString *)aString
{
    //转成了可变字符串
    NSMutableString *str = [NSMutableString stringWithString:aString];
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    
    //再转换为不带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
    NSArray *pinyinArray = [str componentsSeparatedByString:@" "];
    NSMutableString *allString = [NSMutableString new];
    
    int count = 0;
    
    for (int  i = 0; i < pinyinArray.count; i++)
    {
        for(int i = 0; i < pinyinArray.count;i++)
        {
            if (i == count) {
                [allString appendString:@"#"];
                //区分第几个字母
            }
            [allString appendFormat:@"%@",pinyinArray[i]];
        }
        [allString appendString:@","];
        count ++;
    }
    NSMutableString *initialStr = [NSMutableString new];
    //拼音首字母
    for (NSString *s in pinyinArray)
    {
        if (s.length > 0)
        {
            [initialStr appendString:  [s substringToIndex:1]];
        }
    }
    [allString appendFormat:@"#%@",initialStr];
    [allString appendFormat:@",#%@",aString];
    return allString;
}

+ (NSMutableString*)getCookieValue{
    // 在此处获取返回的cookie
    NSMutableDictionary *cookieDic = [NSMutableDictionary dictionary];
    NSMutableString *cookieValue = [NSMutableString stringWithFormat:@""];
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [cookieJar cookies]) {
        [cookieDic setObject:cookie.value forKey:cookie.name];
        
    }
    // cookie重复，先放到字典进行去重，再进行拼接
    for (NSString *key in cookieDic) {
        NSString *appendString = [NSString stringWithFormat:@"%@=%@;", key, [cookieDic valueForKey:key]];
        [cookieValue appendString:appendString];
    }
    return cookieValue;
}




@end
