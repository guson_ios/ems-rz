//
//  UIColor+Category.h
//  ems-rz
//
//  Created by Gusont on 2020/1/9.
//  Copyright © 2020 xyj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


#define CHex666666 (UIColor *)[UIColor colorFromHexString:@"#666666"]
#define CHex3787FF (UIColor *)[UIColor colorFromHexString:@"#3787FF"]
#define CHex7D7D7E (UIColor *)[UIColor colorFromHexString:@"#7D7D7E"]
#define CHexB7B7B8 (UIColor *)[UIColor colorFromHexString:@"#B7B7B8"]
#define CHexFFFFFF (UIColor *)[UIColor colorFromHexString:@"#FFFFFF"]
#define CHex9AC1FA (UIColor *)[UIColor colorFromHexString:@"#9AC1FA"]
#define CHexFFCE37 (UIColor *)[UIColor colorFromHexString:@"#FFCE37"]

@interface UIColor (Category)

//选择按钮颜色
+ (UIColor *)muneBtnSelColor;
+ (UIColor *)muneBtnNorColor;
+ (UIColor *)labelTextColor;

+ (UIColor *)colorFromHexString:(NSString *)title alpha:(CGFloat)alpha;
@end

NS_ASSUME_NONNULL_END
