//
//  UIColor+Category.m
//  ems-rz
//
//  Created by Gusont on 2020/1/9.
//  Copyright © 2020 xyj. All rights reserved.
//

#import "UIColor+Category.h"

@implementation UIColor (Category)

+ (UIColor *)muneBtnSelColor{
    return [UIColor colorFromHexString:@"#3C84EF"];
}

+(UIColor *)muneBtnNorColor{
    return  [UIColor colorFromHexString:@"#666666"];
}

+ (UIColor *)labelTextColor{
    return [UIColor colorFromHexString:@"#AAAAAA"];
}

+ (UIColor *)colorFromHexString:(NSString *)title {
    return [self colorFromHexString:title alpha:1];
}

+ (UIColor *)colorFromHexString:(NSString *)title alpha:(CGFloat)alpha {
    // 去空格换行  转大写
    title = [[title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // 如果字符串长度小于6,格式不正确,返回透明的颜色
    if (title.length < 6) {
        return [UIColor clearColor];
    }
    
    // 去字符串头
    if ([title hasPrefix:@"0X"]) {
        title = [title substringFromIndex:2];
    }else if ([title hasPrefix:@"#"]) {
        title = [title substringFromIndex:1];
    }
    
    // 去字符串头后如果字符串长度不为6,格式不正确,返回透明颜色
    if (title.length != 6) {
        return [UIColor clearColor];
    }
    
    // 6位16进制转10进制
    NSString *rString = [title substringWithRange:NSMakeRange(0, 2)];
    NSString *gString = [title substringWithRange:NSMakeRange(2, 2)];
    NSString *bString = [title substringWithRange:NSMakeRange(4, 2)];
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:alpha];
}

@end
