//
//  UIImage+UIImage.h
//  ems-rz
//
//  Created by Gusont on 2020/3/12.
//  Copyright © 2020 xyj. All rights reserved.
//



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (UIImage)

+ (UIImage*) thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time;

+ (UIImage*) getVideoPreViewImage:(NSString *)path;
@end

NS_ASSUME_NONNULL_END
