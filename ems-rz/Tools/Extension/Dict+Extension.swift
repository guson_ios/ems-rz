//
//  Dict+Extension.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/28.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation

func pageDict(_ size: String = "10", current: Int) -> [String : String] {
    return ["size" : size, "current" : String(current)]
}
extension Dictionary{
    func combinationDict(_ dict: [Key: Value]) -> [Key: Value] {
        var newDict = self
        for (key, value) in dict {
            newDict[key] = value
        }
        return newDict
    }
    
    func dictToJsonString() -> String {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted) {
            return String(data: jsonData, encoding: .utf8) ?? ""
        }
        return ""
    }
    
}
