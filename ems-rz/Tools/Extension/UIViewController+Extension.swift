//
//  UIViewController+Extension.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/17.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

protocol afProgressPlugin: class {
    func progressShow()
    func progressHide()
}
extension UIViewController: afProgressPlugin{
    
    func progressShow() {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func progressHide() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    static var className: String {
        return String(describing: self)
    }
    static var loadViewControllewWithNib: UIViewController {
        return self.init(nibName: self.className, bundle: nil)
    }
}


extension UIApplication {

    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

extension UIViewController{
    static var vcIdentifier: String {
        return String(describing: self)
    }
}

