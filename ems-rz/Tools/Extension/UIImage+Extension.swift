//
//  UIImage+Extension.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/3.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation
import Kingfisher
extension UIImage{
    
    
    static  func loadRemoteImage(_ urlPath: String?) -> UIImage? {
        if let path = urlPath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: path) {
            if let data = try? Data(contentsOf: url) {
                return UIImage(data: data)
            }
        }
        return nil
    }
    
    func compressImage() -> Data? {
        if let data = self.jpegData(compressionQuality: 1) {
            var imageData = data
            
            var scale = 0.9
            while imageData.count / 1024 > 200 {
                imageData = self.jpegData(compressionQuality: CGFloat(scale))!
                scale -= 0.1
            }
            return imageData
        }
        return nil
    }
    
    func imageToString() -> String {
        let data = self.jpegData(compressionQuality: 1)
        return data?.base64EncodedString(options: .endLineWithLineFeed) ?? ""
    }
    
    func imageFromString(_ string: String) -> UIImage? {
        guard let data = Data(base64Encoded: string, options: .ignoreUnknownCharacters) else { return nil }
        return UIImage(data: data)
    }
    
//    func thumbnailImageForVideo(_ videiUrl: URL, time: TimeInterval) -> UIImage? {
//        let asset = AVURLAsset(url: videiUrl)
//        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
//        assetImageGenerator.appliesPreferredTrackTransform = true
//        assetImageGenerator.apertureMode = .encodedPixels
//
//        let thumbnailImageRef = NULL
//    }
}



