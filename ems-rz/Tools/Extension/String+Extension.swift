//
//  String+Extension.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/27.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation
import CommonCrypto

let passWordSha256 = "JxGF7gjNeje5evqzHk7z5GM3OR6OSZUFbvJRnRjKzxkop3OuQ40BVRU86jjmgvT4"
extension String{
    // MARK: - sha1
    func sha1() -> String{
        //UnsafeRawPointer
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        let newData = NSData.init(data: data)
        CC_SHA1(newData.bytes, CC_LONG(data.count), &digest)
        let output = NSMutableString(capacity: Int(CC_SHA1_DIGEST_LENGTH))
        for byte in digest {
            output.appendFormat("%02x", byte)
        }
        return output as String
        
    }
    
    // MARK: - sha1
    func sha256() -> String{
        //UnsafeRawPointer
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA256_DIGEST_LENGTH))
        let newData = NSData.init(data: data)
        CC_SHA256(newData.bytes, CC_LONG(data.count), &digest)
        let output = NSMutableString(capacity: Int(CC_SHA256_DIGEST_LENGTH))
        for byte in digest {
            output.appendFormat("%02x", byte)
        }
        return output as String
        
    }
    
    
    func ValidLength() -> String? {
        
        if self.count <= 0 {
            
        }
        return self
    }
    
    
    static func currentTimeDifference(_ startTimeTamp: String, dateFormat: String) -> String {
        
        let timeDiff =  Int((Double(String.currentTimeTamp())! - Double(startTimeTamp)!) / 1000)
        
        var m = 0
        m = timeDiff / 60 >= 60 ? (timeDiff % 3600) / 60 : timeDiff / 60
//        let s = timeDiff % 60
        let h = timeDiff / (3600)
        if h > 0 {
            let str = String(h) + "小时" + String(m) + "分钟"
            return str
        }
        let str = String(m) + "分钟"
        return str
        
    }
    
    static func currentTimeTamp() -> String {
        let date = Date(timeIntervalSinceNow: 0)
        let time = date.timeIntervalSince1970 * 1000
        return String(Int64(time))
    }
    
    static func currentTimeFormatter(_ dateFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        return formatter.string(from: Date())
    }
   static func timeFromFormatter(_ fomatter: String, timeString: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = fomatter
        return formatter.string(from: Date(timeIntervalSinceReferenceDate: Double(timeString)! / 1000))
    }

    static func stringToDict(_ dictStr: String) -> [String : Any]? {
       let str = NSString.changeJsonString(toTrueJsonString: dictStr)
        if let data = str?.data(using: .utf8) {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String : Any]{
                return dict
            }
        }
        return nil
    }
    
    func stringBoolValue() -> Bool {
        if self == "1" {
            return true
        }
        return false
    }
}

func boolToStringValue(_ value: Bool) -> String {
    return value ? "1" : "0"
}

