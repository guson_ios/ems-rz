//
//  CALayer+LayerColor.h
//  SharedParking
//
//  Created by gui_huan on 2018/4/10.
//  Copyright © 2018年 gui_huan. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
@interface CALayer (LayerColor)


@property (nonatomic, strong)UIColor * borderColorFromUIColor;

@end
