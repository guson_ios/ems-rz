//
//  NSLayoutConstraint+IBDesignable.m
//  SharedParking
//
//  Created by gui_huan on 2018/6/20.
//  Copyright © 2018年 tangshuanghui. All rights reserved.
//

#import "NSLayoutConstraint+IBDesignable.h"
#import <objc/runtime.h>

@implementation NSLayoutConstraint (IBDesignable)

#ifndef kSCREEN_HEIGHT
#define kSCREEN_HEIGHT   (CGFloat)([[UIScreen mainScreen] bounds].size.height)
#endif
#ifndef kSCREEN_WIDTH
#define kSCREEN_WIDTH (CGFloat)([[UIScreen mainScreen] bounds].size.width)
#endif
/** 参照宽高  375 * 667 **/
#define KReferenceHeight 667.f
#define KReferenceWidth 375.f
/** 宽高比例 **/
#define KScaleW  (CGFloat)(kSCREEN_WIDTH/KReferenceWidth)
- (void)setAdapterScreen:(BOOL)adapterScreen{
    
    NSNumber * adapterScreenNumber = [NSNumber numberWithBool:adapterScreen];
    objc_setAssociatedObject(self, @selector(adapterScreen), adapterScreenNumber, OBJC_ASSOCIATION_COPY);
    if (adapterScreen) {
        self.constant = self.constant * KScaleW;
    }
}

- (BOOL)adapterScreen{
    NSNumber *adapterScreenNumber = objc_getAssociatedObject(self, @selector(adapterScreen));
    
    return [adapterScreenNumber boolValue];
}

@end
