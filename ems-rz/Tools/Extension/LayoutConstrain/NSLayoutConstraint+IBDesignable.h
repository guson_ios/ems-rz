//
//  NSLayoutConstraint+IBDesignable.h
//  SharedParking
//
//  Created by gui_huan on 2018/6/20.
//  Copyright © 2018年 tangshuanghui. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (IBDesignable)

@property(nonatomic, assign) IBInspectable BOOL adapterScreen;

@end
