//
//  DefineColor.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/16.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation
import UIKit

let COLshadowColor = UIColor(red: 51/255.0, green: 120/255.0, blue: 220/255.0, alpha: 0.13)
let COLF4F4F4 = UIColor(hexString: "F4F4F4")
let COL9FB9F9 = UIColor(hexString: "9FB9F9")
let COL206EDD = UIColor(hexString: "206EDD")
let COL666666 = UIColor(hexString: "666666")
let COL999999 = UIColor(hexString: "999999")

let COL3C84EF = UIColor(hexString: "3C84EF")
let COL7AAAF4 = UIColor(hexString: "7AAAF4")
let COL3A88FC = UIColor(hexString: "3A88FC")
let COLB7B8BA = UIColor(hexString: "B7B8BA")
let COLA1A0A0 = UIColor(hexString: "A1A0A0")

let COL3787FF = UIColor(hexString: "3787FF")

let COLF97890 = UIColor(hexString: "F97890")




