//
//  Authorize.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/7.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation
import AVFoundation
import Photos
import AssetsLibrary


class AuthorizeTool: NSObject {
    

     // MARK: - ---定位权限
    static func authorizelocationWith(comletion:@escaping (Bool) -> Void) {
       let granted = CLLocationManager.authorizationStatus()
        switch granted {
        case CLAuthorizationStatus.denied, CLAuthorizationStatus.restricted:
            comletion(false)
        default:
            comletion(true)
        }
    }
     // MARK: - ---获取相册权限
       static func authorizePhotoWith(comletion:@escaping (Bool) -> Void) {
           let granted = PHPhotoLibrary.authorizationStatus()
           switch granted {
           case PHAuthorizationStatus.authorized:
               comletion(true)
           case PHAuthorizationStatus.denied, PHAuthorizationStatus.restricted:
               comletion(false)
           case PHAuthorizationStatus.notDetermined:
               PHPhotoLibrary.requestAuthorization({ (status) in
                   DispatchQueue.main.async {
                       comletion(status == PHAuthorizationStatus.authorized ? true:false)
                   }
               })
           @unknown default:
            break
        }
       }

       // MARK: - --相机权限
       static func authorizeCameraWith(comletion:@escaping (Bool) -> Void ) {
           let granted = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)

           switch granted {
           case .authorized:
               comletion(true)
               break
           case .denied:
               comletion(false)
               break
           case .restricted:
               comletion(false)
               break
           case .notDetermined:
               AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) in
                   DispatchQueue.main.async {
                       comletion(granted)
                   }
               })
           @unknown default:
            break
        }
       }

       // MARK: 跳转到APP系统设置权限界面
       static func jumpToSystemPrivacySetting() {
           let appSetting = URL(string: UIApplication.openSettingsURLString)

           if appSetting != nil {
               if #available(iOS 10, *) {
                   UIApplication.shared.open(appSetting!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
               } else {
                   UIApplication.shared.openURL(appSetting!)
               }
           }
       }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
