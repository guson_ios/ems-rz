//
//  URLDefine.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/14.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation



enum baseUrl: String {
    case define             = "http://172.17.6.15:7060"
    case osn                = "http://172.17.6.15:9010"
    
    case testOnLine         = "http://218.56.157.76:9010"
    case sjx                = "http://192.168.0.53:7060"
    case s1                 = "http://172.18.20.76:9010"
    case s2                 = "http://218.56.157.76:9011"
//    case s3                 = "http://172.17.6.15:9010"
//    case s4                 = "http://218.56.157.76:9010"

}
var afBaseUrl = baseUrl.testOnLine.rawValue
//var afBaseUrl = baseUrl.sjx


///
func afUrl(_ baseUrl: String = afBaseUrl, addUrl: String) -> String {
    return baseUrl + "/ems" + addUrl
}
// MARK: - 登录
let emsLogin                    = "/api/v1/login"

// MARK: - 查询我的资料
let emsUserInfo                   = "/api/v1/system/users/info"

// MARK: - 获取用户组织
let emsloginUserOrg             = "/api/v1/system/orgs/loginUserOrg"

// MARK: - 设备列表
let emsEquipInfoList            = "/api/v1/equipInfo/list"

// MARK: - 设备全景接口
let emsEquipInfo                = "/api/v1/equipInfo/info/id"

// MARK: - 技术资料列表
let emsTechnologyList                = "/api/v1/technology/list"

// MARK: - 基础数据
let emsBasicNameTree            = "/api/v1/basic/data/basicName/tree"

// MARK: - 公司
let emsCommonCompany            = "/api/v1/common/company"

// MARK: - 设备数量
let emsEquipTotalNumber            = "/api/v1/equipInfo/app/equipTotalNumber"

// MARK: - 组织
let emsCommonOrg                = "/api/v1/common/org"

// MARK: - 分页查询润滑保养记录列表-app端
let emsMaintainOrderList        = "/api/v1/plan/maintain/order/page/appList"

// MARK: - 资源上传
let emsFilesUpload              = "/api/v1/files/upload"

// MARK: - 故障管理列表--app
let emsFaultManageList          = "/api/v1/fault/manage/app/list"

// MARK: - 查询基础信息目录列表
let emsBasicData                = "/api/v1/basic/data"

// MARK: - 日常维修流程启动
let emsMaintainDailyStart       = "/api/v1/maintain/daily/start"

// MARK: - 点检计划待办列表
let emsPlanCheckAppBacklogList      = "/api/v1/plan/check/app/backlogList"

// MARK: - 点检计划已办列表
let emsPlanCheckAppAlreadyList      = "/api/v1/plan/check/app/alreadyList"

// MARK: - 点检计划获取标准信息列表
let emsPlanCheckAppStandardList     = "/api/v1/plan/check/app/standardList"

// MARK: - 点检计划提交
let emsPlanCheckAppUpdate           = "/api/v1/plan/check/app/update"

// MARK: - 点检计划提交 id来源于：扫码成功后提交点检计划返回的记录id
let emsPlanCheckAppUpAdd            = "/api/v1/plan/check/app/add"

// MARK: - 根据大中小类查询所有巡检部位-App接口
let emsPlanInspectionByType         = "/api/v1/planInspectionRecord/getPlanInspectionRecordPositionByType"

// MARK: - 根据大中小类部位查询巡检所有部件-App接口
let emsPlanInspectionByTypeAndPos        = "/api/v1/planInspectionRecord/getPlanInspectionRecordPartByTypeAndPosition"

// MARK: - 根据大中小类查询所有日检部位-App
let emsPlanDailyInspectionByType         = "/api/v1/planDailyInspection/getPlanDailyInspectionPositionByType"

// MARK: - 根据大中小类部位id查询所有日检部件-App
let emsPlanDailyInspectionByTypeAndPos        = "/api/v1/planDailyInspection/getPlanDailyInspectionPartByTypeAndPosition"

// MARK: - 生成巡检记录-App接口
let emsPlanInspectionRecordSave        = "/api/v1/planInspectionRecord/save"

// MARK: - 修改巡检记录-App接口
let emsPlanInspectionRecordUpdate          = "/api/v1/planInspectionRecord/update"

// MARK: - 生成日检记录-App接口
let emsPlanDailyInspectionRecordSave        = "/api/v1/planDailyInspectionRecord/save"

// MARK: - 修改生成日检计划记录
let emsPlanDailyInspectionRecordUpdate          = "/api/v1/planDailyInspectionRecord/update"

// MARK: - 获取设备基本信息
let emsEquipInfoBaseId        = "/api/v1/equipInfo/base/"

// MARK: - 获取货种列表
let emsEquipWorkCargoList        = "/api/v1/equip/work/cargo/list"

// MARK: - 开始作业
let emsEquipWorkStart        = "/api/v1/equip/work/start"

// MARK: - 结束作业
let emsEquipWorkFinish       = "/api/v1/equip/work/finish"

// MARK: - 分页查询润滑保养记录列表-app端
let emsPlanMaintainOrderList       = "/api/v1/plan/maintain/order/app/list"

// MARK: - 扫码获取润滑保养部位列表
let emsPlanMaintainOrderPosition       = "/api/v1/plan/maintain/order/app/position"

// MARK: - 扫码部位获取对应润滑标准内容
let emsPlanMaintainOrderPart       = "/api/v1/plan/maintain/order/app/part"

// MARK: - 开始保养
let emsPlanMaintainOrderStart       = "/api/v1/plan/maintain/order/app/start"

// MARK: - 保养完成提交(保养结束)
let emsPlanMaintainOrderEnd       = "/api/v1/plan/maintain/order/app/end"

// MARK: - app--设备履历--检测历史（运行数据统计）  废弃
let emsStatisticEquipMonthAppWork     = "/api/v1/statistic/equip/month/appWork"

// MARK: - 设备履历-指标数据
let emsEquipInfoIndicatorData     = "/api/v1/equipInfo/indicatorData"

// MARK: - 日常维修记录列表
let emsMaintainDailyList     = "/api/v1/maintain/daily/list"

// MARK: - 待办、已办
let emsMaintainDailyAppList     = "/api/v1/maintain/daily/app/list"

// MARK: - 设备队、维修队、外协维修开始维修
let emsMaintainStart     = "/api/v1/maintain/daily/maintainStart"

// MARK: - 润滑保养开始验收
let emsAcceptanceStart     = "/api/v1/plan/maintain/order/app/acceptanceStart"

// MARK: - 润滑保养结束验收
let emsAcceptanceEnd     = "/api/v1/plan/maintain/order/app/acceptanceEnd"


// MARK: - 维修队-班组人员/外修队长-确认是否领用物资
let emsMaintainIsPickingConfirm     = "/api/v1/maintain/daily/maintainIsPickingConfirm"

// MARK: - 维修完成
let emsMaintainComplete     = "/api/v1/maintain/daily/maintainComplete"

// MARK: - 日常维修流程详情
let emsMaintainDailyDetails     = "/api/v1/maintain/daily/details"

// MARK: - 技术员/班长/司机验收
let emsMaintainAcceptanceUser     = "/api/v1/maintain/daily/acceptanceUser"

// MARK: - 确认是否有新发现故障
let emsMaintainConfirmNewFault     = "/api/v1/maintain/daily/confirmNewFault"

// MARK: - 获取验收列表-分页
let emsTaskAcceptanceList     = "/api/v1/order/app/task/acceptanceList"

// MARK: - 查询条件模糊查询用户列表
let emsUsersCondition     = "/api/v1/system/users/condition/page"

// MARK: - 获取技术员/班长/维修队技术员列表
let emsUsersRoleUseList     = "/api/v1/system/users/roleUseList"


// MARK: - 分页查询物资系统数据
let emsMaterialSystem     = "/data/api/v1/materialSystem/page"



// MARK: - 获取消息列表
let emsMsgList     = "/api/v1/msg/read"

// MARK: - 设置消息为已读
let emsMsgSetRead     = "/api/v1/msg/setRead"

// MARK: - 设备履历-检修历史
let emsGetOverhaulHistory     = "/api/v1/equipInfo/getOverhaulHistory"

// MARK: - 设备履历-检修历史-润滑保养历史
let emsGetMaintenanceHistory     = "/api/v1/equipInfo/getMaintenanceHistory"

// MARK: - 设备履历-检修历史-维修历史
let emsGetMaintainHistory     = "/api/v1/equipInfo/getMaintainHistory"

// MARK: - 设备履历-检修历史-大修历史
let emsGetPlanRepairOrderHistory     = "/api/v1/equipInfo/getPlanRepairOrderHistory"

// MARK: - 设备履历-检修历史-保险维修历史
let emsGetMaintainInsuranceHistory     = "/api/v1/equipInfo/getMaintainInsuranceHistory"

// MARK: - app端待办任务数量
let emsBacklogList     = "/api/v1/app/backlog/list"

// MARK: - 获取最新包
let emsAppsNewest     = "/api/v1/apps/newest"

// MARK: - 获取流程审批token
let emsGetFlowExaminedToken     = "/api/v1/web/service/getFlowExaminedToken"

// MARK: - app设备数量获取柱形图详情接口
let emsEquipColumnChart    = "/api/v1/equipInfo/app/equipColumnChart"

// MARK: - 维修作业安全告知
let emsMaintenanceAgreement    = "/api/v1/basic/data/baseName"


// MARK: - 分页查询维修定额标准列表
let emsStandardRepairMoney    = "/api/v1/standard/repair/money/page"


