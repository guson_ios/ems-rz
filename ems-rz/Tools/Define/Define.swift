//
//  Define.swift
//  ems-Manager
//
//  Created by mac on 2019/5/5.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit
@_exported import SwiftyJSON
@_exported import RxSwift
@_exported import RxCocoa
@_exported import HandyJSON
@_exported import UIKit
@_exported import SnapKit
@_exported import Alamofire
@_exported import SwiftyUserDefaults
@_exported import swiftScan
@_exported import PGDatePicker

// MARK: - 流程平台秘钥值
let EMSSecrect = "cwaJpc"

let KScreenHeight = UIScreen.main.bounds.size.height
let KScreenWidth  = UIScreen.main.bounds.size.width

let KReferenceHeight    = 667.0
let KReferenceWidth     = 375.0
let KScaleW             = KScreenWidth / CGFloat(KReferenceWidth)


#if DEBUG
///全局函数 T是泛型 传入不同的参数
func PrintLog<T>(_ message:T,file:String = #file,funcName:String = #function,lineNum:Int = #line){
    
    let file = (file as NSString).lastPathComponent;
    // 文件名：行数---要打印的信息
    print("\(file):(\(lineNum))--\(String(describing: message))--\(funcName)");
}
#endif
