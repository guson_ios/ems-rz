//
//  SoundRecordStartView.h
//  EquipmentMS
//
//  Created by mac on 2018/9/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LVRecordTool.h"
typedef void(^RecordDataBlock)(NSData *recorData);

@interface SoundRecordStartView : UIView
/** 录音工具 */
@property (nonatomic, strong) LVRecordTool *recordTool;

///转mp3格式后的Data回调
@property (nonatomic, copy)RecordDataBlock recordDataBlock;

///acc格式后的Data回调
@property (nonatomic, copy)RecordDataBlock accDataBlock;



@end
