//
//  LVRecordTool.m
//  RecordAndPlayVoice
//
//  Created by PBOC CS on 15/3/14.
//  Copyright (c) 2015年 liuchunlao. All rights reserved.
//
///默认文件的地址
#define LVRecordFielName @"lvRecord.caf"
#import "lame.h"
#import "LVRecordTool.h"

#define ETRECORD_RATE 44100.0

@interface LVRecordTool () <AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    NSInteger _second;
    NSDate * _startDate;
}

/** 录音文件地址 */
@property (nonatomic, copy) NSURL *oldRecordFileUrl;

/** 录音文件地址 */
@property (nonatomic, strong) NSURL *recordFileUrl;

/** 录音转mp3文件地址 */
@property (nonatomic, strong) NSURL *recordMP3FileUrl;

/** 定时器 */
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) AVAudioSession *session;

@end

@implementation LVRecordTool

///开始录音
- (void)startRecording {
    // 录音时停止播放 删除曾经生成的文件
    [self stopPlaying];
    [self destructionRecordingFile];
    
    // 真机环境下需要的代码
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *sessionError;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    
    
    if(session == nil){
        NSLog(@"Error creating session: %@", [sessionError description]);

    }else{
        [session setActive:YES error:nil];
    }
    self.session = session;
    
    [self.recorder record];

    NSTimer *timer = [NSTimer timerWithTimeInterval:0.5 target:self selector:@selector(updateImage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    _startDate = [NSDate date];
    [timer fire];
    self.timer = timer;
}

- (void)updateImage {
    
    [self.recorder updateMeters];
    double lowPassResults = pow(10, (0.05 * [self.recorder peakPowerForChannel:0]));
    float result  = 10 * (float)lowPassResults;
    NSLog(@"--%f", result);
    int no = 0;///分贝
    if (result > 0 && result <= 1.3) {
        no = 1;
    } else if (result > 1.3 && result <= 2) {
        no = 2;
    } else if (result > 2 && result <= 3.0) {
        no = 3;
    } else if (result > 3.0 && result <= 3.0) {
        no = 4;
    } else if (result > 5.0 && result <= 10) {
        no = 5;
    } else if (result > 10 && result <= 40) {
        no = 6;
    } else if (result > 40) {
        no = 7;
    }
    
    

    double deltaTime = [[NSDate date] timeIntervalSinceDate:_startDate];
    _second = (NSInteger)(deltaTime+0.5) ;
    if (_second < 0.0) {
        self.timerLength(@"00:00");
    }else{
        NSInteger m = 0;
        {
            m =  _second / 60 >= 60 ? ((_second % 3600) / 60) : (_second / 60);
        }
        NSInteger s = _second % 60;
        NSInteger h = _second / (60*60);
        if (self.timerLength) {
            self.timerLength([NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)h,(long)m,(long)s]);
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(recordTool:didstartRecoring:)]) {
        [self.delegate recordTool:self didstartRecoring: no];
    }
}

- (void)stopRecording {
    if ([self.recorder isRecording]) {
        [self.recorder stop];
        [self.timer invalidate];
    }
}

- (void)playRecordingFile {
    // 播放时停止录音

    [self.recorder stop];
    if (self.oldRecordFileUrl == nil) {
        NSLog(@"数据为空");
        return;
    }

    // 正在播放就返回
    if ([self.player isPlaying]) return;
    NSError *error = nil;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:self.oldRecordFileUrl error:&error];
    self.player.delegate = self;
    [self.session setCategory:AVAudioSessionCategorySoloAmbient error:&error];

    [self.player play];
}

- (void)stopPlaying {
    [self.player stop];
}

static id instance;
#pragma mark - 单例
+ (instancetype)sharedRecordTool {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [[self alloc] init];
        }
    });
    return instance;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [super allocWithZone:zone];
        }
    });
    return instance;
}

#pragma mark - 懒加载
- (AVAudioRecorder *)recorder {

    if (!_recorder) {
//        [self destructionRecordingFile];
        // 1.获取沙盒地址
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *filePath = [path stringByAppendingPathComponent:LVRecordFielName];
        self.recordFileUrl = [NSURL fileURLWithPath:filePath];
        NSLog(@"---%@", filePath);
        // 3.设置录音的一些参数
        NSMutableDictionary *setting = [NSMutableDictionary dictionary];
        // 音频格式
        setting[AVFormatIDKey] = @(kAudioFormatLinearPCM);
        // 录音采样率(Hz) 如：AVSampleRateKey==8000/44100/96000（影响音频的质量）
        setting[AVSampleRateKey] = @(ETRECORD_RATE);
        // 音频通道数 1 或 2
        setting[AVNumberOfChannelsKey] = @(2);
        // 线性音频的位深度  8、16、24、32
        setting[AVLinearPCMBitDepthKey] = @(16);
        
        //录音的质量
        setting[AVEncoderAudioQualityKey] = [NSNumber numberWithInt:AVAudioQualityMax];
        
        _recorder = [[AVAudioRecorder alloc] initWithURL:self.recordFileUrl settings:setting error:NULL];
        _recorder.delegate = self;
        _recorder.meteringEnabled = YES;
        [_recorder prepareToRecord];
    }
    return _recorder;
}

- (void)destructionRecordingFile {

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (self.recordFileUrl) {
        [fileManager removeItemAtURL:self.recordFileUrl error:NULL];
    }
    if (self.recordMP3FileUrl) {
        [fileManager removeItemAtURL:self.recordMP3FileUrl error:NULL];
    }
}

//#转为Mp3
- (void)toMp3
{
    // 1.获取沙盒地址
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *filePath = [path stringByAppendingPathComponent:LVRecordFielName];

    NSString *mp3FileName = [NSString stringWithFormat:@"%@.mp3",@"Mp3RecordFile"];
    NSString *mp3FilePath = [path stringByAppendingPathComponent:mp3FileName];
    
    @try {
        
        int read, write;
        
        FILE *pcm = fopen([filePath cStringUsingEncoding:NSUTF8StringEncoding], "rb");  //source 被转换的音频文件位置
        
        if(pcm == NULL)
            
        {
            
            NSLog(@"file not found");
            return;
        }
        
        
        
        fseek(pcm, 4*1024, SEEK_CUR);                                   //skip file header
        
        FILE *mp3 = fopen([mp3FilePath cStringUsingEncoding:NSUTF8StringEncoding], "wb+");  //output 输出生成的Mp3文件位置
        
        
        
        const int PCM_SIZE = 8192;
        
        const int MP3_SIZE = 8192;
        
        short int pcm_buffer[PCM_SIZE*2];
        
        unsigned char mp3_buffer[MP3_SIZE];
        
        lame_t lame = lame_init();
        
        lame_set_num_channels(lame,2);//设置1为单通道，默认为2双通道
        
        lame_set_in_samplerate(lame, ETRECORD_RATE);//11025.0
        
        lame_set_VBR(lame, vbr_default);
        
//        lame_set_brate(lame,8);
//
//        lame_set_mode(lame,3);
//
//        lame_set_quality(lame,2); /* 2=high 5 = medium 7=low 音质*/
        
        lame_init_params(lame);
        
        do {
            
            read = (int)fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
            
            if (read == 0)
                
                write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
            
            else
                
                write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
            
            fwrite(mp3_buffer, write, 1, mp3);
            
        } while (read != 0);
        lame_mp3_tags_fid(lame, mp3);
        lame_close(lame);
        fclose(mp3);
        fclose(pcm);
    }
    
    
    @catch (NSException *exception) {
        NSLog(@"%@",[exception description]);
    }
    @finally {
        
        if (self.recordDataBlock) {
            self.recordMP3FileUrl = [NSURL fileURLWithPath:mp3FilePath];
            NSData *data = [NSData dataWithContentsOfURL:self.recordMP3FileUrl];
            self.recordDataBlock(data);
        }
        if (self.accDataBlock) {
            self.recordMP3FileUrl = [NSURL fileURLWithPath:filePath];
            self.accDataBlock([NSData dataWithContentsOfURL:self.recordMP3FileUrl]);

        }
        NSLog(@"--------ToMp3Success");
    }
}

- (void)setVoiceData:(NSData *)voiceData{
    _voiceData = voiceData;
    NSLog(@"-----%lu",(unsigned long)voiceData.length);
    if (_voiceData.length) {
        NSString *mp3FileName = [NSString stringWithFormat:@"%@.mp3",@"Mp3RecordFile"];
        NSString *mp3FilePath = [[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"] stringByAppendingPathComponent:mp3FileName];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (self.oldRecordFileUrl) {
            [fileManager removeItemAtURL:self.oldRecordFileUrl error:NULL];
        }
        self.oldRecordFileUrl = [NSURL fileURLWithPath:mp3FilePath];
        BOOL result = [_voiceData writeToURL:self.oldRecordFileUrl atomically:YES];
        if (result) {
            NSLog(@"-写入成功");
        }
    }
}

#pragma mark - AVAudioRecorderDelegate--------完成录音
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    
    if (flag) {
        NSData *data = [NSData dataWithContentsOfURL:self.recordFileUrl];
        // 1.获取沙盒地址
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *filePath = [path stringByAppendingPathComponent:LVRecordFielName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            //文件夹已存在
            [[NSFileManager defaultManager]  removeItemAtPath:filePath error:nil];
            [[NSFileManager defaultManager] createFileAtPath:filePath contents:data attributes:nil];
            
        } else {
            [[NSFileManager defaultManager] createFileAtPath:filePath contents:data attributes:nil];
            //创建文件夹
        }
        self.oldRecordFileUrl = [NSURL fileURLWithPath:filePath];

        if (self.oldRecordFileUrl) {
            [self toMp3];
        }
        [self.session setActive:NO error:nil];
    }
}
#pragma mark - AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    if (flag) {
        // 真机环境下需要的代码
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *sessionError;
        [session setCategory:AVAudioSessionCategorySoloAmbient error:&sessionError];
        
        if(session == nil){
            NSLog(@"Error creating session: %@", [sessionError description]);
        }else{
            [session setActive:YES error:nil];
        }
        self.session = session;
    }
}
@end
