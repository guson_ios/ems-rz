//
//  SoundRecordStartView.m
//  EquipmentMS
//
//  Created by mac on 2018/9/3.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "SoundRecordStartView.h"
@interface SoundRecordStartView ()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation SoundRecordStartView

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addContentView];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addContentView];
    }
    return self;
}

- (void)addContentView{
    [[NSBundle mainBundle] loadNibNamed:@"SoundRecordStartView" owner:self options:nil];
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    self.contentView.frame = self.bounds;
    [self addSubview:self.contentView];
    self.recordTool = [LVRecordTool sharedRecordTool];
}
- (IBAction)startBtnClick:(UIButton *)sender {
//    if (![PermissionTool canRecord]) {
//        [ZFToast ShowWithMessage:@"请打开麦克风权限"];
//        return;
//    }
    sender.enabled = NO;
    [[LVRecordTool sharedRecordTool] startRecording];
    __weak UILabel *blockTimeLabel = self.timeLabel;
    self.recordTool.timerLength = ^(NSString *timer) {
        blockTimeLabel.text = timer;
    };

}
- (IBAction)saveBtnClick:(id)sender {
    double currentTime = self.recordTool.recorder.currentTime;
    NSLog(@"%lf", currentTime);
    if (currentTime < 2) {
//        [ZFToast ShowWithMessage:@"说话时间太短"];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            [self.recordTool stopRecording];
            [self.recordTool destructionRecordingFile];
        });
    } else {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [self.recordTool stopRecording];
        });
        // 已成功录音
        NSLog(@"已成功录音");
        typeof(self) __weak wkSelf = self;
        self.recordTool.recordDataBlock = ^(NSData *recordData) {
            typeof(wkSelf) __strong stSelf=wkSelf;
            if (stSelf.recordDataBlock) {
                stSelf.recordDataBlock(recordData);
            }
            [wkSelf hiddenBtnClick:nil];
        };
//        self.recordTool.accDataBlock = ^(NSData *recordData) {
//           typeof(wkSelf) __strong stSelf=wkSelf;
//            if (stSelf.accDataBlock) {
//                stSelf.accDataBlock(recordData);
//            }
//        };
    }
}
- (IBAction)hiddenBtnClick:(id)sender {
    [self.recordTool stopRecording];
    self.hidden = YES;
    [self removeFromSuperview];
}

- (void)dealloc {
    NSLog(@"SoundRecordStartView dealloc");
//    CheckRunWhere;
    if ([self.recordTool.recorder isRecording]){
        [self.recordTool stopRecording];
    }
    if ([self.recordTool.player isPlaying]){
        [self.recordTool stopPlaying];
        
    }
}

@end
