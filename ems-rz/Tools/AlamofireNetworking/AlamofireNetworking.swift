//
//  AlamofireNetworking.swift
//  ems-Manager
//
//  Created by mac on 2019/4/23.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit


typealias resultClosure<T> = (_ result: T?) -> Void

protocol ModelTransformDelegate{
    func didRecieveDataUpdate<T>(_ jsonData: JSON?, result:resultClosure<T>)
}

extension HandyJSON{
    func didRecieveDataUpdate<T: HandyJSON>(_ jsonData: JSON?, result:resultClosure<T>){
        let model = T.deserialize(from: jsonData?.dictionaryObject)
        result(model)
    }
}
extension Array: ModelTransformDelegate where Element: HandyJSON{
   
    func didRecieveDataUpdate<T>(_ jsonData: JSON?, result: (T?) -> Void){
        let resultArray = [Element].deserialize(from: jsonData?["funcList"].arrayObject)
        result(resultArray as? T)
    }
}
///设置body中string
extension String: ParameterEncoding {
    
    private func query(_ parameters: [String: Any]) -> String {
        var components: [(String, String)] = []

        for key in parameters.keys.sorted(by: <) {
            let value = parameters[key]!
            components += URLEncoding().queryComponents(fromKey: key, value: value)
        }
        return components.map { "\($0)=\($1)" }.joined(separator: "&")
    }
   
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        
        var urlRequest = try urlRequest.asURLRequest()
        urlRequest.httpBody = self.data(using: .utf8, allowLossyConversion: false)

        guard let url = urlRequest.url else {
            throw AFError.parameterEncodingFailed(reason: .missingURL)
        }
        guard let parameters = parameters else { return urlRequest }
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), !parameters.isEmpty {
            let percentEncodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + query(parameters)
            urlComponents.percentEncodedQuery = percentEncodedQuery
            urlRequest.url = urlComponents.url
        }
        return urlRequest
    }
}
enum ContentType: String {
    case json       = "application/json"
    case define     = "application/x-www-form-urlencoded"
    case formData   = "multipart/form-data"
}

func headersFunc(_ header: [String : String]? = nil, contentType: ContentType = .define) -> [String : String] {
    var headers  = ["Authorization" : EmsGlobalObj.obj.loginData?.token ?? "", "Content-Type" : contentType.rawValue]
    if header != nil {
        for (key, value) in header! { headers[key] = value}
    }
    return headers
}

open class AlamofireNetworking{
    
    let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 15
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()
    
    var task: URLSessionDataTask?
    var request:DataRequest?
    
    public class var sharedInstance: AlamofireNetworking {
        struct Static {
            //Singleton instance. Initializing keyboard manger.
            static let afNetWorking = AlamofireNetworking()
        }
        /** @return Returns the default singleton instance. */
        return Static.afNetWorking
    }
    // MARK: - 文件上传
    func uploadFile(_ multipartFormData: @escaping (MultipartFormData) -> Void, urlString: String, headers: [String : String]? = nil, progressPlugin: afProgressPlugin? = nil, success: @escaping (JSON) -> (Void),failure: @escaping (Any) -> (Void)) ->  Void{
        weak var weakProgressPlugin = progressPlugin
        assert(urlString.count >= 0, "URL 不能为空"); weakProgressPlugin?.progressShow()
        
        AlamofireNetworking.sharedInstance.sessionManager.upload(multipartFormData: multipartFormData, to: urlString, headers: headers ?? headersFunc()) { (encodingResult) in
    
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseString { response in
                    guard let data = response.data, let json = try? JSON(data:data)else{
                        ZFToast.show(withMessage: "数据解析失败")
                        print("\(String(describing: response.data))"); failure("数据解析失败" as Any); return}
                    guard json["code"].intValue == 0 else{
                        print(json.stringValue); ZFToast.show(withMessage: json["msg"].stringValue); return}
                    self.nullToNullString(urlString, json)
                    success(json)
                    weakProgressPlugin?.progressHide()
                }
            case .failure(let encodingError):
                print(encodingError)
                weakProgressPlugin?.progressHide()
            }
        }
    }

    /// 不带数据处理的网络请求
    func requestData(_ urlString: String, parameter: [String: Any]?, method: HTTPMethod , encoding: ParameterEncoding = URLEncoding.default, headers: [String : String]? = nil, progressPlugin: afProgressPlugin? = nil, success: @escaping (JSON) -> (Void),failure: @escaping (Any) -> (Void)){
        weak var weakProgressPlugin = progressPlugin
        assert(urlString.count >= 0, "URL 不能为空"); weakProgressPlugin?.progressShow()
        
        self.request = AlamofireNetworking.sharedInstance.sessionManager.request(urlString, method: method, parameters: parameter, encoding: encoding, headers: headers ?? headersFunc()).validate().responseJSON { (response) in
            weakProgressPlugin?.progressHide()
            guard response.result.isSuccess else{
                ZFToast.show(withMessage: "网络请求失败")
                print("\(urlString)---\(response.result.error.debugDescription)");   failure(response.result.error as Any); return}
            guard let data = response.data, let json = try? JSON(data:data)else{
                ZFToast.show(withMessage: "网络请求失败")
                print("\(urlString)---\(String(describing: response.data))"); failure("数据解析失败" as Any); return}
            guard json["code"].intValue == 0 else{
                print("\(urlString)---\(json.dictionaryObject as Any)"); ZFToast.show(withMessage: json["msg"].stringValue); failure(json); ResponseProcess.resultProcess(json["code"].stringValue); return}
            self.nullToNullString(urlString, json)
            success(json)
        }
    }
    
    ///还是不太灵活，只能处理特定数据
    /// 通用数据处理的网络请求
    func requestData<T>(_ urlString: String, parameter: Dictionary<String, Any>?, method: HTTPMethod, progressPlugin: afProgressPlugin? = nil, dataModel: T,success: @escaping resultClosure<T>,failure: @escaping (Any) -> (Void)){
        assert(urlString.count >= 0, "URL 不能为空"); progressPlugin?.progressShow()
        AlamofireNetworking.sharedInstance.sessionManager.request(urlString, method: method, parameters: parameter, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            progressPlugin?.progressHide()
            guard response.result.isSuccess  else{ failure(response.result.error as Any); return}
            let json = try? JSON(data:response.data!)
            (dataModel as? ModelTransformDelegate)?.didRecieveDataUpdate(json, result: success)
        }
    }
    
    /// 返回Model的网络请求
    func requestData<T>(_ urlString: String, parameter: Dictionary<String, Any>?, method: HTTPMethod, progressPlugin: afProgressPlugin? = nil, dataModel: T,success: @escaping resultClosure<T>,failure: @escaping (Any) -> (Void)) where T:HandyJSON{
        assert(urlString.count >= 0, "URL 不能为空"); progressPlugin?.progressShow()
        AlamofireNetworking.sharedInstance.sessionManager.request(urlString, method: method, parameters: parameter, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            progressPlugin?.progressHide()
            guard response.result.isSuccess else{ failure(response.result.error as Any); return}
            let json = try? JSON(data:response.data!)
            dataModel.didRecieveDataUpdate(json, result: success)
        }
    }

    private func nullToNullString(_ urlString: String, _ json: JSON) {
        if let data = try? json.rawData(options: .prettyPrinted){
            let replacStr = String(data: data, encoding: .utf8)?.replacingOccurrences(of: ": null", with: ": \"nullString\"")
            if let replacData = replacStr?.data(using: .utf8) {
                let jsonDict = try? JSON(data:replacData).dictionaryValue
                print("\(urlString)===\(String(describing: jsonDict))")
            }
        }
    }
}
