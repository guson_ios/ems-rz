//
//  NetworkingResultProcess.swift
//  ems-rz
//
//  Created by Gusont on 2020/2/28.
//  Copyright © 2020 xyj. All rights reserved.
//

import Foundation


enum ResponseCode: String {
    case success = "00000000"
    case tokenExpired = "00010401"
    
}
struct ResponseProcess {
    
    static func resultProcess(_ code: String) {
        
        switch code{
        case ResponseCode.success.rawValue:
            break
        case ResponseCode.tokenExpired.rawValue:
            UIAlertController.alertShow("提示", message: "登录凭证已过期，请重新登录", sureBtnTitle: "确定", cancelBtnTitle: nil, sureBtnClick: {
                signOutViewController(UIApplication.getTopViewController()?.tabBarController)
                
            }, cancelBtnClick: nil)
            break
        default:
            break
        }
    }
}
