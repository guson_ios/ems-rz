//
//  UIButton+ImageTitleSpacing.m
//  SystemPreferenceDemo
//
//  Created by moyekong on 12/28/15.
//  Copyright © 2015 wiwide. All rights reserved.
//

#import "UIButton+ImageTitleSpacing.h"
#import <objc/runtime.h>

@implementation UIButton (ImageTitleSpacing)



///值属性只能赋值

-(CGFloat)space{
    NSNumber *spaceNumber = objc_getAssociatedObject(self, @selector(space));
    return [spaceNumber floatValue];
}
- (void)setSpace:(CGFloat)space{
    NSNumber *spaceNumber = [NSNumber numberWithFloat:space];
    objc_setAssociatedObject(self, @selector(space), spaceNumber, OBJC_ASSOCIATION_COPY);
    [self layoutButtonWithEdgeInsetsStyle:self.btnEdgeInstsStyle imageTitleSpace:[spaceNumber floatValue]];
}

-(MKButtonEdgeInsetsStyle)btnEdgeInstsStyle{
    NSNumber *btnEdgeInstsStyleNumber = objc_getAssociatedObject(self, @selector(btnEdgeInstsStyle));
    return [btnEdgeInstsStyleNumber intValue];

}
- (void)setBtnEdgeInstsStyle:(MKButtonEdgeInsetsStyle)btnEdgeInstsStyle{
    NSNumber *btnEdgeInstsStyleNumber = [NSNumber numberWithUnsignedInteger:btnEdgeInstsStyle];
    objc_setAssociatedObject(self, @selector(btnEdgeInstsStyle), btnEdgeInstsStyleNumber, OBJC_ASSOCIATION_COPY);
    [self layoutButtonWithEdgeInsetsStyle:[btnEdgeInstsStyleNumber intValue] imageTitleSpace:self.space];

}

- (void)layoutButtonWithEdgeInsetsStyle:(MKButtonEdgeInsetsStyle)style
                        imageTitleSpace:(CGFloat)space
{
//    self.backgroundColor = [UIColor cyanColor];
    
    /**
     *  前置知识点：titleEdgeInsets是title相对于其上下左右的inset，跟tableView的contentInset是类似的，
     *  如果只有title，那它上下左右都是相对于button的，image也是一样；
     *  如果同时有image和label，那这时候image的上左下是相对于button，右边是相对于label的；title的上右下是相对于button，左边是相对于image的。
     */

    
    // 1. 得到imageView和titleLabel的宽、高
    UIImage *image = self.currentImage;
    CGFloat imageWith = image.size.width;
    CGFloat imageHeight =image.size.height;
    //当放在cell.Xib中运行过程中发生约束警告
//    CGFloat imageWith = self.imageView.frame.size.width;
//    CGFloat imageHeight = self.imageView.frame.size.height;
    
    CGFloat labelWidth = 0.0;
    CGFloat labelHeight = 0.0;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        // 由于iOS8中titleLabel的size为0，用下面的这种设置
        labelWidth = self.titleLabel.intrinsicContentSize.width;
        labelHeight = self.titleLabel.intrinsicContentSize.height;
    } else {
        labelWidth = self.titleLabel.frame.size.width;
        labelHeight = self.titleLabel.frame.size.height;
    }
    
    // 2. 声明全局的imageEdgeInsets和labelEdgeInsets
    UIEdgeInsets imageEdgeInsets = UIEdgeInsetsZero;
    UIEdgeInsets labelEdgeInsets = UIEdgeInsetsZero;

    // 3. 根据style和space得到imageEdgeInsets和labelEdgeInsets的值
    switch (style) {
        case MKButtonEdgeInsetsStyleTop:
        {
            imageEdgeInsets = UIEdgeInsetsMake(-labelHeight-space/2.0, 0, 0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight-space/2.0, 0);
        }
            break;
        case MKButtonEdgeInsetsStyleLeft:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, -space/2.0, 0, space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, space/2.0, 0, -space/2.0);
        }
            break;
        case MKButtonEdgeInsetsStyleBottom:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, 0, -labelHeight-space/2.0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(-imageHeight-space/2.0, -imageWith, 0, 0);
        }
            break;
        case MKButtonEdgeInsetsStyleRight:
        {
            labelWidth = labelWidth > CGRectGetWidth(self.frame) ? CGRectGetWidth(self.frame) - imageWith : labelWidth;
            imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth+space/2.0, 0, -labelWidth-space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith-space/2.0, 0, imageWith+space/2.0);
        }
            break;
        default:
            break;
    }
    
//     4. 赋值
    self.titleEdgeInsets = labelEdgeInsets;
    self.imageEdgeInsets = imageEdgeInsets;
}

@end

//@implementation UIButtonImageTitleSpace
//
/////值属性只能赋值
//- (void)setSpace:(CGFloat)space{
//    _space = space;
//    [self layoutButtonWithEdgeInsetsStyle:self.btnEdgeInstsStyle imageTitleSpace:_space];
//    [self text];
//}
//- (void)text{
//
//}
//- (void)setBtnEdgeInstsStyle:(MKButtonEdgeInsetsStyle)btnEdgeInstsStyle{
//    _btnEdgeInstsStyle = btnEdgeInstsStyle;
//    [self layoutButtonWithEdgeInsetsStyle:btnEdgeInstsStyle imageTitleSpace:_space];
//}
//@end
