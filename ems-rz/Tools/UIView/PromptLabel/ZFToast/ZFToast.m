#import "ZFToast.h"
#import "NSString+Category.h"
NSTimeInterval const kShowLabelTime = 2.0;
NSInteger const kRemoveTime = 3.0;
@interface ZFToast ()

/// @brief 存放文本的UILabel
@property (strong,nonatomic) UILabel *textLabel;
@property (strong,nonatomic) NSTimer *timer;
/// @brief 记录是否移除
@property (assign,nonatomic) NSInteger currentDate;

@end
@implementation ZFToast


+(void)ShowWithMessage:(NSString *)message{
    
    ZFToast *toast = [[ZFToast alloc]init];
    [toast popUpToastWithMessage:message];
}

//- (instancetype)initWithMessage:(NSString *)message
//{
//    self = [super init];
//    if (self) {
////        self.currentDate = 0;
//        [self popUpToastWithMessage:message];
//    }
//
//    return self;
//}
//有点..
//+ (instancetype)shareClient
//{
//    static ZFToast *zfToast = nil;
//    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        zfToast = [[ZFToast alloc] init];
//        
//    });
//    
//    return zfToast;
//}

- (void)popUpToastWithMessage:(NSString *)message
{
//    [self.textLabel removeFromSuperview];
    /// @brief 创建定时器
//    [self createTimer];
  
    dispatch_async(dispatch_get_main_queue(), ^{
        /// @brief 初始化Label
        [self initLabel:message];
        /// @brief 初始化底层视图
        [self initBottomView];
    });
}

#pragma mark - 创建定时器
- (void)createTimer
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kShowLabelTime target:self selector:@selector(onTimer) userInfo:self repeats:YES];
    
    //暂停定时器
    [self.timer setFireDate:[NSDate distantFuture]];
    
}

#pragma mark - 初始化Label
- (void)initLabel:(NSString *)message
{
    //获取屏幕宽度
    CGFloat screenWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    //获取屏幕高度
    //    CGFloat screenHeight = CGRectGetHeight([[UIScreen mainScreen] bounds]);
    /// @brief Label的字号
    UIFont *font = [UIFont systemFontOfSize:15];
    /// @brief 控件的宽
    CGFloat width = screenWidth / 3.0 * 2.0;
    /// @brief Label所需的宽高
    CGSize labelSize = [NSString calculationTextNeedSizeWithText:message font:15 width:width];
    //    NSLog(@"%f",labelSize.width);
    
//    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, labelSize.width, labelSize.height)];
    //    NSLog(@"%f",labelSize.width);
//    self.textLabel.backgroundColor = [UIColor clearColor];
//    self.textLabel.textColor = [UIColor whiteColor];
    self.textLabel.font = font;
    self.textLabel.text = message;
    self.textLabel.frame = CGRectMake(10, 10, labelSize.width, labelSize.height);
//    self.textLabel.numberOfLines = 0;
//    self.textLabel.textAlignment = NSTextAlignmentCenter;
}

#pragma mark - 初始化底层视图
- (void)initBottomView
{
    //获取屏幕宽度
    CGFloat screenWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    //获取屏幕高度
    CGFloat screenHeight = CGRectGetHeight([[UIScreen mainScreen] bounds]);
    self.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.8];
    self.frame = CGRectMake((screenWidth - self.textLabel.frame.size.width)/2.0, (screenHeight - self.textLabel.frame.size.height)/2.0, self.textLabel.frame.size.width + 20, self.textLabel.frame.size.height + 20);
    [self addSubview:self.textLabel];
    //设置ImageView是否可以设为圆角
    self.layer.masksToBounds = YES;
    //设置圆角度数
    self.layer.cornerRadius = 10;
    //位置可更改
    UIViewController *vc = [self getCurrentShowVC];
//    UIViewController * vc = (UIViewController *)[[UIApplication sharedApplication] keyWindow];

    
    self.center = vc.view.center;
    self.tag = 10001;
    [vc.view addSubview:self];
    //启动定时器
//    [self.timer setFireDate:[NSDate distantPast]];
    
    [UIView animateWithDuration:kShowLabelTime animations:^{
        self.alpha = 0.6;

    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)dealloc{
//    CheckRunWhere;
}

- (void)onTimer
{
    self.currentDate++;
   
    if (self.currentDate == kRemoveTime) {
        //暂停定时器
        [self.timer setFireDate:[NSDate distantFuture]];
        [self.timer invalidate];
        self.timer = nil;
        [self removeFromSuperview];
        self.currentDate = 0;
    }
    
}
- (UILabel *)textLabel{
    if (!_textLabel) {
        self.textLabel = [[UILabel alloc] init];
        //    NSLog(@"%f",labelSize.width);
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.numberOfLines = 0;
        self.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _textLabel;
}
- (UIViewController *)getCurrentShowVC
{

    
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    if (window.windowLevel != UIWindowLevelNormal || window == nil) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for (UIWindow * tmpWin in windows) {
            if (tmpWin.windowLevel == UIWindowLevelNormal) {
                window = tmpWin;
                break;
            }
        }
    }
    UIViewController *topViewController = [window rootViewController];
    
    while (true) {
        
        if (topViewController.presentedViewController) {
            
            topViewController = topViewController.presentedViewController;
            
        } else if ([topViewController isKindOfClass:[UINavigationController class]] && [(UINavigationController*)topViewController topViewController]) {
            
            topViewController = [(UINavigationController *)topViewController topViewController];
            
        } else if ([topViewController isKindOfClass:[UITabBarController class]]) {
            
            UITabBarController *tab = (UITabBarController *)topViewController;
            topViewController = tab.selectedViewController;
            
        } else {
            break;
        }
    }
    return topViewController;
}

@end
