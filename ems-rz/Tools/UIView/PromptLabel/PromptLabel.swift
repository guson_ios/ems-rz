//
//  PromptLabel.swift
//  ems-rz
//
//  Created by Gusont on 2020/1/13.
//  Copyright © 2020 xyj. All rights reserved.
//

import UIKit

class PromptLabel: UIView {

    var textLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
     func showMessage(_ message: String) -> Void {
        DispatchQueue.main.async {
            if  let vc = UIApplication.getTopViewController(){
                vc.view.addSubview(self.textLabel)
                self.textLabel.text = message
                self.textLabel.snp.makeConstraints { (maker) in
                    maker.center.equalTo(vc.view)
                }
            }
            UIView.animate(withDuration: 2.0, animations: {
                
            }) { (true) in
                self.textLabel.removeFromSuperview()
            }
        }
    }
    
    
}
