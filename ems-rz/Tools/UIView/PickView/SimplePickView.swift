//
//  SimplePickView.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/21.
//  Copyright © 2019 xyj. All rights reserved.
//

import UIKit
protocol SimplePickViewProtocol {
    func pickViewModelToString() -> String?
}

typealias simplePickClosure = (_ row: Int, _ model: SimplePickViewProtocol?) -> Void
class SimplePickView: UIView {

    @IBOutlet weak var pickerView: UIPickerView!
    
    var simplePickClosure: simplePickClosure?
    
    var pickerModels: [SimplePickViewProtocol]? {
        
        willSet{
            
        }
        didSet{
            self.pickerView.reloadAllComponents()
            self.isHidden = false
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.confignSimplePickView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.confignSimplePickView()
    }
    
    func confignSimplePickView() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.loadXibView()
        self.pickerView.showsSelectionIndicator = true
        self.isHidden = true
    }
    @IBAction func pickSureBtnClick(_ sender: Any) {
        
        guard (pickerModels?.count ?? 0 > 0) else { self.isHidden = true ; return}
        let row = pickerView.selectedRow(inComponent: 0)
        if (self.simplePickClosure != nil) {
            self.simplePickClosure!(row, self.pickerModels?[row])
            self.isHidden = true
            self.pickerView.selectRow(0, inComponent: 0, animated: true)
            self.pickerView(pickerView, didSelectRow: 0, inComponent: 0)
        }
        
    }
    
    @IBAction func pickCleanBtnClick(_ sender: Any) {
        self.isHidden = true
    }
    
}

extension SimplePickView: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerModels?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        for singleLine in pickerView.subviews{
            if singleLine.frame.size.height < 1 {
                singleLine.backgroundColor = COLF4F4F4
            }
        }
        let pickerCellView = UIView(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 40))
        pickerCellView.backgroundColor = .white
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 40))
        label.textAlignment = .center
        label.text = self.pickerModels?[row].pickViewModelToString()
        pickerCellView.addSubview(label)
        return pickerCellView
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
}
