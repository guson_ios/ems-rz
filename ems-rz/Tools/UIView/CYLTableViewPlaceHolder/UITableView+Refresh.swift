//
//  UITableView+Refresh.swift
//  ems-rz
//
//  Created by Gusont on 2019/12/28.
//  Copyright © 2019 xyj. All rights reserved.
//

import Foundation

typealias tableListCount = (_ listCount: Int) -> Void
typealias tableViewRefresh = (_ pageNo: Int, _ listCount: @escaping tableListCount) -> Void

private var TListCount: String = "listCount"
private var TTablePlaceView: String = "tablePlaceView"



extension UITableView: CYLTableViewPlaceHolderDelegate{
    public func makePlaceHolderView() -> UIView! {
        return self.tablePlaceView
    }
    
    
    var tablePlaceView: TTTableViewPlaceView?{
        get{
            return objc_getAssociatedObject(self, &TTablePlaceView) as? TTTableViewPlaceView
        }
        set(newValue){
            objc_setAssociatedObject(self, &TTablePlaceView, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    
    var listCount: tableListCount?{
        get{
            return objc_getAssociatedObject(self, &TListCount) as? tableListCount
        }
        set(newValue){
            objc_setAssociatedObject(self, &TListCount, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    func tableViewRefreshClouse(_ refreshClouse: @escaping tableViewRefresh) -> Void {
        var pageNo = 1
        self.mj_header =  MJRefreshNormalHeader{
            pageNo = 1
            if let listCount = self.listCount{
                refreshClouse(pageNo, listCount)
            }
        }
        self.mj_footer = MJRefreshBackNormalFooter(refreshingBlock: {
            pageNo += 1
            if let listCount = self.listCount{
                refreshClouse(pageNo, listCount)
            }
            return
        })
        self.mj_header?.beginRefreshing()
        self.listCount = { [weak self] listCount in
            self?.tableviewRefreshpostListCount(listCount)
        }
        return
    }
    
    func tableviewRefreshpostListCount(_ listCount: Int) -> Void {
        if self.mj_header?.isRefreshing ?? true {
            self.mj_header?.endRefreshing()
        }
        DispatchQueue.main.async {
                self.mj_footer?.state = (listCount != 10) ? .noMoreData : .idle
        }
        if self.tablePlaceView == nil , listCount <= 0 {
            self.tablePlaceView = TTTableViewPlaceView(frame: self.frame)
            self.tablePlaceView?.placeImageViewTap = { [weak self] in
                self?.mj_header?.beginRefreshing()
            }
        }
        
        self.tablePlaceView?.placeImageView.image = listCount < 0 ? UIImage(named: "bd_wangluoyichang") : UIImage(named: "icon_blank")
        self.tablePlaceView?.placeImageView.isUserInteractionEnabled = listCount < 0 ? true : false
        self.tablePlaceView?.titleLabel.isHighlighted = listCount < 0 ? true : false
        self.cyl_reloadData()
        
    }
}
