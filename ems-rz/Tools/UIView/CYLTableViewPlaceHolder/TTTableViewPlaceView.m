//
//  TTTableViewPlaceView.m
//  TTLife
//
//  Created by gui_huan on 2017/12/7.
//  Copyright © 2017年 tangshuanghui. All rights reserved.
//

#import "TTTableViewPlaceView.h"
@interface TTTableViewPlaceView()

@end
@implementation TTTableViewPlaceView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self confignUI];
    }
    return self;
}
- (void)confignUI{
    [self addSubview:self.placeImageView];
    [self addSubview:self.titleLabel];
    self.placeImageView.frame = CGRectMake(0, 0, 70, 51);
    self.placeImageView.center = self.center;
    
    self.titleLabel.frame = CGRectMake(0, 0, 200, 40);
    self.titleLabel.center = CGPointMake(self.center.x, self.center.y + 60);
}

/**
 会遮盖试图导致不能响应事件，透过上层试图，
 */
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    // 1.这是当前点击的视图，如果没有找到合适的响应操作的视图，则直接返回这个
    UIView *hitView = [super hitTest:point withEvent:event];
    if(hitView == self){
        return nil;
    }
    return hitView;
}

- (UIImageView *)placeImageView{
    if (!_placeImageView) {
        _placeImageView = [[UIImageView alloc]init];
//        _placeImageView.image = ImageName(@"icon_blank");
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(placeImageViwTap)];
        _placeImageView.userInteractionEnabled = YES;
        [_placeImageView addGestureRecognizer:tap];
    }
    return _placeImageView;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _titleLabel.text = @"没有数据";
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor darkGrayColor];
    }
    return _titleLabel;
}

- (void)placeImageViwTap{
    if (self.placeImageViewTap) {
        self.placeImageViewTap();
    }
}
@end
