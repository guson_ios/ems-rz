//
//  UITableView+Refresh.m
//  SharedParking
//
//  Created by gui_huan on 2018/5/22.
//  Copyright © 2018年 tangshuanghui. All rights reserved.
//

#import "UITableView+Refresh.h"
#import "UITableView+CYLTableViewPlaceHolder.h"
#import "CYLTableViewPlaceHolderDelegate.h"
#import "TTTableViewPlaceView.h"
#import <objc/runtime.h>
@interface UITableView (MJRefresh)<CYLTableViewPlaceHolderDelegate>
@property (nonatomic, strong) TTTableViewPlaceView *tablePlaceView;

@end
@implementation UITableView (Refresh)
static char *count = "listCount";

- (void)tableviewRefresh:(id)dataArray TableViewRefresh:(TableViewRefresh)tableViewRefresh{
    MJWeakSelf
    __block NSInteger pageNo = 1;
    self.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if ([(NSMutableArray *)dataArray count] > 0) {
            [(NSMutableArray *)dataArray removeAllObjects];
        }
        pageNo = 1;
        tableViewRefresh(pageNo, weakSelf.listCount);
    }];
    
    self.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageNo++;
        tableViewRefresh(pageNo, weakSelf.listCount);
    }];
    [self.mj_header beginRefreshing];
    self.listCount = ^(NSInteger listCount) {
        [weakSelf tableviewRefreshpostListCount:listCount];
    };
    
}
- (void)tableviewRefreshpostListCount:(NSInteger)listCount{
    [self endRefreshing];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mj_footer.state = listCount != 10 ? MJRefreshStateNoMoreData : MJRefreshStateIdle;
        if (!self.tablePlaceView && listCount <= 0) {//这里虽然加了 [self cyl_reloadData];会移除
            self.tablePlaceView = [[TTTableViewPlaceView alloc]initWithFrame:self.frame];
            MJWeakSelf
            self.tablePlaceView.placeImageViewTap = ^{
                [weakSelf.mj_header beginRefreshing];
            };
        }
        
        self.tablePlaceView.placeImageView.image = listCount < 0 ? [UIImage imageNamed:@"bd_wangluoyichang"] : [UIImage imageNamed:@"icon_blank"];
        self.tablePlaceView.placeImageView.userInteractionEnabled = listCount < 0 ? YES : NO;
        self.tablePlaceView.titleLabel.hidden = listCount < 0 ? YES : NO;
        [self cyl_reloadData];
    });
}
- (void)endRefreshing{
    if([self.mj_header isRefreshing]) {
        [self.mj_header endRefreshing];
    }
}

- (TableListCount)listCount{
    return objc_getAssociatedObject(self, count);
}
- (void)setListCount:(TableListCount)listCount{
    objc_setAssociatedObject(self, count, listCount, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
#pragma mark - CYLTableViewPlaceHolderDelegate
- (UIView *)makePlaceHolderView
{
    return self.tablePlaceView;
}

- (void)placeImageTap{
    
}
- (TTTableViewPlaceView *)tablePlaceView{
    return objc_getAssociatedObject(self, @selector(tablePlaceView));
}
- (void)setTablePlaceView:(TTTableViewPlaceView *)tablePlaceView{
    objc_setAssociatedObject(self, @selector(tablePlaceView), tablePlaceView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//- (void)dealloc{
//    CheckRunWhere;
//}
@end
