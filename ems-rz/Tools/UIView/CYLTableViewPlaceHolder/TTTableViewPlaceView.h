//
//  TTTableViewPlaceView.h
//  TTLife
//
//  Created by gui_huan on 2017/12/7.
//  Copyright © 2017年 tangshuanghui. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PlaceImageViewTap)(void);

@interface TTTableViewPlaceView : UIView


@property(nonatomic, strong)UIImageView *placeImageView;
@property(nonatomic, strong)UILabel *titleLabel;

@property (nonatomic, copy)PlaceImageViewTap placeImageViewTap;
@end
