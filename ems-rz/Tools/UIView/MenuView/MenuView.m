//
//  MenuView.m
//  ems-rz
//
//  Created by Gusont on 2020/1/9.
//  Copyright © 2020 xyj. All rights reserved.
//

#import "MenuView.h"
#import "UIColor+Category.h"
#import "UIButton+ImageTitleSpacing.h"
@interface MenuView()

@property (nonatomic, strong)UIView *lineView;
@property (nonatomic, strong)UIButton *exChengBtn;
@property (nonatomic, assign)NSInteger titlesCount;

@end

@implementation MenuView

- (instancetype)initWithFrame:(CGRect)frame TitleArray:(NSArray *)titles{
    self = [super initWithFrame:frame];
    if (self) {
        [self confignMuneButtonWithTitles:titles];
    }
    return self;
}
- (void)confignMuneButtonWithTitles:(NSArray *)titles{
    
    self.titlesCount = titles.count;
    float btnWidth = CGRectGetWidth(self.frame) / titles.count;
    
    for (NSInteger i = 0; i < titles.count ; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titles[i] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        btn.frame = CGRectMake( btnWidth * i, 0, btnWidth, CGRectGetHeight(self.frame));
        [btn addTarget:self action:@selector(btnClick:) forControlEvents: UIControlEventTouchUpInside];
        btn.tag = 1133 + i;
        [btn setTitleColor:[UIColor muneBtnNorColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor muneBtnSelColor] forState:UIControlStateSelected];
        [self addSubview:btn];
    }
    self.exChengBtn = (UIButton *)[self viewWithTag:1133];
    self.exChengBtn.selected = YES;
    
    [self addSubview:self.lineView];
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = [UIColor muneBtnSelColor];
        _lineView.frame = CGRectMake(CGRectGetMinX(_exChengBtn.frame), CGRectGetMaxY(_exChengBtn.frame) - 3, CGRectGetWidth(_exChengBtn.frame), 3);
    }
    return _lineView;
}
- (void)btnClick:(UIButton *)btn{
    if (self.exChengBtn == btn && btn.selected == YES) {
        return;
    }
    btn.selected = YES;
    self.exChengBtn.selected = NO;
    [self setLineViewFrameWith:btn];
    self.exChengBtn = btn;
    if (self.menuBtnClick) {
        self.menuBtnClick(btn.tag - 1133);
    }
    
}

- (void)setLineViewFrameWith:(UIButton *)btn{

    [UIView animateWithDuration:0.3 animations:^{
        CGFloat width = CGRectGetWidth(btn.frame) -  [NSString boundingALLRectWithSize:btn.titleLabel.text Font:[UIFont systemFontOfSize:14] Size:CGSizeMake(0, CGFLOAT_MAX)].width;

        _lineView.frame = _lineWidth ? CGRectMake(CGRectGetMinX(btn.frame) + width / 2 , CGRectGetMaxY(btn.frame) - 3, CGRectGetWidth(btn.frame) - width, 3) : CGRectMake(CGRectGetMinX(btn.frame), CGRectGetMaxY(btn.frame) - 3, CGRectGetWidth(btn.frame), 3);
    }];
}

- (void)setTitleFont:(UIFont *)titleFont{
    for (int i = 0; i < self.titlesCount; i++)
    {
        UIButton *btn = (UIButton *)[self viewWithTag:i+1133];
        btn.titleLabel.font = titleFont;
    }
}
- (void)setLineWidth:(BOOL)lineWidth{
    _lineWidth = lineWidth;
    if (_lineWidth) {
        CGFloat width = CGRectGetWidth(_exChengBtn.frame) -  [NSString boundingALLRectWithSize:_exChengBtn.titleLabel.text Font:[UIFont systemFontOfSize:14] Size:CGSizeMake(0, CGFLOAT_MAX)].width;

        _lineView.frame = CGRectMake(CGRectGetMinX(_exChengBtn.frame) + width / 2 , CGRectGetMaxY(_exChengBtn.frame) - 3, CGRectGetWidth(_exChengBtn.frame) - width, 3);
    }
    
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    /** 防止被连续点中 **/
    _selectedIndex = selectedIndex;
    //1.清除所有按钮的状态
    [self clearAllButtonSelectedStatus];
    
    //2.修改当前选中按钮的selected
    UIButton *btn = (UIButton *)[self viewWithTag:selectedIndex + 1133];
    [self btnClick:btn];
}
#pragma mark - 清除所有按钮的状态
- (void)clearAllButtonSelectedStatus
{
    for (int i = 0; i < self.titlesCount; i++)
    {
        UIButton *btn = (UIButton *)[self viewWithTag:i+1133];
        btn.selected = NO;
    }
}

- (void)setTitles:(NSArray *)titles{
    _titles = titles;
    for (int i = 0; i < self.titlesCount; i++){
        UIButton *btn = (UIButton *)[self viewWithTag:i+1133];
        if (i < _titles.count) {
            [btn setTitle:_titles[i] forState:UIControlStateNormal];
        }
        btn.hidden = !(i < _titles.count);
    }
}
@end


#define ScreenWidth (CGFloat)([[UIScreen mainScreen] bounds].size.width)
//按钮宽度
#define KDefButtonWidth ScreenWidth/5.0
#define KButtonWidth 104.0


@interface MenuScrollerView()
{
    menuViewDidSelectedAtIndexHandle _menuViewDidSelectedAtIndexHandle;
}
@property (nonatomic ,weak) UIScrollView *menuScrollView;
//标题
@property (nonatomic ,strong)NSArray *titleItems;
@end

@implementation MenuScrollerView

- (instancetype)initWithFrame:(CGRect)frame withTitleItems:(NSArray*)titleItems
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _unSelectedTextColor = [UIColor colorFromHexString:@"#666666" alpha:1.0];
        _selectedTextColor = [UIColor colorFromHexString:@"#3787FF" alpha:1.0];
        self.titleItems = titleItems;
        
    }
    return self;
}

- (UIScrollView *)menuScrollView{
    if (!_menuScrollView) {
        UIScrollView *scr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        scr.showsHorizontalScrollIndicator = NO;
        scr.bounces = NO;
        [self addSubview:scr];
        _menuScrollView = scr;
    }
    return _menuScrollView;
}

- (void)setTitleItems:(NSArray *)titleItems{

    _titleItems = titleItems;
    self.menuScrollView.contentSize = CGSizeMake(KButtonWidth * titleItems.count, self.menuScrollView.frame.size.height);
    
    for (NSInteger i = 0; i < titleItems.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.frame = CGRectMake(KButtonWidth * i, 0, KButtonWidth , self.frame.size.height);
        [button setTitle:_titleItems[i] forState:UIControlStateNormal];
        
        [button setTitleColor:_selectedTextColor forState:UIControlStateSelected];
        
        [button setTitleColor:_unSelectedTextColor forState:UIControlStateNormal];
        button.tag = i + 1;
        button.titleLabel.font = [UIFont systemFontOfSize:10];
        [button setImage:[UIImage imageNamed: @"liuchengtu2"] forState: UIControlStateNormal];
        [button setImage:[UIImage imageNamed: @"liuchengtu"] forState: UIControlStateSelected];

        [button layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:-3];
       


        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.menuScrollView addSubview:button];
        
    }
}

- (void)setMenuViewDidSelectedAtIndex:(menuViewDidSelectedAtIndexHandle)handle{
    
    _menuViewDidSelectedAtIndexHandle = handle;

}

#pragma mark - 菜单按钮点击
- (void)buttonClick:(UIButton *)button
{
    NSInteger index = button.tag - 1;
    if (_selectedIndex != index) {
        //1.按钮的中心点
        CGFloat buttonCenterX = button.center.x;
        
        //2.处理按钮选中
        [self menuButtonClickHandle:button];
        
        //3.滚动视图偏移量的处理
        [self menuScrollViewContentOffsetHandleWithCenterX:buttonCenterX];
        
        //4.记录当前的index
        _selectedIndex = index;
        
        //4.回调
        if (_menuViewDidSelectedAtIndexHandle)
        {
            _menuViewDidSelectedAtIndexHandle(index);
        }
        UIView *view = [self.menuScrollView viewWithTag:1122];
        
        CGFloat view_X = CGRectGetMinX(view.frame);
        CGFloat button_X = CGRectGetMinX(button.frame);
        CGFloat absX  = fabs(button_X - view_X);
        CGFloat timeX = absX /CGRectGetWidth(button.frame) *0.3;
        //    NSLog(@"%f",timeX);
        [UIView animateWithDuration:timeX animations:^{
            view.frame = CGRectMake(CGRectGetMinX(button.frame), CGRectGetMaxY(button.frame)-12, CGRectGetWidth(button.frame), 3);
        }];

    }
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
   
    //1.清除所有按钮的状态
    [self clearAllButtonSelectedStatus];
    
    //2.修改当前选中按钮的selected
    UIButton *btn = (UIButton *)[self.menuScrollView viewWithTag:selectedIndex + 1];
    [self buttonClick:btn];
    btn.selected = YES;
    
    /** 防止被连续点中 **/
    _selectedIndex = selectedIndex;
    //3.修改偏移
    [self menuScrollViewContentOffsetHandleWithCenterX:btn.center.x];
    
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(btn.frame), CGRectGetMaxY(btn.frame)-20*WLScaleH, CGRectGetWidth(btn.frame), 3)];
//    view.backgroundColor = [WLUIDesign wlNavHomeGreenColor];
//    view.tag = 1122;
//    [self.menuScrollView addSubview:view];
}

#pragma mark - 清除所有按钮的状态
- (void)clearAllButtonSelectedStatus
{
    for (int i = 0; i < _titleItems.count; i++)
    {
        UIButton *btn = (UIButton *)[self.menuScrollView viewWithTag:i+1];
        btn.selected = NO;
    }
}

#pragma mark - 处理按钮选中
- (void)menuButtonClickHandle:(UIButton *)button
{
    //清除所有按钮的状态
    [self clearAllButtonSelectedStatus];
    
    button.selected = YES;
}

#pragma mark - 滚动视图偏移量的处理
- (void)menuScrollViewContentOffsetHandleWithCenterX:(CGFloat)centerX
{
    //滚动视图X的偏移量 = 按钮的中心点x - 当前屏幕宽度的一半
    CGFloat xOffset = centerX - self.menuScrollView.frame.size.width/2;
    
    //左边偏移限制:如果按钮的中心点小于屏幕的一半。不能偏移。
    if (centerX < self.menuScrollView.frame.size.width/2)
    {
        xOffset = 0;
    }
    else if (centerX > self.menuScrollView.contentSize.width - self.menuScrollView.frame.size.width/2)
    {
        //最大偏移量
        xOffset = self.menuScrollView.contentSize.width - self.menuScrollView.frame.size.width;
    }
    
    
    //修改偏移量
    [self.menuScrollView setContentOffset:CGPointMake(xOffset, 0) animated:YES];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end



@interface MenuScrollerViewDef()
{
    menuViewDidSelectedAtIndexHandle _menuViewDidSelectedAtIndexHandle;
}
@property (nonatomic ,weak) UIScrollView *menuScrollView;
//标题
@property (nonatomic ,strong)NSArray *titleItems;
@end

@implementation MenuScrollerViewDef

- (instancetype)initWithFrame:(CGRect)frame withTitleItems:(NSArray*)titleItems
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _unSelectedTextColor = [UIColor colorFromHexString:@"#666666" alpha:1.0];
        _selectedTextColor = [UIColor colorFromHexString:@"#3787FF" alpha:1.0];
        _selectedIndex = -1;
        self.titleItems = titleItems;
        
    }
    return self;
}

- (UIScrollView *)menuScrollView{
    if (!_menuScrollView) {
        UIScrollView *scr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        scr.showsHorizontalScrollIndicator = NO;
        scr.bounces = NO;
        [self addSubview:scr];
        _menuScrollView = scr;
    }
    return _menuScrollView;
}

- (void)setTitleItems:(NSArray *)titleItems{

    _titleItems = titleItems;
    self.menuScrollView.contentSize = CGSizeMake(KDefButtonWidth * titleItems.count, self.menuScrollView.frame.size.height);
    
    for (NSInteger i = 0; i < titleItems.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.frame = CGRectMake(KDefButtonWidth * i, 0, KDefButtonWidth , self.frame.size.height);
        [button setTitle:_titleItems[i] forState:UIControlStateNormal];
        
        [button setTitleColor:_selectedTextColor forState:UIControlStateSelected];
        
        [button setTitleColor:_unSelectedTextColor forState:UIControlStateNormal];
        button.tag = i + 1;
        button.titleLabel.font = [UIFont systemFontOfSize:10];
        
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.menuScrollView addSubview:button];
        
    }
}

- (void)setMenuViewDidSelectedAtIndex:(menuViewDidSelectedAtIndexHandle)handle{
    
    _menuViewDidSelectedAtIndexHandle = handle;

}

#pragma mark - 菜单按钮点击
- (void)buttonClick:(UIButton *)button
{
    NSInteger index = button.tag - 1;
    if (_selectedIndex != index) {
        //1.按钮的中心点
        CGFloat buttonCenterX = button.center.x;
        
        //2.处理按钮选中
        [self menuButtonClickHandle:button];
        
        //3.滚动视图偏移量的处理
        [self menuScrollViewContentOffsetHandleWithCenterX:buttonCenterX];
        
        //4.记录当前的index
        _selectedIndex = index;
        
        //4.回调
        if (_menuViewDidSelectedAtIndexHandle)
        {
            _menuViewDidSelectedAtIndexHandle(index);
        }
        UIView *view = [self.menuScrollView viewWithTag:1122];
        
        CGFloat view_X = CGRectGetMinX(view.frame);
        CGFloat button_X = CGRectGetMinX(button.frame);
        CGFloat absX  = fabs(button_X - view_X);
        CGFloat timeX = absX /CGRectGetWidth(button.frame) *0.3;
        //    NSLog(@"%f",timeX);
        [UIView animateWithDuration:timeX animations:^{
            view.frame = CGRectMake(CGRectGetMinX(button.frame), CGRectGetMaxY(button.frame) - 12, CGRectGetWidth(button.frame), 3);
        }];

    }
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
   
    //1.清除所有按钮的状态
    [self clearAllButtonSelectedStatus];
    
    //2.修改当前选中按钮的selected
    UIButton *btn = (UIButton *)[self.menuScrollView viewWithTag:selectedIndex + 1];
    [self buttonClick:btn];
    btn.selected = YES;
    
    /** 防止被连续点中 **/
    _selectedIndex = selectedIndex;
    //3.修改偏移
    [self menuScrollViewContentOffsetHandleWithCenterX:btn.center.x];
    
    UIView *lineView = [self.menuScrollView viewWithTag:1122];
    if (lineView == nil) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(btn.frame), CGRectGetMaxY(btn.frame) - 12 , CGRectGetWidth(btn.frame), 3)];
        view.backgroundColor = [UIColor colorFromHexString:@"#3787FF" alpha:1.0];
        view.tag = 1122;
        [self.menuScrollView addSubview:view];
    }
}

#pragma mark - 清除所有按钮的状态
- (void)clearAllButtonSelectedStatus
{
    for (int i = 0; i < _titleItems.count; i++)
    {
        UIButton *btn = (UIButton *)[self.menuScrollView viewWithTag:i+1];
        btn.selected = NO;
    }
}

#pragma mark - 处理按钮选中
- (void)menuButtonClickHandle:(UIButton *)button
{
    //清除所有按钮的状态
    [self clearAllButtonSelectedStatus];
    
    button.selected = YES;
}

#pragma mark - 滚动视图偏移量的处理
- (void)menuScrollViewContentOffsetHandleWithCenterX:(CGFloat)centerX
{
    //滚动视图X的偏移量 = 按钮的中心点x - 当前屏幕宽度的一半
    CGFloat xOffset = centerX - self.menuScrollView.frame.size.width/2;
    
    //左边偏移限制:如果按钮的中心点小于屏幕的一半。不能偏移。
    if (centerX < self.menuScrollView.frame.size.width/2)
    {
        xOffset = 0;
    }
    else if (centerX > self.menuScrollView.contentSize.width - self.menuScrollView.frame.size.width/2)
    {
        //最大偏移量
        xOffset = self.menuScrollView.contentSize.width - self.menuScrollView.frame.size.width;
    }
    
    
    //修改偏移量
    [self.menuScrollView setContentOffset:CGPointMake(xOffset, 0) animated:YES];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end



@interface MenuScrollerViewDef1()
{
    menuViewDidSelectedAtIndexHandle _menuViewDidSelectedAtIndexHandle;
}
@property (nonatomic ,weak) UIScrollView *menuScrollView;
//标题
@property (nonatomic ,strong)NSArray *titleItems;
@end

@implementation MenuScrollerViewDef1

- (instancetype)initWithFrame:(CGRect)frame withTitleItems:(NSArray*)titleItems
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _unSelectedTextColor = [UIColor colorFromHexString:@"#666666" alpha:1.0];
        _selectedTextColor = [UIColor colorFromHexString:@"#3787FF" alpha:1.0];
        self.titleItems = titleItems;
        
    }
    return self;
}

- (UIScrollView *)menuScrollView{
    if (!_menuScrollView) {
        UIScrollView *scr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        scr.showsHorizontalScrollIndicator = NO;
        scr.bounces = NO;
        [self addSubview:scr];
        _menuScrollView = scr;
    }
    return _menuScrollView;
}

- (void)setTitleItems:(NSArray *)titleItems{

    _titleItems = titleItems;
    self.menuScrollView.contentSize = CGSizeMake(KDefButtonWidth * titleItems.count, self.menuScrollView.frame.size.height);
    
    for (NSInteger i = 0; i < titleItems.count; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.frame = CGRectMake((KDefButtonWidth - 12) * i + (2 * i + 1) * 6, 0, KDefButtonWidth - 12 , self.frame.size.height);
        [button setTitle:_titleItems[i] forState:UIControlStateNormal];
        
        [button setTitleColor:_selectedTextColor forState:UIControlStateSelected];
        [button setTitleColor:_unSelectedTextColor forState:UIControlStateNormal];
        button.layer.cornerRadius = 6;
        
        button.tag = i + 1;
        button.titleLabel.font = [UIFont systemFontOfSize:10];
        
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.menuScrollView addSubview:button];
        
    }
}

- (void)setMenuViewDidSelectedAtIndex:(menuViewDidSelectedAtIndexHandle)handle{
    
    _menuViewDidSelectedAtIndexHandle = handle;

}

#pragma mark - 菜单按钮点击
- (void)buttonClick:(UIButton *)button
{
    NSInteger index = button.tag - 1;
    if (_selectedIndex != index) {
        //1.按钮的中心点
        CGFloat buttonCenterX = button.center.x;
        
        //2.处理按钮选中
        [self menuButtonClickHandle:button];
        
        //3.滚动视图偏移量的处理
        [self menuScrollViewContentOffsetHandleWithCenterX:buttonCenterX];
        
        //4.记录当前的index
        _selectedIndex = index;
        
        //4.回调
        if (_menuViewDidSelectedAtIndexHandle)
        {
            _menuViewDidSelectedAtIndexHandle(index);
        }
        UIView *view = [self.menuScrollView viewWithTag:1122];
        
        CGFloat view_X = CGRectGetMinX(view.frame);
        CGFloat button_X = CGRectGetMinX(button.frame);
        CGFloat absX  = fabs(button_X - view_X);
        CGFloat timeX = absX /CGRectGetWidth(button.frame) *0.3;
        //    NSLog(@"%f",timeX);
        [UIView animateWithDuration:timeX animations:^{
            view.frame = CGRectMake(CGRectGetMinX(button.frame), CGRectGetMaxY(button.frame) - 12, CGRectGetWidth(button.frame), 3);
        }];

    }
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
   
    //1.清除所有按钮的状态
    [self clearAllButtonSelectedStatus];
    
    //2.修改当前选中按钮的selected
    UIButton *btn = (UIButton *)[self.menuScrollView viewWithTag:selectedIndex + 1];
    [self buttonClick:btn];
    btn.backgroundColor = [UIColor yellowColor];
    btn.selected = YES;
    
    /** 防止被连续点中 **/
    _selectedIndex = selectedIndex;
    //3.修改偏移
    [self menuScrollViewContentOffsetHandleWithCenterX:btn.center.x];
    
//    UIView *lineView = [self.menuScrollView viewWithTag:1122];
//    if (lineView == nil) {
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(btn.frame), CGRectGetMaxY(btn.frame) - 12 , CGRectGetWidth(btn.frame), 3)];
//        view.backgroundColor = [UIColor redColor];
//        view.tag = 1122;
//        [self.menuScrollView addSubview:view];
//    }
}

#pragma mark - 清除所有按钮的状态
- (void)clearAllButtonSelectedStatus
{
    for (int i = 0; i < _titleItems.count; i++)
    {
        UIButton *btn = (UIButton *)[self.menuScrollView viewWithTag:i+1];
        btn.backgroundColor = [UIColor grayColor];
        btn.selected = NO;
    }
}

#pragma mark - 处理按钮选中
- (void)menuButtonClickHandle:(UIButton *)button
{
    //清除所有按钮的状态
    [self clearAllButtonSelectedStatus];
    button.backgroundColor = [UIColor yellowColor];
    button.selected = YES;
}

#pragma mark - 滚动视图偏移量的处理
- (void)menuScrollViewContentOffsetHandleWithCenterX:(CGFloat)centerX
{
    //滚动视图X的偏移量 = 按钮的中心点x - 当前屏幕宽度的一半
    CGFloat xOffset = centerX - self.menuScrollView.frame.size.width/2;
    
    //左边偏移限制:如果按钮的中心点小于屏幕的一半。不能偏移。
    if (centerX < self.menuScrollView.frame.size.width/2)
    {
        xOffset = 0;
    }
    else if (centerX > self.menuScrollView.contentSize.width - self.menuScrollView.frame.size.width/2)
    {
        //最大偏移量
        xOffset = self.menuScrollView.contentSize.width - self.menuScrollView.frame.size.width;
    }
    
    
    //修改偏移量
    [self.menuScrollView setContentOffset:CGPointMake(xOffset, 0) animated:YES];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
