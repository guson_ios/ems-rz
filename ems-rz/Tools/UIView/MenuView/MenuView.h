//
//  MenuView.h
//  ems-rz
//
//  Created by Gusont on 2020/1/9.
//  Copyright © 2020 xyj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+Category.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^menuButtonClick)(NSInteger index);

@interface MenuView : UIView

- (instancetype)initWithFrame:(CGRect)frame TitleArray:(NSArray *)titles;

@property (nonatomic ,copy) menuButtonClick menuBtnClick;
//选中的按钮下标======加了点击操作，使用时需注意  有set方法？
@property (nonatomic)NSInteger selectedIndex;

@property (nonatomic, strong)UIFont *titleFont;

@property (nonatomic, strong)NSArray *titles;

@property (nonatomic, assign)BOOL lineWidth;

@end



typedef void(^menuViewDidSelectedAtIndexHandle)(NSInteger index);

@interface MenuScrollerView : UIView

//选中的字体颜色
@property (nonatomic ,strong)UIColor * _Nonnull   selectedTextColor;

//字体颜色
@property (nonatomic ,strong)UIColor * _Nullable unSelectedTextColor;

//选中的按钮下标
@property (nonatomic)NSInteger selectedIndex;

- (instancetype _Nullable )initWithFrame:(CGRect)frame withTitleItems:(NSArray*_Nullable)titleItems;

- (void)setMenuViewDidSelectedAtIndex:(menuViewDidSelectedAtIndexHandle _Nullable )handle;

@end

/// 瞎写吧

@interface MenuScrollerViewDef : UIView

//选中的字体颜色
@property (nonatomic ,strong)UIColor * _Nonnull   selectedTextColor;

//字体颜色
@property (nonatomic ,strong)UIColor * _Nullable unSelectedTextColor;

//选中的按钮下标
@property (nonatomic)NSInteger selectedIndex;

- (instancetype _Nullable )initWithFrame:(CGRect)frame withTitleItems:(NSArray*_Nullable)titleItems;

- (void)setMenuViewDidSelectedAtIndex:(menuViewDidSelectedAtIndexHandle _Nullable )handle;

@end

@interface MenuScrollerViewDef1 : UIView

//选中的字体颜色
@property (nonatomic ,strong)UIColor * _Nonnull   selectedTextColor;

//字体颜色
@property (nonatomic ,strong)UIColor * _Nullable unSelectedTextColor;

//选中的按钮下标
@property (nonatomic)NSInteger selectedIndex;

- (instancetype _Nullable )initWithFrame:(CGRect)frame withTitleItems:(NSArray*_Nullable)titleItems;

- (void)setMenuViewDidSelectedAtIndex:(menuViewDidSelectedAtIndexHandle _Nullable )handle;

@end

NS_ASSUME_NONNULL_END



