//
//  BaseTabbarViewController.swift
//  ems-Manager
//
//  Created by mac on 2019/5/13.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

struct TabBarItemConfig {
    let storyboardName: String
    let title: String?
    let norImageName: String
    let selectedImageName: String
}

let HomepagesStoryboard = "Homepages"
let LedgerStoryboard = "Ledger"
let MessageVCStoryboard = "MessageVC"
let PersonViewControllerStoryboard = "PersonViewController"
let ApprovalViewControllersStoryboard = "ApprovalViewControllers"

class BaseTabbarViewController: UITabBarController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addChildVC(TabBarItemConfig(storyboardName: HomepagesStoryboard, title: "首页", norImageName: "icon_home_nor", selectedImageName: "icon_home_pre"))
        self.addChildVC(TabBarItemConfig(storyboardName: LedgerStoryboard, title: "台账", norImageName: "icon_home_taizhang_nor", selectedImageName: "icon_home_taizhang_pre"))
        self.addChildVC(TabBarItemConfig(storyboardName: MessageVCStoryboard, title: "消息", norImageName: "icon_home_news_nor", selectedImageName: "icon_home_news_pre"))
        if EmsGlobalObj.obj.loginData?.menus?.filter({$0.funcCode == "appMgmt"}).first?.children?.filter({$0.funcCode == "approvalList"}) != nil{
            self.addChildVC(TabBarItemConfig(storyboardName: ApprovalViewControllersStoryboard, title: "审批", norImageName: "icon_home_shengpi_nor", selectedImageName: "icon_home_shengpi_pre"))
        }
        self.addChildVC(TabBarItemConfig(storyboardName: PersonViewControllerStoryboard, title: "我的", norImageName: "icon_home_my_nor", selectedImageName: "icon_home_my_pre"))
    }

    func addChildVC(_ config: TabBarItemConfig) -> Void {
        guard let vc = UIStoryboard(name: config.storyboardName, bundle: nil).instantiateInitialViewController() else {return}
        vc.tabBarItem.title = config.title
        vc.tabBarItem.image = UIImage(named: config.norImageName)?.withRenderingMode(.alwaysOriginal)
        vc.tabBarItem.selectedImage = UIImage(named: config.selectedImageName)?.withRenderingMode(.alwaysOriginal)
        self.addChild(BaseNavViewController(rootViewController: vc))
    }
       

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
