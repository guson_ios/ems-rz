//
//  BaseViewController.swift
//  ems-Manager
//
//  Created by mac on 2019/4/29.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class BaseViewController: UIViewController {
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = COLF4F4F4
    }
    
    deinit {

//        AlamofireNetworking.sharedInstance.sessionManager.session.getTasksWithCompletionHandler {
//            (sessionDataTask, uploadData, downloadData) in
//            sessionDataTask.forEach { $0.cancel() }
//            uploadData.forEach { $0.cancel() }
//            downloadData.forEach { $0.cancel() }
//        }
#if DEBUG
        
        PrintLog(self)
#endif
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
