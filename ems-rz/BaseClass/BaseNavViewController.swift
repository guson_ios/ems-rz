//
//  BaseNavViewController.swift
//  ems-Manager
//
//  Created by mac on 2019/5/13.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class BaseNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationBar.barStyle = .default
        self.navigationBar.isTranslucent = true
        UINavigationBar.appearance().barTintColor = COL206EDD
        UINavigationBar.appearance().titleTextAttributes = [
            .font: UIFont.boldSystemFont(ofSize: 18),
            .foregroundColor: UIColor.white]
        self.delegate = self
        self.interactivePopGestureRecognizer?.delegate = self
    }
}
extension BaseNavViewController: UIGestureRecognizerDelegate{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if self.viewControllers.count <= 1 {
            return false
        }
        return true
    }
}

extension BaseNavViewController: UINavigationControllerDelegate{

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    
        let tabbarNavBar = TabBarNavBar.tabBarNavBarHidden(viewController)
        viewController.navigationController?.navigationBar.isTranslucent = false
        viewController.navigationController?.setNavigationBarHidden(tabbarNavBar.navigationBar, animated: true)
        viewController.tabBarController?.tabBar.isHidden = tabbarNavBar.tabBar
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        if viewController.navigationItem.leftBarButtonItem == nil , self.viewControllers.count > 1 {

            let space = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            space.width = -5
            let backItem = self.createBackButtonWithImage(UIImage(named: "nav_icon_back"), highLightedImage: UIImage(named: "nav_icon_back"), title: nil, taget: nil, action: nil)
            viewController.navigationItem.leftBarButtonItems = [space, backItem]
        }
    }
    
    func createBackButtonWithImage(_ image: UIImage?, highLightedImage: UIImage?, title: String?, taget: Any?, action: Selector?) -> UIBarButtonItem  {
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        if (taget != nil) {
            backBtn.addTarget(taget, action: action ?? #selector(popSelf), for: .touchUpInside)
        }else{
            backBtn.addTarget(self, action: #selector(popSelf), for: .touchUpInside)
        }
        backBtn.setImage(image, for: .normal)
        backBtn.setImage(highLightedImage, for: .highlighted)
        backBtn.setTitle(title, for: .normal)
        backBtn.setTitleColor(UIColor.white, for: .normal)
        backBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        return UIBarButtonItem(customView: backBtn)
    }
    
   @objc func popSelf() {
        self.popViewController(animated: true)
    }
}

struct TabBarNavBar {
    var tabBar: Bool = true
    var navigationBar: Bool = false
    
    
    static func tabBarNavBarHidden(_ viewController: UIViewController) -> TabBarNavBar {
        var tabBarNavBar = TabBarNavBar()
        switch viewController {
        case is HomepageViewController, is LedgeViewController, is MessageViewController, is PersonViewController, is ApprovalViewController:
            tabBarNavBar.tabBar = false
            break
        default:
            break
        }
        
        switch viewController {
        case is HomepageViewController:
            tabBarNavBar.navigationBar = true
            break
        default:
            break
        }
        return tabBarNavBar
    }
    
}




